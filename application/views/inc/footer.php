<!-- begin::Footer -->
</div>
</div>
<footer class="m-grid__item		m-footer ">
	<div class="m-container m-container--fluid m-container--full-height m-page__container">
		<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
			<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
				<span class="m-footer__copyright">
					<?=date('Y')?> &copy; powered by <a href="http://www.milsistemas.com.br" class="m-link"><img src="<?php echo base_url('bootstrap/img/logoMil.png');?>" style="width: 35px;"></a>
				</span>
			</div>
		</div>
	</div>
</footer>
<!-- end::Footer -->		

<!-- end:: Page -->	    
<!-- begin::Scroll Top -->
<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
	<i class="la la-arrow-up"></i>
</div>

</div>
<!-- begin::Quick Nav -->	


<?php
	// carregar js particular de cada View
$js = explode('/', $view); 
$loadjs = $js[count($js) - 1].'.js';	
?>
<script src="<?=base_url('bootstrap/js/plugins/bootstrap-slider.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('bootstrap/js/'.$loadjs)?>" type="text/javascript"></script>

<script type="text/javascript">

	function confirma_visualizacao(alerta_id){
		$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
		var fl_visualizado = 1;
		
		$.ajax({
            method: "POST",
            url: base_url+'areaAdministrador/confirmaVisualizacaoAlerta',
            async: true,
            data: { alerta_id    :   alerta_id }
            }).done(function( data ) {
                var dados = $.parseJSON(data);
                if(dados.retorno == 'sucesso'){
                    $('#alerta_'+alerta_id).fadeOut('slow');
                    var total = $('.numero_alerta').attr('total');
                    $('.numero_alerta').text(total-1);
                }else{
                    alert('erro!');
                }

       	}); 
	}

</script>

<!--end::Base Scripts -->   
</body>
<!-- end::Body -->
</html>