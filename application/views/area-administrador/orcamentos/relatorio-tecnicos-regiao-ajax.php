<table class="" id="html_table" width="100%">
<thead>
	<tr>
		<th style="width: 5%;">#ID</th>
		<th style="width: 30%;">Técnico</th>						
		<th style="width: 15%;">E-mail</th>						
		<th style="width: 25%;">Empresa</th>	
		<th style="width: 20%;text-align: center;">Estado</th>
		<th style="width: 5%;">Ações</th>
	</tr>
</thead>
<tbody>					
	<?php foreach($dados as $dado){	?>
		<tr>
			<td style="text-align: center;"><?php echo $dado['id']; ?></td>
			<td style=""><?php echo $dado['nome']; ?></td>											
			<td style="text-transform: lowercase;">
				<?php echo mb_strtolower($dado['email']); ?>
			</td>
			<td style=""><?php echo mb_strtoupper($dado['razao_social']); ?></td>	
			<td style="text-align: center; ">
				<?php echo $dado['estado']; ?>							
			</td>							
			<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
				<a href="<?php echo base_url('AreaAdministrador/editarUsuario/'.$dado['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-edit"></i>
				</a>				
			</td>
		</tr>
		<?php } ?> 
	</tbody>
</table>