<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/cotacaoFreteMaritimo'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/cotacaoFreteMaritimo'); ?>">Voltar</a>	Cadastro de Cotação de Frete Marítimo
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/cadastrarCotacaoProduto');?>" method="post">
				<div class="m-portlet__body">							
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Produto:</label>
							<div class="m-input-icon m-input-icon--left">
								<select name="produto_id" class="form-control" required>
									<option value="">Selecione o produto</option>
									<?php foreach($produtos as $produto){ ?>
										<option value="<?=$produto['id']?>"><?=$produto['tipo_produto'].' - '.$produto['descricao']?></option>
									<?php } ?>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-file-text-o"></i></span></span>
							</div>							
						</div>
					</div>
					<div class="form-group m-form__group row">						
						<div class="col-lg-4">
							<label>Qtd:</label>
							<div class="m-input-icon m-input-icon--left">
								<input type="text" name="qtd" class="form-control" required />
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-balance-scale"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Iconterms:</label>
							<div class="m-input-icon m-input-icon--left">
								<select name="incoterm" class="form-control">
									<option value="">Selecione o tipo de iconterms</option>
									<option value="EXW">EXW (Ex Works)</option>
									<option value="FCA">FCA (Free Carrier)</option>
									<option value="FAS">FAS (Free Alongside Ship)</option>
									<option value="FOB">FOB (Free On Board)</option>
									<option value="CFR">CFR (Cost And Freight)</option>
									<option value="CIF">CIF (Cost, Insurance And Freight)</option>
									<option value="CPT">CPT (Carriage Paid To)</option>
									<option value="CIP">CIP (Carriage And Insurance Paid To)</option>
									<option value="DAT">DAT (Delivered At Terminal)</option>
									<option value="DAP">DAP (Delivered at Place)</option>
									<option value="DDP">DDP (Delivered Duty Paid)</option>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-file-text-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Observação:</label>
							<div class="m-input-icon m-input-icon--left">
								<textarea name="obs" class="form-control" required /></textarea>
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-file-text-o"></i></span></span>
							</div>							
						</div>
					</div>			
		            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
									<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	