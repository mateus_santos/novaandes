<?php

class AcessoModel extends CI_Model {

	public function add ($data) {

	    $this->db->insert('acesso_patio', $data);
		return $this->db->insert_id();
	}
	
	public function excluirCota($id, $dia){ 
        
        $this->db->where('id_empresa', $id);
		$this->db->where('dia_semana', $dia);
        if($this->db->delete('cotas') ){
            return true;
        }else{
            return false;
        }
    }


 	public function select() {
        $this->db->order_by('peso');
        return $this->db->get('cotas')->result();
    }

    public function findTipoAcesso($placa){
		
		//$placa = str_replace("-","",$placa);
		$sql =  "Select count(*) as existe from acesso_patio where placa = '".$placa."' and ((dthr_saida is null and dthr_saida_manutencao is null) or (dthr_saida is null and dthr_retorno_manutencao is not null))";
		$query = $this->db->query($sql);
        return $query->result();
    }
/*Catptura as informações de entrada do veiculo*/
    public function getAcesso($placa,$imprime,$id)
     {
		//$placa = str_replace("-","",$placa);
        
		if($imprime == 1){
			$sql =  "select agendamento.id,
					empresas.razao_social,
					motorista.id as motorista_id,
					motorista.nome,
					agendamento_item.descricao as no_produto,
					DATE_FORMAT(acesso_patio.dthr_retorno_manutencao,'%d/%m/%Y %H:%i:%s') AS dthr_retorno_manutencao,
					acesso_patio.placa,
					DATE_FORMAT(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i:%s') AS dthr_entrada,
					motorista.nr_cnh,
					date_format(acesso_patio.dthr_saida_manutencao,'%d/%m/%Y %H:%i') as dthr_saida_manutencao,
					acesso_patio.id as id_acesso
					from agendamento inner join empresas on agendamento.id_empresa  = empresas.id
					inner join motorista on agendamento.id_motorista = motorista.id 
					inner join agendamento_item	 on agendamento.id = agendamento_item.id_agenda
					inner  join acesso_patio on acesso_patio.id_agenda = agendamento.id ";
			if($id == null or $id == 'null'){
				$sql .=" where agendamento.placa = '".$placa."'";
			}else{
				$sql .= " WHERE	acesso_patio.id = ".$id;
			}
			$sql .=" and acesso_patio.dthr_saida is null
					order by agendamento.id desc ";
		}else{
			$sql =	"select agendamento.id,
					empresas.razao_social,
					motorista.id as motorista_id,
					motorista.nome,
					agendamento_item.descricao as no_produto,
					motorista.nr_cnh
					from agendamento inner join empresas on agendamento.id_empresa  = empresas.id
					inner join motorista on agendamento.id_motorista = motorista.id 
					inner join agendamento_item	 on agendamento.id = agendamento_item.id_agenda
					where agendamento.placa = '".$placa."' order by agendamento.id desc";
		}
       
        $query = $this->db->query($sql);
        return $query->result();   
    }
	/*Catptura as informações de saída do veiculo*/
	public function getSaida($placa,$imprime,$id)
    {
		//$placa = str_replace("-","",$placa);
        $sql =  "SELECT acesso_patio.id as id_acesso,
					agendamento.id,
					empresas.razao_social,
					motorista.id as id_motorista,
					motorista.nome,
					motorista.nr_cnh,
					agendamento_item.descricao as no_produto,
					acesso_patio.id_classificadora,
					b.razao_social as classificadora,
					agendamento.id_terminal,
					terminal.no_terminal,
					date_format(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i') as dthr_entrada,
					acesso_patio.placa,
					date_format(acesso_patio.dthr_saida ,'%d/%m/%Y %H:%i') as dthr_saida,
					nota_fiscal.quant_total,
					embalagem.no_embalagem,
					acesso_patio.fl_atende_classific,
					acesso_patio.dthr_saida_manutencao,
					date_format(acesso_patio.dthr_saida_manutencao,'%d/%m/%Y %H:%i') as dthr_saida_manutencao,
					date_format(acesso_patio.dthr_retorno_manutencao,'%d/%m/%Y %H:%i') as dthr_retorno_manutencao
				FROM                    
					acesso_patio
					left join empresas b on acesso_patio.id_classificadora  = b.id  
                    inner join agendamento on acesso_patio.id_agenda = agendamento.id
					inner join terminal on agendamento.id_terminal = terminal.id
                    inner join empresas on empresas.id = agendamento.id_empresa
					inner join motorista on agendamento.id_motorista = motorista.id 
                    inner join agendamento_item on agendamento.id = agendamento_item.id_agenda  
					left join nota_fiscal on nota_fiscal.id_agenda = agendamento.id 
					LEFT join embalagem on embalagem.id = agendamento_item.id_embalagem";
		if($id == null or $id == 'null'){
			$sql .=	" WHERE	agendamento.placa = '".$placa."'"; 
		}else{
			$sql .=	" WHERE	acesso_patio.id = ".$id; 
		}			
		if($placa <> 'semplaca'){//Indica que é uma reimpressao se vier SEMPLACA			
			if($imprime == 1 ){
				$sql .=	"	and acesso_patio.dthr_saida is not null ";
			}else{
				$sql .=	"	and acesso_patio.dthr_saida is  null ";
			}
		}
		$sql .=	"	ORDER BY
					agendamento.id
				DESC ";   
		
        $query = $this->db->query($sql);
        return $query->result();   
    }
	/*
	Função pra retornar a data e hora exata do banco
	*/
	public function dataHoraExata(){
		$sql = 'SELECT CURRENT_TIMESTAMP  FROM dual';
		$query = $this->db->query($sql);
		$resultado =  $query->result();

		return $resultado[0]->CURRENT_TIMESTAMP;
	}

	 public function atualizaAcesso($dados){
		

        $this->db->where('id', $dados['id']);

        if($this->db->update('acesso_patio', $dados)){
            return true;

        }else{
            return false;
        }
	}
	
	public function ticketAcesso($placa){

		$resultado = $this->getSaida($placa);

		return $resultado;
	}

    public function getEmail($email)
    {

        $sql =  "select count(*) as total from usuarios WHERE email='".$email."'";        
        $query = $this->db->query($sql);
        return $query->result();   
    }

    //Busca todos caminhões no pátio q foram agendados cheios
    public function buscaCaminhoesPatio($empresa_id='',$tipo)
    {
    	$where = ' 1=1';

    	if($empresa_id!=''){
    		$where = ' e.id in ( SELECT cliente_id FROM classificadora_clientes WHERE classificadora_id = '.$empresa_id.')';
    	}

    	$sql = "SELECT 	a.id, a.placa, date_format(a.dthr_entrada,'%d/%m/%Y %H:%i:%s') as dthr_entrada, m.nome as motorista, m.fone, COALESCE(e.fantasia,e.razao_social) as cliente
    			FROM 	acesso_patio a, motorista m, agendamento ag, empresas e
    			WHERE 	a.id_agenda = ag.id and 
    					a.id_motorista_entrada = m.id and 
    					ag.id_empresa = e.id and    					
    					a.id_classificadora is null and 
    					ag.id_tipo_operacao = ".$tipo." and 
    					".$where."
    					ORDER BY a.dthr_entrada ASC";

    	return $this->db->query($sql)->result_array();
    }

    public function buscaCaminhoesClassificadosHoje($usuario_id='', $tipo)
    {
    	$where = ' 1=1';
    	if($usuario_id!=''){
    		$where = ' a.id_usuario_classific = '.$usuario_id;
    	}
    	$sql = "SELECT 	a.id, a.placa, date_format(a.dthr_entrada,'%d/%m/%Y %H:%i:%s') as dthr_entrada, m.nome as motorista, m.fone, COALESCE(e.fantasia,e.razao_social) as cliente, a.fl_atende_classific
    			FROM 	acesso_patio a, motorista m, agendamento ag, empresas e
    			WHERE 	a.id_agenda = ag.id and 
    					a.id_motorista_entrada = m.id and 
    					ag.id_empresa = e.id and    					
    					a.fl_atende_classific is not null and 
    					date(a.dthr_classific) = '".date('Y-m-d')."' and 
    					ag.id_tipo_operacao = ".$tipo." and 
    					".$where."
    					ORDER BY a.id DESC";

    	return $this->db->query($sql)->result_array();
    }
	//Utilizada na reimpressão de ticket
	public function buscaAcessoGeral($empresa_logada){
		$sql = "select acesso_patio.id, acesso_patio.placa, 
				date_format(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i') as dthr_entrada, 
				agendamento_item.descricao, date_format(acesso_patio.dthr_saida,'%d/%m/%Y %H:%i') as dthr_saida,
				date_format(acesso_patio.dthr_saida_manutencao,'%d/%m/%Y %H:%i') as dthr_saida_manutencao,
				date_format(acesso_patio.dthr_retorno_manutencao,'%d/%m/%Y %H:%i') as dthr_retorno_manutencao 
				from acesso_patio 
				inner join agendamento on acesso_patio.id_agenda = agendamento.id
				inner join agendamento_item on agendamento_item.id = agendamento.id
				inner join empresas on empresas.id = agendamento.id_empresa 
				";
		$sql.=" order by 1,3,2,4,5 desc";

		$query = $this->db->query($sql);
        return $query->result();   
	}
    
	public function atualizaRecepcaoCarga($where)
	{
		$sql = "UPDATE acesso_patio SET peso_aferido = ".$where['peso_aferido'].", dthr_pesagem = '".$where['dthr_pesagem']."'
				WHERE id in (select id from (select max(id) as id from acesso_patio ap where ap.placa = upper('".$where['placa']."') ) as aps )";
		if( $this->db->query($sql) ){
			return true;
		}else{
			return false;
		}
	}

	public function buscaAcessoPlaca($placa)
	{
		$sql = "SELECT count(*) as total FROM acesso_patio WHERE placa ='".$placa."'";
		return $this->db->query($sql)->row_array();
	}

}
?>