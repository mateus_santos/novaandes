<?php

class TipoProdutosModel extends CI_Model {
	
 	public function select() {

        $sql =  "SELECT * FROM tipo_produtos";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function inserir($data){
        if($this->db->insert('tipo_produtos', $data)){
            return true;
        }else{
            return false;
        }

    }

    public function excluir($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('tipo_produtos')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizar($dados)
    {
        $this->db->where('id', $dados['id']);        
        if($this->db->update('tipo_produtos', $dados)){
            return true;
        }else{
            return false;
        }
    }

    public function selectById($id) {
        
        $sql =  "SELECT * FROM tipo_produtos WHERE id = ".$id;
        $query = $this->db->query($sql);
        return $query->row();
    }
}
?>