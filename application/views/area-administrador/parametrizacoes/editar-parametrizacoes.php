<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/parametrizacoes'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/parametrizacoes'); ?>">Voltar</a>&nbsp;&nbsp;Edição de Parametrizações
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarParametrizacoes');?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">						
						<div class="col-lg-6">
							<label>Unidade:</label>
							<div class="m-input-icon m-input-icon--left">
								<select name="unidade" id="unidade" class="form-control">
									<option value="M²" <?=($dados['unidade']=='M²')  ? 'selected=selected' : '';?> >M²</option>
									<option value="M³" <?=($dados['unidade']=='M³')  ? 'selected=selected' : '';?> >M³</option>
									<option value="Kg" <?=($dados['unidade']=='Kg')  ? 'selected=selected' : '';?> >Kg</option>
									<option value="Ton"<?=($dados['unidade']=='Ton') ? 'selected=selected' : '';?> >Ton</option>
									<option value="Unidade" <?=($dados['unidade']=='Unidade') ? 'selected=selected' : '';?>>Unidade</option>
								</select>
								<input type="hidden" required name="id" id="id" class="form-control m-input" placeholder="" required style="text-align: right;" value="<?php echo $dados['id'];?>" />
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-file-text-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-6">
							<label>Moeda:</label>
							<div class="m-input-icon m-input-icon--left">
								<select name="moeda" id="moeda" class="form-control">
									<option value="US$"	<?=($dados['moeda']=='US$') ? 'selected=selected' : '';?>>US$</option>
									<option value="R$" 	<?=($dados['moeda']=='R$')  ? 'selected=selected' : '';?>>R$</option>
									<option value="€" 	<?=($dados['moeda']=='€')   ? 'selected=selected' : '';?>>€</option>									
								</select>								
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-file-text-o"></i></span></span>
							</div>
						</div>
					</div>
		            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
									<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	