<?php

class PaisModel extends CI_Model {

	public function add ($data) {
        
	    return $this->db->insert('pais', $data);		

	}

    public function select(){
        $sql =  "SELECT * FROM pais";
        return $this->db->query($sql)->result_array();   
    }

    public function getpaisById($id){

        $sql =  "SELECT * FROM pais where id =".$id;
        $query = $this->db->query($sql);
        return $query->row();
    }   

    public function excluirFrete($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('pais')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaFrete($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('pais', $dados)){
            return true;
        }else{
            return false;
        }

    }

}
?>