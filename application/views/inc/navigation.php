<!-- BEGIN: Left Aside -->
		<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
		<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark " style="background-color: #000;">
			<!-- BEGIN: Aside Menu -->
			<div 
			id="m_ver_menu" 
			class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
			data-menu-vertical="true"
			data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
			>
			
			<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			<?php 

				if($tipo_acesso == "administrador geral"){
					include('menu-area-administrador-geral.php');

				}elseif($tipo_acesso == "clientes"){
					include('menu-area-cliente.php');
								
				}elseif($tipo_acesso == "classificadora"){
					include('menu-area-classificadora.php');
									
				}elseif($tipo_acesso == "administrador empresas"){
					include('menu-area-administrador-empresas.php');
									
				}elseif($tipo_acesso == "expedicao"){
					include('menu-area-expedicao.php');
									
				}elseif($tipo_acesso == "gate"){
					include('menu-area-gate.php');
					
				}
			?>

			</ul>
			
		</div>
		<!-- END: Aside Menu -->
	</div>
	<!-- END: Left Aside -->							
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->