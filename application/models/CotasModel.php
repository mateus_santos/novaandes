<?php

class CotasModel extends CI_Model {

	public function add ($data) {

	    $this->db->insert('cotas', $data);
		return $this->db->insert_id();
	}
	
	public function excluirCota($id, $dia){ 
        
        $this->db->where('id_empresa', $id);
		$this->db->where('dia_semana', $dia);
        if($this->db->delete('cotas') ){
            return true;
        }else{
            return false;
        }
    }


 	public function select() {
        $this->db->order_by('peso');
        return $this->db->get('cotas')->result();
    }

    public function findCotas($dia, $empresa_id){
        $where = ' and 1=1';
        if($empresa_id != '' ){
            $where = ' and empresas.id in (SELECT cliente_id FROM empresas_clientes WHERE administrador_id = ".$empresa_id.")';    
        }

		if(empty($dia)){
			$sql =  "SELECT id, razao_social, cotas.dia_semana, cotas.hr_limite_acesso, cotas.hr_limite_agenda, cotas.peso, cotas.fl_sem_cota FROM `empresas` LEFT join cotas on empresas.id = cotas.id_empresa where empresas.tipo_cadastro_id = 1 ". $where;	
        }else{
			
			$sql =  "SELECT id, razao_social, cotas.dia_semana, cotas.hr_limite_acesso, cotas.hr_limite_agenda, cotas.peso, cotas.fl_sem_cota FROM `empresas` LEFT join cotas on empresas.id = cotas.id_empresa  where empresas.tipo_cadastro_id = 1 and cotas.dia_semana ='".$dia."'". $where;
			$query = $this->db->query($sql);
			$total = $query->num_rows();
			
			if ($total < 1){
				$sql =  "SELECT id, razao_social, '' as dia_semana, null as hr_limite_acesso, null as hr_limite_agenda, null as peso, null as fl_sem_cota FROM `empresas` WHERE empresas.tipo_cadastro_id = 1". $where;	
				$query = $this->db->query($sql);
			}			
			
        }

        return $query->result();
    }

    public function getCotas()
     {

        $sql =  "SELECT id, razao_social, cotas.dia_semana, cotas.hr_limite_acesso, cotas.hr_limite_agenda, cotas.peso, cotas.fl_sem_cota FROM `empresas` LEFT join cotas on empresas.id = cotas.id_empresa ";        
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getEmail($email)
    {

        $sql =  "select count(*) as total from usuarios WHERE email='".$email."'";        
        $query = $this->db->query($sql);
        return $query->result();   
    }

    


}
?>