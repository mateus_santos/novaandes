<?php 
	$bgcolor  = '';
	if( $dados[0]->tipo_cadastro == 'clientes' ){
		$bgcolor = 'btn-success';
	}elseif( $dados[0]->tipo_cadastro == 'classificadora' ){
		$bgcolor = 'btn-warning';	
	}elseif(  $dados[0]->tipo_cadastro == 'administrador geral' ){
		$bgcolor = 'btn-secondary';	
	}else{
		$bgcolor = 'btn-danger';	
	}
?>
<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<a href="<?php echo base_url('AreaAdministrador/gestaoEmpresas/'.$pesquisa);?>"><i class="la la-arrow-left"></i></a>
					</span>
					<h3 class="m-portlet__head-text" style="padding-right: 10px;">	
						<a href="<?php echo base_url('AreaAdministrador/gestaoEmpresas/'.$pesquisa);?>">Voltar</a>						
					</h3>
					<span class="m-portlet__head-icon">
						<i class="la la-building-o"></i>
					</span>

					<h3 class="m-portlet__head-text">
						Empresa #<?php  echo $dados[0]->id; ?>
						<button type="button" class="btn m-btn--pill m-btn--air <?=$bgcolor?>">
								<?php  echo $dados[0]->tipo_cadastro; ?>
						</button>
					</h3>
				
				</div>	

			</div>
			<div style="text-align: right;">
				<a href="<?php echo base_url('AreaAdministrador/editarEmpresa/'.$dados[0]->id.'/'.$pesquisa);?>" class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" style="margin-top: 15px;" data-toggle="m-tooltip" title="" data-original-title="Editar Empresa">
					<i class="la la-edit"></i>
				</a>				
				<a href="#" id="pedidos" class="btn btn-outline-primary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" style="margin-top: 15px;margin-left: 10px;" data-toggle="m-tooltip" title="" data-original-title="Pedidos do cliente">
					<i class="fa fa-ticket"></i>
				</a>
				<a href="#" id="anexos" class="btn btn-outline-metal m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" style="margin-top: 15px;margin-left: 10px;" data-toggle="m-tooltip" title="Anexar Documentos" data-original-title="Anexar Documentos ao Cliente" >
					<i class="la la-file"></i>
				</a>
			</div>		
		</div>
		<div class="m-portlet__body">
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>Empresa: 
						<small class="text-muted"> <?php echo $dados[0]->cnpj.' | '.$dados[0]->razao_social.' | '.$dados[0]->fantasia;?></small>
					</h5>
				</div>	
				<input type="hidden" value="<?=$dados[0]->id?>" id="empresa_id">
				<input type="hidden" value="<?=$dados[0]->tipo_cadastro_id?>" id="tipo_cadastro_id">
				<div class="col-lg-4 col-xs-12">
					<h5>Telefone: 
						<small class="text-muted"> <?php echo $dados[0]->telefone;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>E-mail: 
						<small class="text-muted"> <?php echo $dados[0]->email;?></small>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Enderço: <small class="text-muted"> <?php echo $dados[0]->endereco;?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Cidade: <small class="text-muted"> <?php echo $dados[0]->cidade;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						Estado: <small class="text-muted"> <?php echo $dados[0]->estado;?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Bairro e CEP: <small class="text-muted"> <?php echo $dados[0]->bairro .' CEP:'.$dados[0]->cep;?></small>
					</h5>
				</div>							
			</div>			
			<?php  

				if( $dados[0]->cartao_cnpj != '' ){ 
					$arquivo = explode('/',$dados[0]->cartao_cnpj); 					
			?>
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<h5>Cartão Cnpj: <a href="<?php echo base_url('Consultas/Receita_Federal/'.$arquivo[count($arquivo)-1]); ?>" target="_blank">	
						<img style="width: 50px;" src="<?php echo base_url('bootstrap/img/receita.gif'); ?>" /></a>
					</h5>
				</div>
			</div>
			
			<?php }	?>	
		</div>
	</div>
	<div class="row">
		<div class="m-portlet m-portlet--creative m-portlet--bordered-semi col-lg-12 col-xs-12">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
				
					<h3 class="m-portlet__head-text">

						Novo usuário &nbsp;&nbsp;
						<a data-toggle="modal" href="#modal_add_usuario" style="color: #ffcc00; font-weight: bold;" >
							<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar Novo Funcionário"></i>
						</a>
					</h3>

					<div class="m-portlet__head-title">										
						<h2 class="m-portlet__head-label m-portlet__head-label--warning">
							<span> Usuários - Funcionários Vinculados </span>
						</h2>
					</div>			
				</div>				
			</div>
			<div class="m-portlet__body">
				<?php if( count($dados) >= 1 && $dados[0]->nome != "" ){ ?>
				<table class="table m-table--head-separator-warning">
				  	<thead>
				    	<tr>
				    		<th>#</th>
				      		<th>Nome</th>
				      		<th>Cpf</th>
				      		<th>E-mail</th>						
				      		<th>Ação</th>
				    	</tr>
				  	</thead>
				  	<tbody>
				  		<?php foreach($dados as $dado){  ?>
				    	<tr>
					      	<th scope="row"><?php echo $dado->usuario_id;?></th>
					      	<td><?php echo $dado->nome; ?></td>
					      	<td><?php echo $dado->cpf; ?></td>
					      	<td><?php echo $dado->email_pessoal; ?></td>						
							<td><a href="<?php echo base_url('AreaAdministrador/editarUsuario/'.$dado->usuario_id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" pedido_id="<?php echo $dado->id; ?>" >
								<i class="la la-edit editar"></i>
							</a></td>
				    	</tr>			
				    	<?php } ?>    	
				  	</tbody>
				</table>
				<?php }else{ ?>
				<h2>Nenhum usuário/funcionário vinculado a essa empresa.</h2>
				<?php } ?>
			</div>
		</div>			
	</div>
</div>

<div class="modal fade" id="modal_add_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" >Cadastrar Novo Funcionário</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="max-width: 750px;">
					<div class="row">
						<div class="form-group m-form__group col-lg-9" >
							<label for="exampleSelect1">Nome</label>
							<input type="text" class="form-control m-input m-input--air sub" placeholder="Nome do funcionário" name="nome" id="nome" style="margin-bottom: 10px;float: left;" />
						</div>
						<div class="form-group m-form__group col-lg-3" >
							<label for="exampleSelect1">CPF</label>
							<input type="text"  class="form-control m-input m-input--air qtd" placeholder="CPF do funcionário" name="cpf" id="cpf"  style="margin-bottom: 10px;float: right;" /> 
						</div>
					</div>					
					<div class="row">
						<div class="form-group m-form__group col-lg-4" >
							<label for="exampleSelect1">Telefone</label>
							<input type="text"  class="form-control m-input m-input--air qtd" placeholder="Telefone do funcionário" name="telefone" id="telefone"  style="margin-bottom: 10px;float: right;" /> 
						</div>
						<div class="form-group m-form__group col-lg-4" >
							<label for="exampleSelect1">Email</label>	
							<input type="email" class="form-control m-input m-input--air sub" autocomplete="off" placeholder="E-mail do funcionário" name="email" style="margin-bottom: 10px;float: left;display: none;" />
							<input type="email" class="form-control m-input m-input--air sub" autocomplete="off" placeholder="E-mail do funcionário" name="email" id="email" style="margin-bottom: 10px;float: left;" />
						</div>
						<div class="form-group m-form__group col-lg-4" >
							<label for="">Celular</label>
							<input type="text" class="form-control m-input m-input--air sub" autocomplete="off" placeholder="Celular do funcionário" name="celular" id="celular" style="margin-bottom: 10px;float: left;" />
						</div>
					</div>
					<div class="row">
						<div class="form-group m-form__group col-lg-6">
						<label>Redefinir Senha:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="password" name="senha" autocomplete="off" class="form-control m-input" placeholder=""  value=" " style="display: none;">
							<input type="password" name="senha" autocomplete="off" class="form-control m-input" placeholder=""  value="" id="senha"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span>
							</span>
						</div>
					</div>
					<div class="form-group m-form__group col-lg-6">
						<label class="">Confirmar Senha:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="password" name="senha2" id="senha2" class="form-control m-input" placeholder="" value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span></span>
						</div>	
					</div>
					</div>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="adicionar_funcionario()">Adicionar Funcionário</button>
				</div>
			</div>
		</div>
	</div>

<!-- Modal Pedidos -->
<div class="modal fade" id="modal_pedidos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content" style="width: 100%;">
			<div class="modal-header">
				<h3>Pedidos - <small class="text-muted"> <?php echo $dados[0]->cnpj.' | '.$dados[0]->razao_social.' | '.$dados[0]->fantasia;?></small></h3>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 100%;">					
				<table id="table-pedidos" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;"># Pedido</th>
						<th style="text-align: center;"># Orçamento</th>														
						<th >Situação</th>																
						<th >Dthr. Geração</th>
						<th >Ações</th>
					</tr>
				</thead>
				<tbody>					
					<?php foreach($pedidos as $dado){
						if( $dado->status_pedido_id == 1 ){
							$status = "btn m-btn--pill m-btn--air btn-secondary";
							$background = '';
						}elseif( $dado->status_pedido_id == 2 ){
							$status = "btn m-btn--pill m-btn--air btn-info";
							$background = '';
						}elseif( $dado->status_pedido_id == 3 ){
							$status = "btn m-btn--pill m-btn--air btn-primary";
							$background = '';
						}elseif( $dado->status_pedido_id == 4 ){
							$status = "btn m-btn--pill m-btn--air";
							$background = "style='background: #ff9800;'";
						}elseif( $dado->status_pedido_id == 5 ){
							$status = "btn m-btn--pill m-btn--air";
							$background = "style='background: #18cfe6;'";
						}elseif( $dado->status_pedido_id == 6){
							$status = "btn m-btn--pill m-btn--air btn-success";
							$background = '';
						}elseif($dado->status_pedido_id == 7){
							$status = "btn m-btn--pill m-btn--air btn-danger";
							$background = '';
						}else{
							$status = "btn m-btn--pill m-btn--air btn-danger";
							$background = '';
						}

						?>
						<tr>
							<td style="text-align: center;"><?php echo $dado->id; ?></td>
							<td style="text-align: center;"> <?php echo $dado->orcamento_id; ?></td>									
							<td style="text-transform: capitalize;">
								<button class="status <?php echo $status; ?>" status="<?php echo $dado->status_pedido_id;?>" <?php echo $background;?> title="Status do orçamento" onclick="status(<?php echo $dado->id; ?>);">
									<?php echo ucfirst($dado->status); ?>								
								</button>
							</td>																			
							<td style="">
								<?php echo date('d/m/Y H:i:s',strtotime($dado->dthr_geracao)); ?>							
							</td>							
							<td data-field="Actions" class="m-datatable__cell " style="">
								<span style="overflow: visible;" class="">								
									<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick="gerarPdf(<?php echo $dado->id; ?>);" pedido_id="<?php echo $dado->id; ?>" >
										<i class="la la-print visualizar"></i>
									</a>
									<?php if ( $dado->status_pedido_id != 1 && $dado->status_pedido_id != 7 ){ ?>
									<a href="#" onclick="anexarNfe(<?php echo $dado->id; ?>,'<?php echo str_replace("'","*",$dado->cliente); ?>','<?php echo $dado->nr_nf; ?>','<?php echo $dado->arquivo_nfe; ?>','<?php echo $dado->prazo_entrega; ?>','<?php echo $dado->dt_emissao_nf; ?>' );" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill finalizar_pedido" pedido_id="<?php echo $dado->id; ?>" cliente="<?php echo $dado->cliente; ?>" >
										<i class="flaticon-tool-1"></i>
									</a>
									<?php } ?>						
								</span>
							</td>
						</tr>
				<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">					
			</div>
		</div>
	</div>
</div>
<!-- Modal Anexar documentos -->
<div class="modal fade" id="modal_anexos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-anexar-doc-title" ><?php echo $dados[0]->cnpj.' | '.$dados[0]->razao_social;?> - <span class="m--font-warning">Documentos</span> </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="">					
				<div class="row">
					<div class="col-xs-6 col-lg-6">	
						<table id="table-anexos"  >
							<thead>
								<tr>
									<th style="text-align: center;">#</th>
									<th>Arquivo</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody id="anexos_conteudo">
								<?php $i=1;foreach ($anexos as $anexo) { ?>
								<tr>
									<td style="text-align: center;"><?php echo $i; ?></td>
									<td><?php echo $anexo['descricao']; ?></td>
									<td>
										<a href="<?php echo base_url('empresa_anexos/'.$anexo['anexo']); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" target="_blank" >
											<i class="la la-eye"></i>
										</a>
										<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" onclick="excluirAnexo(<?php echo $anexo['id']; ?>,<?php echo $dados[0]->id; ?>);" >
											<i class="la la-trash"></i>
										</a>
									</td>
								</tr>									

								<?php $i++; } ?>
							</tbody>
						</table>	
					</div>
					<div class="form-group m-form__group col-lg-6 col-xs-6">	
						<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/anexarDocumentosEmpresa');?>" method="post"  enctype="multipart/form-data">					
							<label for="exampleSelect1">Enviar Arquivo</label>
							<input type="text" class="form-control m-input--air" name="descricao" id="descricao" placeholder="Descrição" required="required" />
							<input type="file" class="form-control m-input--air" name="anexo" id="anexo" placeholder="Anexar Arquivo" required="required" />
							<input type="hidden" class="form-control m-input--air" name="empresa_id" value="<?php echo $dados[0]->id;?>"  />
							<div style="float: right;margin-top: 25px;">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
								<button type="submit" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
							</div>
						</form>
						
					</div>						
				</div>				
			</div>
			<div class="modal-footer">
				
			</div>
		</div>			
	</div>
	</form>
</div>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
		   	//window.location = base_url+'AreaAdministrador/gestaoEmpresas';
		});
	</script>	
<?php unset($_SESSION['sucesso']); } ?>