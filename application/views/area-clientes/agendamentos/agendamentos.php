<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Agendamentos 
					</h3>	
					<h3 class="m-portlet__head-text" style=" padding-left: 10px;" >
						<a href="<?php echo base_url('AreaClientes/cadastrarAgendamento'); ?>"  style="color: #ffcc00; font-weight: bold;" data-toggle="m-tooltip" data-placement="top" title="Novo Agendamento" id="Novo Agendamento">
							<i class="la la-plus-circle" style="font-size: 38px;"></i>
						</a>
					</h3>									
				</div>

			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Período:</label>					
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="dt_ini" id="dt_ini" value="<?php echo date('d/m/Y'); ?>" class="form-control m-input datepicker" placeholder="data inicial"style="width: 49%;float: left;" /> 
						<input type="text" name="dt_fim" id="dt_fim" value="<?php echo date('d/m/Y'); ?>" class="form-control m-input datepicker" placeholder="data final"style="width: 49%;float: right;"/>						
					</div>				
				</div>
				<div class="col-lg-6">
					<label>Nota Fiscal:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="nr_nf" id="nr_nf" class="form-control m-input" placeholder="Nota" style="width: 300px;" /> 
					</div>				
				</div>
			</div>	
			<div class="form-group m-form__group row">								
				<div class="col-lg-6">
					<label>Placa:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="placa" id="placa" class="form-control m-input" placeholder="placa" style="width: 300px;" />
					</div>				
				</div>			
			<?php if($tipo_acesso == 'administrador geral' || $tipo_acesso == 'administrador empresas'){ ?>
			
				<div class="col-lg-6">
					<label>Empresas:</label>
					<div class="m-input-icon m-input-icon--right">
						<select name="empresa_id" id="empresa_id" class="form-control m-input" >
							<option value="">	Selecione uma empresa 	</option>
							<?php foreach($empresas as $empresa) {?>
								<option value="<?php echo $empresa['id']; ?>"><?php echo strtoupper($empresa['razao_social']); ?></option>
							<?php } ?>
						</select>
					</div>				
				</div>
			
			<?php } ?>	
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="button" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Filtrar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>	
		</div>						
	</div>
	<!--end::Portlet-->	

	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" >
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Agendamentos
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">#</th>
						<th style="width: 35%;">Cliente</th>						
						<th style="width: 15%;">Data</th>						
						<th style="width: 25%;">Placa</th>	
						<th style="width: 15%;">Motorista</th>
						<th style="width: 5%;">Ações</th>
					</tr>
				</thead>
				<tbody>					
				<?php foreach($agendamentos as $agendamento){	?>
					<tr>
						<td style="text-align: center;"><?php echo $agendamento['id']; ?></td>
						<td style=""><?php echo $agendamento['razao_social']; ?></td>											
						<td style=""><?php echo $agendamento['dt_agenda']; ?></td>
						<td style=""><?php echo $agendamento['placa']; ?></td>	
						<td style=""><?php echo $agendamento['nome']; ?></td>
						<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
							<a href="<?php echo base_url('AreaClientes/editarAgendamento/'.$agendamento['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
								<i class="la la-edit"></i>
							</a>
							<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" target="blank" onclick="excluirAgendamento(<?=$agendamento['id']?>)">
								<i class="la la-trash"></i>
							</a>				
						</td>
					</tr>
					<?php } ?> 
				</tbody>
			</table>			
		</div>
	</div>

	
	<!--end::Portlet-->	
</div>	
<div class="modal fade" id="m_lista_anexo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_lista_anexo_title" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="row anexos">
					
				</div>				
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<!-- end:: Body -->
<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "Atenção!",
           	text: '<?php echo $this->session->flashdata('msg'); ?>',
           	type: "warning"
        }).then(function() {
		   	
		});
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Agendamento realizado com sucesso!',
           	type: "success"
        }).then(function() { });
	</script>	
<?php unset($_SESSION['sucesso']); } ?>
