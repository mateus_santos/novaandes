<div class="m-content">	
	<form accept-charset="utf-8" action="<?php echo base_url('AreaAdministrador/orcamentosAdm/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-list-2"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Cadastro de Proforma
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">
				
			</div>
		</div>
				
		<div class="m-portlet__body">
			<div class="form-group m-form__group m--margin-top-10">
				<div class="alert m-alert m-alert--default" role="alert">
					Insira os dados da empresa interessada!
				</div>
			</div>			
			<div class="form-group m-form__group row">
				<div class="col-lg-3">
					<label id='label_documento'>Documento:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="cnpj" id="cnpj" class="form-control m-input" placeholder="Insira um documento" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
						<input type="hidden"  id="salvar" maxlength="15" name="salvar" value="1"    required />
						<input type="hidden"  id="cartao_cnpj" name="cartao_cnpj"  />
                        <input type="hidden"  id="empresa_id" maxlength="15" name="empresa_id" value=""  />
                        <input type="hidden"  id="pesquisa" name="pesquisa" value="<?php echo $pesquisa; ?>"  />
					</div>					
				</div>
				<div class="col-lg-3">
					<label id="label_razao">Razão Social:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required	name="razao_social" id="razao_social" class="form-control m-input" placeholder="Razão Social" required value="">
						<input type="hidden" name="id" 	class="form-control m-input" placeholder="Insira uma razão social" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>
				</div>
				<div class="col-lg-3" id="campo_fantasia">
					<label class="">Fantasia:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="fantasia" id="fantasia" class="form-control m-input" placeholder="Insira uma fantasia" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>	
				</div>				
				<input type="hidden" name="inscricao_estadual" id="inscricao_estadual" class="form-control m-input" placeholder="Inscrição Estadual" value="">			
			</div>
			<div class="form-group m-form__group row">	
				<div class="col-lg-3">
					<label>E-mail:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="email" required name="email" id="email" class="form-control m-input" placeholder="Insira um email" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>			
				<div class="col-lg-3">
					<label>Telefone:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="telefone" id="telefone" class="form-control m-input" placeholder="Insira um telefone" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Cep:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="cep" id="cep" class="form-control m-input" placeholder="Insira um cep" required value="" >
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Endereço:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="endereco" id="endereco" class="form-control m-input" placeholder="Insira um endereço" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>
				
			</div>
			<div class="form-group m-form__group row">
				<div class="col-lg-3">
					<label>Bairro:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="bairro" id="bairro" class="form-control m-input" placeholder="Insira o bairro" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>	
				<div class="col-lg-3">
					<label class="">Cidade:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" 		name="cidade" required 		id="cidade" 	class="form-control m-input" placeholder="Insira um cidade" value="">
						<input type="hidden"	name="codigo_ibge_cidade" 	id="codigo_ibge_cidade" 	value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
					</div>					
				</div>
				<div class="col-lg-3">
					<label class="">Estado:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" id="estadoE" name="estado" class="form-control" placeholder="Insira um estado" />						
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
					</div>
				</div>					
				<div class="col-lg-3">
					<label for="pais">País</label>
					<select class="form-control m-input" name="pais_id" id="pais_id" required>
						<option value="">Selecione o pais</option>
					<?php foreach ($pais as $pa) { ?>
						<option value="<?php echo $pa['id']; ?>" >
							<?php echo $pa['nome_pt']; ?>
						</option>
					<?php } ?>
					</select>
				</div>
			</div>	
			<hr/> 
			<div class="form-group m-form__group row">
				<div class="col-lg-4">
					<label>Contato:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="contato_posto" id="contato_posto" class="form-control m-input" placeholder="Contato" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>							
				</div>			
				<div class="col-lg-4">
					<label>Celular/Telefone:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="tel_contato_posto" id="tel_contato_posto" class="form-control m-input" placeholder="Insira um telefone/celular" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-4">
					<label>E-mail Contato:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="email" required name="email_contato_posto" id="email_contato_posto" class="form-control m-input" placeholder="Insira um e-mail" required value="" >
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>
				</div>
			</div>
			<hr/>
			<div class="form-group m-form__group row">
				<div class="col-lg-12">
					<label><b>Observação:</b></label>
					<div class="m-input-icon m-input-icon--right">
						<textarea  name="observacao" id="observacao" class="form-control m-input" ></textarea>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	***************************************** Forma de entrega, fretes e forma de pagto *******************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">					
					<h2 class="m-portlet__head-label m-portlet__head-label--primary" style="width: 320px;">
						<span>
							Termos da Proforma
						</span>
					</h2>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body">
			<div class="form-group m-form__group row">
				<div class="col-lg-3">	
					<label for="forma_pagto">
						Forma de Pagamento
					</label>
					<select class="form-control m-input m-input--square" id="forma_pagto" name="forma_pagto">
						<option value="0">Selecione a forma de pagamento</option>					
						<?php foreach ($formaPagto as $pagto) { ?>
							<option value="<?php echo $pagto->id;?>"><?php echo $pagto->descricao; ?></option>								
						<?php } ?>
					</select>
				</div>			
				<div class="col-lg-3">				
					<label for="entrega">
						Prazo de Entrega
					</label>
					<select class="form-control m-input m-input--square" id="entregas" name="entrega">
						<option value="0">Selecione como vai ser realizado a entrega do(s) produto(s)</option>					
						<?php foreach ($entregas as $entrega) { ?>
							<option value="<?php echo $entrega->id;?>"><?php echo $entrega->descricao; ?></option>								
						<?php } ?>
					</select>			
				</div>
				<div class="col-lg-3">
					<label for="Frete">
						Frete
					</label>
					<select class="form-control m-input m-input--square" id="frete" name="frete">
						<option value="0">Selecione o tipo de frete utilizado</option>		
						<?php foreach ($fretes as $frete) { ?>
							<option value="<?php echo $frete->id;?>"><?php echo $frete->descricao; ?></option>								
						<?php } ?>			
					</select>
				</div>
				<div class="col-lg-3">						
					<label>Peso Bruto:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="peso_bruto" id="peso_bruto" class="form-control m-input peso" placeholder="Insira o peso bruto" required />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>					
				</div>
			</div>
			<div class="form-group m-form__group row">				
				<div class="col-lg-3">
					<label>Peso Líquido:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="peso_liquido" id="peso_liquido" class="form-control m-input peso" placeholder="Insira o peso líquido" required />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>
				</div>
				<div class="col-lg-3">
					<label>Iconterms:</label>
					<div class="m-input-icon m-input-icon--left">
						<select name="incoterms" class="form-control">
							<option value="">Selecione o tipo de iconterms</option>
							<option value="EXW">EXW (Ex Works)</option>
							<option value="FCA">FCA (Free Carrier)</option>
							<option value="FAS">FAS (Free Alongside Ship)</option>
							<option value="FOB">FOB (Free On Board)</option>
							<option value="CFR">CFR (Cost And Freight)</option>
							<option value="CIF">CIF (Cost, Insurance And Freight)</option>
							<option value="CPT">CPT (Carriage Paid To)</option>
							<option value="CIP">CIP (Carriage And Insurance Paid To)</option>
							<option value="DAT">DAT (Delivered At Terminal)</option>
							<option value="DAP">DAP (Delivered at Place)</option>
							<option value="DDP">DDP (Delivered Duty Paid)</option>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-file-text-o"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Origem:</label>
					<div class="m-input-icon m-input-icon--left">
						<select name="embarque_id" class="form-control" required>
							<option value="">Selecione o porto de origem</option>
							<?php foreach($origens as $org){ ?>
								<option value="<?=$org['id']?>"><?=$org['abreviacao'].'/'.$org['abreviacao'].' - '.$org['descricao']?></option>
							<?php } ?>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-ship"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Destino:</label>
					<div class="m-input-icon m-input-icon--left">
						<select name="destino_id" class="form-control" required>
							<option value="">Selecione o porto de destino</option>
							<?php foreach($destinos as $destino){ ?>
								<option value="<?=$destino['id']?>"><?=$destino['abreviacao'].'/'.$destino['abreviacao'].' - '.$destino['descricao']?></option>
							<?php } ?>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-ship"></i></span></span>
					</div>							
				</div>
			</div>
			<div class="form-group m-form__group row">
				
			</div>
		</div>
	</div>	
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	********************************************************* Produtos ************************************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--primary">
						<span>Produtos</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<table class="table m-table m-table--head-separator-primary">
			  	<thead>
			    	<tr>			      		
			      		<th>NCM | Descrição</th>
			      		<th>Quantidade</th>
			      		<th>Valor Unitário R$</th>
			      		<th>Cotações</th>
			      		<th>
			      			<a class="btn btn-outline-primary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar produto">
								<i class="la la-plus"></i>
							</a>
						</th>				      		
			    	</tr>
			  	</thead>
			  	<tbody>
			  		 <tr id="modelo" indice='0' total_indice='0'>
                            <td>
                                <?php 
                                    $option = "";
                                    $optiongroup = "";
                                    $optiongroup_aux = "";                                    
                                    foreach ($produtos as $produto) {
										if($optiongroup == ''){
											$option.='<optgroup label="'.$produto['tipo_produto'].'" data-subtext="optgroup subtext">'.mb_strtoupper($produto['tipo_produto']);
										}elseif($optiongroup_aux != $produto['tipo_produto']){
											$option.='</optiongroup><optgroup label="'.$produto['tipo_produto'].'" data-subtext="optgroup subtext">'.mb_strtoupper($produto['tipo_produto']);
										}
										$option.= '<option value="'.$produto['id'].'" valor_unitario="'.$produto['valor_unitario'].'" >'.$produto['ncm'].' | '.$produto['descricao'].'</option>';

										$optiongroup_aux = $optiongroup = $produto['tipo_produto'];
									}
									$option.= "</optiongroup>";
                                ?>
                                <select  class="form-control bombas" style="font-size: 15px;" name="produto[]" indice='0' >
                                	<option value="">Selecione um produto</option>
                                    <?=$option?>
                                </select>
                            </td>
                            <td>
                                <input type="text" maxlength="3" class="form-control qtd" style="width: 70px;" name="quantidade[]" required />
                            </td>
                            <td style="font-size: 15px;">
                                <input type="text" name="valor[]" class="form-control valor_unitario" value="" id="valor_unitario_0" indice="0" maxlength="14" />
                            </td>
                            <td>
	                            <a class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill frete_maritimo" data-toggle="m-tooltip" data-placement="top" title="Visualizar Cotações Frete Marítimo" indice="0">
									<i class="fa fa-ship"></i>
								</a>
								<a class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill cotacao_produto" data-toggle="m-tooltip" data-placement="top" title="Visualizar Cotações Produto" indice="0">
									<i class="fa fa-list-alt"></i>
								</a>
							</td>
                            <td>
                                <i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
                            </td>
                        </tr>
                    </tbody>
                </table> 
                <div style="text-align: center;">
                    <button type="submit" id="enviar" class="btn btn-primary" >Solicitar orçamento</button>
                </div>
		</div>
	</div>
	</form>
</div>

<!-- Modal Cotação Frete -->
<div class="modal fade" id="m_lista_frete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">	
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-excluir-title" >Cotações de Frete Marítimo &nbsp;&nbsp;
						<a href="<?php echo base_url('AreaAdministrador/cadastrarCotacaoFreteM')?>" class="" style="color: #56594a; font-weight: bold;" data-toggle="m-tooltip" data-placement="top" title="Adicionar cotação de frete">
							<i class="la la-plus-circle" style="font-size: 38px;"></i>
						</a>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="frete_maritimo" style="width: 1000px;font-size: 12px;">
									
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>				
			</div>
		</div>
	</div>	
</div>	

<!-- Modal Cotação Produto -->
<div class="modal fade" id="m_lista_cotacao_produto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">	
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-excluir-title" >Cotações de Produtos&nbsp;&nbsp;
					<a href="<?php echo base_url('AreaAdministrador/cadastrarCotacaoProduto')?>" class="" style="color: #56594a; font-weight: bold;" data-toggle="m-tooltip" data-placement="top" title="Adicionar cotação de produtos">
						<i class="la la-plus-circle" style="font-size: 38px;"></i>
					</a>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="cotacao_produto" style="width: 1000px;">									
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>				
			</div>
		</div>
	</div>	
</div>

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Orçamento solicitado com sucessoo, em breve entraremos em contato!',
                type: 'success'
            }).then(function() {
                window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>