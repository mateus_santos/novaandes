<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/gestaoUsuarios'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/gestaoUsuarios'); ?>">Voltar</a>						
						</h3>
						<h3 class="m-portlet__head-text">	
							&nbsp;&nbsp;&nbsp;&nbsp;Editar Usuário
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarUsuario');?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Nome:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required	name="nome"	class="form-control m-input" placeholder="" required value="<?php echo $dados['nome'];?>">
								<input type="hidden" name="id" 	class="form-control m-input" placeholder="" required value="<?php echo $dados['id'];?>">								
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
								</span>
							</div>
						</div>
						<div class="col-lg-4">
							<label class="">E-mail:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="email" required name="email" class="form-control m-input" placeholder="" required value="<?php echo $dados['email'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-envelope-o"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-4">
							<label>CPF:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="cpf" id="cpf" class="form-control m-input" placeholder="Insira seu cpf" value="<?php echo $dados['cpf'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>
							
						</div>
					</div>	 
					<div class="form-group m-form__group row">
						
						<div class="col-lg-6">
							<label class="">Telefone:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="telefone" required id="telefone" class="form-control m-input" placeholder="Insira seu telefone" value="<?php echo $dados['telefone'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
							</div>
							
						</div>
						<div class="col-lg-6">
							<label class="">Celular:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="celular" required id="celular" class="form-control m-input" placeholder="Insira seu telefone" value="<?php echo $dados['celular'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
							</div>
							
						</div>
					</div>	 					
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label for="Acesso">Acesso</label>
							<select class="form-control m-input" name="tipo_cadastro_id" required id="tipo_cadastro_id">
							<?php  foreach ($dados_tipo_acesso as $tipo_acesso) {							 ?>
								<option value="<?php echo $tipo_acesso['id']; ?>" <?php if($tipo_acesso['id'] == $dados['tipo_cadastro_id']) { echo 'selected="selected"'; }?> >
									<?php echo ($tipo_acesso['descricao']=='expedicao') ? 'EXPEDIÇÃO' : mb_strtoupper($tipo_acesso['descricao']); ?>									
								</option>
							<?php } ?>							
							
							</select>
						</div>
						<div class="col-lg-6">
							<label for="Empresa">Empresa</label>
							<select class="form-control m-input" name="empresa_id" required>
								<?php  foreach ($dados_empresa as $empresa) { ?>
									<option value="<?php echo $empresa['id']; ?>" <?php if($empresa['id'] == $dados['empresa_id']) { echo 'selected="selected"'; }?> >
										<?php echo $empresa['razao_social'] . " CNPJ:". $empresa['cnpj']; ?>
									</option>
								<?php } ?>							
							</select>
						</div>
					</div>           	
	            
				
	            <div class="form-group m-form__group row">
					<div class="col-lg-6">	
	            		<div class="m-radio-inline">	            		
		            		<label for="Empresa">Ativo</label><br/>
							<label class="m-radio">
								<input type="radio" name="ativo" class="ativo" <?php if($dados['ativo']=='1') echo 'checked="checked"' ?> value="1" user_id="<?php echo $dados['ativo']; ?>"> Sim
								<span></span>
							</label>									
							<label class="m-radio">
								<input type="radio" name="ativo" class="ativo" <?php if($dados['ativo']=='0') echo 'checked="checked"' ?> value="0" user_id="<?php echo $dados['ativo']; ?>"> Não
								<span></span>
							</label>								
						</div>	
					</div>		
					<div class="col-lg-6">
						<label class="">Data/Hora Cadastro:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="dthr_cadastro" disabled="disabled" class="form-control m-input" placeholder="Data/Hora Cadastro" value="<?php echo date('d/m/Y H:i:s', strtotime($dados['dthr_cadastro']));?>">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar-o"></i></span></span>
						</div>						
					</div>							
								
				</div>	
				<div class="form-group m-form__group row">
					<div class="col-lg-6">
						<label>Redefinir Senha:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="password" name="senha" class="form-control m-input" placeholder=""  value="" id="senha"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span>
							</span>
						</div>
					</div>
					<div class="col-lg-6">
						<label class="">Confirmar Senha:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="password" name="senha2" id="senha2" class="form-control m-input" placeholder="" value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span></span>
						</div>	
					</div>
				</div>	 
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</div>
			</form>
			<!--end::Form-->
		</div>
</div>	