<table class="" id="html_table" width="100%">
<thead>
	<tr>
		<th style="width: 5%;">#</th>
		<th style="width: 35%;">Cliente</th>						
		<th style="width: 15%;">Data</th>						
		<th style="width: 25%;">Placa</th>	
		<th style="width: 15%;">Motorista</th>
		<th style="width: 5%;">Ações</th>
	</tr>
</thead>
<tbody>					
	<?php foreach($agendamentos as $agendamento){	?>
		<tr>
			<td style="text-align: center;"><?php echo $agendamento['id']; ?></td>
			<td style=""><?php echo $agendamento['razao_social']; ?></td>											
			<td style=""><?php echo $agendamento['dt_agenda']; ?></td>
			<td style=""><?php echo $agendamento['placa']; ?></td>	
			<td style=""><?php echo $agendamento['nome']; ?></td>
			<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
				<a href="<?php echo base_url('AreaClientes/editarAgendamento/'.$agendamento['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-edit"></i>
				</a>	
				<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" target="blank" onclick="excluirAgendamento(<?=$agendamento['id']?>)">
					<i class="la la-trash"></i>
				</a>			
			</td>
		</tr>
		<?php } ?> 
	</tbody>
</table>	