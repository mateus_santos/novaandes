
<table class="" id="html_table" width="100%">
<thead>	
	<tr>
		<th style="width: 25%;font-size: 12px;">Cliente</th>
		<th style="width: 10%;font-size: 12px;text-align: center;">Orçamento</th>											
		<th style="width: 10%;font-size: 12px;text-align: center;">Pedido</th>
		<th style="width: 10%;font-size: 12px;text-align: center;">Situação</th>
		<th style="width: 10%;font-size: 12px;">Valor</th>		
		
		<th style="width: 5%;font-size: 12px;">Ações</th>
	</tr>
</thead>
<tbody>					
	<?php foreach($dados as $dado){	?>
		<tr>
			<td style="font-size: 12px;padding: 20px;"><?php echo $dado['cliente']; ?></td>
			<td style="font-size: 12px;text-align: center;">
				<?php echo $dado['id'].'-<b>'.$dado['emissao'].'</b>'; ?>
				<i class="la la-plus-square orcamento" title="Andamento do Orçamento" style="color: #ffcc00" onclick="andamento(<?php echo $dado['id']; ?>,'orcamento');">
					
				</i>			
			</td>			
			<td style="font-size: 12px;text-align: center;">
				<?php echo '<a href="'.base_url('AreaAdministrador/pedidos').'">'.$dado['pedido_id'].'</a> - <b>'.$dado['dthr_geracao'].'</b>'; ?>
				<i class="la la-plus-square pedido" title="Andamento do Pedido" style="color: #ffcc00" onclick="andamento(<?php echo $dado['pedido_id']; ?>,'pedido');">					
				</i>					
			</td>
			<td style="font-size: 12px;text-align: center;">
				<?php echo $dado['situacao']; ?>					
			
			</td>
			<td style="font-size: 12px;">R$ <?php echo $dado['valor']; ?></td>
			<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
				<a href="<?php echo base_url('AreaAdministrador/gerarHistoriconegociacao/'.$dado['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-print"></i>
				</a>				
			</td>
		</tr>
		<?php } ?> 
	</tbody>
</table>