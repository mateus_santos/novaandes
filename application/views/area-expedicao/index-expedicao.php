<div class="m-content">
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-calendar" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Classificação 
					</h3>	
					<h3 class="m-portlet__head-text" style=" padding-left: 10px;" >						
					</h3>									
				</div>

			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
									
				<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">#</th>
						<th style="width: 10%;">Placa</th>
						<th style="width: 15%;">Entrada</th>						
						<th style="width: 25%;">Motorista</th>							
						<th style="width: 15%;">Celular</th>
						<th style="width: 25%;">Cliente</th>
						<th style="width: 5%;">Ações</th>
					</tr>
				</thead>
				<tbody>					
				<?php foreach($classificacao as $class){	?>
					<tr>
						<td style="text-align: center;"><?php echo $class['id']; ?></td>
						<td style=""><?php echo $class['placa']; ?></td>											
						<td style=""><?php echo $class['dthr_entrada']; ?></td>
						<td style=""><?php echo $class['motorista']; ?></td>	
						<td style=""><?php echo $class['fone']; ?></td>
						<td style=""><?php echo $class['cliente']; ?></td>
						<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
							<a acesso_id="<?php echo $class['id']; ?>" placa="<?php echo $class['placa']; ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill classificar" target="blank">
								<i class="la la-edit"></i>
							</a>							
						</td>
					</tr>
					<?php } ?> 
				</tbody>
			</table>
						
		</div>						
	</div>
	<!--end::Portlet-->	

	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" >
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Cargas Classificadas Hoje
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="" class="m-portlet__body " >

			<!--begin: Datatable -->
			<table class="" id="table2" width="100%">
				<thead>
					<tr>
						<th style="">#</th>												
						<th style="">Placa</th>				
						<th style="">Data/hora</th>															
						<th style="">Situação</th>
						<th style="">Ações</th>
					</tr>
				</thead>
				<tbody>					
				<?php foreach($classificado as $class){	?>
					<tr>
						<td style="text-align: center;"><?php echo $class['id']; ?></td>
						<td style=""><?php echo $class['placa']; ?></td>											
						<td style=""><?php echo $class['dthr_entrada']; ?></td>
						<td style=""><?php echo ($class['fl_atende_classific']=='S') ? 'Atende - Liberado' : 'Não Atende'; ?></td>						
						<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
							<a acesso_id="<?php echo $class['id']; ?>" placa="<?php echo $class['placa']; ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill classificar_edit" target="blank" situacao="<?php echo $class['fl_atende_classific']; ?>">
								<i class="la la-edit"></i>
							</a>
						</td>
					</tr>
					<?php } ?> 
				</tbody>
			</table>			
		</div>
	</div>
</div>

<div class="modal fade" id="m_atualiza" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_atualiza_title" id="exampleModalLongTitle">
					
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="">					
				
				<div class="row">
					<div class="form-group m-form__group col-lg-12" >
						<label for="exampleSelect1">Classificação</label>							
						<select class="form-control" name="classificacao" id="classificacao">
							<option value="">Selecione</option>	
							<option value="S">Atende - Liberar Saída</option>
							<option value="N">Não Atende</option>
						</select>
					</div>
				</div>	
				
				<div class="row motivo_esconde" style="display: none;">
					<div class="form-group m-form__group col-lg-12" >
						<label for="exampleSelect1">Motivo</label>							
						<textarea class="form-control" name="motivo" id="motivo"></textarea>
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" id="classificar" acesso_id="">Classificar</button>
			</div>
		</div>
	</div>
</div>