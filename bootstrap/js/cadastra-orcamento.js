$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#cadastro').hide();    
    $('.peso').mask('###0.000', {reverse: true});
    $('.esconde_cotacoes').hide();
    $('.valor_unitario').mask('#.##0,00', {reverse: true});
    $('.frete_maritimo').bind('click', function(){
        var indice = $(this).attr('indice');
        if($('#pais_id').val() == undefined || $('#pais_id').val() == '' || $('.bombas[indice='+indice+']').val() == '' ){
            swal({
                title:  "Atenção!",
                text:   "Verifique se foi selecionado um país e um produto",
                type:   'warning'
            }).then(function() {
                
            });    
        }else{
            var destino_id  =   $('#pais_id').val();
            var produto_id  =   $('.bombas[indice='+indice+']').val();
            $.ajax({
                method: "POST",
                url: base_url+'AreaAdministrador/listaCotacoesFreteProforma',
                async: false,
                data: { destino_id  :   destino_id,
                        produto_id  :   produto_id }
            }).done(function( data ) {
                var dados = $.parseJSON(data);    
                $('#frete_maritimo').empty();
                $('#frete_maritimo').append(dados.retorno);
                $("#m_lista_frete").modal({
                    show: true
                });
            });

        }
    });

    $('.cotacao_produto').bind('click', function(){
        var indice = $(this).attr('indice');
        if($('.bombas[indice='+indice+']').val() == '' ){
            swal({
                title:  "Atenção!",
                text:   "Verifique se foi selecionado um produto",
                type:   'warning'
            }).then(function() {
                
            });    
        }else{            
            var produto_id  =   $('.bombas[indice='+indice+']').val();
            $.ajax({
                method: "POST",
                url: base_url+'AreaAdministrador/listaCotacoesProdutosProforma',
                async: false,
                data: { produto_id  :   produto_id }
            }).done(function( data ) {
                var dados = $.parseJSON(data);    
                $('#cotacao_produto').empty();
                $('#cotacao_produto').append(dados.retorno);
                $("#m_lista_cotacao_produto").modal({
                    show: true
                });
            });

        }
    });

    $('.add').bind('click', function(){

        var html = $('#modelo').clone();
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas').attr('indice', indice);
        $(html).find('.frete_maritimo').attr('indice', indice);
        $(html).find('.cotacao_produto').attr('indice', indice);
        $(html).find('.valor_unitario').attr('indice', indice);
        $(html).find('.valor_unitario').attr('id', 'valor_unitario_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();
        body+="</tr>";
        $('.table').append(body);
        $('#modelo').attr('total_indice',indice);

        excluir();
        mostra_valor(indice);

    });



    $('#formulario').submit(function(){
        $('#enviar').attr('disabled', 'disabled');

    });

    $('#cnpj').bind('focusout', function(){
        var cnpj = $(this).val(); 
        var tp_cadastro = $('.tp_cadastro:checked').val();
        var erro = 0;

        if(cnpj != ""){ 
            $.ajax({
                method: "POST",
                url: base_url+'clientes/verificaOrcamento',
                async: true,
                data: { cnpj    :   cnpj }
                }).done(function( data ) {
                    var dados = $.parseJSON(data);
                    if( dados.total == 0){

                        $.ajax({
                            method: "POST",
                            url: base_url+'clientes/verifica_cnpj',
                            async: false,
                            data: { cnpj    :   cnpj }
                            }).done(function( data ) {
                                var dados = $.parseJSON(data);                          
                                if(dados.length > 0){
                                    
                                    $('#razao_social').val(dados[0].razao_social);      
                                    $('#razao_social').attr('disabled', true);      
                                    $('#fantasia').val(dados[0].fantasia);      
                                    $('#fantasia').attr('disabled', true);
                                    $('#telefone').val(dados[0].telefone);
                                    $('#telefone').attr('disabled', true);
                                    $('#endereco').val(dados[0].endereco);      
                                    $('#endereco').attr('disabled', true);                                  
                                    $('#email').attr('disabled', true);
                                    $('#email').val(dados[0].email);
                                    $('#cidade').attr('disabled', true);
                                    $('#cidade').val(dados[0].cidade);  
                                    $('#estadoE').attr('disabled', true);
                                    $('#estadoE').val(dados[0].estado);                        
                                    $('#pais_id').attr('disabled', true);
                                    $('#pais_id').val(dados[0].pais_id);
                                    $('#salvar').val('0');
                                    $('#empresa_id').val(dados[0].id);          
                                    $('#cep').attr('disabled', true);
                                    $('#cep').val(dados[0].cep);
                                    $('#bairro').attr('disabled', true);
                                    $('#bairro').val(dados[0].bairro);
                                    
                                    
                                }                        
                        });  
                    }else{

                        swal({
                            title: "Atenção!",
                            text: "Já Existe uma proforma em andamento para este cliente, verifique!",
                            type: 'warning'
                        }).then(function() {
                            $('#cnpj').val('');
                        }); 
                   }
                });

            }
    });

    $('.tp_cadastro').bind('click', function(){
        $('#cadastro').slideDown('slow');
        
        if( $(this).val() == 'PJ' ){
            $('#cadastro input').attr('disabled', false);
            $('#cnpj').mask('00.000.000/0000-00');
            $('#cnpj').attr('placeholder','INSIRA UM CNPJ');
            $('#razao_social').attr('placeholder','INSIRA UMA RAZÃO SOCIAL');
            $('#label_documento').text('CNPJ:');    
            $('#label_razao').text('RAZÃO SOCIAL:');
            $('#campo_fantasia').fadeIn('slow');
            $('#campo_ie').fadeIn('slow');
            $('#inscricao_estadual').attr('required',true);
            $('#fantasia').attr('required',true);

            $('#estadoE').removeAttr('name');
            $('#estado').attr('name','estado');
            $('#estadoE').fadeOut('slow');
            $('#estado').fadeIn('slow');

            var SPMaskBehavior = function (val) {
              return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
              onKeyPress: function(val, e, field, options) {
                  field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
            $('#telefone').mask(SPMaskBehavior, spOptions);
            $('#tel_contato_posto').mask(SPMaskBehavior, spOptions);

        }else if(   $(this).val() == 'PF'   ){
            $('#cadastro input').attr('disabled', false);
            $('#cnpj').mask('000.000.000-00');
            $('#cnpj').attr('placeholder','INSIRA UM CPF');
            $('#razao_social').attr('placeholder','INSIRA O NOME');
            $('#label_documento').text('CPF:');
            $('#label_razao').text('NOME:');
            $('#campo_fantasia').fadeOut('slow');
            $('#campo_ie').fadeOut('slow');
            $('#inscricao_estadual').removeAttr('required');
            $('#fantasia').removeAttr('required');

            $('#estadoE').removeAttr('name');
            $('#estado').attr('name','estado');
            $('#estadoE').fadeOut('slow');
            $('#estado').fadeIn('slow');

            var SPMaskBehavior = function (val) {
              return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
              onKeyPress: function(val, e, field, options) {
                  field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
            $('#telefone').mask(SPMaskBehavior, spOptions);       
            $('#tel_contato_posto').mask(SPMaskBehavior, spOptions);

        }else{
            $('#cadastro input').attr('disabled', false);
            $('#cnpj').unmask();
            $('#cnpj').attr('placeholder','INSIRA O NR. DO DOCUMENTO');
            $('#razao_social').attr('placeholder','INSIRA UMA RAZÃO SOCIAL');
            $('#label_documento').text('ID. DOC:');    
            $('#label_razao').text('RAZÃO SOCIAL');
            $('#campo_fantasia').fadeIn('slow');
            $('#campo_ie').fadeOut('slow');
            $('#inscricao_estadual').attr('required',false);
            $('#fantasia').attr('required',false);            
            $('#estado').attr('required',false);

            $('#estadoE').attr('type','text');
            $('#estadoE').fadeIn('slow');
            $('#estadoE').attr('name','estado');
            $('#estado').attr('required',false);
            $('#estado').removeAttr('name');
            $('#estado').fadeOut('slow');    
            $('#estadoE').attr('readonly',true);
            $('#estadoE').val('EX'); 

            var SPMaskBehavior = function (val) {
              return val.replace(/\D/g, '').length === 11 ? '+00 (00) 00000-0000' : '+00 (00) 0000-00009';
            },
            spOptions = {
              onKeyPress: function(val, e, field, options) {
                  field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
            $('#telefone').mask(SPMaskBehavior, spOptions);       
            $('#tel_contato_posto').mask(SPMaskBehavior, spOptions);       
        }

    });
    
    //$('#cnpj').mask('00.000.000/0000-00');
    //$('#cpf').mask('000.000.000-00');   
    $('#cep').mask('00000-000');  
    
    
    

    $('.bombas').bind('change', function(){
        indice = $(this).attr('indice');
        $('#valor_unitario_'+indice).val($(this).find(':selected').attr('valor_unitario'));
        $('.valor_unitario').unmask();
        $('.valor_unitario').mask('#.##0,00', {reverse: true});
    });

    $( "#nome_indicador" ).autocomplete({
        source: base_url+"AreaAdministrador/retornaIndicadores",
        minLength: 2,
        select: function( event, ui ) {

            $('#indicador_id').val(ui.item.id);
            var indicador   =   ui.item.id;
            
        }
    });

    $('.indicacao').bind('click', function(){
        if($(this).val() == 1){
            $('.nome_indicador').fadeIn('slow');
            $('#nome_indicador').attr('required','required');

        }else{

            $('.nome_indicador').fadeOut('slow');
            $('#nome_indicador').removeAttr('required');
            $('#nome_indicador').val('');           
        }
    });

});
	
	
function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}

function mostra_valor(indice)
{
   $('.bombas').bind('change', function(){
        
        $('#valor_unitario_'+indice).val($(this).find(':selected').attr('valor_unitario'));
        $('.valor_unitario').unmask();
        $('.valor_unitario').mask('#.##0,00', {reverse: true});
    });

}

function isCNPJValid(cnpj) {  
    var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
    if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
        return false;
    for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
    if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
    if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    return true; 
};