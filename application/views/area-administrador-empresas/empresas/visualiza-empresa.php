<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<a href="<?php echo base_url('AreaAdministradorEmpresas/gestaoEmpresas/'.$pesquisa);?>"><i class="la la-arrow-left"></i></a>
					</span>
					<h3 class="m-portlet__head-text" style="padding-right: 10px;">	
						<a href="<?php echo base_url('AreaAdministradorEmpresas/gestaoEmpresas/'.$pesquisa);?>">Voltar</a>						
					</h3>
					<span class="m-portlet__head-icon">
						<i class="la la-building-o"></i>
					</span>

					<h3 class="m-portlet__head-text">
						Empresa #<?php  echo $dados[0]->id; ?>
					</h3>
				
				</div>	

			</div>
			<div style="text-align: right;">				
				<a href="<?php echo base_url('AreaAdministradorEmpresas/editarEmpresa/'.$dados[0]->id.'/'.$pesquisa);?>" class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" style="margin-top: 15px;" data-toggle="m-tooltip" title="" data-original-title="Editar Empresa">
					<i class="la la-edit"></i>
				</a>				
			</div>			
		</div>
		<div class="m-portlet__body">
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>Empresa: 
						<small class="text-muted"> <?php echo $dados[0]->cnpj.' | '.$dados[0]->razao_social.' | '.$dados[0]->fantasia;?></small>
					</h5>
				</div>	
				<input type="hidden" value="<?=$dados[0]->id?>" id="empresa_id">
				<input type="hidden" value="1" id="tipo_cadastro_id">
				<div class="col-lg-4 col-xs-12">
					<h5>Telefone: 
						<small class="text-muted"> <?php echo $dados[0]->telefone;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>E-mail: 
						<small class="text-muted"> <?php echo $dados[0]->email;?></small>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Enderço: <small class="text-muted"> <?php echo $dados[0]->endereco;?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Cidade: <small class="text-muted"> <?php echo $dados[0]->cidade;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						Estado: <small class="text-muted"> <?php echo $dados[0]->estado;?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Bairro e CEP: <small class="text-muted"> <?php echo $dados[0]->bairro .' CEP:'.$dados[0]->cep;?></small>
					</h5>
				</div>							
			</div>
			<?php if($dados[0]->tipo_cadastro == 'clientes') { ?>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Nr. Bombas: <small class="text-muted"> <?php echo $dados[0]->nr_bombas;?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Marcas: <small class="text-muted"> <?php echo $dados[0]->marca_bombas;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						Nr. Bicos Ativos: <small class="text-muted"> <?php echo $dados[0]->bicos;?></small>
					</h5>
				</div>							
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Postos ou Rede: <small class="text-muted"> <?php echo $dados[0]->nr_bombas;?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Nr. Postos: <small class="text-muted"> <?php echo $dados[0]->nr_postos;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						Mecânico que atende: <small class="text-muted"> <?php echo $dados[0]->mecanico_atende;?></small>
					</h5>
				</div>							
			</div>
			<?php }elseif( $dados[0]->tipo_cadastro == 'representantes' ){ ?>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Região que atende: <small class="text-muted"> <?php echo $dados[0]->regiao_atua;?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Data da Constituição da empresa: <small class="text-muted"> <?php echo $dados[0]->data_constituicao;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						Nr. de Funcionários: <small class="text-muted"> <?php echo $dados[0]->nr_funcionarios;?></small>
					</h5>
				</div>
			</div>	
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Representante Legal: <small class="text-muted"> <?php echo $dados[0]->representante_legal;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						Produtos que representa: <small class="text-muted"> <?php echo $dados[0]->representante_legal;?></small>
					</h5>
				</div>
			</div>
			
			<?php } 

				if( $dados[0]->cartao_cnpj != '' ){ 
					$arquivo = explode('/',$dados[0]->cartao_cnpj); 					
			?>
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<h5>Cartão Cnpj: <a href="<?php echo base_url('Consultas/Receita_Federal/'.$arquivo[count($arquivo)-1]); ?>" target="_blank">	
						<img style="width: 50px;" src="<?php echo base_url('bootstrap/img/receita.gif'); ?>" /></a>
					</h5>
				</div>
			</div>
			
			<?php }	?>	
		</div>
	</div>
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi <?php echo ( $dados[0]->tipo_cadastro_id == 17 ) ? 'col-lg-6 col-xs-12 classificadora' : ''; ?>">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
			
				<h3 class="m-portlet__head-text">

					Novo usuário &nbsp;&nbsp;
					<a data-toggle="modal" href="#modal_add_usuario" style="color: #ffcc00; font-weight: bold;" >
						<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar Novo Funcionário"></i>
					</a>
				</h3>

				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span> Usuários - Funcionários </span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<?php if( count($dados) >= 1 && $dados[0]->nome != "" ){ ?>
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>
			    		<th>#</th>
			      		<th>Nome</th>
			      		<th>Cpf</th>
			      		<th>E-mail</th>						
			      		<th>Ação</th>
			    	</tr>
			  	</thead>
			  	<tbody>
			  		<?php foreach($dados as $dado){  ?>
			    	<tr>
				      	<th scope="row"><?php echo $dado->usuario_id;?></th>
				      	<td><?php echo $dado->nome; ?></td>
				      	<td><?php echo $dado->cpf; ?></td>
				      	<td><?php echo $dado->email_pessoal; ?></td>						
						<td><a href="<?php echo base_url('AreaAdministradorEmpresas/editarUsuario/'.$dado->usuario_id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" pedido_id="<?php echo $dado->id; ?>" >
							<i class="la la-edit editar"></i>
						</a></td>
			    	</tr>			
			    	<?php } ?>    	
			  	</tbody>
			</table>
			<?php }else{ ?>
			<h2>Nenhum usuário/funcionário vinculado a essa empresa.</h2>
			<?php } ?>
		</div>
	</div>
		

</div>

<div class="modal fade" id="modal_add_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

		<div class="modal-dialog modal-dialog-centered" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title modal-andamento-title" >Cadastrar Novo Funcionário</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body" style="max-width: 750px;">
					<div class="row">
						<div class="form-group m-form__group col-lg-9" >

							<label for="exampleSelect1">Nome</label>							

							<input type="text" class="form-control m-input m-input--air sub" placeholder="Nome do funcionário" name="nome" id="nome" style="margin-bottom: 10px;float: left;" />

						</div>

						<div class="form-group m-form__group col-lg-3" >

							<label for="exampleSelect1">CPF</label>							

							<input type="text"  class="form-control m-input m-input--air qtd" placeholder="CPF do funcionário" name="cpf" id="cpf"  style="margin-bottom: 10px;float: right;" /> 

						</div>

					</div>
					
					<div class="row">						

						<div class="form-group m-form__group col-lg-4" >

							<label for="exampleSelect1">Telefone</label>							

							<input type="text"  class="form-control m-input m-input--air qtd" placeholder="Telefone do funcionário" name="telefone" id="telefone"  style="margin-bottom: 10px;float: right;" /> 

						</div>
						<div class="form-group m-form__group col-lg-4" >

							<label for="exampleSelect1">Email</label>							

							<input type="email" class="form-control m-input m-input--air sub" autocomplete="off" placeholder="E-mail do funcionário" name="email" style="margin-bottom: 10px;float: left;display: none;" />
							<input type="email" class="form-control m-input m-input--air sub" autocomplete="off" placeholder="E-mail do funcionário" name="email" id="email" style="margin-bottom: 10px;float: left;" />


						</div>
						<div class="form-group m-form__group col-lg-4" >

							<label for="">Celular</label>							

							<input type="text" class="form-control m-input m-input--air sub" autocomplete="off" placeholder="Celular do funcionário" name="celular" id="celular" style="margin-bottom: 10px;float: left;" />

						</div>

					</div>
					<div class="row">
						<div class="form-group m-form__group col-lg-6">
						<label>Redefinir Senha:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="password" name="senha" autocomplete="off" class="form-control m-input" placeholder=""  value=" " style="display: none;">
							<input type="password" name="senha" autocomplete="off" class="form-control m-input" placeholder=""  value="" id="senha"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span>
							</span>
						</div>
					</div>
					<div class="form-group m-form__group col-lg-6">
						<label class="">Confirmar Senha:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="password" name="senha2" id="senha2" class="form-control m-input" placeholder="" value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span></span>
						</div>	
					</div>
					</div>
					
				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>

					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="adicionar_funcionario()">Adicionar Funcionário</button>

				</div>

			</div>

		</div>

	</div>

<!-- Modal Anexar nfe ao pedido -->
<div class="modal fade" id="vincularCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/vincularClienteClass');?>" method="post"  enctype="multipart/form-data">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-anexar-nfe-title" >Vincular Cliente a Classificadora: <b><?php echo $dados[0]->razao_social; ?></b></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="max-width: 750px;">					
				<div class="row">
					<div class="form-group m-form__group col-lg-12 col-xs-12">						
						<label for="">Cliente</label>
						<input type="hidden" name="classificadora_id" value="<?php  echo $dados[0]->id; ?>">
						<select class="form-control" name="cliente_id" required="required">
							<option value="" > Selecione um cliente </option>
						<?php 	foreach($clientes as $cliente){	?>
							<option value="<?php echo $cliente['id']; ?>"> <?php echo $cliente['cliente']; ?> </option>
						<?php } ?>
						</select>
					</div>						
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="submit" class="btn btn-primary" >Enviar</button>
			</div>
		</div>			
	</div>
	</form>
</div>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
		   	//window.location = base_url+'AreaAdministrador/gestaoEmpresas';
		});
	</script>	
<?php unset($_SESSION['sucesso']); } ?>