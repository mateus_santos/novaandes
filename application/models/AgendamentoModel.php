<?php

class AgendamentoModel extends CI_Model {

	public function insere($agendamento)
	{
        $this->db->insert('agendamento', $agendamento);
		return $this->db->insert_id();
    }

    public function insereItem($item)
	{
        $this->db->insert('agendamento_item', $item);
		return $this->db->insert_id();
    }

    public function atualiza($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('agendamento', $dados)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaItem($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('agendamento_item', $dados)){
            return true;
        }else{
            return false;
        }
    }

    public function excluirItens($id_agenda){
		$this->db->where('id_agenda', $id_agenda);
        if(	$this->db->delete('agendamento_item') ){
            return true;
        }else{
            return false;
        }
	}

	public function excluir($id){
		if( $this->excluirItens($id) ){
			$this->db->where('id', $id);
	        if(	$this->db->delete('agendamento') ){
	            return true;
	        }else{
	            return false;
	        }
    	}else{
    		return false;
    	}
	}

	public function buscaAgendamentosDoDia($empresa_id,$tipo_acesso)
	{
		$where_empresa = '';
		if( $empresa_id != '' && $tipo_acesso != 'administrador empresas'){
			$where_empresa = " and 	agendamento.id_empresa = ".$empresa_id;
		}
		// se for administrador de empresas, busca apenas clientes vinculados
		if( $tipo_acesso == 'administrador empresas' ){
			$where_empresa = " and 	agendamento.id_empresa in (select cliente_id from empresas_clientes where administrador_id = ".$empresa_id.")" ;	
		}

		$sql = "SELECT	agendamento.id,
						agendamento.id_tipo_operacao,
	          			empresas.razao_social,
	          			date_format(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
						agendamento.placa,
						motorista.nome,
						motorista.cpf,
						motorista.fone, 
						embalagem.no_embalagem,
						nota_fiscal.nr_nf,
						nota_fiscal.quant_total,
						agendamento_item.descricao
				FROM 	agendamento
				INNER JOIN empresas ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista ON agendamento.id_motorista = motorista.id
				INNER JOIN agendamento_item ON agendamento.id = agendamento_item.id_agenda
				LEFT JOIN embalagem ON agendamento_item.id_embalagem = embalagem.id
				LEFT JOIN nota_fiscal ON agendamento.id = nota_fiscal.id_agenda
				".$where_empresa."
				and 	agendamento.dt_agenda = '".date('Y-m-d')."'				
				ORDER BY agendamento.id DESC ";

		return $this->db->query($sql)->result_array();
	}

	public function buscaAgendamentos($empresa_id,$where='1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " 	agendamento.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT	agendamento.id,
				agendamento.id_tipo_operacao,
				empresas.razao_social,
				date_format(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
				agendamento.placa,
				motorista.nome,
				motorista.cpf,
				motorista.fone,
				embalagem.no_embalagem,
				nota_fiscal.nr_nf,
				nota_fiscal.quant_total,
				agendamento_item.descricao
				FROM 	agendamento
				INNER JOIN empresas ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista ON agendamento.id_motorista = motorista.id
				INNER JOIN agendamento_item ON agendamento.id = agendamento_item.id_agenda
				LEFT JOIN embalagem ON agendamento_item.id_embalagem = embalagem.id
				LEFT JOIN nota_fiscal ON agendamento.id = nota_fiscal.id_agenda
				WHERE 1=1 and 				
				".$where_empresa." ".$where."
				ORDER BY agendamento.id DESC ";
		return $this->db->query($sql)->result_array();
	}

	public function buscaAgendamentoPorId($id_agenda)
	{
		$sql = "SELECT	agendamento.id,
						agendamento.id_tipo_operacao,
						agendamento.id_empresa,
          				empresas.razao_social,
          				date_format(agendamento.dt_agenda,'%d/%m/%Y') as dt_agenda,
						agendamento.placa,
						agendamento.id_motorista,
						motorista.nome,
						motorista.fone,
						motorista.cpf,
						motorista.nr_cnh,
						nota_fiscal.nr_nf,
						nota_fiscal.serie_nf,
						nota_fiscal.arquivo
				FROM 	agendamento
				INNER JOIN empresas ON agendamento.id_empresa  = empresas.id
				INNER JOIN motorista ON  agendamento.id_motorista = motorista.id				
				LEFT JOIN nota_fiscal ON agendamento.id = nota_fiscal.id_agenda 	
				WHERE agendamento.id = ".$id_agenda."
			ORDER BY agendamento.id DESC ";

		return $this->db->query($sql)->row_array();			
	}

	public function buscaEmbalagem()
	{
		$sql = "SELECT * FROM embalagem";
		return $this->db->query($sql)->result_array();
	}

	public function verificaCpfCnh($where)
	{
		if($where['cpf'] != ''){
			$situacao = "cpf = '".$where['cpf']."'" ;
		}
		if($where['cnh'] != ''){
			$situacao = "nr_cnh = '".$where['cnh']."'";	
		}

		$sql = "	SELECT 	id, nome, cpf, nr_cnh, fone 
					FROM 	motorista 
					WHERE 	".$situacao."
					AND 	FL_BLOQUEADO = 'N'	";

		return $this->db->query($sql)->row_array();
	}

	public function verificaNotaAgendamento($chave)
	{
		$sql = "SELECT 	agendamento.id, agendamento.placa
				FROM 	agendamento, nota_fiscal
				where 	nota_fiscal.id_agenda = agendamento.id
				and 	nota_fiscal.chave_acesso = '".$chave."'";

		return $this->db->query($sql)->row_array();
	}

	public function verificaCotasAgendamento($empresa_id, $dt_agendamento)
	{
		$sql = "SELECT  hr_limite_agenda, hr_limite_acesso, peso, fl_sem_cota
				FROM 	cotas
				WHERE 	id_empresa = ".$empresa_id." 
				and 	dia_semana = (SELECT CASE 	WHEN DAYOFWEEK('".$dt_agendamento."') = 1 THEN 'DOM'          
                                                  	WHEN DAYOFWEEK('".$dt_agendamento."') = 2 THEN 'SEG' 
                                                 	WHEN DAYOFWEEK('".$dt_agendamento."') = 3 THEN 'TER' 
                                                 	WHEN DAYOFWEEK('".$dt_agendamento."') = 4 THEN 'QUA' 
                                                 	WHEN DAYOFWEEK('".$dt_agendamento."') = 5 THEN 'QUI' 
                                                	WHEN DAYOFWEEK('".$dt_agendamento."') = 6 THEN 'SEX' 
                                       				ELSE 'SAB' END as dia_semana from dual)";

		return $this->db->query($sql)->row_array();
	}

	public function buscaTotalAgendadoPeriodo($empresa_id, $periodo_ini)
	{
		$sql = "SELECT 	COALESCE(sum(ai.peso),0) as total 
				FROM 	agendamento_item ai 
				INNER JOIN 	agendamento a ON a.id = ai.id_agenda 
				WHERE 	a.id_empresa = ".$empresa_id." and 
						a.dt_agenda = '".$periodo_ini."' 
				GROUP by a.id";
				
		return $this->db->query($sql)->row_array();
	}

	public function verificaCfop($cfop)
	{
		$sql = "SELECT coalesce(count(*)) as total_cfop FROM cfop WHERE cfop =".$cfop;
		return $this->db->query($sql)->row_array();	
	}

	public function buscaItensAgendamento($id_agenda)
	{
		$sql = "SELECT * FROM agendamento_item WHERE id_agenda=".$id_agenda;
		return $this->db->query($sql)->result_array();	
	}

	public function verificaEntradaPatio($id_agenda)
	{
		$sql = "SELECT COALESCE(count(*),0) as total FROM acesso_patio WHERE id_agenda=".$id_agenda;
		return $this->db->query($sql)->row_array();	
	}

	/*Busca os caminhões que saíram */
	public function buscaSaidas($empresa_id,$where='and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and 	agendamento.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT
					agendamento.id,
					IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'Mon','Segunda',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'tue','Terça',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'wed','Quarta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'thu','Quinta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'fri','Sexta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'sat','Sabado',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'sun','Domingo','err'))))))) as dia,
					empresas.razao_social,
					DATE_FORMAT(agendamento.dt_agenda, '%d/%m/%Y') AS dt_agenda,
					agendamento.placa,
					motorista.nome,
					motorista.cpf,
					motorista.fone,
					embalagem.no_embalagem,
					nota_fiscal.nr_nf,
					nota_fiscal.quant_total,
					agendamento_item.descricao,
					DATE_FORMAT(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i:%s') AS dthr_entrada,
					CASE 
						WHEN convert(date_format(acesso_patio.dthr_classific,'%d'),CHAR) > 0 THEN
							date_format(acesso_patio.dthr_classific,'%d/%m/%Y %H:%i:%s')
						ELSE ''
					END dthr_classific,
					DATE_FORMAT(acesso_patio.dthr_saida,'%d/%m/%Y %H:%i:%s') AS dthr_saida
				FROM
					agendamento,
					empresas,
					motorista,
					agendamento_item,
					embalagem,
					nota_fiscal,
					cotas,
					acesso_patio
				WHERE
					agendamento.id_empresa = empresas.id 
					AND agendamento.id = agendamento_item.id_agenda 
					AND agendamento.id_motorista = motorista.id 
					AND agendamento_item.id_embalagem = embalagem.id 
					AND agendamento.id = nota_fiscal.id_agenda 
					AND empresas.id = cotas.id_empresa 
					and agendamento.id = acesso_patio.id_agenda
					AND DATE_FORMAT(agendamento.dt_agenda, '%a') = IF(cotas.dia_semana = 'seg','Mon',IF(cotas.dia_semana = 'ter', 'tue', IF(cotas.dia_semana = 'qua','wed',IF(cotas.dia_semana = 'qui','thu',IF(cotas.dia_semana = 'sex','fri',IF(cotas.dia_semana = 'sab','sat',IF(cotas.dia_semana = 'dom','sun','err')))))))
					AND acesso_patio.dthr_saida is not null
				".$where_empresa." ".$where."
				ORDER BY agendamento.id DESC ";
		 
		return $this->db->query($sql)->result_array();
	}

	/*Busca os caminhões que Entraram e estão aguardando saída */
	public function buscaEstacionados($empresa_id,$where='and 1=1')
	{
		$where_empresa = '';
		if( $empresa_id != '' ){
			$where_empresa = " and 	agendamento.id_empresa = ".$empresa_id;
		}

		$sql = "SELECT
					agendamento.id,
					IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'Mon','Segunda',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'tue','Terça',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'wed','Quarta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'thu','Quinta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'fri','Sexta',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'sat','Sabado',IF(DATE_FORMAT(agendamento.dt_agenda, '%a') = 'sun','Domingo','err'))))))) as dia,
					empresas.razao_social,
                    agendamento_item.descricao,
                    agendamento_item.nr_pedido,
                    embalagem.no_embalagem,
					nota_fiscal.quant_total,
                    IF(acesso_patio.fl_atende_classific = 'S','LIBERADO',IF(acesso_patio.fl_atende_classific = 'N','NÃO LIBERADO','SEM CLASSIFICAÇÃO')) AS fl_atende_classific,
					DATE_FORMAT(agendamento.dt_agenda, '%d/%m/%Y') AS dt_agenda,
					agendamento.placa,
					nota_fiscal.nr_nf,
					motorista.nome,
					motorista.cpf,
					motorista.fone,
					DATE_FORMAT(acesso_patio.dthr_entrada,'%d/%m/%Y %H:%i:%s') AS dthr_entrada,
					CASE 
						WHEN convert(date_format(acesso_patio.dthr_classific,'%d'),CHAR) > 0 THEN
							date_format(acesso_patio.dthr_classific,'%d/%m/%Y %H:%i:%s')
						ELSE ''
					END dthr_classific,
					DATE_FORMAT(acesso_patio.dthr_saida,'%d/%m/%Y %H:%i:%s') AS dthr_saida
				FROM
					agendamento,
					empresas,
					motorista,
					agendamento_item,
					embalagem,
					nota_fiscal,
					cotas,
					acesso_patio
				WHERE
					agendamento.id_empresa = empresas.id 
					AND agendamento.id = agendamento_item.id_agenda 
					AND agendamento.id_motorista = motorista.id 
					AND agendamento_item.id_embalagem = embalagem.id 
					AND agendamento.id = nota_fiscal.id_agenda 
					AND empresas.id = cotas.id_empresa 
					and agendamento.id = acesso_patio.id_agenda
					AND DATE_FORMAT(agendamento.dt_agenda, '%a') = IF(cotas.dia_semana = 'seg','Mon',IF(cotas.dia_semana = 'ter', 'tue', IF(cotas.dia_semana = 'qua','wed',IF(cotas.dia_semana = 'qui','thu',IF(cotas.dia_semana = 'sex','fri',IF(cotas.dia_semana = 'sab','sat',IF(cotas.dia_semana = 'dom','sun','err')))))))
					AND acesso_patio.dthr_saida is null
				".$where_empresa." ".$where."
				ORDER BY agendamento.id DESC ";
		 
		return $this->db->query($sql)->result_array();
	}

	
}