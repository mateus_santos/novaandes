<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/cotacaoFreteMaritimo'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/cotacaoFreteMaritimo'); ?>">Voltar</a>	Cadastro de Cotação de Frete Marítimo
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/cadastrarCotacaoFreteM');?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Origem:</label>
							<div class="m-input-icon m-input-icon--left">
								<select name="origem_id" class="form-control" required>
									<option value="">Selecione o porto de origem</option>
									<?php foreach($origens as $org){ ?>
										<option value="<?=$org['id']?>"><?=$org['abreviacao'].'/'.$org['abreviacao'].' - '.$org['descricao']?></option>
									<?php } ?>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-ship"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Destino:</label>
							<div class="m-input-icon m-input-icon--left">
								<select name="destino_id" class="form-control" required>
									<option value="">Selecione o porto de destino</option>
									<?php foreach($destinos as $destino){ ?>
										<option value="<?=$destino['id']?>"><?=$destino['abreviacao'].'/'.$destino['abreviacao'].' - '.$destino['descricao']?></option>
									<?php } ?>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-ship"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Container:</label>
							<div class="m-input-icon m-input-icon--left">
								<select name="container" class="form-control" required>
									<option value="">Selecione o tipo de container</option>
									<option value="Dry Box 20 ft">Dry Box 20 ft</option>
									<option value="Dry Box 40 ft">Dry Box 20 ft</option>
									<option value="High Cube 40 ft">High Cube 40 ft</option>
									<option value="Open Top 20 ft">Open Top 20 ft</option>
									<option value="Open Top 40 ft">Open Top 40 ft</option>
									<option value="Graneleiro Dry 20 ft">Graneleiro Dry 20 ft</option>
									<option value="Flat Rack 20 ft">Flat Rack 20 ft</option>
									<option value="Flat Rack 40 ft">Flat Rack 40 ft</option>
									<option value="Container Tanque">Container Tanque</option>
									<option value="Container Ventilado">Container Ventilado</option>
									<option value="Plataforma 20 ft">Plataforma 20 ft</option>
									<option value="Plataforma 40 ft">Plataforma 40 ft</option>
									<option value="Reefer 20 ft">Reefer 20 ft</option>
									<option value="Reefer 40 ft">Reefer 40 ft</option>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-th-large"></i></span></span>
							</div>							
						</div>						
					</div>	
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Produto:</label>
							<div class="m-input-icon m-input-icon--left">
								<select name="produto_id" class="form-control" required>
									<option value="">Selecione o produto</option>
									<?php foreach($produtos as $produto){ ?>
										<option value="<?=$produto['id']?>"><?=$produto['tipo_produto'].' - '.$produto['descricao']?></option>
									<?php } ?>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-file-text-o"></i></span></span>
							</div>							
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Peso Bruto:</label>
							<div class="m-input-icon m-input-icon--left">
								<input type="text" name="peso_bruto" class="form-control peso_bruto" required />
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-balance-scale"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Volume Bruto:</label>
							<div class="m-input-icon m-input-icon--left">
								<input type="text" name="volume_bruto" class="form-control volume" required />
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-balance-scale"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Embalagem Externa:</label>
							<div class="m-input-icon m-input-icon--left">
								<input type="text" name="embalagem_externa" class="form-control" required />
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-balance-scale"></i></span></span>
							</div>							
						</div>
					</div>			
		            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
									<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	