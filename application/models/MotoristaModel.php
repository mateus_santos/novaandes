<?php

class MotoristaModel extends CI_Model {

	public function insere($motorista)
	{
        $this->db->insert('motorista', $motorista);
		return $this->db->insert_id();
    }

    public function atualiza($dados){

        $this->db->where('id', $dados['id']);
        if($this->db->update('motorista', $dados)){
            return true;
        }else{
            return false;
        }
    }
}