<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">					
					<span class="m-portlet__head-icon">
						<a href="<?php echo base_url('AreaAdministrador/orcamentos/'.$pesquisa);?>"><i class="la la-arrow-left"></i></a>
					</span>
					<h3 class="m-portlet__head-text" style="padding-right: 10px;">	
						<a href="<?php echo base_url('AreaAdministrador/orcamentos/'.$pesquisa);?>">Voltar</a>						
					</h3>
					<h3 class="m-portlet__head-text">						
						Orçamento <b>#<?php echo $dados[0]->id_orcamento; ?></b> &nbsp;&nbsp; <?php if( $dados[0]->status == "aberto" ){
							echo '<button class="btn m-btn--pill m-btn--air btn-secondary" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Aberto </button>';
						}elseif( $dados[0]->status == "fechado" ){
							$status = "";
							echo '<button class="btn m-btn--pill m-btn--air btn-success" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Fechado </button>';
						}elseif( $dados[0]->status == "perdido outros" || $dados[0]->status == "perdido para gilbarco" || $dados[0]->status == "perdido para wayne" ){
							echo '<button class="btn m-btn--pill m-btn--air btn-danger" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento">'.$dados[0]->status.' </button>';
						}elseif( $dados[0]->status == "cancelado" ){
							
							echo '<button class="btn m-btn--pill m-btn--air btn-warning" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Cancelado </button>';
						}elseif( $dados[0]->status == "orçamento entregue" ){							
							echo '<button class="btn m-btn--pill m-btn--air btn-info" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Orçamento Entregue </button>';
						}elseif($dados[0]->status == "em negociação"){
							echo '<button class="btn m-btn--pill m-btn--air btn-primary" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Em Negociação </button>';							
						}elseif($dados[0]->status == "Solicitação de Orçamentos - Indicador"){
							echo '<button class="status btn m-btn--pill m-btn--air" status="Solicitação de Orçamentos - Indicador" style="background: #ff9800;color:#fff;" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento">Solicitação de Orçamentos - Indicador</button>';
						}else{
							echo '<button class="status btn m-btn--pill m-btn--air" status="Indicação não aprovada" style="background: #999;color:#fff;" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento">'.$dados[0]->status.'</button>';
						}

						 ?>
					</h3>
				</div>			
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<!-- GERAR DESCONTO -->
					<!--<li class="m-portlet__nav-item">
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="geraDesconto" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" data-toggle="modal" data-target="#m_modal_8" data-placement="top" title="Gerar Desconto">
							<i class="la la-minus"></i>
						</a>	
					</li>-->
					<input type="hidden" id="fl_especial" value="<?php echo $dados[0]->fl_especial; ?>">
					<li class="m-portlet__nav-item">
						<a class="btn btn-outline-primary m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="geraPdf" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>' data-toggle="modal" data-target="#m_modal_10"  title="Gerar PDF do Orçamento">
							<i class="la la-print"></i>
						</a>	
					</li>
					<li class="m-portlet__nav-item">						
						<a class="btn btn-outline-primary m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="emitirOrcamento" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>" data-toggle="modal" data-target="#m_modal_7" title="Emitir orçamento ao cliente">
							<i class="la la-send"></i>
						</a>							
					</li>
				</ul>
			</div>
		</div>
		<div class="m-portlet__body">
			<?php if($dados[0]->origem != "Site" && $dados[0]->origem != ""){ ?>
			<div class="row">
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest"> Solicitante: </span>
						 <?php echo $solicitante[0]->nome.' | '.$solicitante[0]->razao_social.' | '.$solicitante[0]->cnpj;?>
					</h5>
				</div>							
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest"> Telefone: </span>
						<?php echo $solicitante[0]->telefone;?>
					</h5>
				</div>
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">E-mail: </span>
						<?php echo $solicitante[0]->email;?>
					</h5>
				</div>
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Local: </span>
						<?php echo $solicitante[0]->local;?>
					</h5>
				</div>
			</div>
			<?php }else{ ?>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Solicitante: </span>
						Orçamento realizado no site 
					</h5>
				</div>							
				
			</div>
			<?php } ?>
			<hr/>
			<div class="row">
				<div class="col-lg-4 col-xs-12"> 
					<h5>
						<span class="m--font-warning m--font-boldest">Empresa</span>						
						<a target="blank" href="<?php echo base_url('AreaAdministrador/visualizarEmpresa/'.$dados[0]->empresa_id); ?>" class="m-link m--font-transform-u">
							<?php echo $dados[0]->cnpj.' | '.$dados[0]->razao_social.' | '.$dados[0]->fantasia;?>
						</a>						
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Telefone: </span>
						<?php echo $dados[0]->telefone;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">E-mail: </span>
						<?php echo $dados[0]->email;?>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Enderço:</span>
						 <?php echo $dados[0]->endereco;?>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Cidade:</span>
						 <?php echo $dados[0]->cidade;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Estado:</span>
						 <?php echo $dados[0]->estado;?>
					</h5>
				</div>							
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-12 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Contato do Posto:</span>
						<?php if($dados[0]->contato_do_posto == "") {?>
							<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="cadastrarContato" orcamento_id="<?php echo  $dados[0]->id_orcamento; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>' data-toggle="modal" data-target="#modal_add_usuario" title="cadastrar contato">
								<i class="la la-user"></i>
							</a>
						<?php }else{ ?>	
							
							<a target="blank" href="<?php echo base_url('AreaAdministrador/editarUsuario/'.$dados[0]->contato_id); ?>" class="m-link m--font-transform-u">
								<?php echo ($dados[0]->contato_do_posto == "") ? $dados[0]->contato_posto : $dados[0]->contato_do_posto; ?>
							</a>
							<input type="hidden" id="contato_posto" name="contato_posto" value="<?php echo ($dados[0]->contato_do_posto == "") ? $dados[0]->contato_posto : $dados[0]->contato_do_posto; ?>">
						<?php } ?> 
					</h5>
				</div>	
			</div>							
			<hr/>
			<div class="form-group m-form__group row">
				<div class="col-lg-12">
					<label><b>Observacao:</b></label>
					<div class="m-input-icon m-input-icon--right">
						<textarea  name="observacao" id="observacao" class="form-control m-input" ><?php echo $dados[0]->observacao; ?></textarea>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>												
				</div>
				
			</div>
		</div>
	</div>
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	***************************************** Forma de entrega, fretes e forma de pagto *******************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">					
					<h2 class="m-portlet__head-label m-portlet__head-label--primary" style="width: 320px;">
						<span>
							Forma de Pagto, Entrega e Frete
						</span>
					</h2>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body">
			<div class="form-group m-form__group row">
				<div class="col-lg-3">	
					<label for="forma_pagto">
						Forma de Pagamento
					</label>
					<select class="form-control m-input m-input--square entregaFretePagto" id="forma_pagto" name="forma_pagto" orcamento_id='<?php echo $dados[0]->id_orcamento; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>
						<option value="0">Selecione a forma de pagamento</option>					
						<?php foreach ($formaPagto as $pagto) { ?>
							<option value="<?php echo $pagto->id;?>" <?php echo ( $dados[0]->forma_pagto_id == $pagto->id  ) ? 'selected="selected"' : '';?>><?php echo $pagto->descricao; ?></option>								
						<?php } ?>
					</select>
				</div>			
				<div class="col-lg-3">	
					<label for="Entrega">
						Entrega
					</label>
					<select class="form-control m-input m-input--square entregaFretePagto" id="entregas" name="entrega" orcamento_id='<?php echo $dados[0]->id_orcamento; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>
						<option value="0">Selecione como vai ser realizado a entrega do(s) produto(s)</option>					
						<?php foreach ($entregas as $entrega) { ?>
							<option value="<?php echo $entrega->id;?>"<?php echo ( $dados[0]->entrega_id == $entrega->id  ) ? 'selected="selected"' : '';?>><?php echo $entrega->descricao; ?></option>								
						<?php } ?>
					</select>
				</div>				
				<div class="col-lg-3">	
					<label for="Frete">
						Frete
					</label>
					<select class="form-control m-input m-input--square entregaFretePagto" id="frete" name="frete" orcamento_id='<?php echo $dados[0]->id_orcamento; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>
						<option value="0">Selecione o tipo de frete utilizado</option>		
					<?php foreach ($fretes as $frete) { ?>
						<option value="<?php echo $frete->id;?>" <?php echo ( $dados[0]->frete_id == $frete->id  ) ? 'selected="selected"' : '';?>><?php echo $frete->descricao; ?></option>								
					<?php } ?>			
					</select>
				</div>
				<div class="col-lg-3">						
					<label>Peso Bruto:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="peso_bruto" id="peso_bruto" class="form-control m-input peso" placeholder="Insira o peso bruto" required value="<?=$dados[0]->peso_bruto?>" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>					
				</div>
			</div>
			<div class="form-group m-form__group row">
				<div class="col-lg-3">
					<label>Peso Líquido:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="peso_liquido" id="peso_liquido" class="form-control m-input peso" placeholder="Insira o peso líquido" required value="<?=$dados[0]->peso_liquido?>" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>
				</div>
				<div class="col-lg-3">
					<label>Iconterms:</label>
					<div class="m-input-icon m-input-icon--left">
						<select name="incoterms" class="form-control">
							<option value="">Selecione o tipo de iconterms</option>
							<option value="EXW" <?php echo ( $dados[0]->incoterms == "EXW" ) ? 'selected="selected"' : '';?> >EXW (Ex Works)</option>
							<option value="FCA" <?php echo ( $dados[0]->incoterms == "FCA" ) ? 'selected="selected"' : '';?> >FCA (Free Carrier)</option>
							<option value="FAS" <?php echo ( $dados[0]->incoterms == "FAS" ) ? 'selected="selected"' : '';?> >FAS (Free Alongside Ship)</option>
							<option value="FOB" <?php echo ( $dados[0]->incoterms == "FOB" ) ? 'selected="selected"' : '';?> >FOB (Free On Board)</option>
							<option value="CFR" <?php echo ( $dados[0]->incoterms == "CFR" ) ? 'selected="selected"' : '';?> >CFR (Cost And Freight)</option>
							<option value="CIF" <?php echo ( $dados[0]->incoterms == "CIF" ) ? 'selected="selected"' : '';?> >CIF (Cost, Insurance And Freight)</option>
							<option value="CPT" <?php echo ( $dados[0]->incoterms == "CPT" ) ? 'selected="selected"' : '';?> >CPT (Carriage Paid To)</option>
							<option value="CIP" <?php echo ( $dados[0]->incoterms == "CIP" ) ? 'selected="selected"' : '';?> >CIP (Carriage And Insurance Paid To)</option>
							<option value="DAT" <?php echo ( $dados[0]->incoterms == "DAT" ) ? 'selected="selected"' : '';?> >DAT (Delivered At Terminal)</option>
							<option value="DAP" <?php echo ( $dados[0]->incoterms == "DAP" ) ? 'selected="selected"' : '';?> >DAP (Delivered at Place)</option>
							<option value="DDP" <?php echo ( $dados[0]->incoterms == "DDP" ) ? 'selected="selected"' : '';?> >DDP (Delivered Duty Paid)</option>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-file-text-o"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Origem:</label>
					<div class="m-input-icon m-input-icon--left">
						<select name="embarque_id" class="form-control" required>
							<option value="">Selecione o porto de origem</option>
							<?php foreach($origens as $org){ ?>
								<option value="<?=$org['id']?>" <?php echo ( $dados[0]->embarque_id == $org['id'] ) ? 'selected="selected"' : '';?> ><?=$org['abreviacao'].'/'.$org['abreviacao'].' - '.$org['descricao']?></option>
							<?php } ?>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-ship"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Destino:</label>
					<div class="m-input-icon m-input-icon--left">
						<select name="destino_id" class="form-control" required>
							<option value="">Selecione o porto de destino</option>
							<?php foreach($destinos as $destino){ ?>
								<option value="<?=$destino['id']?>" <?php echo ( $dados[0]->destino_id == $destino['id'] ) ? 'selected="selected"' : '';?>><?=$destino['abreviacao'].'/'.$destino['abreviacao'].' - '.$destino['descricao']?></option>
							<?php } ?>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-ship"></i></span></span>
					</div>							
				</div>
			</div>
			</div>
		</div>
	</div>	
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	********************************************************* Produtos ************************************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--primary">
						<span>Produtos</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<table class="table m-table m-table--head-separator-primary">
			  	<thead>
			    	<tr>
			      		<th>NCM | Descrição</th>
			      		<th>Quantidade</th>
			      		<th>Valor Unitário R$</th>
			      		<th>Cotações</th>			      			
			    	</tr>
			  	</thead>
			  	<tbody total_indice='<?php echo count($dados); ?>'>
			  		<?php $i=1;$total=0; foreach($dados as $dado){ ?>
			    	<tr indice='<?php echo $i; ?>' class="novo_produto_orc">				      	
				      	<td>
				      		<?php 
                                    $option = "";
                                    $optiongroup = "";
                                    $optiongroup_aux = "";                                    
                                    foreach ($produtos as $produto) {
                                    	$selected = "";
                                    	if( $produto['id'] == $dado->produto_id ) $selected = "selected=selected";

										if($optiongroup == ''){
											$option.='<optgroup label="'.$produto['tipo_produto'].'" data-subtext="optgroup subtext">'.mb_strtoupper($produto['tipo_produto']);
										}elseif($optiongroup_aux != $produto['tipo_produto']){
											$option.='</optiongroup><optgroup label="'.$produto['tipo_produto'].'" data-subtext="optgroup subtext">'.mb_strtoupper($produto['tipo_produto']);
										}
										$option.= '<option value="'.$produto['id'].'" valor_unitario="'.$produto['valor_unitario'].'" '.$selected.' >'.$produto['ncm'].' | '.$produto['descricao'].'</option>';

										$optiongroup_aux = $optiongroup = $produto['tipo_produto'];
									}
									$option.= "</optiongroup>";
                                ?>
                                <select  class="form-control bombas" style="font-size: 15px;" name="produto[]" indice='0' >
                                	<option value="">Selecione um produto</option>
                                    <?=$option?>
                                </select>

				      	</td>
				      	<td>
				      		<input type="text" name="qtd[]" indice='<?php echo $i;?>' class='form-control qtd' orcamento_produto_id="<?php echo $dado->orcamento_produto_id;?>" value='<?php echo $dado->qtd; ?>' style="width: 50px;    float: left;" maxlength="3" id="qtd_<?php echo $i;?>" disabled='disabled' />
				      		<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill editar_qtd" data-toggle="m-tooltip"  data-original-title="Editar Quantidade" style="float: right;margin-top: -5px;"  indice='<?php echo $i;?>'>
								<i class="la la-edit"></i>
							</button>
				      	</td>
				      	<td>
				      		<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill editar_produto" data-toggle="m-tooltip"  data-original-title="Editar valor do produto" style="float: right;"  indice='<?php echo $i;?>'>
								<i class="la la-edit"></i>
							</button>
							<input type="text" name="valor_unitario" id="valor_produto_<?php echo $i;?>" class="valor_produto valor_unitario form-control" value="<?php echo $dado->valor; ?>" required="required" maxlength="14" orcamento_produto_id='<?php echo $dado->orcamento_produto_id; ?>' orcamento_id = "<?php echo $dados[0]->id_orcamento;?>" disabled="disabled" style="width: 71%;text-align: right;padding: 8px;" qtd="<?php echo $dado->qtd; ?>">	
				      	</td>
				      	<td>
                            <a class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill frete_maritimo" data-toggle="m-tooltip" data-placement="top" title="Visualizar Cotações Frete Marítimo" indice="0">
								<i class="fa fa-ship"></i>
							</a>
							<a class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill cotacao_produto" data-toggle="m-tooltip" data-placement="top" title="Visualizar Cotações Produto" indice="0">
								<i class="fa fa-list-alt"></i>
							</a>
						</td>
                        <td>
                            <i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
                        </td>
				      	
			    	</tr>
			    	<?php $i++; $total = ($dado->qtd * $dado->valor) + $total; } ?>
			    	<tr>			    		
			    		<td></td>
						<td></td>
			    		<td><b style="text-align: right;">Total</b></td>			    		
			    		<td style="text-align: right;">R$ <b class="valor_subtotal" style="margin-right: 10px;"><?php echo number_format($total,2,",",".");?> </b>
			    			<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill atualiza_valor" data-toggle="m-tooltip"  data-original-title="Atualizar Valores" style="float: right;margin-top: -5px;"  indice='<?php echo $i;?>'>
								<i class="la la-refresh"></i>
							</button>
							<input type="hidden" name="valor_total" id="valor_total" value="<?php echo $total;?>" />			    			
			    			<input type="hidden" name="orcamento_id" id="orcamento_id" value="<?php echo $dados[0]->id_orcamento; ?>">
			    			<input type="hidden" name="status_id" id="status_id" value="<?php echo $dados[0]->status_orcamento_id; ?>">
			    			<input type="hidden" id="pais_id" value="<?php echo $dados[0]->pais_id; ?>">
			    			  
			    		</td>
			    	</tr>
			    	<!-- CALCULO DE IMPOSTOS QUE FOI RETIRADO A PEDIDO DO JOÃO-->
			    	<!--<tr>
			    		<td></td>
			    		<td></td> 
			    		<td></td>
						<td></td>
			    		<td><b>Total ( Sub-Total + <?php echo $dados[0]->valor_tributo."%";?> de ICMS + 5% de IPI )</b></td>			    		
			    		<td style="text-align: right;">R$ <b class="valor_total" style="margin-right: 15px;">
			    			<?php echo number_format($total+($total/100 * $dados[0]->valor_tributo)+($total/100 * 5),2,",","."); ?> </b>			    			
			    			<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill atualiza_valor" data-toggle="m-tooltip"  data-original-title="Atualizar Valores" style="float: right;margin-top: -5px;"  indice='<?php echo $i;?>'>
								<i class="la la-refresh"></i>
							</button>
			    		</td>
			    	</tr>-->
			    	<?php if(count($desconto) > 0 ){
			    		//calcula o ipi
			    		$valor_ipi		= 	$total+($total/100 * 5);
			    		//adiciona o icms= (PV/((1+IPI))*ICMS)
			    		$valor_icms		= 	$valor_ipi+($valor_ipi/100 * $dados[0]->valor_tributo);			    		
			    		//adiciona a comissão
			    		$valor_total 	= 	$valor_icms+($valor_icms/100 * $desconto[0]['valor_desconto']);

			    	 ?>
			    	
			    	<?php } ?>
			    	<!--<tr>
			    		<td></td>
			    		<td></td>
			    		<td></td>  
			    		<td></td>
			    		<td><b>Valor Final</b></td>			    		
			    		<td>R$ <b class="" style="color: red;"><?php echo number_format($dados[0]->valor_orcamento,2,",",".");?> </b><!--<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_6" title="" orcamento_id="76" data-original-title="Editar o valor final do orçamento">
										<i class="la la-edit"></i>
									</button>
			    		</td>
			    	</tr>-->
			    	<?php if($dados[0]->status_orcamento_id == 12){ ?>
			    	<tr>			    		
			    		<td colspan="4" style="text-align: center;">
			    			<a class="btn btn-outline-success btn-sm m-btn m-btn--icon m-btn--outline-2x" onclick="aprovarOrcamento(<?php echo $dados[0]->orcamento_id;?>)">
								<span>
									<i class="la la-check"></i>
									<span>Aprovar</span>
								</span>
							</a>
			    		</td>
			    	</tr>
			    	<?php } ?>	
			  	</tbody>
			</table>			
		</div>
	</div>
</div>	

<!-- Modal Emitir orçamento -->

<!-- Modal Emitir orçamento -->
<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Emitir orçamento para cliente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">						
					<div class="col-lg-12">
						<label>Enviar para o e-mail:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="email" required="" name="email_destino" id="email_destino" class="form-control m-input" placeholder="" style="text-align: right;"  value="<?php echo $dados[0]->email;?>">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-envelope"></i></span></span>
						</div>							
					</div>
					
				</div>
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="emitir_orcamento" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>Enviar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Desconto -->
<div class="modal fade" id="m_modal_8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Adicionar Desconto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">											
					<div class="col-lg-6">
						<label>Desconto:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required="" name="valor_desconto_orc" id="valor_desconto_orc" value="<?php if( count($desconto) > 0) echo $desconto[0]['valor_desconto']; ?>" class="form-control m-input" placeholder="" style="text-align: right;" maxlength="5">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-percent"></i></span></span>
						</div>							
					</div>
					<div class="col-lg-6">
						<label>Motivo do desconto:</label>
						<div class="m-input-icon m-input-icon--right">							
							<textarea name="motivo_desconto_orc" id="motivo_desconto_orc" class="form-control m-input" disabled="disabled"><?php if( count($desconto) > 0) echo $desconto[0]['motivo_desconto']; ?></textarea>
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-comment"></i></span></span>
						</div>							
					</div>
				</div>							
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="adicionar_desconto_orc" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>">Enviar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Alterar o valor -->
<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Alterar Valor</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">											
					<div class="col-lg-12">
						<label>Novo Valor:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required="" name="valor_orcamento" id="valor_orcamento" value="" class="form-control m-input valor_unitario" placeholder="" style="text-align: right;" maxlength="20" >
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-dollar"></i></span></span>
						</div>
					</div>	
				</div>										
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="alterar_valor" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>Enviar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal inserir representante e contato 
<div class="modal fade" id="m_modal_10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Dados Contato do posto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">																
					<div class="col-lg-12">
						<label>Contato do posto:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required="" name="contato_posto" id="contato_posto" value="<?php echo $dados[0]->contato_posto;?>" class="form-control m-input" placeholder="" style="" maxlength="" >
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-user"></i></span></span>
						</div>
					</div>	
				</div>										
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="geraPdf" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>Enviar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Adiciona produto -->
<div class="modal fade" id="m_modal_9" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Adicionar Produto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 850px;">
				<div class="m-portlet__body">
					<form id="produtos_novos">
					<table class="table m-table m-table--head-separator-warning novo">
						
				  	<thead>
				    	<tr>
				      		
				      		<th>Código | Modelo | Descrição</th>			      		
				      		<th>Quantidade</th>			      		
				      		<th style="text-align: right;">Valor Unitário R$</th>
				      		<th>
				      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar produto">
									<i class="la la-plus"></i>
								</a>
							</th>	
				    	</tr>
				  	</thead>
				  	<tbody >
			  		<?php $i=0;?>
			    	<tr id="modelo" indice='0' total_indice='0'>	
				      	<td>
				      		<?php 
                                    $bombas_op     =   "";
                                    $opcionais_op  =   "";
                                    $bombas_op_slim  =   "";
                                    $bombas_op_baixa  =   "";
                                    $bombas_dispenser 	= 	"";

                                    foreach( $bombas as $bomba )
                                    {
                                        if( $bomba->tipo_produto_id == 4 ){
                                        	$opcionais_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 1){
                                        	
                                            $bombas_op_baixa.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 2){                                        	
                                            $bombas_op_slim.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif ($bomba->tipo_produto_id == 3){
                                        	$bombas_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' >".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 6){
                                                $bombas_dispenser.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'>".$bomba->modelo." | ".$bomba->descricao."</option>";
                                        }  
                                    } 
                            ?>
                            <select  class="form-control bombas_new" style="font-size: 12px;" name="produto_id" indice='0' id="bombas_0">
                            	<option value="">Selecione um produto</option>
                                <optgroup label="Bombas Mangueira Alta" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op; ?>
                                </optgroup>    
                                <optgroup label="Bombas Mangueira Slim" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op_slim; ?>
                                </optgroup>    
                                <optgroup label="Bombas Mangueira Baixa" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op_baixa; ?>
                                </optgroup>
                                <optgroup label="Dispensers" data-subtext="optgroup subtext">
                                    <?php echo $bombas_dispenser; ?>
                                </optgroup>
                                <optgroup label="Opcionais" data-subtext="optgroup subtext">
                                    <?php echo $opcionais_op; ?>
                                </optgroup>
                            </select>

				      	</td>
				      	<td>
				      		<input type="text" name="qtd" indice='0' class='form-control'  maxlength="3" id="qtd_new_0"  />
				      		
				      	</td>
				      	<td>				      		
							<input type="text" name="valor_unitario" id="valor_unitario_0" class="valor_unitario form-control" required="required" maxlength="14" orcamento_id = "<?php echo $dados[0]->id_orcamento;?>" />
				      	</td>
				      	<td>
                        	<i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
                        </td>
			    	</tr>		    	
			    	
			  		</tbody>
					</table>

				</div>										
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="adicionar_produto" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>Enviar</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<div class="modal fade" id="modal_add_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" >Cadastrar Novo Funcionário</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">			

					<div class="row">
						<div class="form-group m-form__group col-lg-6" >
							<label for="Nome">Nome</label>
							<input type="text" class="form-control m-input m-input--air" placeholder="Nome do funcionário" name="nome" id="nome" style="margin-bottom: 10px;float: left;" />
							<input type="hidden" id="empresa_id" value="<?php echo $dados[0]->empresa_id; ?>">

						</div>
						<div class="form-group m-form__group col-lg-6" >
							<label for="CPF">CPF</label>	
							<input type="text"  class="form-control m-input m-input--air" placeholder="CPF do funcionário" name="cpf" id="cpf"  style="margin-bottom: 10px;float: right;" />
						</div>
					</div>					
					<div class="row">
						<div class="form-group m-form__group col-lg-6" >
							<label for="Email">Email</label>
							<input type="email" class="form-control m-input m-input--air" placeholder="E-mail do funcionário" name="email" id="email" style="margin-bottom: 10px;float: left;" />
						</div>
						<div class="form-group m-form__group col-lg-6">
							<label for="Telefone">Telefone/Celular</label>
							<input type="text"  class="form-control m-input m-input--air" placeholder="Telefone do funcionário" name="telefone" id="telefone"  style="margin-bottom: 10px;float: right;" /> 
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="adicionar_funcionario()">Adicionar Funcionário</button>
				</div>
			</div>
		</div>
	</div>

<!-- Modal Reemissao -->
<div class="modal fade" id="modal_reemissao" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Motivo da reemissão do orçamento</h5>
			</div>
			<div class="modal-body" style="width: 750px;">				
				<div class="form-group m-form__group">
					<div class="form-group m-form__group" >
						<label for="exampleSelect1"></label>
						<textarea type="text" class="form-control m-input m-input--air" id="motivo_emissao" placeholder="Motivo"></textarea>
					</div>
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" id ="adicionar_motivo" class="btn btn-primary" status_orcamento="<?php echo $dados[0]->status_orcamento_id; ?>" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>">Adicionar Andamento</button>
			</div>
		</div>
	</div>		
</div>	
<!-- Modal Cotação Frete -->
<div class="modal fade" id="m_lista_frete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">	
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-excluir-title" >Cotações de Frete Marítimo &nbsp;&nbsp;
						<a href="<?php echo base_url('AreaAdministrador/cadastrarCotacaoFreteM')?>" class="" style="color: #56594a; font-weight: bold;" data-toggle="m-tooltip" data-placement="top" title="Adicionar cotação de frete">
							<i class="la la-plus-circle" style="font-size: 38px;"></i>
						</a>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="frete_maritimo" style="width: 1000px;font-size: 12px;">
									
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>				
			</div>
		</div>
	</div>	
</div>	

<!-- Modal Cotação Produto -->
<div class="modal fade" id="m_lista_cotacao_produto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">	
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-excluir-title" >Cotações de Produtos&nbsp;&nbsp;
					<a href="<?php echo base_url('AreaAdministrador/cadastrarCotacaoProduto')?>" class="" style="color: #56594a; font-weight: bold;" data-toggle="m-tooltip" data-placement="top" title="Adicionar cotação de produtos">
						<i class="la la-plus-circle" style="font-size: 38px;"></i>
					</a>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="cotacao_produto" style="width: 1000px;">									
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>				
			</div>
		</div>
	</div>	
</div>
