<?php

class TransitoMovimentacaoModel extends CI_Model {
	
	public function add($data)
	{
		
	    $this->db->insert('transito_movimentacao', $data);
		return $this->db->insert_id();
	}

	public function buscaDadosTransito($cnpj)
	{
		$sql = "SELECT 	id_transito, nr_due, peso_manif
			 	FROM 	transito_simplificado_due,
			            empresas
		       WHERE 	transito_simplificado_due.id_empresa = empresas.id
		         AND 	transito_simplificado_due.fl_aberta = 0
		         AND 	empresas.cnpj = '".$cnpj."' ";

		return $this->db->query($sql)->row_array();
	}

}