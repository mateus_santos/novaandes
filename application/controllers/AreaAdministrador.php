<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Dompdf\Dompdf;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');


class AreaAdministrador extends MY_Controller { 
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() ||  ($this->session->userdata('tipo_acesso') != 'administrador geral' )) {
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');		
		$this->load->model('ProdutosModel', 'produtosM');
		$this->load->model('OrcamentosModel', 'orcamentosM');
		$this->load->model('FretesModel', 'fretesM');
		$this->load->model('EntregasModel', 'entregasM');
		$this->load->model('FormaPagtoModel', 'formaPagtoM');
		$this->load->model('PedidosModel' , 'pedidosM');
		$this->load->model('TipoProdutosModel' , 'tipoProdutosM');
		$this->load->model('ParametrizacoesModel' , 'paramM');		
		$this->load->model('CotacaoFreteMaritimosModel' , 'fretesMM');		
		$this->load->model('PortosModel' , 'portosM');		
		$this->load->model('PaisModel' , 'paisM');
		$this->load->model('CotacaoProdutosModel' , 'cotacaoPM');
		
		$this->load->helper('form');
		$this->load->helper('url');
		
	}
 
	public function index()
	{
		$parametros				=	$this->session->userdata();		
		$parametros['title'] 	=	"Área do Administrador do sistema";		
		$this->_load_view('area-administrador/index',$parametros);

	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){
			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-administrador/editar-cliente',	$parametros );	
			}

		}else{

			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-administrador/editar-cliente',	$parametros );	
		}
	
	}

	public function gestaoUsuarios()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();		
		$parametros['title']	=	"Gestão de Usuários";
		$this->_load_view('area-administrador/gestao-usuario/gestao-usuario',$parametros);
	}

	/****************************************************************************
	**************** Método Ajax - Alterar status do usuario ********************
	*****************************************************************************/
	public function alteraStatus()
	{
		
		$dados = array(	'id'		=>	$_POST['id'],
						'ativo'	=>	$_POST['ativo']	);

		if($this->usuariosM->atualizaStatus($dados)){
			$this->log('Área Administrador | ativa usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $dados,$dados,$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
		
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}
	
	private function enviaEmailConfirmacao($email_destino){

 		$email = '	<html>
						<head></head>
						<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
							<div class="content" style="width: 600px; height: 100%;">
								<img src="http://www.wertco.com.br/bootstrap/img/confirmacaoExpopostos.png" />
							</div>
						</body>
					</html>';	
		
		$this->load->library('email');
		
		$result = $this->email
		    ->from('mateus.santos@gmail.com')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('WERTCO - Confirmação de Acesso')
		    ->message($email)
		    ->send();

		return $result;	

 	}

	/****************************************************************************
	*****************************************************************************
	**************** Método Responsável por Editar Usuários	*********************
	*****************************************************************************
	*****************************************************************************/

	public function editarUsuario($id = null){

		if($this->input->post('salvar') == 1){
			
			$update = $this->input->post();			
			unset($update['salvar']);
			unset($update['senha2']);
			unset($update['senha']);	
			

			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			 
			if($this->usuariosM->atualizaUsuario($update)){				
				$this->log('Área Administrador | atualiza usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['title']	=	"Gestão de Usuários";
				$parametros['dados']	=	$this->usuariosM->getUsuarios();
				$this->_load_view('area-administrador/gestao-usuario/gestao-usuario',	$parametros );
			}
		}else{
			
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($id);
			$parametros['title']				=	"Editar Usuários";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();
			$parametros['subtipos']				= 	$this->usuariosM->buscaSubtipoCadastro();			
			$this->_load_view('area-administrador/gestao-usuario/editar-usuario',	$parametros );
		}
	}

	
    public function gestaoEmpresas($pesquisa = null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getEmpresas();
		$parametros['title']	=	"Gestão de Empresas";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador/empresas/gestao-empresas',$parametros);
	}

	public function visualizarEmpresa($id,$pesquisa=null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getEmpresasUsuarios($id);			
		$parametros['subtipos']	=	$this->empresasM->getSubtipoByTipo($parametros['dados'][0]->tipo_cadastro_id);	
		//$parametros['pedidos'] 	= 	$this->pedidosM->selectPedidosPorEmpresa($id);
		$parametros['anexos'] 	= 	$this->empresasM->selectAnexos($id);	
		$parametros['title']	=	"Empresa #".$id;
		$parametros['pesquisa'] = 	$pesquisa;
		
		$this->_load_view('area-administrador/empresas/visualiza-empresa',$parametros);
	}

	public function anexarDocumentosEmpresa(){
		
		if($_FILES['anexo']['name'] != 'NULL' && $_FILES['anexo']['name'] != '' ){

			$tipo 			= 	explode('.', $_FILES['anexo']['name']);
			$arquivo 		=	md5($_FILES['anexo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao = array(
		        'upload_path'   	=> 	'./empresa_anexos/',		        
		        'allowed_types' 	=> 	'gif|jpg|jpeg|png|pdf|doc|docx|xls|csv|txt|xlsx|csv|vnd.ms-outlook|msg|zip|rar|log|html|htm|mp4|mp3|ogg',
		        'file_name'     	=> 	$arquivo
		    );      
			
		    $this->load->library('upload', $configuracao);
		    
		    if ($this->upload->do_upload('anexo')){

		    	$inserir = array( 	'empresa_id'	=> 	$this->input->post('empresa_id'),
		    						'descricao'		=>	$this->input->post('descricao'),
		    						'anexo'			=> 	$arquivo,
		    						'usuario_id'	=> 	$this->session->userdata('usuario_id')	);
		    
		    	if( $this->empresasM->insereAnexo($inserir) ){		    		
		    		$this->session->set_flashdata('sucesso', 'ok'); 
		    		
		    	}else{
		    		$this->session->set_flashdata('erro', 'erro');
		    	}
		    }
		}

	    redirect('areaAdministrador/visualizarEmpresa/'.$this->input->post('empresa_id'));

	}

	public function excluirAnexoEmpresa(){

		if( $this->empresasM->excluirAnexo( $this->input->post('id') ) ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}

	public function cadastrarEmpresa()
	{
		if($this->input->post()){

			$dadosEmpresa = array(
		        'razao_social' 		=>	$this->input->post('razao_social'),
				'fantasia' 			=>	$this->input->post('fantasia'),
				'cnpj' 				=>	$this->input->post('cnpj'),
				'telefone' 			=>	$this->input->post('telefone'),
				'endereco' 			=>	$this->input->post('endereco'),
				'email' 			=>	$this->input->post('email'),
				'cidade' 			=>	$this->input->post('cidade'),
				'estado' 			=>	$this->input->post('estado'),
				'tipo_cadastro_id'	=> 	$this->input->post('tipo_cadastro_id'),
				'insc_estadual'		=> 	$this->input->post('inscricao_estadual'),
				'pais_id'			=> 	$this->input->post('pais_id'),
				'bairro'			=> 	$this->input->post('bairro'),
				'cep'				=> 	$this->input->post('cep'),
				'cartao_cnpj'		=>	$this->input->post('cartao_cnpj'),
				'fl_internacional'	=> 	($this->input->post('pais_id') != 1) ? 1 : 0
		    );
			
			$conteudo= 	"CNPJ: ". 	$this->input->post('cnpj')." <br/> ";
			$conteudo.= "Razão Social: ". 	$this->input->post('razao_social')." <br/> ";
			$conteudo.=	'Fantasia: '.	$this->input->post('fantasia').' <br/> ';
			$conteudo.=	'CNPJ: '.	$this->input->post('cnpj').' <br/> ';
			$conteudo.=	'Telefone: '.	$this->input->post('telefone').' <br/> ';
			$conteudo.=	'Endereço: '.	$this->input->post('endereco').' <br/> ';
			$conteudo.=	'Cidade:  '	.	$this->input->post('cidade').' <br/> ';
			$conteudo.=	'Estado:  '	.	$this->input->post('estado').' <br/> ';			
			$conteudo.=	'Usuário que cadastrou:	  '.	$this->session->userdata('nome');

			if($this->empresasM->add($dadosEmpresa))
			{
				$this->log('Área Administrador | cadastro empresa','empresas','INSERÇÃO', $this->session->userdata('usuario_id'),$dadosEmpresa,$dadosEmpresa,$_SERVER['REMOTE_ADDR']);
				$this->enviaEmail('mateus.santos@gmail.com','Empresa Cadastrada',$conteudo,$this->input->post('cartao_cnpj'));
				$this->session->set_flashdata('sucesso', 'sucesso');	
				$this->gestaoEmpresas();			
			}else{
				$this->session->set_flashdata('erro', 'erro');
				$this->gestaoEmpresas();
			}					 
			
		}else{


			$parametros 					= 	$this->session->userdata();					
			$parametros['title']			=	"Cadastro de Empresas";			
			$parametros['tipo_cadastros']	=	$this->usuariosM->getTiposCadastro();
			$parametros['pais'] 			=	$this->paisM->select();
			$this->_load_view('area-administrador/empresas/cadastra-empresa',$parametros);
		}
	}	

	public function editarEmpresa($id = null, $pesquisa = null){

		if($this->input->post('salvar') == 1){

			$update = $this->input->post();
			
			unset($update['salvar']);
			unset($update['tipo_acesso_id']);
			$update['tipo_cadastro_id'] = $this->input->post('tipo_acesso_id');
			
			if($this->empresasM->atualizaEmpresas($update)){
				$this->session->set_flashdata('retorno', 'Sucessso');
				$this->log('Área Administrador | editar empresa','empresas','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				
				redirect('/AreaAdministrador/gestaoEmpresas');
			}

		}else{
			$row 								= 	$this->empresasM->getEmpresa($id);		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Empresas";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();			
			$parametros['pesquisa']				= 	$pesquisa;			 
			$this->_load_view('area-administrador/empresas/editar-empresa',	$parametros );	
		}
	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->send();
		}else{
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)
			    ->send();
		}

		return $result;		
	}

	public function ValidaCnpj(){
    	$cnpj 			= 	$this->input->post('cnpj');		
		$consulta_rf 	= 	$this->ConsultaCNPJRF($cnpj);

        if (isset($consulta_rf) && $consulta_rf->{'code'} == '1')
        {
            if ($consulta_rf->{'data'}->{'situacao_cadastral'} == 'ATIVA')
            {
            	$arr['retorno'] 	= 	$consulta_rf->{'data'};
            	$arr['save'] 		= 	$consulta_rf->{'Save_Receita'};
                $arr['result'] 		= 	true;
                $arr['situacao'] 	= 	$consulta_rf->{'data'}->{'situacao_cadastral'};
            	//echo json_encode($consulta_rf);            	
                
            	$arr['result'] = true;
				$arr['erro'] = 0;

            }else {
            	$arr['erro'] 	= 1;
            	$arr['msg'] 	= '* Situação cadastral '.$consulta_rf->{'data'}->{'situacao_cadastral'}.' na Receita Federal.'; 
            }
		}else {

            if (isset($consulta_rf)){
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 2 - '.$consulta_rf->{'code'}.')';
            }else{
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 3)';
            }

        }

		echo json_encode(array(	'receita' 	=> $arr));
	}
	
	public function produtos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->produtosM->select();
		$parametros['title']	=	"Gestão de Produtos";
		$this->_load_view('area-administrador/produtos/produtos',$parametros);
	}

	/****************************************************************************
	************* Método Responsável por cadastrar produtos da wertco ***********
	*****************************************************************************/
	public function cadastraProdutos()
	{
		
		if($this->input->post('salvar') == 1){

			$insert = array('id'				=>	$this->input->post('id'),
							'ncm'				=>	$this->input->post('ncm'),
							'descricao'			=>	$this->input->post('descricao'),
							'valor_unitario'	=>	str_replace(",", ".", str_replace(".","", $this->input->post('valor_unitario'))),
							'tipo_produto_id'	=>	$this->input->post('tipo_produto_id'));
						 
			if($this->produtosM->insereProduto($insert)){
				$this->log('Área Administrador | cadastra produtos','produtos','INSERÇÃO', $this->session->userdata('usuario_id'), $insert,$insert,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				redirect('/AreaAdministrador/produtos');
			}else{
				$this->session->set_flashdata('erro', 'erro');
				redirect('/AreaAdministrador/produtos');
			}

		}else{
			$parametros 						= 	$this->session->userdata();		
			$parametros['dados_tipo_cadastro']	=	$this->produtosM->tipoProdutos();
			$parametros['title']				=	"Gestão de Produtos";
			$this->_load_view('area-administrador/produtos/cadastra-produtos',$parametros);
		}

	}
	/****************************************************************************
	**************** Método Ajax - Excluir Produto ******************************
	*****************************************************************************/
	public function excluirProduto()
	{

		if($this->produtosM->excluirProduto($_POST['id'])){
			$this->log('Área Administrador | excluir produtos','produtos','EXCLUSÃO', $this->session->userdata('usuario_id'), array('id' => $_POST['id']),array('id' => $_POST['id']),$_SERVER['REMOTE_ADDR']);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			
			echo json_encode(array('retorno' => 'erro'));
		}
	
	}
	/****************************************************************************
	************* Método Responsável por editar os produtos da wertco ***********
	*****************************************************************************/
	public function editarProduto($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'ncm'				=>	$this->input->post('ncm'),
							'descricao'			=>	$this->input->post('descricao'),
							'tipo_produto_id'	=>	$this->input->post('tipo_produto_id'),
							'valor_unitario'	=>	(strpos($this->input->post('valor_unitario'),',') !== false ) ? str_replace(",", ".", str_replace(".","", $this->input->post('valor_unitario'))) : $this->input->post('valor_unitario') );
						 
			if($this->produtosM->atualizaProduto($update)){
				$this->log('Área Administrador | atualizar produtos','produtos','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');				
				redirect('/AreaAdministrador/produtos');

			}else{

				$this->session->set_flashdata('erro', 'erro');
				redirect('/AreaAdministrador/produtos');
			}

		}else{

			$row 								= 	$this->produtosM->getProduto($id);
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Produtos";
			$parametros['dados_tipo_produto']	=	$this->produtosM->tipoProdutos();
			$this->_load_view('area-administrador/produtos/editar-produto',	$parametros );
		}
	}

	public function tipoProdutos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->tipoProdutosM->select();
		$parametros['title']	=	"Tipo de Produtos";
		$this->_load_view('area-administrador/tipo-produtos/tipo-produtos',$parametros);
	}

	public function cadastraTipoProdutos()
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados  = array('descricao' => 	$this->input->post('descricao'));

			if( $this->tipoProdutosM->inserir($dados) ){
				$this->log('Área Administrador | insere tipo de produto','tipo_produtos','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			redirect('areaAdministrador/tipoProdutos');

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Tipos de produtos";
			$this->_load_view('area-administrador/tipo-produtos/cadastra-tipo-produto',$parametros);
		}
	
	}

	public function editarTipoProdutos($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'descricao'			=>	$this->input->post('descricao'));
						 
			if($this->tipoProdutosM->atualizar($update)){
				$this->log('Área Administrador | atualizar frete','fretes','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
			}else{
				$this->session->set_flashdata('erro', 'erro');				
			}

			redirect('areaAdministrador/tipoProdutos');
		}else{

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->tipoProdutosM->selectById($id);
			$parametros['title']	=	"Editar Tipo de Produtos";			
			$this->_load_view('area-administrador/tipo-produtos/editar-tipo-produtos',$parametros );
		}
	}

	public function fretes()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->fretesM->selectFretes();
		$parametros['title']	=	"Fretes";
		$this->_load_view('area-administrador/fretes/fretes',$parametros);
	}

	public function cadastraFretes()
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados  = array('descricao' 		=> 	$this->input->post('descricao'));

			if( $this->fretesM->add($dados) ){
				$this->log('Área Administrador | insere frete','fretes','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->fretesM->selectFretes();
			$parametros['title']	=	"Fretes";
			$this->_load_view('area-administrador/fretes/fretes',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Cartão de Técnicos";
			$this->_load_view('area-administrador/fretes/cadastra-fretes',$parametros);
		}
	
	}	
	/****************************************************************************
	************* Método Responsável por editar os Fretes da WERTCO *************
	*****************************************************************************/
	public function editarFretes($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'descricao'			=>	$this->input->post('descricao'));
						 
			if($this->fretesM->atualizaFrete($update)){
				$this->log('Área Administrador | atualizar frete','fretes','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->fretesM->selectFretes();
				$parametros['title']	=	"Fretes";
				$this->_load_view('area-administrador/fretes/fretes',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->fretesM->selectFretes();
				$parametros['title']	=	"Fretes";
				$this->_load_view('area-administrador/fretes/fretes',$parametros);
			}

		}else{

			$row 					= 	$this->fretesM->getFretesById($id);
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['title']	=	"Editar Fretes";			
			$this->_load_view('area-administrador/fretes/editar-fretes',$parametros );
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Fretes ******************************
	*****************************************************************************/
	public function excluirFrete()
	{
																															
		if($this->fretesM->excluirFrete($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));

		}

	}

	
	public function entregas()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->entregasM->selectEntregas();
		$parametros['title']	=	"Fretes";
		$this->_load_view('area-administrador/entregas/entregas',$parametros);
	}

	public function cadastraEntregas()
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados  = array('descricao' 		=> 	$this->input->post('descricao'));

			if( $this->entregasM->add($dados) ){
				$this->log('Área Administrador | cadastra entregas','entregas','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->entregasM->selectEntregas();
			$parametros['title']	=	"Fretes";
			$this->_load_view('area-administrador/entregas/entregas',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Tipo de Entrega da mercadoria";
			$this->_load_view('area-administrador/entregas/cadastra-entregas',$parametros);
		}
	
	}
	
	/****************************************************************************
	************* Método Responsável por editar os tipos de entregas feitas *****
	******************************* pela da WERTCO ******************************
	*****************************************************************************/

	public function editarEntregas($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'descricao'			=>	$this->input->post('descricao'));
						 
			if($this->entregasM->atualizaEntregas($update)){
				$this->log('Área Administrador | editar entregas','entregas','INSERÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);

				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->entregasM->selectEntregas();
				$parametros['title']	=	"Entregas";
				$this->_load_view('area-administrador/entregas/entregas',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->entregasM->selectEntregas();
				$parametros['title']	=	"Fretes";
				$this->_load_view('area-administrador/entregas/entregas',$parametros);
			}

		}else{

			$row 					= 	$this->entregasM->getEntregasById($id);
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['title']	=	"Editar entregas";			
			$this->_load_view('area-administrador/entregas/editar-entregas',$parametros );
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Fretes ******************************
	*****************************************************************************/
	public function excluirEntrega()
	{
																															
		if($this->entregasM->excluirEntrega($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));

		}

	}

	public function formaPagto(){
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']	=	"Formas de pagamento";
		$this->_load_view('area-administrador/forma-pagto/forma-pagto',$parametros);
	}

	public function cadastraFormaPagto()
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados  = array('descricao' => 	$this->input->post('descricao')	);

			if( $this->formaPagtoM->add($dados) ){
				$this->log('Área Administrador | cadastra formaPagtoM','forma_pagto','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->formaPagtoM->selectFormaPagto();
			$parametros['title']	=	"Formas de pagamento";
			$this->_load_view('area-administrador/forma-pagto/forma-pagto',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro Forma de Pagamento";
			$this->_load_view('area-administrador/forma-pagto/cadastra-forma-pagto',$parametros);
		}
	
	}
	
	/****************************************************************************
	************* Método Responsável por editar os tipos de FormaPagto aceitas **
	******************************* pela da WERTCO ******************************
	*****************************************************************************/

	public function editarFormaPagto($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'descricao'			=>	$this->input->post('descricao') );
						 
			if($this->formaPagtoM->atualizaFormaPagto($update)){
				$this->log('Área Administrador | atualiza formaPagtoM','forma_pagto','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->formaPagtoM->selectFormaPagto();
				$parametros['title']	=	"Formas de pagamento";
				$this->_load_view('area-administrador/forma-pagto/forma-pagto',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->formaPagtoM->selectFormaPagto();
				$parametros['title']	=	"Formas de pagamento";
				$this->_load_view('area-administrador/forma-pagto/forma-pagto',$parametros);
			}

		}else{

			$row 					= 	$this->formaPagtoM->getFormaPagtoById($id);
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['title']	=	"Editar forma de pagto";			
			$this->_load_view('area-administrador/forma-pagto/editar-forma-pagto',$parametros );
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Fretes ******************************
	*****************************************************************************/
	public function excluirFormaPagto()
	{
																															
		if($this->formaPagtoM->excluirFormaPagto($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));
			
		}

	}

	public function orcamentos($pesquisa = null)
	{
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'administrador geral' ) {

			redirect(base_url('usuarios/login'));			
		}

		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Proforma";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) 	? 	str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$parametros['status'] 	=	$this->orcamentosM->selectStatus();
		$this->_load_view('area-administrador/orcamentos/orcamentos',$parametros);
	}

	public function orcamentosExpirados($pesquisa = null)
	{
		$parametros 			= 	$this->session->userdata();		
		$parametros['title']	=	"Proforma";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$parametros['zonas']			= 	$this->zonaM->select();
		$parametros['representantes']	=	$this->usuariosM->retornaRepresentantesAtivos();
		$parametros['status']			=	$this->orcamentosM->selectStatus();
		$this->_load_view('area-administrador/orcamentos/orcamentos-expirados',$parametros);
	}
	
	public function buscaOrcamentosSS(){

		$dados 		=	$this->input->post();
		$retorno 	=	$this->orcamentosM->buscaOrcamentos($dados);
		echo json_encode($retorno);
	
	}

	public function buscaOrcamentosSSExpirados(){
		$dados 	 = $this->input->post();
		$retorno =	$this->orcamentosM->buscaOrcamentosExpirados($dados);
		echo json_encode($retorno);
	}

	public function buscaProximosStatus()
	{
		$retorno = $this->orcamentosM->buscaProximosStatusOrcamento($_POST['orcamento_id']);			
		echo json_encode($retorno);
		
	}

	public function buscaAndamentos()
	{
		$retorno = $this->orcamentosM->buscaAndamentos($_POST['orcamento_id']);
			
		echo json_encode($retorno);
		
	}
	
	public function alteraStatusOrcamento()
	{

		if( $_POST['indicacao'] == 1  ){
			if($_POST['aprovar_indicacao'] == 0){
				$_POST['status_orcamento'] = 8;
			}
		}

		$dados = array('id'						=>	$_POST['orcamento_id'],
					   'status_orcamento_id'	=>	$_POST['status_orcamento']);

		if($this->orcamentosM->atualizaStatusOrcamento($dados))
		{
			$this->log('Área Administrador | atualizar status orçamento','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);			
				
			switch($_POST['status_orcamento']){
				case 1:
			        $andamento = "Proforma Aberta!";
			        break;
			    case 2:
			        $andamento = "Proforma Fechada!";
			        break;
			    case 3:
			        $andamento = "Proforma Perdida.";
			        break;
			    case 4:
			        $andamento = "Proforma Cancelada.";
			        break;
			    case 5:
			        $andamento = "Proforma entregue ao cliente.";
			        break;        
			    case 6:
			        $andamento = "Proforma em negociação.";
			        break;			    
			    case 14:
			    	$andamento = "Proforma Sem Retorno.";
			        break;			
			}

			$inserirAndamento = array(	'orcamento_id'			=>	$_POST['orcamento_id'],
										'status_orcamento_id'	=> 	$_POST['status_orcamento'],
										'andamento'				=>	$andamento,
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id')	);

			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
			{
				
				echo json_encode(array('retorno' => 'sucesso'));	
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}
	
	public function alteraIndicadorOrcamento(){

		$update = array('id' 			=> 	$_POST['orcamento_id'], 
						'indicador_id'	=>	$_POST['indicador'],
						'indicador'		=> 	$_POST['indicador'] );

		$indicador = $this->orcamentosM->getIndicador($_POST['orcamento_id']);

		if( $_POST['indicador'] == '' && isset($indicador[0]->id) ){
			$andamento 				= 'Exclusão do indicador #'.$indicador[0]->id.' ao orçamento.';
			

		}else{
			$andamento = (isset($indicador[0]->id)) ? 'Alteração de Indicador: #'.$indicador[0]->id.' => #'.$_POST['indicador'] : 'Indicador #'.$_POST['indicador'].'  	vinculado ao orçamento.'; 
		}

		$inserirAndamento = array(	'orcamento_id'			=>	$_POST['orcamento_id'],
									'status_orcamento_id'	=> 	(isset($indicador[0]->id)) ? $indicador[0]->status_orcamento_id : $_POST['status_orcamento_id'],
									'andamento'				=>	$andamento,
									'dthr_andamento'		=>	date('Y-m-d H:i:s'),
									'usuario_id'			=> 	$this->session->userdata('usuario_id') 	);

		if($this->orcamentosM->atualizaIndicador($update)){
			$this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{

			echo json_encode(array('retorno' => 'erro'));	
		}
	}

	public function insereAndamentoOrcamento(){

		$inserirAndamento = array(	'orcamento_id'			=>	$_POST['orcamento_id'],
									'status_orcamento_id'	=> 	$_POST['status_orcamento_id'],
									'andamento'				=>	$_POST['andamento'],
									'dthr_andamento'		=>	date('Y-m-d H:i:s'),
									'usuario_id'			=> 	$this->session->userdata('usuario_id')	);

		$last_id = $this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento);

		if($last_id){
			//Insere alerta orçamento!			
			if(	$_POST['dthr_alerta']	!=	''	){
				$data_alerta 	= 	explode('*', $_POST['dthr_alerta']);
				
				$insereAlerta 	= 	array(	'orcamento_andamento_id'	=>	$last_id,
											'dthr_alerta'				=>	$data_alerta[0].'-'.$data_alerta[1].'-'.$data_alerta[2],
											'usuario_id'				=> 	$this->session->userdata('usuario_id')	);		

				$this->orcamentosM->insereAlerta($insereAlerta);

			}

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));	

		}

	}

	public function retornaResponsavelOrcamentos()
	{
	
		$rows = $this->usuariosM->retornaIndicadores($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		 =>		$row['label'],
									'id'	 	 =>		$row['id'],
									'value'		 =>		$row['label'],
									'empresa_id' => 	$row['empresa_id']	);
				
			}

			echo json_encode($dados);
		}
	}

	public function alteraResponsavelOrcamento()
	{
		// SE O ID DA TABELA VIER EM BRANCO, INSERE NOVO REPREENTANTE, SE NÃO ATUALIZA O REPRESENTANTE RESPONSÁVEL
		if( $_POST['orcamento_responsavel_id'] == '' )
		{
			$insereResponsavel =  array('orcamento_id' 	=> 	$_POST['orcamento_id'],
										'usuario_id'	=> 	$_POST['responsavel_id']	);

			if($this->orcamentosM->insereResponsavelOrcamento($insereResponsavel)){

				$this->log('Área Administrador | responsavel_orcamento','orcamentos','INSERÇÃO', $this->session->userdata('usuario_id'), $insereResponsavel,$insereResponsavel,$_SERVER['REMOTE_ADDR']);
				$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													  	'andamento'				=> 	'Orçamento enviado para o representante responsável',
													  	'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													  	'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													  	'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);

				if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
				{

					echo json_encode(array('retorno' => 'sucesso'));	
				}else{

					echo json_encode(array('retorno' => 'erro'));	
				}			
			}else{

				echo json_encode(array('retorno' => 'erro'));	
			}

		}else{
			$atualizaResponsavel 	= 	array(	'id'			=>	$_POST['orcamento_responsavel_id'],
					   	   						'usuario_id' 	=>	$_POST['responsavel_id']);

			if( $this->orcamentosM->atualizaResponsavelOrcamento($atualizaResponsavel) )
			{
				$this->log('Área Administrador | responsavel_orcamento','orcamentos','EDIÇÂO', $this->session->userdata('usuario_id'), $atualizaResponsavel,$atualizaResponsavel,$_SERVER['REMOTE_ADDR']);
				$insereAndamentoOrcamento 	= 	array('orcamento_id' 		=> 	$_POST['orcamento_id'],
													  'andamento'			=> 	'Alterado o indicador responsável pelo orçamento.',
													  'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													  'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													  'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
				
				if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
				{

					echo json_encode(array('retorno' => 'sucesso'));	
				}else{

					echo json_encode(array('retorno' => 'erro'));	
				}
			}else{

				echo json_encode(array('retorno' => 'erro'));	
			}
		}
	}

	public function visualizarOrcamento($id,$pesquisa = null)
	{
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'administrador geral' ) {
			redirect(base_url('usuarios/login'));
		}

		$parametros 				= 	$this->session->userdata();
		$parametros['dados']		=	$this->orcamentosM->getOrcamentoProdutos($id);
		$parametros['desconto']		=	$this->orcamentosM->getOrcamentoDesconto($id);
		$parametros['solicitante']	=	$this->orcamentosM->getSolicitante($id);								
		$parametros['contato_posto']= 	$this->orcamentosM->selectContatoPosto($id);
		$parametros['produtos'] 	= 	$this->produtosM->select();
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['pais']			=	$this->paisM->select();
		$parametros['origens'] 		=	$this->fretesMM->buscaPortos(0);
		$parametros['destinos'] 	=	$this->fretesMM->buscaPortos(1);
		$parametros['title']		=	"Orçamento #".$id;
		$parametros['pesquisa']		= 	htmlentities($pesquisa);
		$this->_load_view('area-administrador/orcamentos/visualiza-orcamento',$parametros);
	}

	public function emitirOrcamento()
	{
		if( $_POST['salvar_desconto'] == "1")
		{
			$dados_desconto = array('orcamento_id' 		=> 	$_POST['orcamento_id'],
									'valor_desconto'	=>	$_POST['valor_desconto'],
									'motivo_desconto'	=> 	$_POST['motivo_desconto'] );

			if( $this->orcamentosM->insereOrcamentosDesconto( $dados_desconto ) )
			{

				
				$status_orcamento_id = $_POST['status_orcamento_id'];
				

				//altera o status para emitido	
				$dados = array('id'						=>	$_POST['orcamento_id'],
					   			'status_orcamento_id'	=>	$status_orcamento_id);

				if($this->orcamentosM->atualizaStatusOrcamento($dados)){

					//orçamento entregue/emitido
					$andamentoOrcamento = array('andamento' 			=> 'Orçamento emitido ao cliente por '.$this->session->userdata('nome').' da WERTCO!',
			 									'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 									'orcamento_id'			=> $_POST['orcamento_id'],
			 									'status_orcamento_id'	=> $status_orcamento_id,
			 									'usuario_id'			=> $this->session->userdata('usuario_id')
			 									);	
		 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento)){

						$retorno = $this->enviaEmailEmissaoCliente($_POST['email_destino'],$_POST['orcamento_id'], $_POST['valor_tributo'], $_POST['valor_total'],$_POST['contato_posto']);
						if($retorno){
							$this->enviaEmailEmissaoCliente('mateus.santos@gmail.com',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');
							echo json_encode(array('retorno' => 'sucesso'));
						}else{
							echo json_encode(array('retorno' => 'erro'));	
						}
					}else{
						echo json_encode(array('retorno' => 'erro'));	
					}
				}else{
					echo json_encode(array('retorno' => 'erro'));	
				}	
			}else{
				echo json_encode(array('retorno' => 'erro'));
			}
		}else{			
			
			$status_orcamento_id = $_POST['status_orcamento_id'];
			
			//altera o status para emitido	
			$dados = array('id'						=>	$_POST['orcamento_id'],
				   		   'status_orcamento_id'	=>	$status_orcamento_id);

			if($this->orcamentosM->atualizaStatusOrcamento($dados)){

				//orçamento entregue/emitido
				$andamentoOrcamento = array('andamento' 			=> 'Orçamento emitido ao cliente por '.$this->session->userdata('nome').' da WERTCO!',
		 									'dthr_andamento'		=> date('Y-m-d H:i:s'),
		 									'orcamento_id'			=> $_POST['orcamento_id'],
		 									'status_orcamento_id'	=> $status_orcamento_id,
		 									'usuario_id'			=> $this->session->userdata('usuario_id') );	
	 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento)){

					$retorno = $this->enviaEmailEmissaoCliente($_POST['email_destino'],$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total']);
					if($retorno){						
						$this->enviaEmailEmissaoCliente('mateus.santos@gmail.com',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total']);
						echo json_encode(array('retorno' => 'sucesso'));
					}else{
						echo json_encode(array('retorno' => 'erro'));	
					}
				}else{
					echo json_encode(array('retorno' => 'erro'));	
				}
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}	
		}
	}

	private function enviaEmailEmissaoCliente( $email_destino, $orcamento_id, $valor_tributo, $valor_total, $contato_posto = "" )
	{
		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);
		
		$total_desconto 	= 	"";
		
		$anexo = $this->geraOrcamento($orcamento_id,'email',$contato_posto);

		$email = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>tst</title><style type="text/css">
        /* Resets */
        .ReadMsgBody { width: 100%; background-color: #ebebeb;}
        .ExternalClass {width: 100%; background-color: #ebebeb;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }        
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {margin:0; padding:0;}
        .yshortcuts a {border-bottom: none !important;}
        .rnb-del-min-width{ min-width: 0 !important; }

        /* Add new outlook css start */
        .templateContainer{
            max-width:590px !important;
            width:auto !important;
        }
        /* Add new outlook css end */

        /* Image width by default for 3 columns */
        img[class="rnb-col-3-img"] {
        max-width:170px;
        }

        /* Image width by default for 2 columns */
        img[class="rnb-col-2-img"] {
        max-width:264px;
        }

        /* Image width by default for 2 columns aside small size */
        img[class="rnb-col-2-img-side-xs"] {
        max-width:180px;
        }

        /* Image width by default for 2 columns aside big size */
        img[class="rnb-col-2-img-side-xl"] {
        max-width:350px;
        }

        /* Image width by default for 1 column */
        img[class="rnb-col-1-img"] {
        max-width:550px;
        }

        /* Image width by default for header */
        img[class="rnb-header-img"] {
        max-width:590px;
        }

        /* Ckeditor line-height spacing */
        .rnb-force-col p, ul, ol{margin:0px!important;}
        .rnb-del-min-width p, ul, ol{margin:0px!important;}

        /* tmpl-2 preview */
        .rnb-tmpl-width{ width:100%!important;}

        /* tmpl-11 preview */
        .rnb-social-width{padding-right:15px!important;}

        /* tmpl-11 preview */
        .rnb-social-align{float:right!important;}

        @media only screen and (min-width:590px){
        /* mac fix width */
        .templateContainer{width:590px !important;}
        }

        @media screen and (max-width: 360px){
        /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
        .rnb-yahoo-width{ width:360px !important;}
        }

        @media screen and (max-width: 380px){
        /* fix width and font size "tmpl-4 tmpl-6" in mobile preview */
        .element-img-text{ font-size:24px !important;}
        .element-img-text2{ width:230px !important;}
        .content-img-text-tmpl-6{ font-size:24px !important;}
        .content-img-text2-tmpl-6{ width:220px !important;}
        }

        @media screen and (max-width: 480px) {
        td[class="rnb-container-padding"] {
        padding-left: 10px !important;
        padding-right: 10px !important;
        }

        /* force container nav to (horizontal) blocks */
        td.rnb-force-nav {
        display: inherit;
        }
        }

        @media only screen and (max-width: 600px) {

        /* center the address &amp; social icons */
        .rnb-text-center {text-align:center !important;}

        /* force container columns to (horizontal) blocks */
        td.rnb-force-col {
        display: block;
        padding-right: 0 !important;
        padding-left: 0 !important;
        width:100%;
        }

        table.rnb-container {
         width: 100% !important;
        }

        table.rnb-btn-col-content {
        width: 100% !important;
        }
        table.rnb-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-last-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table[class~="rnb-col-2"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-col-2-noborder-onright {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        }

        table.rnb-col-2-noborder-onleft {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-top: 10px;
        padding-top: 10px;
        }

        table.rnb-last-col-2 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table.rnb-col-1 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        img.rnb-col-3-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xs {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xl {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-1-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-header-img {
        /**max-width:none !important;**/
        width:100% !important;
        margin:0 auto;
        }

        img.rnb-logo-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        td.rnb-mbl-float-none {
        float:inherit !important;
        }

        .img-block-center{text-align:center !important;}

        .logo-img-center
        {
            float:inherit !important;
        }

        /* tmpl-11 preview */
        .rnb-social-align{margin:0 auto !important; float:inherit !important;}

        /* tmpl-11 preview */
        .rnb-social-center{display:inline-block;}

        /* tmpl-11 preview */
        .social-text-spacing{margin-bottom:0px !important; padding-bottom:0px !important;}

        /* tmpl-11 preview */
        .social-text-spacing2{padding-top:15px !important;}

    }</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>

<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#f9fafc" style="background-color: rgb(249, 250, 252);">

    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
    <tr>
        <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_5" id="Layout_5">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:600px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/backgroundCabecalho.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_6">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div>&nbsp;</div>

<div>
<table style="color: rgb(0, 0, 0); font-family: sans-serif; font-style: normal; padding-top: 20px; font-size: 16px; margin-left: 15px; width: 100%;">
   <tbody>
                    <tr>
                        <td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
                        <td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
                        <td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
                        <td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
                        <td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
                        <td style="padding-bottom: 18px;font-weight: 700;"></td>
                        <td style="padding-bottom: 18px;"></td>                        
                    </tr>
                    <tr>
                        <td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
                        <td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
                        <td style=" font-weight: 700; text-align: right;">Telefone:</td>
                        <td>'.$empresa['telefone'].'</td>
                    </tr>
                    <tr>
                        <td style=" font-weight: 700;">E-mail:</td>
                        <td colspan="3">'.$empresa['email'].'</td>
                    </tr>
                </tbody>
</table>
</div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_7" id="Layout_7">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffcc00" align="center" cellspacing="0" style="background-color: rgb(255, 204, 0);">
                            <tbody><tr>
                                <td valign="top" align="left">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:139;;max-width:139px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="139" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/produtos.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_8">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><table cellspacing="0" style="width: 500px;margin-left: 14px">
    <thead style="height: 55px">
        <tr>
            <th style="height: 30px; text-align: center;">Modelo</th>
            <th style="text-align: center;">Descrição</th>
            <th>Qtd</th>
            <th style="text-align: right;">Valor</th>
        </tr>
    </thead>
    <tbody style="background-color: #fff;margin-left: 10px;font-size: 12px">';
       $total = 0; foreach($produtos as $produto){
            
            $email.='   <tr>
                        <td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
                        <td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
                        <td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>
                        <td style="   text-align: right; padding: 10px;">R$ '.number_format($produto['valor'], 2, ',', '.').'</td>
                    </tr>';                 
            $total = $total + ($produto['valor'] * $produto['qtd']);
        }   
        $email.='   <tr>
                        <td colspan="3" style="border-top: 1px solid #f0f0f0; border-right: 1px solid #f0f0f0;    padding: 10px;text-align: right;">
                        <b>Total (Sub-total +'.$valor_tributo.'% de ICMS + 5% de IPI)</b></td>
                        <td style="border-top: 1px solid #f0f0f0; text-align: right; padding: 10px;"><b>R$ '.number_format($total, 2, ',', '.').'</b></td>
                    </tr>';
        /*          <tr>
                        <td colspan="3" style=" border-top: 1px solid #f0f0f0;border-right: 1px solid #f0f0f0;    padding: 10px;text-align: right;"><b>Total (Sub-Total + '.$valor_tributo.'% de ICMS + 5% de IPI)</b></td>
                        <td style="border-top: 1px solid #f0f0f0; padding: 10px; text-align: right;"><b>R$ '.number_format($valor_total, 2, ',', '.').'</b></td>
                    </tr>';*/
        $email.='      
    </tbody>
</table>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_10" id="Layout_10">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffcc00" align="center" cellspacing="0" style="background-color: rgb(255, 204, 0);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:5px; width:542;;max-width:542px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="542" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 5px; " src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_4" id="Layout_4">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="590" class="rnb-container rnb-yahoo-width" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#f9fafc" style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                    <div>© 2018 wertco</div>
                                </td></tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        </td>
        </tr>
        </tbody></table>

</body></html>';	
		
		$this->load->library('email');
		$result = $this->email
		    ->from('mateus.santos@gmail.com')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('Orçamento Wertco')
		    ->message($email)
		    ->attach('./pdf/'.$anexo)
		    ->send();

		return $result;		
	
	}

	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraOrcamento($orcamento_id, $origem_solicitacao,$contato)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-'.$orcamento_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $dados				=	$this->orcamentosM->getOrcamentoProdutos($orcamento_id);
	    $total_descontos 	= 	$this->orcamentosM->getOrcamentoDesconto($orcamento_id);
	    $responsavel 		= 	$this->orcamentosM->getResponsavel($orcamento_id);
	    $orcamento 			= 	$this->orcamentosM->getFreteEntregaPagto($orcamento_id);	    
	    $data['frete']		= 	(is_object($orcamento)) ? $orcamento->frete : ' ';
	    $data['entrega']	= 	(is_object($orcamento)) ? $orcamento->entrega : ' ';
	    $data['formaPagto']	= 	(is_object($orcamento)) ? $orcamento->forma_pagto : ' ';
	    $data['emissao']	= 	$this->orcamentosM->retornaDiferencaEmissao($orcamento_id);
	    $data['primeira_emissao']	= 	$this->orcamentosM->primeiraEmissao($orcamento_id);
	    
	    $data['empresa'] = array(	'razao_social' 			=> 	$dados[0]->razao_social,
    								'fantasia' 				=> 	$dados[0]->fantasia,
									'cnpj' 					=> 	$dados[0]->cnpj,
									'telefone'				=> 	$dados[0]->telefone,
									'email'					=> 	$dados[0]->email,
									'endereco'				=> 	$dados[0]->endereco,
									'cidade'				=> 	$dados[0]->cidade,		
									'pais'					=> 	$dados[0]->pais,
									'estado'				=> 	$dados[0]->estado,
									'orcamento_id'			=> 	$dados[0]->orcamento_id,
									'emissao'				=> 	$dados[0]->emissao,
									'valor_tributo'			=>	$dados[0]->valor_tributo,
									'representante_legal' 	=> 	$dados[0]->representante_legal,
									'inscricao_estadual' 	=> 	$dados[0]->insc_estadual,
									'bairro'				=> 	$dados[0]->bairro,
									'cep'					=> 	$dados[0]->cep,
									'cel_contato_posto'		=> 	$dados[0]->cel_contato_posto,
									'observacao'			=> 	$dados[0]->observacao,									
									'fl_especial' 			=> 	$dados[0]->fl_especial,
									'celular'				=> 	$dados[0]->celular,
									'tp_cadastro'			=> 	$dados[0]->tp_cadastro 	);

	    foreach ($dados as $dado) {
	    	$data['produtos'][] = array('codigo' 			=> 	$dado->codigo,
	    								'descricao'			=>	$dado->descricao,
										'modelo'			=>	$dado->modelo,
	    								'qtd'				=>	$dado->qtd,
	    								'valor_unitario' 	=>	$dado->valor_unitario,
	    								'valor_orcamento'	=> 	$dado->valor_orcamento,
	    								'valor_produto' 	=> 	$dado->valor);
	    }		
	    
	    $data['info']	= 	array(	'nome' 			=> 	$this->session->userdata('nome'),
	    						 	'email'			=> 	$this->session->userdata('email'),
	    						 	'telefone'		=>	$this->session->userdata('telefone_usuario'),	    						 	
	    						 	'celular'		=>	$this->session->userdata('celular_usuario'),
	    						 	'contato'		=> 	( isset($_POST['contato'])) ? $_POST['contato'] : $contato 	);

	    $data['representante'] 	= 	array(	'responsavel' 	=>  $responsavel->responsavel,
								  			'empresa' 		=>	$responsavel->razao_social	);	    
	    foreach( $total_descontos as $total_desconto ){
	    	$data['desconto'] = array('valor_desconto' => $total_desconto['valor_desconto'] );
	    }

	    if(!isset($data['desconto'])){ 

	    	$data['desconto'] = array('valor_desconto' => '' );	    		    	
	    }

	    $data['indicador'] = array(	'indicador'	=>	(count($indicador) > 0) ? $indicador[0]->indicador : '');	    

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-administrador/orcamentos/orcamento-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'orcamento-'.$orcamento_id.'.pdf'));
	    	}else{
	    		return $retorno['retorno'] = 'orcamento-'.$orcamento_id.'.pdf';
	    	}
	    }	    
	    
    }

    public function cadastraOrcamentos($pesquisa = null)
	{
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'administrador geral' ) {
			redirect(base_url('usuarios/login'));
		}

		$parametros 				= 	$this->session->userdata();
		$parametros['dados']		=	$this->orcamentosM->buscaOrcamentosIndicadores($parametros['usuario_id']);
		$parametros['produtos'] 	= 	$this->produtosM->select();
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['pais']			=	$this->paisM->select();
		$parametros['origens'] 		=	$this->fretesMM->buscaPortos(0);
		$parametros['destinos'] 	=	$this->fretesMM->buscaPortos(1);
		$parametros['title']		=	"Proforma";
		$parametros['pesquisa']		= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador/orcamentos/cadastra-orcamento',$parametros);
	}

	/* FUNÇÃO RESPONSÁVEL POR CADASTRAR Proforma 	*/
	public function orcamentosAdm()
	{
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'administrador geral' ) {
			redirect(base_url('usuarios/login'));
		}

		if( $this->input->post() )
		{
			
			if( $this->input->post('salvar') == '1')
			{
				$dadosEmpresa = array(
			        'razao_social' 		=>	$this->input->post('razao_social'),
					'fantasia' 			=>	$this->input->post('fantasia'),
					'cnpj' 				=>	$this->input->post('cnpj'),
					'telefone' 			=>	$this->input->post('telefone'),
					'endereco' 			=>	$this->input->post('endereco'),
					'email' 			=>	$this->input->post('email'),
					'cidade' 			=>	$this->input->post('cidade'),
					'estado' 			=>	$this->input->post('estado'),
					'tipo_cadastro_id'	=> 	1,
					'insc_estadual'		=> 	'',
					'pais_id'			=> 	$this->input->post('pais_id'),
					'bairro'			=> 	$this->input->post('bairro'),
					'cep'				=> 	$this->input->post('cep'),
					'cartao_cnpj'		=>	$this->input->post('cartao_cnpj'),
					'fl_internacional'	=> 	1
			    );

				if($this->empresasM->add($dadosEmpresa))
				{
					$empresa_id = $this->db->insert_id();										
				}
	 		}else{
	 			$empresa_id = $this->input->post('empresa_id');

	 		}

	 		$dadosOrcamento = array( 	'empresa_id' 			=> 	$empresa_id,
		 								'status_orcamento_id'	=> 	1,
		 								'origem'				=> 	'Administrador',
		 								'solicitante_id'		=> 	$this->session->userdata('usuario_id'),
		 								'usuario_id'			=> 	$this->session->userdata('usuario_id'),
		 								'frete_id'				=> 	$this->input->post('frete'),
		 								'entrega_id'			=> 	$this->input->post('entrega'),
		 								'forma_pagto_id'		=> 	$this->input->post('forma_pagto'),		 								
		 								'contato_posto'			=> 	$this->input->post('contato_posto'),
	 									'cel_contato_posto'		=> 	$this->input->post('tel_contato_posto'),
	 									'observacao'			=> 	$this->input->post('observacao'),
	 									'peso_bruto' 			=> 	$this->input->post('peso_bruto'),
	 									'peso_liquido' 			=> 	$this->input->post('peso_liquido'),
	 									'embarque_id' 			=> 	$this->input->post('embarque_id'),
	 									'destino_id' 			=> 	$this->input->post('destino_id'),
	 									'incoterms'				=> 	$this->input->post('incoterms') );

	 		if( $this->orcamentosM->insereOrcamento($dadosOrcamento) )
	 		{
	 			$orcamento_id  = $this->db->insert_id();
	 			$this->log('Área Administrador | nova proforma','orcamentos','INSERÇÃO', $this->session->userdata('usuario_id'), $dadosOrcamento,$dadosOrcamento,$_SERVER['REMOTE_ADDR']);
	 			$erro = 0;

	 			$dadosContato = array(	'empresa_id' 		=> 	$empresa_id,
	 									'nome'				=> 	$this->input->post('contato_posto'),
	 									'email'				=> 	$this->input->post('email_contato_posto'),
	 									'celular'			=> 	$this->input->post('tel_contato_posto'),
	 									'tipo_cadastro_id'	=> 	1 	);

	 			if( $this->usuariosM->add($dadosContato) )
	 			{

	 				$contato_id = $this->db->insert_id();

	 				$updateContato = array(	'id' 			=> 	$orcamento_id,
	 										'contato_id'	=> 	$contato_id 	);

	 				$this->orcamentosM->atualizaOrcamento($updateContato);

		 			$andamentoOrcamento = array(	'andamento' 			=> 'Proforma criada por  pelo '.$this->session->userdata('nome'),
			 										'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 										'orcamento_id'			=> $orcamento_id,
			 										'status_orcamento_id'	=> 1,
			 										'usuario_id'			=> $this->session->userdata('usuario_id') 	);	

		 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento))
		 			{
		 				
		 				$orcamentoResp = array(	'orcamento_id' 	=> 	$orcamento_id,
			 									'usuario_id' 	=> 	$this->session->userdata('usuario_id') );
	 					if($this->orcamentosM->insereResponsavelOrcamento($orcamentoResp))
	 					{
	 						
	 					
				 			for ($i=0; $i < count($this->input->post('produto')) ; $i++) 	 			  
				 			{
				 				$orcamentoProdutos = array(	'orcamento_id' 	=>	$orcamento_id,
				 											'produto_id'  	=> 	$this->input->post('produto')[$i],
				 											'qtd'			=>	$this->input->post('quantidade')[$i],
				 											'valor'			=> 	str_replace(',','.', str_replace('.','',$this->input->post('valor')[$i]))	);	

				 				if(!$this->orcamentosM->insereOrcamentoProdutos($orcamentoProdutos))
				 				{
									$erro++;
								}		 					 			
				 			}
				 		}else{
							$erro++;
						}	 					

					}else{
						$erro++;
					}
				}else{
						$erro++;
				}
			}else{
				$erro++;
			}

	 		if( $erro == 0){
	 				
	 			if($this->enviarEmailOrcamento($orcamento_id))
 				{
 				
 					$this->session->set_flashdata('sucesso', 'ok.');
 				}else{
 					
 					$this->session->set_flashdata('erro', 'erro.');	
 				}

 			}else{
 				$this->session->set_flashdata('erro', 'erro.');
 			}
			
			$this->orcamentos();
		}					

	}

	public function insereDescontoOrcamento()
	{
		$dados_desconto = array('orcamento_id' 		=> 	$_POST['orcamento_id'],
								'valor_desconto'	=>	str_replace(',', '.', $_POST['valor_desconto']),
								'motivo_desconto'	=> 	$_POST['motivo_desconto'] );

		if( $this->orcamentosM->insereOrcamentosDesconto( $dados_desconto ) )
		{
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}		

	public function alterarValorProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['orcamento_produto_id'],
							 	'valor'	=>	str_replace(',','.', str_replace('.','',$_POST['valor_produto']))	);
		
		if($this->orcamentosM->atualizaValorProduto($dadosUpdate)){
			$this->log('Área Administrador | atualiza valor orçamento','orcamento_produtos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Valor de um produto foi alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{					

				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}
		}else{
			echo json_encode(array('retorno' => 'erro' ));		
		}

	}

	public function alterarProdutoOrcamento(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['orcamento_produto_id'],
							 	'produto_id'	=>	$_POST['produto_id']	);

		if($this->orcamentosM->alterarProdutoOrcamento($dadosUpdate)){
			$this->log('Área Administrador | atualiza produto','orcamento_produtos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Produto #'.$_POST['produto_nr'].' alterado para ' .$_POST['produto'],
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function insereContatoOrcamento(){
		$update = array(	'id' 			=> 	$_POST['orcamento_id'],
							'contato_id' 	=> 	$_POST['contato_id'] 	);	
		
		if($this->orcamentosM->atualizaOrcamento($update)){
						$this->log('Área Administrador | atualiza contato orcamento','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso' ));						
		}else{
			echo json_encode(array('retorno' => 'erro' ));
		}					

	}

	public function alterarQtdProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['orcamento_produto_id'],
							 	'qtd'	=>	$_POST['qtd']	);

		if($this->orcamentosM->alterarQtdProduto($dadosUpdate)){

			$this->log('Área Administrador | atualiza qtd produto','orcamento_produtos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Quantidade de um produto foi alterada',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 					 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarObservacao(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['id'],
							 	'observacao'	=>	$_POST['observacao']	);

		if($this->orcamentosM->alterarObservacao($dadosUpdate)){
			$this->log('Área Administrador | atualiza observacao','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
			
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}


	public function alterarFormaPagto(){

		$dadosUpdate = array(	'id' 				=> 	$_POST['orcamento_id'],
							 	'forma_pagto_id'	=>	$_POST['valor']	);

		if(	$this->orcamentosM->alterarFormaPagto($dadosUpdate)	)	{
			$this->log('Área Administrador | atualiza forma pagto','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de Pagamento alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarEntrega(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['orcamento_id'],
							 	'entrega_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarEntrega($dadosUpdate)){
			$this->log('Área Administrador | atualiza entrega','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de entrega alterada.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarFrete(){
		$dadosUpdate = array(	'id' 		=> 	$_POST['orcamento_id'],
							 	'frete_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarFrete($dadosUpdate)){
			$this->log('Área Administrador | atualiza frete','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Frete alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	private function enviarEmailOrcamento($orcamento_id){

		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);		 
		$email = '<html>
				<head></head>
				<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
				<div class="content" style="width: 600px; height: 100%;">
				<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalho.png); height: 528px; "></div>
				<div style="width: 600px; background-color: #ffcc00; height: 100%">
					<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
						<tbody>
							<tr>
								<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
								<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
								<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
								<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
							</tr>
							<tr>								
								<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
								<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
								<td style="padding-bottom: 18px;font-weight: 700;"></td>
								<td style="padding-bottom: 18px;"></td>
							</tr>
							<tr>
								<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
								<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
								<td style=" font-weight: 700; text-align: right;">Telefone:</td>
								<td>'.$empresa['telefone'].'</td>
							</tr>
							<tr>
								<td style=" font-weight: 700;">E-mail:</td>
								<td colspan="3">'.$empresa['email'].'</td>
							</tr>
						</tbody>
					</table>
					<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
					<table style="width: 570px;margin-left: 14px;" cellspacing="0">
						<thead style="height: 55px;">
							<tr><th style="    height: 30px;">Modelo</th>
							<th>Descrição</th>
							<th>Quantidade</th>			
						</tr></thead>
						<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
				$total = 0; foreach($produtos as $produto){
					
					$email.='	<tr>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['ncm'].'</td>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
								<td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>							
							</tr>';					
					$total = $total + ($produto['valor'] * $produto['qtd']);
				}
				
				$email.='		
						</tbody>
					</table>
					<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
				</div>
			</div>
		</body>
		</html>';	


		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to('webmaster@companytec.com.br')
		    ->subject('Proforma criada por  no Sistema pelo administrador:'.$this->session->userdata('nome'))
		    ->message($email)
		    ->send();

		return $result;		
	}

	public function insereProdutosNovosOrcamento(){
		$erro = 0;
		foreach ($_POST['dados'] as $dados) {
			if( $dados['name'] == 'produto_id'){
				$insert['produto_id'] = $dados['value'];
				$i=1;
			}
			if( $dados['name'] == 'qtd'){
				$insert['qtd'] = $dados['value'];
				$i=2;
			}
			if( $dados['name'] == 'valor_unitario'){
				$insert['valor'] = str_replace(",", ".", str_replace(".","", $dados['value']));
				$i=3;
				$insert['orcamento_id'] = $_POST['orcamento_id'];
				if($this->orcamentosM->insereOrcamentoProdutos($insert)){
					$this->log('Área Administrador | insere produto novo','orcamento_produtos','INSERÇÃO', $this->session->userdata('usuario_id'),$insert,$insert,$_SERVER['REMOTE_ADDR']);
					$erro = $erro;
				}else{
					$erro++;
				}
			}			
		}

		if($erro == 0){
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Produto Adicionado ao orçamento por '.$this->session->userdata('nome'),
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			$this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Icms ******************************
	*****************************************************************************/
	public function excluirProdutoOrcamento()
	{			
		if($this->orcamentosM->excluirProduto($_POST['orcamento_produto_id'])){
			$this->log('Área Administrador | excluir produto','orcamento_produtos','INSERÇÃO', $this->session->userdata('usuario_id'),array('id' => $_POST['orcamento_produto_id']),array('id' => $_POST['orcamento_produto_id']),$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function parametrizacoes()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->paramM->select();
		$parametros['title']	=	"Parametrizações";
		$this->_load_view('area-administrador/parametrizacoes/parametrizacoes',$parametros);
	}

	public function editarParametrizacoes($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'		=>	$this->input->post('id'),
							'moeda'		=>	$this->input->post('moeda'),
							'unidade'	=>	$this->input->post('unidade') );
						 
			if($this->paramM->atualizar($update)){
				$this->log('Área Administrador | atualizar parametro','parametrizacoes','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				redirect('areaAdministrador/parametrizacoes');

			}else{

				$this->session->set_flashdata('erro', 'erro');
				redirect('areaAdministrador/parametrizacoes');
			}

		}else{

			$row 					= 	$this->paramM->getParametrizacoes($id);
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['title']	=	"Editar Parametrizações";			
			$this->_load_view('area-administrador/parametrizacoes/editar-parametrizacoes',$parametros );
		}
	}

	public function cotacaoFreteMaritimo()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Cotação de Frete Marítimo";
		$parametros['dados'] 	=	$this->fretesMM->select();
		$this->_load_view('area-administrador/fretes-maritimos/cotacao-frete-maritimos',$parametros);
	}

	public function cadastrarCotacaoFreteM()
	{

		if($this->input->post()){

			$insere = $this->input->post();
			$insere['peso_bruto'] = str_replace(',','.',$insere['peso_bruto']);			

			if($cotacao_id = $this->fretesMM->add($insere))
			{
				$this->log('Área Administrador | cadastro empresa','empresas','INSERÇÃO', $this->session->userdata('usuario_id'),$insere,$insere,$_SERVER['REMOTE_ADDR']);				
				$this->session->set_flashdata('sucesso', 'ok');	
				redirect('areaAdministrador/editarCotacaoFreteM/'.$cotacao_id);
			}else{
				$this->session->set_flashdata('erro', 'erro');
				redirect('areaAdministrador/cadastrarCotacaoFreteM');
			}					 
		}else{
			$parametros 				= 	$this->session->userdata();
			$parametros['title']		=	"Cadastro de Cotação de Frete Marítimo";
			$parametros['origens'] 		=	$this->fretesMM->buscaPortos(0);
			$parametros['destinos'] 	=	$this->fretesMM->buscaPortos(1);
			$parametros['produtos']		=	$this->produtosM->select();
			$parametros['fornecedor']	=	$this->empresasM->selectFornecedor();
			$this->_load_view('area-administrador/fretes-maritimos/cadastrar-cotacao-frete',$parametros);
		}
	}

	public function editarCotacaoFreteM($id)
	{
		if($this->input->post()){

			$atualizar = $this->input->post();
			if($this->fretesMM->atualizaCotacao($atualizar))
			{
				$this->log('Área Administrador | cotação de frete marítimo','cotacoes','INSERÇÃO', $this->session->userdata('usuario_id'),$insere,$insere,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');	
				redirect('areaAdministrador/editarCotacaoFreteM/'.	$atualizar['id']);
			}else{
				$this->session->set_flashdata('erro', 'erro');
				redirect('areaAdministrador/editarCotacaoFreteM/'.	$atualizar['id']);
			}					 

		}else{
			$parametros 				= 	$this->session->userdata();
			$parametros['title']		=	"Cotação de Frete Marítimo";
			$parametros['cotacoes']		=	$this->fretesMM->getFretesById($id);
			$parametros['origens'] 		=	$this->fretesMM->buscaPortos(0);
			$parametros['destinos'] 	=	$this->fretesMM->buscaPortos(1);
			$parametros['produtos']		=	$this->produtosM->select();
			$parametros['fornecedores']	=	$this->empresasM->selectFornecedor();
			$this->_load_view('area-administrador/fretes-maritimos/edicao-cotacao-frete',$parametros);
		}
	}

	public function verificaEmailFornec()
	{
		echo json_encode($this->empresasM->getEmpresa($this->input->post('empresa_id')));
	}
	
	public function insereEnviaCotacao()
	{
		$insere = array("fornecedor_id"	=>	$this->input->post('fornecedor_id'),
						"frete_id" 		=> 	$this->input->post('frete_id'),
						"dthr_envio"	=> 	$this->input->post('dthr_envio'),
						"email"			=> 	$this->input->post('email')	);
		if($cotacao_id = $this->fretesMM->addCotacao($insere))
		{
			$params['dados'] 				= 	$this->input->post();
			$params['dados']['cotacao_id'] 	= 	$cotacao_id;
			$params['cotacoes']				=	$this->fretesMM->getFretesById($this->input->post('frete_id'));
			$email 	= 	$this->load->view('area-administrador/fretes-maritimos/email-solicitacao-frete', $params, true);
			$this->enviaEmail('ssalcedo@novaandes.com.br','Solicitação de Cotação Frete Marítimo', $email);
			$this->cadastraEnvio($cotacao_id,$this->input->post('email'));
			$this->log('Área Administrador | cotação de frete marítimo','cotacoes','INSERÇÃO', $this->session->userdata('usuario_id'),$insere,$insere,$_SERVER['REMOTE_ADDR']);
			$this->session->set_flashdata('sucesso', 'ok');	
			redirect('areaAdministrador/editarCotacaoFreteM/'.$this->input->post('frete_id'));
		}else{
			$this->session->set_flashdata('erro', 'erro');
			redirect('areaAdministrador/cadastrarCotacaoFreteM/'.$this->input->post('frete_id'));
		}		
	}

	public function alterarValorCotacao()
	{
		$update = array('id' 	=> 	$this->input->post('cotacao_id'),
						'valor'	=> 	str_replace(',','.',str_replace('.','',$this->input->post('valor')))	);

		if( $this->fretesMM->atualizaCotacaoFornec($update) ){

			echo json_encode(array('retorno' => 'sucesso'));
		}else{

			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function buscaEmailFornec()
	{
		$email = $this->empresasM->getEmpresa($this->input->post('id'));
		echo json_encode(array('email' => $email[0]['email']));
	}

	public function enviaSolicitacaoFrete()
	{
		$params['dados'] 				= 	$this->input->post();
		$params['dados']['cotacao_id'] 	= 	$this->input->post('cotacao_id');
		$params['cotacoes']				=	$this->fretesMM->getFretesById($this->input->post('frete_id'));
		$email 	= 	$this->load->view('area-administrador/fretes-maritimos/email-solicitacao-frete', $params, true);
		$update = array(	'id' 			=> 	$this->input->post('cotacao_id'),
							'email'			=> 	$this->input->post('email'),
							'dthr_envio'	=> 	date('Y-m-d H:i:s') );
		
		if($this->enviaEmail($this->input->post('email'),'Solicitação de Cotação Frete Marítimo', $email) &&  $this->fretesMM->atualizaCotacaoFornec($update) ){
			$this->cadastraEnvio($this->input->post('cotacao_id'),$this->input->post('email'));
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	private function cadastraEnvio($cotacao_id, $email){

		$insertEnvio = array(	'cotacao_id' 	=>  $cotacao_id,
								'email'			=>	$email,
								'usuario_id'	=> 	$this->session->userdata('usuario_id') 	);

		if( $this->fretesMM->insereEnvio($insertEnvio) ){
			return true;
		}else{
			return false;
		}

	}

	public function listarEmailFrete()
	{
		$cotacoes = $this->fretesMM->buscaCotacoesPorId($this->input->post('cotacao_id'));
		$html = "<table style='width: 100%'>
					<thead style='padding: 30px; background: #56594a;color: #fff;'>
						<tr>
							<th>E-mail</th>
							<th>Data Hora Envio</th>
							<th>Usuário</th>
						</tr>
					</thead>
					<tbody>";
		if(count($cotacoes) > 0){


			foreach($cotacoes as $cotacao){
				$html.="<tr>
							<td>".$cotacao['email']."</td>
							<td>".date('d/m/Y H:i:s',strtotime($cotacao['dthr_envio']))."</td>
							<td>".$cotacao['usuario']."</td>
						</tr>";
			}
		}else{
			$html.="<tr>
						<td colspan='3' style='text-align: center;'>Nenhum resultado encontrado</td>
					</tr>";

		}
		$html.="</tbody></table>";

		echo json_encode(array('retorno' => $html));
	}

	public function excluirCotacao()
	{
		if( $this->fretesMM->excluirCotacao($this->input->post('id')) ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function portos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->portosM->select();
		$parametros['title']	=	"Portos";
		$this->_load_view('area-administrador/portos/portos',$parametros);
	}
	
	public function cadastraPortos()
	{
		
		if($this->input->post()){

			$insert = array('id'				=>	$this->input->post('id'),
							'abreviacao'		=>	$this->input->post('abreviacao'),
							'descricao'			=>	$this->input->post('descricao'),
							'pais_id'			=>	$this->input->post('pais_id'),
							'fl_internacional'	=>	($this->input->post('pais_id') != 1) ? 1 : 0);
						 
			if($this->portosM->add($insert)){
				$this->log('Área Administrador | cadastra porto','porto','INSERÇÃO', $this->session->userdata('usuario_id'), $insert,$insert,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				redirect('/AreaAdministrador/portos');
			}else{
				$this->session->set_flashdata('erro', 'erro');
				redirect('/AreaAdministrador/portos');
			}

		}else{
			$parametros 			= 	$this->session->userdata();		
			$parametros['pais']		=	$this->paisM->select();
			$parametros['title']	=	"Cadastro de Portos";
			$this->_load_view('area-administrador/portos/cadastra-portos',$parametros);
		}

	}
	
	public function excluirPortos()
	{

		if($this->portosM->excluirPorto($_POST['id'])){
			$this->log('Área Administrador | excluir produtos','produtos','EXCLUSÃO', $this->session->userdata('usuario_id'), array('id' => $_POST['id']),array('id' => $_POST['id']),$_SERVER['REMOTE_ADDR']);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			
			echo json_encode(array('retorno' => 'erro'));
		}
	
	}
	
	public function editarPortos($id = null){

		if($this->input->post()){

			$update = array('id'				=>	$this->input->post('id'),
							'abreviacao'		=>	$this->input->post('abreviacao'),
							'descricao'			=>	$this->input->post('descricao'),
							'pais_id'			=>	$this->input->post('pais_id'),
							'fl_internacional'	=>	($this->input->post('pais_id') != 1) ? 1 : 0 );
						 
			if($this->portosM->atualizaPorto($update)){

				$this->log('Área Administrador | atualizar porto','portos','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');				
				redirect('/AreaAdministrador/portos');
			}else{

				$this->session->set_flashdata('erro', 'erro');
				redirect('/AreaAdministrador/portos');
			}

		}else{

			$row 					= 	$this->portosM->getPortosById($id);
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['pais'] 	=	$this->paisM->select();
			$parametros['title']	=	"Editar Portos";
			$this->_load_view('area-administrador/portos/editar-portos',	$parametros );
		}
	}

	public function cotacaoProdutos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Cotação de Produtos";
		$parametros['dados'] 	=	$this->cotacaoPM->select();
		$this->_load_view('area-administrador/cotacao-produto/cotacao-produto',$parametros);
	}

	public function cadastrarCotacaoProduto()
	{

		if($this->input->post()){

			$insere = $this->input->post();			

			if($cotacao_id = $this->cotacaoPM->add($insere))
			{
				$this->log('Área Administrador | cadastro cotação produto','cotacao_produtos','INSERÇÃO', $this->session->userdata('usuario_id'),$insere,$insere,$_SERVER['REMOTE_ADDR']);				
				$this->session->set_flashdata('sucesso', 'ok');	
				redirect('areaAdministrador/editarCotacaoProdutos/'.$cotacao_id);
			}else{
				$this->session->set_flashdata('erro', 'erro');
				redirect('areaAdministrador/cadastrarCotacaoProduto');
			}
		}else{
			$parametros 				= 	$this->session->userdata();
			$parametros['title']		=	"Cadastro de Cotação de Frete Marítimo";			
			$parametros['produtos']		=	$this->produtosM->select();			
			$this->_load_view('area-administrador/cotacao-produto/cadastrar-cotacao-produto',$parametros);
		}
	}

	public function editarCotacaoProdutos($id = null)
	{
		if($this->input->post()){
			$atualizar = $this->input->post();
			if($this->cotacaoPM->atualizaCotacao($atualizar))
			{
				$this->log('Área Administrador | cotação de frete marítimo','cotacoes','INSERÇÃO', $this->session->userdata('usuario_id'),$insere,$insere,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');	
				redirect('areaAdministrador/editarCotacaoProdutos/'.$atualizar['id']);
			}else{
				$this->session->set_flashdata('erro', 'erro');
				redirect('areaAdministrador/editarCotacaoProdutos/'.$atualizar['id']);
			}
		}else{
			$parametros 				= 	$this->session->userdata();
			$parametros['title']		=	"Cotação de Frete Marítimo";
			$parametros['cotacoes']		=	$this->cotacaoPM->getCotacoesById($id);			
			$parametros['produtos']		=	$this->produtosM->select();
			$parametros['fornecedores']	=	$this->empresasM->selectFornecedor();
			$this->_load_view('area-administrador/cotacao-produto/edicao-cotacao-produto',$parametros);
		}
	}

	public function insereEnviaCotacaoProduto()
	{
		$insere = array("fornec_id"				=>	$this->input->post('fornec_id'),
						"cotacao_produto_id" 	=> 	$this->input->post('cotacao_produto_id'),
						"dthr_envio"			=> 	$this->input->post('dthr_envio'),
						"email"					=> 	$this->input->post('email')	);

		if($cotacao_id = $this->cotacaoPM->addCotacao($insere))
		{
			$params['dados'] 				= 	$this->input->post();
			$params['dados']['cotacao_id'] 	= 	$cotacao_id;
			$params['cotacoes']				=	$this->cotacaoPM->getCotacoesById($this->input->post('cotacao_produto_id'));
			$email 	= 	$this->load->view('area-administrador/cotacao-produto/email-solicitacao-cotacao-produto', $params, true);
			$this->enviaEmail('webmaster@companytec.com.br','Solicitação de Cotação de Produtos', $email);
			$this->cadastraEnvioCotacaoProduto($cotacao_id,$this->input->post('email'));
			$this->log('Área Administrador | cotação de produtos','cotacoes','INSERÇÃO', $this->session->userdata('usuario_id'),$insere,$insere,$_SERVER['REMOTE_ADDR']);
			$this->session->set_flashdata('sucesso', 'ok');	
			redirect('areaAdministrador/editarCotacaoProdutos/'.$this->input->post('cotacao_produto_id'));
		}else{
			$this->session->set_flashdata('erro', 'erro');
			redirect('areaAdministrador/editarCotacaoProdutos/'.$this->input->post('cotacao_produto_id'));
		}		
	}

	public function alterarValorCotacaoProduto()
	{
		$update = array('id' 	=> 	$this->input->post('cotacao_id'),
						'valor'	=> 	str_replace(',','.',str_replace('.','',$this->input->post('valor')))	);

		if( $this->cotacaoPM->atualizaCotacaoFornec($update) ){

			echo json_encode(array('retorno' => 'sucesso'));
		}else{

			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function enviaSolicitacaoCotacaoProduto()
	{
		$params['dados'] 				= 	$this->input->post();
		$params['dados']['cotacao_id'] 	= 	$this->input->post('cotacao_produto_id');
		$params['cotacoes']				=	$this->cotacaoPM->getCotacoesById($this->input->post('cotacao_id'));

		$email 	= 	$this->load->view('area-administrador/cotacao-produto/email-solicitacao-cotacao-produto', $params, true);
		$update = array(	'id' 			=> 	$this->input->post('cotacao_id'),
							'email'			=> 	$this->input->post('email'),
							'dthr_envio'	=> 	date('Y-m-d H:i:s') );
		
		if($this->enviaEmail($this->input->post('email'),'Solicitação de Cotação de Produtos', $email) &&  $this->fretesMM->atualizaCotacaoFornec($update) ){
			$this->cadastraEnvioCotacaoProduto($this->input->post('cotacao_produto_id'),$this->input->post('email'));
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	
	public function listarEmailCotacaoProduto()
	{
		$cotacoes = $this->cotacaoPM->buscaCotacoesPorId($this->input->post('cotacao_id'));
		$html = "<table style='width: 100%'>
					<thead style='padding: 30px; background: #56594a;color: #fff;'>
						<tr>
							<th>E-mail</th>
							<th>Data Hora Envio</th>
							<th>Usuário</th>
						</tr>
					</thead>
					<tbody>";
		if(count($cotacoes) > 0){


			foreach($cotacoes as $cotacao){
				$html.="<tr>
							<td>".$cotacao['email']."</td>
							<td>".date('d/m/Y H:i:s',strtotime($cotacao['dthr_envio']))."</td>
							<td>".$cotacao['usuario']."</td>
						</tr>";
			}
		}else{
			$html.="<tr>
						<td colspan='3' style='text-align: center;'>Nenhum resultado encontrado</td>
					</tr>";

		}
		$html.="</tbody></table>";

		echo json_encode(array('retorno' => $html));
	}

	public function excluirCotacaoProduto()
	{
		if( $this->cotacaoPM->excluirCotacao($this->input->post('id')) ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	private function cadastraEnvioCotacaoProduto($cotacao_id, $email){

		$insertEnvio = array(	'cotacao_produto_fornec_id'	=>  $cotacao_id,
								'email'						=>	$email,									
								'usuario_id'				=> 	$this->session->userdata('usuario_id') 	);

		if( $this->cotacaoPM->insereEnvio($insertEnvio) ){
			return true;
		}else{
			return false;
		}

	}

	public function listaCotacoesFreteProforma()
	{
		$cotacoes = $this->fretesMM->buscaCotacoesDestinoProduto($this->input->post('destino_id'),$this->input->post('produto_id'));
		$html = "<table style='width: 100%'>
					<thead style='padding: 30px; background: #56594a;color: #fff;'>
						<tr>
							<th>Origem</th>
							<th>Destino</th>
							<th>Container</th>
							<th>Peso Bruto</th>
							<th>Volume</th>
							<th>Valor</th>
							<th>Dthr. Envio</th>							
						</tr>
					</thead>
					<tbody>";
		if(count($cotacoes) > 0){

			foreach($cotacoes as $cotacao){
				$html.="<tr>
							<td>".$cotacao['origem']."</td>
							<td>".$cotacao['destino']."</td>
							<td>".$cotacao['container']."</td>
							<td>".$cotacao['peso_bruto']."</td>
							<td>".$cotacao['volume_bruto']."</td>
							<td>".$cotacao['valor']."</td>
							<td>".$cotacao['dthr_envio']."</td>							
						</tr>";
			}
		}else{
			$html.="<tr>
						<td colspan='7' style='text-align: center;'>Nenhum resultado encontrado</td>
					</tr>";

		}
		$html.="</tbody></table>";

		echo json_encode(array('retorno' => $html));
	}

	public function listaCotacoesProdutosProforma()
	{
		$cotacoes = $this->cotacaoPM->buscaCotacoesProduto($this->input->post('produto_id'));
		$html = "<table style='width: 100%'>
					<thead style='padding: 30px; background: #56594a;color: #fff;'>
						<tr>
							<th>Incoterms</th>
							<th>Qtd</th>
							<th>Obs</th>
							<th>Fornecedor</th>							
							<th>Valor</th>
							<th>Dthr. Envio</th>							
						</tr>
					</thead>
					<tbody>";
		if(count($cotacoes) > 0){
			foreach($cotacoes as $cotacao){
				$html.="<tr>
							<td>".$cotacao['incoterm']."</td>
							<td>".$cotacao['qtd']."</td>
							<td>".$cotacao['obs']."</td>
							<td>".$cotacao['fornecedor']."</td>
							<td>".$cotacao['valor_f']."</td>							
							<td>".$cotacao['dthr_envio_f']."</td>
						</tr>";
			}		
		}else{
			$html.="<tr>
						<td colspan='6' style='text-align: center;'>Nenhum resultado encontrado</td>
					</tr>";

		}
		$html.="</tbody></table>";

		echo json_encode(array('retorno' => $html));	
	}

}

