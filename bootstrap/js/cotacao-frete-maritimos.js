$(document).ready(function(){
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

});
function excluirCotacao(id){
    swal({
        title: 'Exclusão de OP',
        text: 'Tem certeza que deseja excluir esta cotação?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: "swal2-confirm btn btn-danger m-btn m-btn--custom",
        confirmButtonText: "Excluir",
        cancelButtonText: "Cancelar"
    }).then(function(isConfirm) {
        if (isConfirm.value) {            
           $.ajax({
                method: "POST",
                url: base_url+'AreaAdministrador/excluirCotacao',
                async: true,
                data: { id  :   id }
            }).done(function( data ) {
                var dados = $.parseJSON(data);          
                if(dados.retorno=='sucesso'){
                    swal({
                        title: "Ok!",
                        text:  "Cotação excluída com sucesso",
                        type:  'success'
                    }).then(function() {
                        window.location.href=base_url+"AreaAdministrador/cotacaoFreteMaritimo";
                    });  
                }else{
                    swal({
                        title: "OPS!",
                        text:  "Problema ocorrido",
                        type:  'warning'
                    }).then(function() {
                        
                    });  
                }
            }); 

        }
    });

}