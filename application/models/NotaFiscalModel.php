<?php

class NotaFiscalModel extends CI_Model {

	public function insere($nota)
	{
        $this->db->insert('nota_fiscal', $nota);
		return $this->db->insert_id();
    }

    public function insereItem($item)
	{
        $this->db->insert('nota_fiscal_item', $item);
		return $this->db->insert_id();
    }

    public function excluir($id_agenda){
		$this->db->where('id_agenda', $id_agenda);
        if(	$this->db->delete('nota_fiscal') ){
            return true;
        }else{
            return false;
        }
	}

	
}