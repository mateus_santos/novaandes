<?php

class ProdutosModel extends CI_Model {
	
 	public function select() {
        
        $sql =  "select p.*,tp.descricao as tipo_produto from produtos p, tipo_produtos tp WHERE p.tipo_produto_id = tp.id and p.tipo_produto_id != 5";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function tipoProdutos() {
        
        $sql =  "select * from tipo_produtos";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getProduto($id) {
        
        $sql =  "select * from produtos where id=".$id;
        $query = $this->db->query($sql);

        return $query->row_array();
    }

    public function insereProduto($data){
        if($this->db->insert('produtos', $data)){
            return true;
        }else{
            return false;
        }

    }

    public function excluirProduto($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('produtos')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaProduto($dados)
    {
        $this->db->where('id', $dados['id']);        
        if($this->db->update('produtos', $dados)){
            return true;
        }else{
            return false;
        }
    }
}
?>