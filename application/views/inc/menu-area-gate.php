<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaGate/acessoPatio/');?>">
		<i class="m-menu__link-icon fa fa-car" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Entrada\Saída de Veículos</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaGate/reImpressao/');?>">
		<i class="m-menu__link-icon fa fa-print" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Reimpressão de Tickect</span>
			</span>
		</span>
	</a>
</li>
<!--
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioAgendamento/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório Caminhões Agendados</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioEstacionados/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório Caminhões no Estacionamento</span>
			</span>
		</span> 
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioSaida/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório de Saída</span>
			</span>
		</span>
	</a>
</li>-->