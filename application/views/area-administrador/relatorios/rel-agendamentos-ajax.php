<table class="" id="html_table" width="150%">
<thead>
	<tr>
		<th class="formata_celula" style="width: 1%;">#</th>
		<th class="formata_celula" style="width: 25%;">Cliente</th>						
		<th class="formata_celula" style="width: 4%;">Data</th>	
		<th class="formata_celula" style="width: 4%;">Placa</th>
		<th class="formata_celula" style="width: 24%;">Produto</th>
		<th class="formata_celula" style="width: 4%;">NF</th>
		<th class="formata_celula" style="width: 5%;">Qtd.</th>
		<th class="formata_celula" style="width: 5%;">Emb.</th>	
		<th class="formata_celula" style="width: 19%;">Motorista</th>
		<th class="formata_celula" style="width: 4%;">CPF</th>
		<th class="formata_celula" style="width: 5%;">Telefone</th>
		<!--	<th style="width: 5%;">Ações</th>-->			
	</tr>
</thead> 
<tbody>					
	<?php foreach($agendamentos as $agendamento){	?>
		<tr>
			<td class="formata_celula" style="text-align: center;"><?php echo $agendamento['id']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['razao_social']; ?></td>											
			<td class="formata_celula" style=""><?php echo $agendamento['dt_agenda']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['placa']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['descricao']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['nr_nf']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['quant_total']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['no_embalagem']; ?></td>	
			<td class="formata_celula" style=""><?php echo $agendamento['nome']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['cpf']; ?></td>
			<td class="formata_celula" style=""><?php echo $agendamento['fone']; ?></td>
		<!--	<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
				<a href="<?php echo base_url('AreaClientes/editarAgendamento/'.$agendamento['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-edit"></i>
				</a>				
			</td>-->
		</tr>
		<?php } ?> 
	</tbody>
</table>	