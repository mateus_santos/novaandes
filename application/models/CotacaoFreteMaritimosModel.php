<?php

class CotacaoFreteMaritimosModel extends CI_Model {

	public function add ($data) {
        $this->db->insert('cotacao_frete_maritimos', $data);
        return  $this->db->insert_id();
        
	}

    public function addCotacao($data) {        
        $this->db->insert('cotacao_frete_maritimo_fornec', $data);
        return  $this->db->insert_id();
    }

    public function insereEnvio($data) {        
        $this->db->insert('cotacao_envio', $data);
        return  $this->db->insert_id();
    }    

    public function select(){

        $sql =  "   SELECT  c.*, p.descricao, concat(o.abreviacao,'-',o.descricao) as origem, concat(d.abreviacao,'-',d.descricao) as destino
                    FROM    cotacao_frete_maritimos c
                    INNER JOIN portos o ON o.id = c.origem_id
                    INNER JOIN portos d ON d.id = c.destino_id
                    INNER JOIN  produtos p ON p.id = c.produto_id ";

        return $this->db->query($sql)->result_array();   
    }

    public function getFretesById($id){

        $sql =  "   SELECT  c.*, concat(e.cnpj,' - ',e.razao_social)  as fornecedor, p.descricao, f.valor, f.dthr_envio, f.email, f.id as cotacao_id, p.id as produto_id,concat(o.abreviacao,'-',o.descricao) as origem, concat(d.abreviacao,'-',d.descricao) as destino, f.fornecedor_id
                    FROM        cotacao_frete_maritimos c
                    LEFT JOIN   cotacao_frete_maritimo_fornec f ON f.frete_id = c.id                     
                    LEFT JOIN   empresas e ON e.id = f.fornecedor_id                 
                    INNER JOIN  produtos p ON p.id = c.produto_id    
                    INNER JOIN portos o ON o.id = c.origem_id
                    INNER JOIN portos d ON d.id = c.destino_id
                    WHERE c.id =" .$id;
        
        return $this->db->query($sql)->result_array();
    }   

    public function excluirCotacao($id){
        
        $this->db->where('frete_id', $id);
        if($this->db->delete('cotacao_frete_maritimo_fornec')){
            $this->db->where('id', $id);
            if($this->db->delete('cotacao_frete_maritimos')){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    public function atualizaCotacao($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('cotacao_frete_maritimos', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizaCotacaoFornec($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('cotacao_frete_maritimo_fornec', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function buscaPortos($fl_internacional)
    {
        $sql = "SELECT * FROM portos WHERE fl_internacional =".$fl_internacional;
        return $this->db->query($sql)->result_array();
    }

    public function buscaCotacoesPorId($cotacao_id)
    {
        $sql = "SELECT c.*, u.nome as usuario FROM cotacao_envio c
                inner join usuarios u on u.id = c.usuario_id 
                WHERE c.cotacao_id =".$cotacao_id;
        return $this->db->query($sql)->result_array();
    }    

    public function buscaCotacoesDestinoProduto($destino_id, $produto_id)
    {
        $sql = " SELECT c.*, format(cf.valor,2,'de_DE') as valor, date_format(cf.dthr_envio,'%d/%m%Y %H:%i:%s') as dthr_envio, 
                        upper(concat(o.abreviacao,'-',o.descricao)) as origem, upper(concat(d.abreviacao,'-',d.descricao)) as destino 
                FROM    cotacao_frete_maritimos c 
                INNER JOIN  cotacao_frete_maritimo_fornec cf ON c.id = cf.frete_id 
                INNER JOIN  empresas e ON e.id = cf.fornecedor_id 
                INNER JOIN  portos as d ON c.destino_id = d.id 
                INNER JOIN  portos as o ON c.origem_id = o.id 
                WHERE       o.pais_id = 1   and 
                            d.pais_id = ".$destino_id."  and 
                            c.produto_id = ".$produto_id;
        return $this->db->query($sql)->result_array();
    }

}
?>