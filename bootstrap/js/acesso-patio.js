$(document).ready(function(){
	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$("body").css("position","fixed");
	$(".esc_tabela").css('display','none');
	$("#tab_botao_entrada").css('display','none');
	$("#tab_botao_saida").css('display','none');
	$("#placa").inputmask({mask: ['AAA-9999','AAA-9A99']});
	
	$('#placa').keypress(function(e){
		var k = e.which || e.keyCode; 
		if(k==13){
			e.preventDefault(); 
		}
	});

	$('#placa').change(function(){
		
		busca_placa($('#placa').val());

	});
	
});

function busca_placa(placa){
	
	var v_placa 		= '';
	
	
	$.ajax({
		method: "POST",
		url: base_url+'areaGate/acessoPatioAjax/',
		async: true,
        data: { placa    :   placa }
	}).done(function(data){
		var dados = $.parseJSON(data);
		
		var saida = dados.retorno.acesso[0].existe;
		
		$("#tab_resultado_lista").empty();
		var total_linhas = dados.retorno.listagem.length;
		
		if (total_linhas > 0 ){
			
			if (saida == 0){

				var saida_manutencao 	= dados.retorno.listagem[0].dthr_saida_manutencao;
				var entrada_retorno 	= dados.retorno.listagem[0].dthr_retorno_manutencao;

				if((entrada_retorno == 'null' && saida_manutencao == 'null')||(typeof saida_manutencao ==='undefined')) {

					$('#msg').html('ENTRADA');
					$(".esc_tabela").css('display','block');
					$(".esc_tabela").css('border','0px');
					$("#tab_botao_entrada").css('display','block');
					$("#tab_botao_saida").css('display','none');
					$("#tab_resultado").css('display','block');
					$("#tab_observa").css('display','block');
					$("#dv_saida_manut").css('display','none');
					for(var i=0; i < total_linhas; i++) {
					
						v_id_agenda 		= dados.retorno.listagem[i].id;
						v_razao_social 		= dados.retorno.listagem[i].razao_social;
						v_motorista 		= dados.retorno.listagem[i].nome;
						v_id_motorista 		= dados.retorno.listagem[i].motorista_id;
						v_produto			= dados.retorno.listagem[i].no_produto;
						//v_dthr_retorno		= dados.retorno.listagem[i].dthr_retorno_manutencao;
						//v_id_produto		= dados.retorno.listagem[i].produto_id;
						
					
						var div_linha = "<div class='linha'>";
							div_linha += "<div class='coluna col_cliente'>";
							div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id_agenda' id='id_agenda' value='"+v_id_agenda+"' />  </label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna' style='width:28%'>";
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_motorista'>"+v_motorista+" </div><input type='hidden' 	name='id_motorista_entrada' id='id_motorista_entrada' value='"+v_id_motorista+"'></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente'>"; 
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_produto'>"+v_produto+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							
							div_linha += "</div>";
							$("#tab_resultado_lista").append(div_linha);

					}
				}else{
					 //Entrada de retorno por manutenção
					$('#msg').html('ENTRADA DE RETORNO DE MANUTENÇÃO');
					$(".esc_tabela").css('display','block');
					$(".esc_tabela").css('border','0px');
					$("#tab_botao_entrada").css('display','block');
					$("#tab_botao_saida").css('display','none');
					$("#tab_resultado").css('display','block');
					$("#tab_observa").css('display','block');
					$("#dv_saida_manut").css('display','none');

					for(var i=0; i < total_linhas; i++) {
					
						v_id_acesso 			= dados.retorno.listagem[i].id_acesso;
						v_id_agenda 			= dados.retorno.listagem[i].id;
						v_razao_social 			= dados.retorno.listagem[i].razao_social;
						v_motorista 			= dados.retorno.listagem[i].nome;
						v_id_motorista 			= dados.retorno.listagem[i].motorista_id;
						v_produto				= dados.retorno.listagem[i].no_produto;
						v_dthr_saida_manutencao	= dados.retorno.listagem[i].dthr_saida_manutencao;
						//v_id_produto		= dados.retorno.listagem[i].produto_id;
						
						$("#dthr_saida_manutencao").val(v_dthr_saida_manutencao);
					
						var div_linha = "<div class='linha'>";
							div_linha += "<div class='coluna col_cliente'>";
							div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id' id='id' value='"+v_id_acesso+"' />  </label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna' style='width:28%'>";
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_motorista'>"+v_motorista+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente'>"; 
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_produto'>"+v_produto+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							
							div_linha += "</div>";
							$("#tab_resultado_lista").append(div_linha);
					}
				}
				$('#observacao').focus();
			}else{
				var saida_manutencao 	= dados.retorno.listagem[0].dthr_saida_manutencao;
				var entrada_retorno 	= dados.retorno.listagem[0].dthr_retorno_manutencao;
				
				if((entrada_retorno == 'null' && saida_manutencao == 'null') || (entrada_retorno != 'null' && saida_manutencao != 'null')) {

					$('#msg').html('SAÍDA');
					$("#tab_botao_saida").css('display','block');
					$("#tab_botao_entrada").css('display','none');
					$("#tab_resultado").css('display','block');
					$("#tab_resultado_lista").css('display','block');
					$(".esc_tabela").css('border','0px');
					$("#tab_observa").css('display','none');
					$("#dv_saida_manut").css('display','block');

					for(var i=0; i < total_linhas; i++) {
						
						v_id_acesso 			= dados.retorno.listagem[i].id_acesso;
						v_id_agenda 			= dados.retorno.listagem[i].id;
						v_razao_social 			= dados.retorno.listagem[i].razao_social;
						v_id_motorista 			= dados.retorno.listagem[i].id_motorista;
						v_motorista 			= dados.retorno.listagem[i].nome;
						//v_id_produto			= dados.retorno.listagem[i].produto_id;
						v_produto				= dados.retorno.listagem[i].no_produto;
						v_id_classificadora		= dados.retorno.listagem[i].id_classificadora;
						v_classificadora		= dados.retorno.listagem[i].classificadora;
						v_id_terminal			= dados.retorno.listagem[i].id_terminal;
						v_no_terminal			= dados.retorno.listagem[i].no_terminal;
						v_dthr_entrada			= dados.retorno.listagem[i].dthr_entrada;
						v_fl_atende_classific	= dados.retorno.listagem[i].fl_atende_classific;
						v_dthr_saida_manutencao	= dados.retorno.listagem[i].dthr_saida_manutencao;

						if (v_classificadora == null){

							var div_linha = "<div class='msg' style='font-weight:bold; font-size:110%;' >Veículo não foi classificado para saída, retorne ao pátio para classificação!</div>";
							$("#tab_botao_saida").css('display','none');
							$("#tab_resultado").css('display','none');
							$("#dv_saida_manut").css('display','none');
							$("#tab_resultado_lista").append(div_linha);	
						}else{
							
							if(v_fl_atende_classific == null){
								var div_linha = "<div class='msg' style='font-weight:bold; font-size:110%;' >Veículo classificado para saída porém não foi informado se atende a classificação, retorne ao pátio para classificação!</div>";
								$("#tab_botao_saida").css('display','none');
								$("#tab_resultado").css('display','none');
								$("#dv_saida_manut").css('display','none');
								$("#tab_resultado_lista").append(div_linha);		
							}else{

								var div_linha_cancel = null;
								var div_linha_tipo	 = null;
								if(v_fl_atende_classific == 'N'){
									div_linha_cancel = "<div class='msg' style='font-weight:bold; font-size:110%;' >CANCELADA</div>";	
								}

								if(v_dthr_saida_manutencao != null){
									div_linha_tipo = "<div class='msg_tipo' style='font-weight:bold; font-size:110%;' >SAÍDA PARA MANUTENÇÃO</div>";	
								}

								var div_linha = "<div class='linha'>"; 	
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id' id='id' value='"+v_id_acesso+"' />  </label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna' style='width:28%'>";
								div_linha += "<div class='m-input-icon m-input-icon--right'>";
								div_linha += "<label class='listagem'><div id='dv_motorista'>"+v_motorista+" </div><input type='hidden' name='id_motorista_saida' id='id_motorista_saida' value='"+v_id_motorista+"'></label>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente'>"; 
								div_linha += "<div class='m-input-icon m-input-icon--right'>";
								div_linha += "<label class='listagem'><div id='dv_produto'>"+v_produto+" </div></label>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "</div>";
								

								div_linha += "<div class='linha_titulo'>";
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='titulo'>Data de Entrada</label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna' style='width:28%'>";
								div_linha += "<label class='titulo'>Terminal</label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='titulo'>Classificadora</label></div>";
								div_linha += "</div>";
								div_linha += "<div class='linha'>";
								div_linha += "<div class='coluna col_cliente'>";
								div_linha += "<label class='listagem'><div id='dv_hora_entrada'>"+v_dthr_entrada+" </div>  </label>";
								div_linha += "</div>";
								div_linha += "<div class='coluna' style='width:28%'>";
								div_linha += "<div class='m-input-icon m-input-icon--right'>";
								div_linha += "<label class='listagem'><div id='dv_saida_terminal'>"+v_no_terminal+" </div></label>";
								div_linha += "</div>";
								div_linha += "</div>";
								div_linha += "<div class='coluna col_cliente'>"; 
								div_linha += "<div class='m-input-icon m-input-icon--right'>";
								div_linha += "<label class='listagem'><div id='dv_classificadora'>"+v_classificadora+" </div><input type='hidden' name='id_classificadora' id='id_classificadora' value='"+v_id_classificadora+"'></label>";
								div_linha += "</div>";
								div_linha += "</div>";
								
								div_linha += "</div>";
								if(div_linha_cancel != null){
									$("#msg").append(div_linha_cancel);
								}
								if(div_linha_tipo != null){
									$("#msg_tipo").append(div_linha_tipo);
								}
								$("#tab_resultado_lista").append(div_linha);
								
							}
						}
					}
				}else{ //Entrada de retorno por manutenção
					$('#msg').html('ENTRADA DE RETORNO DE MANUTENÇÃO');
					$(".esc_tabela").css('display','block');
					$(".esc_tabela").css('border','0px');
					$("#tab_botao_entrada").css('display','block');
					$("#tab_botao_saida").css('display','none');
					$("#tab_resultado").css('display','block');
					$("#tab_observa").css('display','block');
					$("#dv_saida_manut").css('display','none');

					for(var i=0; i < total_linhas; i++) {
					
						v_id_acesso 			= dados.retorno.listagem[i].id_acesso;
						//v_id_agenda 			= dados.retorno.listagem[i].id;
						v_razao_social 			= dados.retorno.listagem[i].razao_social;
						v_motorista 			= dados.retorno.listagem[i].nome;
						//v_id_motorista 			= dados.retorno.listagem[i].motorista_id;
						v_produto				= dados.retorno.listagem[i].no_produto;
						v_dthr_saida_manutencao	= dados.retorno.listagem[i].dthr_saida_manutencao;
						//v_id_produto		= dados.retorno.listagem[i].produto_id;
						
						$("#dthr_saida_manutencao").val(v_dthr_saida_manutencao);
					
						var div_linha = "<div class='linha'>";
							div_linha += "<div class='coluna col_cliente'>";
							div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id' id='id' value='"+v_id_acesso+"' />  </label>";
							div_linha += "</div>";
							div_linha += "<div class='coluna' style='width:28%'>";
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_motorista'>"+v_motorista+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							div_linha += "<div class='coluna col_cliente'>"; 
							div_linha += "<div class='m-input-icon m-input-icon--right'>";
							div_linha += "<label class='listagem'><div id='dv_produto'>"+v_produto+" </div></label>";
							div_linha += "</div>";
							div_linha += "</div>";
							
							div_linha += "</div>";
							$("#tab_resultado_lista").append(div_linha);
					}
					
				}
				$('#btn_saida').focus();
				
			}	

		}else{
			
			
			$("#tab_resultado").css('display','none');
			$("#tab_observa").css('display','none');
			$(".msg").empty();
			$("#tab_botao_entrada").css('display','none');
			$("#tab_botao_saida").css('display','none');
			var div_linha = "<div class='msg' style='font-weight:bold; font-size:110%;' >Placa não encontrada ou não agendada</div>";
			$("#tab_resultado_lista").append(div_linha);	
			
			$("#tab_resultado_lista").css('display','block');
		}

	});
	
	
}

