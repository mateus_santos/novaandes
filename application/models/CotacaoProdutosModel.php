<?php

class CotacaoProdutosModel extends CI_Model {

	public function add ($data) {
        $this->db->insert('cotacao_produtos', $data);
        return  $this->db->insert_id();        
	}

    public function addCotacao($data) {        
        $this->db->insert('cotacao_produto_fornec', $data);
        return  $this->db->insert_id();
    }

    public function insereEnvio($data) {
        $this->db->insert('cotacao_produto_envio', $data);
        return  $this->db->insert_id();
    }

    public function select(){

        $sql =  "   SELECT  c.*, p.descricao as produto
                    FROM    cotacao_produtos c                    
                    INNER JOIN  produtos p ON p.id = c.produto_id ";

        return $this->db->query($sql)->result_array();
    }

    public function getCotacoesById($id){

        $sql =  "   SELECT  c.*, concat(e.cnpj,' - ',e.razao_social)  as fornecedor, p.descricao as produto, f.valor, f.id as cotacao_id, p.id as produto_id, f.fornec_id,f.email, f.dthr_envio
                    FROM        cotacao_produtos c
                    LEFT JOIN   cotacao_produto_fornec f ON f.cotacao_produto_id = c.id
                    LEFT JOIN   empresas e ON e.id = f.fornec_id                 
                    INNER JOIN  produtos p ON p.id = c.produto_id
                    WHERE c.id =" .$id;

        return $this->db->query($sql)->result_array();
    }   

    public function excluirCotacao($id){
        
        $this->db->where('cotacao_produto_id', $id);
        if($this->db->delete('cotacao_produto_fornec')){
            $this->db->where('id', $id);
            if($this->db->delete('cotacao_produtos')){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    public function atualizaCotacao($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('cotacao_produtos', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizaCotacaoFornec($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('cotacao_produto_fornec', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function buscaPortos($fl_internacional)
    {
        $sql = "SELECT * FROM portos WHERE fl_internacional =".$fl_internacional;
        return $this->db->query($sql)->result_array();
    }

    public function buscaCotacoesPorId($cotacao_id)
    {
        $sql = "SELECT c.*, u.nome as usuario FROM cotacao_produto_envio c
                inner join usuarios u on u.id = c.usuario_id 
                WHERE c.cotacao_produto_fornec_id =".$cotacao_id;
        return $this->db->query($sql)->result_array();
    }    

    public function buscaCotacoesProduto($produto_id)
    {
        $sql = "SELECT      c.*, cf.*, format(cf.valor,2,'de_DE' ) as valor_f, date_format(cf.dthr_envio,'%d/%m/%Y %H:%i:%s') as dthr_envio_f, concat(e.cnpj,'-',e.razao_social) as fornecedor  
                FROM        cotacao_produtos c 
                INNER JOIN  cotacao_produto_fornec cf ON cf.cotacao_produto_id = c.id
                INNER JOIN  empresas e ON e.id = cf.fornec_id
                WHERE c.produto_id = ".$produto_id;
        return $this->db->query($sql)->result_array();
    }

}
?>