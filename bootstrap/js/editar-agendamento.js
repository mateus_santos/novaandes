$(document).ready(function(){
    $('.datepicker').css('z-index: 99999 !important');
	$('.datepicker').mask('99/99/9999');
    $('#cpf').mask('000.000.000-00');
    $('#telefone').mask('(00) 00000 - 0000');
    //$('.qtd').mask('#');
    $('.qtd').mask("#.##0", {reverse: true});

    $('#cnh').mask('00000000000', {reverse: true});
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

    var hoje = new Date();
    var finalData = '+1d';
    if(hoje.getDay() == 5){
        finalData = '+3d';
    }

	$('.datepicker').datepicker({
        startDate: '0d',
        endDate   : finalData,
        daysOfWeekDisabled : [0],
	    format: 'dd/mm/yyyy'
	    
	});

    $('.editar_xml').bind('click', function(){
        $('#div_arquivo_xml').fadeIn('slow');
        $('#nfe').attr('required', true);
    });

	maskMercosul('#placa');
 	   
    $('#data').bind('focusout', function(){
        data_atual = $('#data_atual').val();
        if( data_atual == $(this).val() ){ 
            $.ajax({
                method: "POST",
                url: base_url+'AreaClientes/verificaHoraLimiteAgendamento',
                async: true,
                data: { empresa_id     :   $('#id_empresa').val(),
                        data           :   formataStringData(data_atual) }
            }).done(function( data ) {
                var dados = $.parseJSON(data);
                if(dados.retorno == 'erro' ){
                    swal({
                        title: "Atenção!",
                        text:  "Hora limite de agendamento para hoje passou ("+dados.hora_limite+"). Agende para amanhã!",
                        type:  'warning'
                    }).then(function() {
                        $('#data').val('');
                    });      
                }

            }); 
        }
    });

 	$('#nfe').bind('change', function() {
	    var file_data = $('#nfe').prop('files')[0];   
	    var form_data = new FormData();                  
	    form_data.append('file', file_data);	    
	    $.ajax({
	        method: "POST",
            url: base_url+'areaClientes/uploadNfe',
            async: false,	        
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: form_data,	        
	        success: function(data){
	            var dados = $.parseJSON(data); 

                if( dados.retorno == 'ja existe' ){
                    swal({
                        title: "Atenção!",
                        text:  "Já existe um agendamento para essa nota fiscal. Agendamento:"+dados.agendamento+" - Placa: "+dados.placa,
                        type:  'warning'
                    }).then(function() {
                        $('#nfe').val('');
                    });
                // envia para o php os dados da nota     
                }else if(dados.retorno == 'sucesso'){
                    //$('#div-nfe').empty();
                    $('#div-nfe').append(dados.nfe);
                    $('#arquivo_xml').remove();
                    $('#nfe').after(dados.arquivo);
                    $('#tabela_produtos').slideUp();
                    //$('#produtos_xml').empty();
                    $('#produtos_xml').append(dados.html);
                    $('#tabela_produtos').slideDown();
                    $('.qtd').unmask();
                    $('.qtd').mask("#.##0", {reverse: true});
                }else if(dados.retorno == 'cfop errado'){

                    swal({
                        title: "Atenção!",
                        text: "Mercadoria Com CFOP errado, verifique a nota e tente novamente.",
                        type: 'warning'
                    }).then(function() {
                        $('#nfe').val('');
                        $('#tabela_produtos').slideUp();
                        $('#div-nfe').empty();
                    }); 

                }else if(dados.retorno == 'Erro de upload' || dados.retorno == 'Nfe errada'){

                    swal({
                        title: "Atenção!",
                        text: "Problema ao enviar a nota fiscal, tente novamente com outra nota fiscal.",
                        type: 'warning'
                    }).then(function() {
                        $('#nfe').val('');
                        $('#tabela_produtos').slideUp();
                        $('#div-nfe').empty();
                    }); 
                }           
	        }
	     });
	});

     $('#cpf').bind('focusout', function(){
        var cpf = $(this).val();
        if(cpf != "" && isCpfValid(cpf) ){            
            verificaMotoristaCpf(cpf);
            
        }else{
            swal({
                title: "Ops!",
                text: "Cpf Inválido",
                type: 'warning'
            }).then(function() {
                $('#cpf').val('');
            }); 
        }
    });

    $('#cnh').bind('focusout', function(){
        var cnh = $(this).val();

        if( $(this).val().length < 11 ){
            swal({
                title: "Atenção!",
                text:  "Nº de CNH Errado, verifique.",
                type:  'warning'
            }).then(function() {
                $('#cnh').val('');
            });
        } 
        
    });

    // verifica peso total ultrapassa o agendamento
    $('form').bind('submit', function(e){
         
        total_qtd = 0; 
        $('.qtd').each(function(){
            total_qtd = total_qtd + parseFloat($(this).val());
        });
             
        $.ajax({
            method: "POST",
            url: base_url+'AreaClientes/verificaCotasAgendamento',
            async: true,
            data: { empresa_id      :   $('#id_empresa').val(),
                    dt_agendamento  :   formataStringData($('#data').val()),
                    total           :   total_qtd }
        }).done(function( data ) {
            var dados = $.parseJSON(data);
            if(dados.retorno == 'erro'){
                
                swal({
                    title: "Atenção!",
                    text: dados.mensagem,
                    type: 'warning'
                }).then(function() {
                    $('#nfe').val('');
                    $('#tabela_produtos').slideUp();
                    $('#div-nfe').empty();
                   
                }); 
                
            }else{
                //sucesso, submete o formulário
                $('form').unbind();
                $('form').submit();
            }

        });
         
        e.preventDefault();
    });

    $('#motorista').bind('focusout', function(){
         $.ajax({
            method: "POST",
            url: base_url+'AreaClientes/atualizaMotorista',
            async: true,
            data: { id_motorista    :   $('#id_motorista').val(),
                    nome            :   $('#motorista').val(),
                    fone            :   $('#telefone').val() }
        }).done(function( data ) {
            var dados = $.parseJSON(data);
            if(dados.retorno == 'sucesso'){

                swal({
                    title: "Ok!",
                    text: 'Nome Motorista Alterado',
                    type: 'success'
                }).then(function() {}); 
                
            }else{
                
                
            }

        });
    });

    $('#telefone').bind('focusout', function(){
         $.ajax({
            method: "POST",
            url: base_url+'AreaClientes/atualizaMotorista',
            async: true,
            data: { id_motorista    :   $('#id_motorista').val(),
                    fone            :   $('#telefone').val(),
                    nome            :   $('#motorista').val() }
        }).done(function( data ) {
            var dados = $.parseJSON(data);
            if(dados.retorno == 'sucesso'){

                swal({
                    title: "Ok!",
                    text: 'Telefone do Motorista Alterado',
                    type: 'success'
                }).then(function() {}); 
                
            }else{
                
                
            }

        });
    });


});

function verificaMotorista(cnh, cpf){
    $.ajax({
        method: "POST",
        url: base_url+'AreaClientes/verificaCpfAgendamento',
        async: true,
        data: { cpf     :   cpf,
                cnh     :   cnh }
    }).done(function( data ) {
        var dados = $.parseJSON(data);
        if(dados != null){
            $('#motorista').val(dados.nome);
            $('#motorista').attr('disabled',true);
            $('#telefone').val(dados.fone);
            $('#telefone').attr('disabled',true);
            $('#motorista').after('<input type="hidden" name="id_motorista" id="id_motorista" value="'+dados.id+'" /> ')
        }else{
            $('#motorista').attr('disabled',false);
            $('#motorista').val('');
            $('#telefone').attr('disabled',false);
            $('#telefone').val('');
        }

    }); 
}

function verificaMotoristaCpf(cpf){
    $.ajax({
        method: "POST",
        url: base_url+'AreaClientes/verificaCpfAgendamento',
        async: true,
        data: { cpf     :   cpf }
    }).done(function( data ) {
        var dados = $.parseJSON(data);
        if(dados != null){
            $('#motorista').val(dados.nome);            
            $('#telefone').val(dados.fone);
            $('#cnh').val(dados.nr_cnh);            
            $('#id_motorista').remove();
            $('#motorista').after('<input type="hidden" name="id_motorista" id="id_motorista" value="'+dados.id+'" /> ')
        }else{

            $('#id_motorista').remove();
            $('#motorista').attr('disabled',false);
            $('#motorista').val('');
            $('#telefone').attr('disabled', false);
            $('#telefone').val('');
            $('#cnh').val('');            
        }

    });     
}

function verificaMotoristaCnh(cnh){
    $.ajax({
        method: "POST",
        url: base_url+'AreaClientes/verificaCnhAgendamento',
        async: true,
        data: { cnh     :   cnh }
    }).done(function( data ) {
        var dados = $.parseJSON(data);
        if(dados != null){
            $('#motorista').val(dados.nome);            
            $('#telefone').val(dados.fone);
            $('#cpf').val(dados.cpf);            
            $('#id_motorista').remove();
            $('#motorista').after('<input type="hidden" name="id_motorista" id="id_motorista" value="'+dados.id+'" /> ')
        }else{

            $('#id_motorista').remove();
            $('#motorista').attr('disabled',false);
            $('#motorista').val('');
            $('#telefone').attr('disabled', false);
            $('#telefone').val('');
            $('#cpf').val('');
        }

    });     
}

function isCpfValid(cpf) {
    
    cpf = cpf.replace(/[^\d]+/g,''); 
    if(cpf == '') return false; 
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 || 
        cpf == "00000000000" || 
        cpf == "11111111111" || 
        cpf == "22222222222" || 
        cpf == "33333333333" || 
        cpf == "44444444444" || 
        cpf == "55555555555" || 
        cpf == "66666666666" || 
        cpf == "77777777777" || 
        cpf == "88888888888" || 
        cpf == "99999999999")
            return false;       
    // Valida 1o digito 
    add = 0;    
    for (i=0; i < 9; i ++)      
        add += parseInt(cpf.charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(cpf.charAt(9)))     
            return false;       
    // Valida 2o digito 
    add = 0;    
    for (i = 0; i < 10; i ++)       
        add += parseInt(cpf.charAt(i)) * (11 - i);  
    rev = 11 - (add % 11);  
    if (rev == 10 || rev == 11) 
        rev = 0;    
    if (rev != parseInt(cpf.charAt(10)))
        return false;       
    return true;   
};

    
function maskMercosul(selector) {
    var MercoSulMaskBehavior = function (val) {
        var myMask = 'AAA0A00';
        var mercosul = /([A-Za-z]{3}[0-9]{1}[A-Za-z]{1})/;
        var normal = /([A-Za-z]{3}[0-9]{2})/;
        var replaced = val.replace(/[^\w]/g, '');
        if (normal.exec(replaced)) {
            myMask = 'AAA-0000';
        } else if (mercosul.exec(replaced)) {
            myMask = 'AAA-0A00';
        }
        return myMask;
    },
    mercoSulOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(MercoSulMaskBehavior.apply({}, arguments), options);
        }
    };
    $(function() {
        $(selector).bind('paste', function(e) {
            $(this).unmask();
        });
        $(selector).bind('input', function(e) {
            $(selector).mask(MercoSulMaskBehavior, mercoSulOptions);
        });
    });
}

function formataStringData(data) {
  var dia  = data.split("/")[0];
  var mes  = data.split("/")[1];
  var ano  = data.split("/")[2];

  return ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);
  // Utilizo o .slice(-2) para garantir o formato com 2 digitos.
}
