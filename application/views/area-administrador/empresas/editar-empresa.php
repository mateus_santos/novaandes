<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('AreaAdministrador/visualizarEmpresa/'.$dados[0]['id'].'/'.$pesquisa);?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text" style="padding-right: 10px;">	
							<a href="<?php echo base_url('AreaAdministrador/visualizarEmpresa/'.$dados[0]['id'].'/'.$pesquisa);?>">Voltar</a>						
						</h3>
						<span class="m-portlet__head-icon m--hide">
						<i class="la la la-building-o"></i>
						</span>
						<h3 class="m-portlet__head-text">	
							Empresa #<?php echo $dados[0]['id'];?>						
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarEmpresa');?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Razão Social:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required	name="razao_social"	class="form-control m-input" placeholder="" required value="<?php echo $dados[0]['razao_social'];?>">
								<input type="hidden" name="id" 	class="form-control m-input" placeholder="" required value="<?php echo $dados[0]['id'];?>">								
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
								</span>
							</div>
						</div>
						<div class="col-lg-3">
							<label class="">Fantasia:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="fantasia" class="form-control m-input" placeholder="" required value="<?php echo $dados[0]['fantasia'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-3">
							<label>CNPJ:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="cnpj" id="cnpj" class="form-control m-input" placeholder="Insira um cnpj" required value="<?php echo $dados[0]['cnpj'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>
							
						</div>
						<div class="col-lg-3">
							<label>Inscrição Estadual:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="insc_estadual" id="insc_estadual" class="form-control m-input" placeholder="Insira a inscrição estadual" value="<?php echo $dados[0]['insc_estadual'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>
							
						</div>
					</div>	 
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>E-mail:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="email" required name="email" id="email" class="form-control m-input" placeholder="Insira um email" required value="<?php echo $dados[0]['email'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-3">
							<label>Telefone:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="telefone" id="telefone" class="form-control m-input" placeholder="Insira um telefone" required value="<?php echo $dados[0]['telefone'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-3">
							<label>Endereço:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="endereco" id="endereco" class="form-control m-input" placeholder="Insira um endereço" required value="<?php echo $dados[0]['endereco'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-3">
							<label>Bairro:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="bairro" id="bairro" class="form-control m-input" placeholder="Insira um Bairro" required value="<?php echo $dados[0]['bairro'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
							</div>							
						</div>
					</div>
					<div class="form-group m-form__group row">	
						<div class="col-lg-3">
							<label class="">CEP:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="cep" required id="cep" class="form-control m-input" placeholder="Insira um cep" value="<?php echo $dados[0]['cep'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-3">
							<label class="">Cidade:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="cidade" required id="cidade" class="form-control m-input" placeholder="Insira um cidade" value="<?php echo $dados[0]['cidade'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
							</div>
							
						</div>
						<div class="col-lg-3">
							<label class="">Estado:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="estado" required id="estado" class="form-control m-input" placeholder="Insira um estado" value="<?php echo $dados[0]['estado'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
							</div>							
						</div>					
						<div class="col-lg-3">
							<label for="Acesso">País</label>							
							<select class="form-control m-input" name="pais_id" required>
								<option value="">Selecione o país</option>
							<?php foreach ($pais as $pa) { ?>
								<option value="<?php echo $pa['id']; ?>" <?=( $dados[0]['pais_id'] == $pa['id'] ) ? 'selected=selected' : ''?> >
									<?php echo $pa['nome_pt']; ?>									
								</option>
							<?php } ?>							
							</select>
						</div>
					</div>					
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label for="Acesso">Tipo de Empresa</label>
							<select class="form-control m-input" name="tipo_acesso_id" required>
							<?php  foreach ($dados_tipo_acesso as $tipo_acesso) { ?>
								<option value="<?php echo $tipo_acesso['id']; ?>" <?php if($tipo_acesso['id'] == $dados[0]['tipo_cadastro_id']) { echo 'selected="selected"'; }?> >
									<?php echo $tipo_acesso['descricao']; ?>									
								</option>
							<?php } ?>
							
							</select>
						</div>
						
					</div>	
					
	            </div>
	            <div class="form-group m-form__group row">
	            				
					<div class="col-lg-6">
						<label class="">Data/Hora Cadastro:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="dthr_cadastro" disabled="disabled" class="form-control m-input" placeholder="Data/Hora Cadastro" value="<?php echo date('d/m/Y H:i:s', strtotime($dados[0]['dthr_cadastro']));?>">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar-o"></i></span></span>
						</div>						
					</div>
					<?php if( $dados[0]['cartao_cnpj'] != '' ){ 
							 $arquivo = explode('/',$dados[0]['cartao_cnpj']); 
							if(count($arquivo) == 5){
					?>
					<div class="col-lg-6">
						<label class="">Cartão CNPJ:</label>
						<div class="">

							<a href="<?php echo base_url('Consultas/Receita_Federal/'.$arquivo[5]); ?>" target="_blank">
								<img  src="<?php echo base_url('bootstrap/img/receita.gif'); ?>" />
							</a>
							
						</div>						
					</div>
				<?php }
					} 
				?>					  
				</div>				 
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	