<table class="" id="html_table" width="150%">
<thead>
	<tr>
		<th class="formata_celula" style="width: 5%;  ">Dia Cota</th>
		<th class="formata_celula" style="width: 20%;">Cliente</th>	
		<th class="formata_celula" style="width: 15%; ">Produto</th>
		<th class="formata_celula" style="width: 5%; ">Placa</th>
		<th class="formata_celula" style="width: 5%; ">NF</th>
		<th class="formata_celula" style="width: 3%; ">Qtd.</th>
		<th class="formata_celula" style="width: 3%; ">Emb.</th>
		<th class="formata_celula" style="width: 10%; ">Motorista</th>	
		<th class="formata_celula" style="width: 7%; ">CPF</th>
		<th class="formata_celula" style="width: 7%; ">Telefone</th>				
		<th class="formata_celula" style="width: 5%; ">Agendamento</th>
		<th class="formata_celula" style="width: 5%; ">Data Entrada</th>
		<th class="formata_celula" style="width: 5%; ">Data Classificada</th>
		<th class="formata_celula" style="width: 5%; ">Data Saída</th>
		<!--	<th style="width: 5%;">Ações</th>-->			
	</tr>
</thead>
<tbody>					
	<?php foreach($agendamentos as $agendamento){	?>
		<tr>
        	<td class="formata_celula" ><?php echo $agendamento['dia']; ?></td>
			<td class="formata_celula" ><?php echo $agendamento['razao_social']; ?></td>	
			<td class="formata_celula"><?php echo $agendamento['descricao']; ?></td>										
			<td class="formata_celula"><?php echo $agendamento['placa']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['nr_nf']; ?></td>
			<td class="formata_celula" style="text-align:right; padding-right:5px; "><?php echo $agendamento['quant_total']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['no_embalagem']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['nome']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['cpf']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['fone']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['dt_agenda']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['dthr_entrada']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['dthr_classific']; ?></td>
			<td class="formata_celula"><?php echo $agendamento['dthr_saida']; ?></td>
		<!--	<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
				<a href="<?php echo base_url('AreaClientes/editarAgendamento/'.$agendamento['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-edit"></i>
				</a>				
			</td>-->
		</tr>
		<?php } ?> 
	</tbody>
</table>	