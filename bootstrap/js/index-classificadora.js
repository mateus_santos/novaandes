$(document).ready(function(){
	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);	

	$('#html_table').DataTable({
	    	"scrollX": true,
	    	"order": [[ 0, "desc" ]],
			"language": {
				"sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_    Resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": '<i class="la la-angle-double-right"></i>',
			        "sPrevious": '<i class="la la-angle-double-left"></i>',
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
			    }
			}
		});
	    
	 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
		$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
		$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
		$('.dataTables_filter').css('float','right');
		$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
		$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
		$('table thead').css('background-color', '#f4f3f8');
		//$('table thead').css('height', '150px');
		$('table tbody tr:odd').addClass('zebraUm');
		$('table tbody tr:even').addClass('zebraDois');

   		$('#table2').DataTable({
	    	"scrollX": true,
	    	"order": [[ 0, "desc" ]],
			"language": {
				"sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_    Resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": '<i class="la la-angle-double-right"></i>',
			        "sPrevious": '<i class="la la-angle-double-left"></i>',
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
			    }
			}
		});
		$('#table2_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
		$('#table2_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
		$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
		$('.dataTables_filter').css('float','right');
		$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
		$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
		$('table thead').css('background-color', '#f4f3f8');
		//$('table thead').css('height', '150px');
		$('table tbody tr:odd').addClass('zebraUm');
		$('table tbody tr:even').addClass('zebraDois');

		$('.classificar').bind('click', function(){
			console.log('here');
			acesso_id = $(this).attr('acesso_id');
			placa = $(this).attr('placa');
			$('.m_atualiza_title').empty();
			$('.m_atualiza_title').text('Classificar #'+acesso_id+' Placa: '+placa);
			$('#classificar').attr('acesso_id', acesso_id);
			$('#m_atualiza').modal('show');

		});

		$('.classificar_edit').bind('click', function(){
			console.log('here');
			acesso_id = $(this).attr('acesso_id');
			placa = $(this).attr('placa');
			situacao = $(this).attr('situacao');
			$('.m_atualiza_title').empty();
			$('.m_atualiza_title').text('Alterar Classificação #'+acesso_id+' Placa: '+placa);
			$('#classificar').attr('acesso_id', acesso_id);
			$('#classificacao option[value="'+situacao+'"]').attr('selected','selected');
			$('#m_atualiza').modal('show');

		});

	$('#classificacao').bind('change', function(){
			
		if($(this).val()=='N' ){
			$('.motivo_esconde').slideDown('slow');
		}else{
			$('.motivo_esconde').slideUp('slow');
		}

	});	


	$('#classificar').bind('click', function(){

		acesso_id 	= 	$(this).attr('acesso_id');
		situacao 	= 	$('#classificacao').val();
		motivo 		= 	$('#motivo').val();
		if( situacao == '' ){
			swal({
				title: 'Atenção!',
				text:  'Selecione uma classificação para essa placa!',
				type:  'warning'
			}).then(function() {}); 

		}else{
			$.ajax({
				method: "POST",
				url: base_url+"AreaClassificadora/classificaAcesso",
				async: true,
				data: { acesso_id 		: 	acesso_id,
						situacao 		: 	situacao,
						motivo  		: 	motivo	}
			}).done(function( data ) {
				var dados = $.parseJSON(data);
				if(dados.retorno == 'sucesso'){
					swal({
						title: 'Ok!',
						text:  'Classificação realizada com sucesso!',
						type:  'success'
					}).then(function() {					
						window.location.href=base_url+"AreaClassificadora/";
						
					}); 		
				}else{
					swal({
						title: 'Erro!',
						text:  'Atenção, houve algum erro, entre em contato com o suporte técnico!',
						type:  'warning'
					}).then(function() {					
						window.location.href=base_url+"AreaClassificadora/";
						
					}); 		
				}
			});
		}
			
		
	})

});


function datatable(){

	$('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');



}


