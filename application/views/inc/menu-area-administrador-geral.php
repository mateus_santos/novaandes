<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/editarCliente');?>">
		<i class="m-menu__link-icon la la-user" style="color: #ffffff;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>				
<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
	<a href="javascript:;" class="m-menu__link m-menu__toggle">
		<i class="m-menu__link-icon fa fa-wpforms" style="color: #ffffff;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-text">
			Cadastros
		</span>
		<i class="m-menu__ver-arrow la la-angle-right"></i>
	</a>
	<div class="m-menu__submenu " m-hidden-height="80" style="">
		<span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
				<span class="m-menu__link">
					<span class="m-menu__link-text">
						Cadastros
					</span>
				</span>
			</li>	
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/parametrizacoes');?>">
					<i class="m-menu__link-icon la la-cogs"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Configurações</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/portos');?>">
					<i class="m-menu__link-icon la la-ship"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Gestão de Portos</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoEmpresas');?>">
					<i class="m-menu__link-icon la la-building-o" style="text-shadow: 2px 6px 4px #4e4c4cfc;"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Gestão de Empresas</span>
						</span>
					</span>
				</a>
			</li>			
			<li class="m-menu__item " >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoUsuarios');?>">
					<i class="m-menu__link-icon fa fa-users" style="text-shadow: 2px 6px 4px #4e4c4cfc;"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Gestão de Usuários</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/tipoProdutos');?>">
					<i class="m-menu__link-icon fa fa-product-hunt"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Tipos de Produtos</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/produtos');?>">
					<i class="m-menu__link-icon fa fa-product-hunt"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Gestão de Produtos</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/fretes');?>">
					<i class="m-menu__link-icon la la-truck"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Fretes</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/entregas');?>">
					<i class="m-menu__link-icon flaticon-open-box"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Prazos de Entregas</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/formaPagto');?>">
					<i class="m-menu__link-icon la la-cc-mastercard"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Formas Pgto.</span>
						</span>
					</span>
				</a>
			</li>				
		</ul>
	</div>	
</li>
<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
	<a href="javascript:;" class="m-menu__link m-menu__toggle">
		<i class="m-menu__link-icon fa fa-usd" style="color: #ffffff;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-text">
			Cotações
		</span>
		<i class="m-menu__ver-arrow la la-angle-right"></i>
	</a>
	<div class="m-menu__submenu " m-hidden-height="80" style="">
		<span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
				<span class="m-menu__link">
					<span class="m-menu__link-text">
						Cotações
					</span>
				</span>
			</li>
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/cotacaoFreteMaritimo');?>">
					<i class="m-menu__link-icon fa fa-ship" style="text-shadow: 2px 6px 4px #4e4c4cfc;"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Frete Marítimo</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/cotacaoProdutos');?>">
					<i class="m-menu__link-icon fa fa-list-alt" style="text-shadow: 2px 6px 4px #4e4c4cfc;"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Produtos</span>
						</span>
					</span>
				</a>
			</li>
		</ul>
	</div>
</li>
<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
	<a href="javascript:;" class="m-menu__link m-menu__toggle">
		<i class="m-menu__link-icon fa fa-shopping-cart" style="color: #ffffff;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-text">
			Oportunidades
		</span>
		<i class="m-menu__ver-arrow la la-angle-right"></i>
	</a>
	<div class="m-menu__submenu " m-hidden-height="80" style="">
		<span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
				<span class="m-menu__link">
					<span class="m-menu__link-text">
						Oportunidades
					</span>
				</span>
			</li>
			<li class="m-menu__item " aria-haspopup="true" >
				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/orcamentos');?>">
					<i class="m-menu__link-icon la la-building-o" style="text-shadow: 2px 6px 4px #4e4c4cfc;"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Proforma</span>
						</span>
					</span>
				</a>
			</li>
		</ul>
	</div>
</li>