$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
});

function replicar_identificador(id){
	var i = 1;
	while ($("#identificador_"+id+"_"+i).length > 0) {
		$("#identificador_"+id+"_"+i).val($("#identificador_"+id+"_0").val());
		i++;
	}
	$(".selectpicker").selectpicker('refresh');
}

function salvar_configuracao(){
	if($("#concentrador_id").val() == null){
		swal({
			title: "Campo em branco",
			text: "Selecione um concentrador",
			type: "warning"
		}).then(function() {
			$("#concentrador_id").focus();
		});
		return;
	}
	if($("#flag_identificacao").val() == null){
		swal({
			title: "Campo em branco",
			text: "Informe se as bombas utilizão algum sistema de identificação",
			type: "warning"
		}).then(function() {
			$("#flag_identificacao").focus();
		});
		return;
	}
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/salvarConfiguracaoWS',
		async: true,
		data: {
			concentrador_id: $("#concentrador_id").val(),
			flag_identificacao: $("#flag_identificacao").val(),
			empresa_id: $("#empresa_id").val()
		},
		success: function( data ) {
			//console.log(data);
			var dados = $.parseJSON(data);
			swal({
				title: dados.titulo,
				text: dados.texto,
				type: dados.tipo
			}).then(function() {
				//console.log($("#flag_identificacao").val());
				if($("#flag_identificacao").val() == 1){
					$("#configuracoes").css('display', 'block');
				}
				else {
					$("#configuracoes").css('display', 'none');
				}
			});
		}
	});
}

function salvar_configuracao_bomba(configuracao_id, id){
	if($("#serie_"+id).val() == ''){
		swal({
			title: "Campo em branco",
			text: "Informe o número de série da bomba",
			type: "warning"
		}).then(function() {
			$("#serie_"+id).focus();
		});
		return;
	}
	var produto_id = id.split('_')[0];
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/salvarConfiguracaoBombaWS',
		async: true,
		data: {
			numero_serie: $("#serie_"+id).val(),
			identificador_id: $("#identificador_"+id).val(),
			produto_id: produto_id,
			id: configuracao_id,
			empresa_id: $("#empresa_id").val()
		},
		success: function( data ) {
			//console.log(data);
			var dados = $.parseJSON(data);
			swal({
				title: dados.titulo,
				text: dados.texto,
				type: dados.tipo
			}).then(function() {
				if(dados.tipo == 'success'){
					if(configuracao_id == 0){
						$("#botao_salvar_"+id).attr('onclick', "salvar_configuracao_bomba('"+dados.id+"', '"+id+"')");
						$("#acoes_"+id).append("<a href='#' id='botao_remover_"+id+"' class='' title='Remover'><i class='la la-remove'></i></a>");
						$("#botao_remover_"+id).attr('onclick', "remover_configuracao_bomba('"+dados.id+"', '"+id+"')");
						$("#botao_remover_"+id).addClass("m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill");					
					}
				}
			});
		}
	});
}

function remover_configuracao_bomba(configuracao_id, id){
	swal({
		title: "Confirmação de exclusão",
		text: "Deseja excluir as informações para esta bomba?",
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Sim',
		cancelButtonText: "Não"
    }).then(function(isConfirm) {
		if (isConfirm.value === true) {
			$.ajax({
				method: "POST",
				url: base_url+'areaAdministrador/removerConfiguracaoBombaWS',
				async: true,
				data: {
					id: configuracao_id
				},
				success: function( data ) {
					//console.log(data);
					var dados = $.parseJSON(data);
					swal({
						title: dados.titulo,
						text: dados.texto,
						type: dados.tipo
					}).then(function() {
						if(dados.tipo == 'success'){
							$("#serie_"+id).val('');
							$("#identificador_"+id).val(0);
							$("#identificador_"+id).selectpicker('refresh');
							$("#botao_salvar_"+id).attr('onclick', "salvar_configuracao_bomba(0, '"+id+"')");
							$("#botao_remover_"+id).remove();
						}
					});
				}
			});
		}
    });
	
}

function show_modal_add_concentrador(){
	$("#modal_add_concentrador").modal('show');
	$("#concentrador").focus();
}

function show_modal_add_identificador(select_id){
	$("#modal_add_identificador").modal('show');
	$("#identificador").attr('select_id', select_id);
	$("#identificador").focus();
}

function adicionar_concentrador(){
	var concentrador = $("#concentrador").val();
	if(concentrador == ''){
		swal({
			title: "Campo em branco",
			text: "Informe o modelo do concentrador",
			type: "warning"
		}).then(function() {
			$("#concentrador").focus();
		});
		return;
	}
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/inserirConcentradorWS',
		async: true,
		data: {
			modelo: concentrador
		},
		success: function( data ) {
			//console.log(data);
			var dados = $.parseJSON(data);
			swal({
				title: dados.titulo,
				text: dados.texto,
				type: dados.tipo
			}).then(function() {
				if(dados.tipo == 'success'){
					$("#concentrador").val('');
					$("#concentrador_id").append("<option selected value='"+dados.id+"'>"+concentrador+"</option>");
					$("#concentrador_id").selectpicker('refresh');
				}
			});
		}
	});
}

function adicionar_identificador(){
	var identificador = $("#identificador").val();
	if(identificador == ''){
		swal({
			title: "Campo em branco",
			text: "Informe o modelo do identificador",
			type: "warning"
		}).then(function() {
			$("#identificador").focus();
		});
		return;
	}
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/inserirIdentificadorWS',
		async: true,
		data: {
			modelo: identificador
		},
		success: function( data ) {
			//console.log(data);
			var dados = $.parseJSON(data);
			swal({
				title: dados.titulo,
				text: dados.texto,
				type: dados.tipo
			}).then(function() {
				if(dados.tipo == 'success'){
					$("#identificador").val('');
					$(".selectpicker").each(function( index ) {
						if(this.id.indexOf("identificador") != -1) {
							$(this).append("<option value='"+dados.id+"'>"+identificador+"</option>");
						}
					});
					var id = $("#identificador").attr('select_id');
					$("#identificador_"+id).val(dados.id);
					$("#identificador").attr('select_id', '');
					$(".selectpicker").selectpicker('refresh');
				}
			});
		}
	});
}