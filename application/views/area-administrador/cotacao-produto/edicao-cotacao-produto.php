<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/cotacaoProdutos'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/cotacaoProdutos'); ?>">Voltar</a>	Cotação de Produtos
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarCotacaoProdutos/'.$cotacoes[0]['id']);?>" method="post">
				<div class="m-portlet__body">							
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Produto:</label>
							<div class="m-input-icon m-input-icon--left">
								<select name="produto_id" class="form-control" required>
									<option value="">Selecione o produto</option>
									<?php foreach($produtos as $produto){ ?>
										<option value="<?=$produto['id']?>" <?=($cotacoes[0]['produto_id']==$produto['id']) ? 'selected=selected' : '';?>><?=$produto['tipo_produto'].' - '.$produto['descricao']?></option>
									<?php } ?>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-file-text-o"></i></span></span>
								<input type="hidden" name="id" class="form-control" required value="<?=$cotacoes[0]['id']?>" />
							</div>							
						</div>
					</div>
					<div class="form-group m-form__group row">						
						<div class="col-lg-4">
							<label>Qtd.:</label>
							<div class="m-input-icon m-input-icon--left">
								<input type="text" name="qtd" class="form-control" required value="<?=$cotacoes[0]['qtd']?>" />
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-balance-scale"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Iconterms:</label>
							<div class="m-input-icon m-input-icon--left">
								<select name="incoterm" class="form-control">
									<option value="">Selecione o tipo de iconterms</option>
									<option value="EXW" <?=($cotacoes[0]['incoterm']=='EXW') ? 'selected=selected' : ''?> >EXW (Ex Works)</option>
									<option value="FCA" <?=($cotacoes[0]['incoterm']=='FCA') ? 'selected=selected' : ''?> >FCA (Free Carrier)</option>
									<option value="FAS" <?=($cotacoes[0]['incoterm']=='FAS') ? 'selected=selected' : ''?> >FAS (Free Alongside Ship)</option>
									<option value="FOB" <?=($cotacoes[0]['incoterm']=='FOB') ? 'selected=selected' : ''?> >FOB (Free On Board)</option>
									<option value="CFR" <?=($cotacoes[0]['incoterm']=='CFR') ? 'selected=selected' : ''?> >CFR (Cost And Freight)</option>
									<option value="CIF" <?=($cotacoes[0]['incoterm']=='CIF') ? 'selected=selected' : ''?> >CIF (Cost, Insurance And Freight)</option>
									<option value="CPT" <?=($cotacoes[0]['incoterm']=='CPT') ? 'selected=selected' : ''?> >CPT (Carriage Paid To)</option>
									<option value="CIP" <?=($cotacoes[0]['incoterm']=='CIP') ? 'selected=selected' : ''?> >CIP (Carriage And Insurance Paid To)</option>
									<option value="DAT" <?=($cotacoes[0]['incoterm']=='DAT') ? 'selected=selected' : ''?> >DAT (Delivered At Terminal)</option>
									<option value="DAP" <?=($cotacoes[0]['incoterm']=='DAP') ? 'selected=selected' : ''?> >DAP (Delivered at Place)</option>
									<option value="DDP" <?=($cotacoes[0]['incoterm']=='DDP') ? 'selected=selected' : ''?> >DDP (Delivered Duty Paid)</option>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-file-text-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Observação:</label>
							<div class="m-input-icon m-input-icon--left">
								<textarea name="obs" class="form-control" required /><?=$cotacoes[0]['obs']?></textarea>
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-file-text-o"></i></span></span>
							</div>							
						</div>
					</div>
					</div>			
		            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
									<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
			
	</div>
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">					
					<h2 class="m-portlet__head-label m-portlet__head-label--primary" style="width: 320px;">
						<span>
							Cotações 
						</span>
						<a data-toggle="modal" href="#modal_cotacao" class="" style="color: #fff !important; font-weight: bold;" >
							<i class="la la-plus-circle" style="font-size: 38px;padding-top: 5px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar nova cotação"></i>
						</a>
					</h2>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >					
			<!--begin: Datatable -->
			<table class="" id="tabela_cotacao" width="100%">
				<thead style="background: #56594a;color: #fff;">
					<tr>
						<th style="text-align: center;">#</th>
						<th style="text-align: center;">Fornecedor</th>
						<th style="text-align: center;">E-mail enviado</th>
						<th style="text-align: center;">Dthr. envio</th>
						<th style="text-align: center;">Valor</th>
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>					
					<?php foreach($cotacoes as $cotacao){
							if($cotacao['cotacao_id']){
					 ?>
						<tr>
							<td style="width: 5%;text-align: center;"><?php echo  $cotacao['cotacao_id']; ?></td>
							<td style="width: 35%;text-align: center;"><?php echo $cotacao['fornecedor']; ?></td>
							<td style="width: 30%;text-align: center;"><?php echo $cotacao['email']; ?></td>
							<td style="width: 15%;text-align: center;"><?php echo date('d/m/Y H:i:s', strtotime($cotacao['dthr_envio'])); ?></td>	
							<td style="width: 10%;text-align: center;"><input type="text" name="valor" cotacao_id="<?php echo  $cotacao['cotacao_id']; ?>" class="form-control valor" value="<?php echo $cotacao['valor']; ?>" /></td>
							<td data-field="Actions" class="m-datatable__cell" style="width: 25%;text-align: center !important;">	
								<a cotacao_id="<?php echo $cotacao['cotacao_id']; ?>"  class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill"  onclick="solicita_email(<?=$cotacao['cotacao_id']?>,'<?=$cotacao['fornecedor']?>',<?=$cotacao['fornec_id']?>,<?=$cotacoes[0]['id']?>)" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Enviar email para cotação">
									<i class="la la-send" ></i>
								</a>
								<a cotacao_id="<?php echo $cotacao['cotacao_id']; ?>"  class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill"  onclick="listar_email(<?=$cotacao['cotacao_id']?>,'<?=$cotacao['fornecedor']?>')" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listar e-mails enviados">
									<i class="la la-list" ></i>
								</a>
							</td>
						</tr>
						<?php }else{ ?>
							<tr>
								<td style="text-align: center;color: #ffcc00; padding: 15px;" colspan="6">Nenhuma cotação inserida</td>
							</tr>
						<?php	} 
						} ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>
</div>	
<div class="modal fade" id="modal_cotacao" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/insereEnviaCotacaoProduto');?>" method="post" enctype="multipart/form-data">
		<div class="modal-content" style="width: 850px;">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Emitir Cotação</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body row">
				<div class="form-group m-form__group col-lg-6" >
					<label for="exampleSelect1">Fornecedor </label>			
					<div class="m-input-icon m-input-icon--left">				
						<select name="fornec_id" id="fornecedor_id" class="form-control" required>
							<option value="">Selecione o forncecedor</option>
							<?php foreach($fornecedores as $fornecedor){ ?>
								<option value="<?=$fornecedor['id']?>" ><?=$fornecedor['cnpj'].' - '.$fornecedor['razao_social']?></option>
							<?php } ?>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-ship"></i></span></span>
					</div>					
				</div>
				<div class="form-group m-form__group col-lg-6 e-mail_esconde" style="display: none;">
					<label for="exampleSelect1">E-mail </label>							
					<input type="text" 	class="form-control m-input m-input--air sub" placeholder="E-mail" name="email" id="email" style="" value="" />
					<input type="hidden" name="cotacao_produto_id"  value="<?=$cotacoes[0]['id'];?>" />
					<input type="hidden" name="dthr_envio"  value="<?=date('Y-m-d H:i:s');?>" />
					<input type="hidden" name="fornecedor"  id="fornecedor" />
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>				
				<button type="submit" class="btn btn-primary" >Salvar e Solicitar Cotação</button>
			</div>
		</div>
		</form>
	</div>
</div>
<div class="modal fade" id="modal_solicita_cotacao" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">		
		<div class="modal-content" style="width: 850px;">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Solicitar Cotação - <span id="titulo_fornecedor"></span> </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body row">				
				<div class="form-group m-form__group col-lg-6 e-mail_esconde" >
					<label for="exampleSelect1">E-mail </label>							
					<input type="text" 	class="form-control m-input m-input--air sub" 	placeholder="E-mail" name="email" id="email_solicitacao" style="" value="" /> 
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>				
				<button type="submit" class="btn btn-primary" fornecedor="" cotacao_produto_id="" id="solicitar_cotacao">Solicitar Cotação</button>
			</div>
		</div>		
	</div>
</div>
<div class="modal fade" id="modal_listar_envios" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">		
		<div class="modal-content" style="width: 850px;">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Solicitação de Cotação para <span id="lista_fornecedor"></span> </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body lista_envios">
					
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>				
			
			</div>
		</div>		
	</div>
</div>
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
            title: "OK!",
            text: 'Cadastro realizado com sucesso!',
            type: "success"
        }).then(function() {
		 	
		}); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>