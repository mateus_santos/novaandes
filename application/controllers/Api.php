<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Api extends CI_Controller {
	public function __construct() {
		parent::__construct();
		// carrega a model a ser utilizada neste controller
		$this->load->model('AcessoModel', 'acessoM');
		$this->load->model('TransitoMovimentacaoModel', 'transitoMM');
		$this->load->model('LogApiModel', 'logApiM');
	}

	public function recepcaoCarga(){
		
		if( $this->input->post('hash') == '937a19a6c487c1e7fafd3c233020e234' ){
			
			$info_erro = "";
			if( $this->input->post('placa') != ''){
				if( strlen($this->input->post('placa')) != 7 ){
					
					$info_erro.= '0 - Erro: Campo Placa do Cavalo (placa) com numero de caracteres errado :'.strlen($this->input->post('placa')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Placa do Cavalo (placa) Vazio. ';
			}			

			if( $this->input->post('dthr_pesagem') != ''){
				if( strlen($this->input->post('dthr_pesagem')) != 19 ){
					
					$info_erro.= '0 - Erro: Campo Data-Hora da 2 Pesagem (dthr_pesagem) com numero de caracteres errado :'.strlen($this->input->post('dthr_pesagem')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Data-Hora da 2 Pesagem (dthr_pesagem) Vazio. ';
			}

			if( $this->input->post('cnpj_dono') != ''){
				if( strlen($this->input->post('cnpj_dono')) != 14 ){
					
					$info_erro.= '0 - Erro: Campo Dono da Carga (CNPJ) (cnpj_dono) com numero de caracteres errado :'.strlen($this->input->post('dthr_pesagem')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Dono da Carga (CNPJ) (cnpj_dono) Vazio. ';
			}

			if( $this->input->post('peso_liquido') != ''){
				if (strpos($this->input->post('peso_liquido'), ',') !== false) {
					$peso = explode(',', $this->input->post('peso_liquido'));
					if( strlen($peso[0]) > 13 || strlen($peso[1]) > 3 ){
						$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) com formato errado, verifique o manual. ';
					}
				}else{
					$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) com formato errado, verifique o manual. ';		
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) Vazio. ';
			}
			$protocolo = ' Protocolo: '.md5(date('d-m-Y H:i:s:u'));
			$insereLog['protocolo'] = $protocolo;

			if( $info_erro == '' ){

				$placa = substr_replace($this->input->post('placa'),'-',3, 0);

				$peso_liquido = str_replace(',', '.', $this->input->post('peso_liquido'));
				$dadosUpdate = array( 	'peso_aferido' 	=> 	$peso_liquido,
										'dthr_pesagem' 	=> 	$this->input->post('dthr_pesagem'),
										'placa'			=> 	$placa );

				$acessos = $this->acessoM->buscaAcessoPlaca($placa);

				if( $acessos['total'] > 0 ){

					if( $this->acessoM->atualizaRecepcaoCarga( $dadosUpdate ) ){
						$insereLog['retorno'] = '1 - Enviado-Registrado com sucesso.';
						$this->logApiM->insere($insereLog);
												
						echo json_encode(array('retorno' => '1 - Enviado-Registrado com sucesso.'.$protocolo ));
					}else{
						$insereLog['retorno'] = '0 - Erro na atualização de dados, entre em contato com a MilSistemas.';
						$this->logApiM->insere($insereLog);
						echo json_encode(array('retorno' => '0 - Erro na atualização de dados, entre em contato com a MilSistemas.'.$protocolo ));
					}
				}else{
					$insereLog['retorno'] = '0 - Erro a placa não realizou agendamento, entre em contato com a MilSistemas.';
					$this->logApiM->insere($insereLog);
					echo json_encode(array('retorno' => '0 - Erro a placa não realizou agendamento, entre em contato com a MilSistemas.' .$protocolo));
				}

			}else{
				$insereLog['retorno'] = $info_erro;
				$this->logApiM->insere($insereLog);
				echo json_encode(array('retorno' => $info_erro));	
			}

		}else{
			$insereLog['retorno'] = '0 - Erro: Chave Incorreta.';
			$this->logApiM->insere($insereLog);
			echo json_encode(array('retorno' => '0 - Erro: Chave Incorreta.'));
		}

	}

	public function embarqueCarga(){
		
		if( $this->input->post('hash') == '937a19a6c487c1e7fafd3c233020e234' ){
			
			$info_erro = "";
			if( $this->input->post('placa') != ''){
				if( strlen($this->input->post('placa')) != 7 ){
					
					$info_erro.= '0 - Erro: Campo Placa do Cavalo (placa) com numero de caracteres errado :'.strlen($this->input->post('placa')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Placa do Cavalo (placa) Vazio. ';
			}

			if( $this->input->post('cpf_motorista') != ''){
				if( strlen($this->input->post('cpf_motorista')) != 7 ){
					
					$info_erro.= '0 - Erro: Campo CPF Motorista (cpf_motorista) com numero de caracteres errado :'.strlen($this->input->post('cpf_motorista')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Placa do Cavalo (placa) Vazio. ';
			}

			if( $this->input->post('dthr_pesagem') != ''){
				if( strlen($this->input->post('dthr_pesagem')) != 19 ){
					
					$info_erro.= '0 - Erro: Campo Data-Hora da 2 Pesagem (dthr_pesagem) com numero de caracteres errado :'.strlen($this->input->post('dthr_pesagem')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Data-Hora da 2 Pesagem (dthr_pesagem) Vazio. ';
			}

			if( $this->input->post('cnpj_dono') != ''){
				if( strlen($this->input->post('cnpj_dono')) != 14 ){
					
					$info_erro.= '0 - Erro: Campo Dono da Carga (CNPJ) (cnpj_dono) com numero de caracteres errado :'.strlen($this->input->post('dthr_pesagem')).' ';	
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Dono da Carga (CNPJ) (cnpj_dono) Vazio. ';
			}

			if( $this->input->post('peso_liquido') != ''){
				if (strpos($this->input->post('peso_liquido'), ',') !== false) {
					$peso = explode(',', $this->input->post('peso_liquido'));
					if( strlen($peso[0]) > 13 || strlen($peso[1]) > 3 ){
						$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) com formato errado, verifique o manual. ';
					}
				}else{
					$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) com formato errado, verifique o manual. ';		
				}
			}else{
				
				$info_erro.= '0 - Erro: Campo Peso Liquido (Kg) (peso_liquido) Vazio. ';
			}

			if( $info_erro == '' ){

				$placa = substr_replace($this->input->post('placa'),'-',3, 0);

				$peso_liquido = str_replace(',', '.', $this->input->post('peso_liquido'));
				
				$acessos = $this->acessoM->buscaAcessoPlaca($placa);

				if( $acessos['total'] > 0 ){

					$dados = $this->acessoM->buscaDadosTransito($this->input->post('cnpj_dono'));

					$dadosInsert = array(	'dthr_movimento' 	=> 	$this->input->post('dthr_pesagem'),
											'id_transito'		=> 	$dados['id_transito'],
											'nr_due' 			=> 	$dados['nr_due'],
											'placa'				=> 	$placa,
											'cpf_motorista'		=> 	$this->input->post('cpf_motorista'),
											'peso_manif'		=> 	$peso_liquido,
											'peso_aferido'		=> 	$dados['peso_aferido']	 );

					if( $this->transitoMM->add( $dadosInsert ) ){
						echo json_encode(array('retorno' => '1 - Enviado-Registrado com sucesso.' ));
					}else{
						echo json_encode(array('retorno' => '0 - Erro na atualização de dados, entre em contato com a MilSistemas.' ));
					}
				}else{
					echo json_encode(array('retorno' => '0 - Erro a placa não realizou agendamento, entre em contato com a MilSistemas.'));
				}

			}else{
				echo json_encode(array('retorno' => $info_erro));	
			}

		}else{
			echo json_encode(array('retorno' => '0 - Erro: Chave Incorreta.'));
		}

	}	
		
}