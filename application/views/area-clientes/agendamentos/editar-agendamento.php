<div class="m-content">	
	<form accept-charset="utf-8" action="<?php echo base_url('AreaClientes/atualizaAgendamento/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-building"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Editar Agendamento #<?php echo $dados['id']; ?>
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">
				
			</div>
		</div>
				
		<div class="m-portlet__body">
			<div class="form-group m-form__group row">									
				<div class="col-lg-12">
					<label>Tipo de Agendamento</label>
					<div class="m-radio-inline">
						<label class="m-radio">
							<input type="radio" name="id_tipo_operacao" class="ativo"  id="tipo_operacao_clas" value="1" <?=($dados['id_tipo_operacao']=='1') ? 'checked="checked"' : ''?> disabled="disabled"> Classificação (Caminhão Cheio)
							<span></span>
						</label>									
						<label class="m-radio">
							<input type="radio" name="id_tipo_operacao" id="tipo_operacao_exp" value="2" <?=($dados['id_tipo_operacao']=='2') ? 'checked="checked"' : ''?> disabled="disabled"> Expedição (Caminhão Vazio)
							<span></span>
						</label>								
					</div>					
				</div>
			</div>					
			<div class="form-group m-form__group row">									
				<div class="col-lg-12">
					<label>Empresa</label>
					<?php if( $tipo_acesso == 'administrador geral' || $tipo_acesso == 'administrador empresas' ){ ?>						
						<div class="m-input-icon m-input-icon--right">
							<select  name="id_empresa" id="id_empresa" class="form-control m-input" required>
								<option value="">	Selecione uma empresa 	</option>
							<?php foreach($empresas as $empresa) { ?>
								<option value="<?php echo $empresa['id']; ?>" <?php echo ($dados['id_empresa'] == $empresa['id']) ? 'selected=selected' : ''; ?> >
									<?php echo strtoupper($empresa['razao_social']); ?>										
								</option>
							<?php } ?>
							</select>
							<span class="m-input-icon__icon m-input-icon__icon--right">
								<span><i class="la la-building"></i></span>
							</span>						
						</div>
					<?php }else{ ?>
					<h4 class="m--font-warning"><?php echo $empresa; ?>	</h4>
					<input type="hidden" name="id_empresa" id="id_empresa" value="<?php echo $dados['id_empresa']; ?>">
					<?php } ?>
					<input type="hidden" name="id_agenda" id="id_agenda"  value="<?php echo $dados['id']; ?>">
				</div>	
			</div>			
			<div class="form-group m-form__group row">
				<div class="col-lg-6">
					<label id>Data</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="data" id="data" class="form-control m-input datepicker" required value="<?php echo $dados['dt_agenda']; ?>"  />
						<input type="hidden" id="data_atual" value="<?php echo date('d/m/Y'); ?>"  />
						<span class="m-input-icon__icon m-input-icon__icon--right">
							<span><i class="la la-calendar"></i></span>
						</span>						
					</div>
				</div>
				<div class="col-lg-6">
					<label >Placa</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required	name="placa" id="placa" value="<?php echo $dados['placa']; ?>" class="form-control m-input placa" placeholder="">						
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-truck"></i></span>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group m-form__group row">
				<div class="col-lg-6" >
					<label class="">CPF</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="cpf" id="cpf" class="form-control m-input" placeholder="" required value="<?php echo $dados['cpf']; ?>">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
					</div>	
				</div>
				<div class="col-lg-6" id="campo_ie">
					<label class="">CNH</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="cnh" id="cnh" class="form-control m-input" placeholder="" required value="<?php echo $dados['nr_cnh']; ?>">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-id-card-o"></i></span></span>
					</div>	
				</div>	
			</div>	  
			<div class="form-group m-form__group row">	
				<div class="col-lg-6">
					<label>Motorista</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text"  name="motorista" id="motorista" class="form-control m-input" placeholder="" value="<?php echo $dados['nome']; ?>"  />
						<input type="hidden"  name="id_motorista" id="id_motorista" class="form-control m-input" placeholder="" value="<?php echo $dados['id_motorista']; ?>"  />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>							
				</div>			
				<div class="col-lg-6">
					<label>Telefone:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="telefone" id="telefone" class="form-control m-input" placeholder="Insira um telefone" required value="<?php echo $dados['fone']; ?>" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
					</div>							
				</div>
			</div>
			<hr/>
			<div class="form-group m-form__group row">	
				<div class="col-lg-3">
					<label>Nota Fiscal</label>
					<div class="m-input-icon m-input-icon--right">
						<a href="<?php echo base_url('bootstrap/nfe_agendamento/'.$dados['arquivo'])?>" target="blank"><?php echo $dados['nr_nf'].'-'.$dados['serie_nf']; ?></a>
						<span class="m-input-icon__icon m-input-icon__icon--right" style="right: 40%;"><span><i class="la la-pencil editar_xml" title="alterar nota fiscal" style="color: #ffcc00;"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-9" id="div_arquivo_xml" style="display: none;">
					<label>Nota Fiscal (XML)</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="file" id="nfe" class="form-control m-input" placeholder="Selecione uma nota fiscal" value="" accept=".xml" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>							
				</div>
			</div>
			<div class="form-group m-form__group row">	
				<div class="col-lg-12">
					<table class="table" id="tabela_produtos" >
						<thead>
							<th>Nº Pedido</th>
							<th>Produto</th>
							<th>Embalagem</th>
							<th>Quant.(KG)</th>
						</thead>
						<tbody id="produtos_xml">					
						<?php foreach($agendamento_itens as $item){	
								$peso = explode('.',$item['peso']);
							?>
							<tr>
								<td>
									<input type="text" class="form-control" name="nr_pedido[]" value="<?php echo $item['nr_pedido']; ?>" />
									<input type="hidden" class="form-control" name="id_agenda_item[]" value="<?php echo $item['id']; ?>" />
								</td>
								<td><input type="text" class="form-control" name="mercadoria[]" required value="<?php echo $item['descricao']; ?>" />	
								</td> 
								<td>
									<select class="form-control" name="embalagem[]" >
										<option value="">Selecione</option>
										<?php foreach($embalagens as $emb){?>
											<option value="<?=$emb['id']; ?>" <?php echo ($emb['id']==$item['id_embalagem']) ? 'selected=selected' : ''?>>
												<?=$emb['no_embalagem']?>
											
											</option>
										<?php } ?>
									</select>
								</td>
								<td><input type="text" class="form-control qtd" name="qtd[]" required value="<?php echo $peso[0]; ?>" /></td> 
							</tr>
							<?php } ?> 
						</tbody>
					</table>
					<div id="div-nfe" style="display: none;">

					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="form-group row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">
								Enviar
							</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>
					</div>
				</div>
			</div>					
		</div>		
	</div>
</form>
</div>
		

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Orçamento solicitado com sucessoo, em breve entraremos em contato!',
                type: 'success'
            }).then(function() {
                //window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>