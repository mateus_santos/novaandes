<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						<i class="m-menu__link-icon la la-ship"></i> Portos <a href="<?php echo base_url('AreaAdministrador/cadastraPortos')?>" class="" style="color: #5867dd; font-weight: bold;">
									<i class="la la-plus-circle" style="font-size: 38px;"></i>
								</a>
					</h3>
				</div>
			</div>
			<div class="col-md-4 m-portlet__head-caption">													
				
			</div>					
		</div>
		<div class="m-portlet__body">

			<!--begin: Datatable -->
			<table class="" id="html_table">
				<thead>
					<tr>
						<th title="id" style="text-align: center;" >ID</th>
						<th title="abreviação">Abreviação</th>												
						<th title="descricao">Descrição</th>
						<th title="Pais">País</th>
						<th title="">Ações</th>
					</tr>
				</thead>
				<tbody>
					
					<?php foreach($dados as $dado){ ?>
					<tr>
						<td style="text-align: center;"><?php echo $dado['id']; ?></td>
						<td><?php echo $dado['abreviacao']; ?></td>						
						<td><?php echo $dado['descricao']; ?></td>
						<td><?php echo $dado['pais']; ?></td>						
						<td data-field="Actions" class="m-datatable__cell">
							<span style="overflow: visible; width: 110px;">								
								<a href="<?php echo base_url('AreaAdministrador/editarPortos/'.$dado['id']); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" porto_id="<?php echo $dado['id']; ?>" >
									<i class="la la-edit"></i>
								</a>
								<button id="excluir" onclick="excluirPorto(<?php echo $dado['id']; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" porto_id="<?php echo $dado['id']; ?>">
									<i class="la la-trash"></i>
								</button>								
							</span>
						</td>
					</tr>
					<?php } ?>
					
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>		
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']); } ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
            title: "OK!",
            text: 'Cadastro realizado com sucesso!',
            type: "success"
        }).then(function() {
		 
		}); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>