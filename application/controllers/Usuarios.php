<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	public function __construct() {
		parent::__construct();

		// carrega a model a ser utilizada neste controller
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->library('session');
		$this->load->helper('form');
	}


	public function index()
	{
		$this->load->view('auth/login');
	}

	public function login()
	{
		$this->load->view('auth/login');
	}

	public function loggout()
	{
		$this->session->sess_destroy();
		$this->load->view('auth/login');
	}

	public function autenticacao()
	{
		$result = $this->usuariosM->autenticacao(stripslashes($_POST['email']), stripslashes($_POST['senha']));
		
		if( count($result) > 0 ){

			$newdata = array(	'usuario_id'		=> 	$result[0]->id,
								'nome'  			=>	$result[0]->nome,
								'email'     		=>	$result[0]->email,
								'telefone'     		=>	$result[0]->telefone,
								'tipo_acesso'		=>	$result[0]->tipo_acesso,
								'empresa_id'		=>	$result[0]->empresa_id,
								'foto'				=>	$result[0]->foto,
								'celular_usuario'	=>	$result[0]->celular,
								'telefone_usuario'	=>	$result[0]->telefone_usuario,
								'subtipo_acesso'	=> 	$result[0]->subtipo_cadastro,
								'razao_social'		=> 	$result[0]->razao_social,
								'logged_in' 		=> 	TRUE 	);

			$this->session->set_userdata($newdata);			
			$area = explode(' ',$result[0]->tipo_acesso);

			if (count($area) >= 2 ){
				if($area[1] == 'geral'){
					$area_acesso = ucfirst(trim($area[0]));
				}else{
					$area_acesso = ucfirst(trim($area[0])).ucfirst(trim($area[1]));
				}
			}else{
				$area_acesso = ucfirst($result[0]->tipo_acesso);				
			}
						
			redirect(base_url('Area'.$area_acesso));
			//$this->load->view('Area'.ucfirst($result[0]->tipo_acesso));

		}else{
			$this->session->set_flashdata('erro', 'Dados inválidos!');
			$this->load->view('auth/login');

		}	
			
	}		

	public function esqueciMinhaSenha()
	{		
		
		if( $this->input->post() )
		{
			$cpf 	=	$this->input->post('cpf');
			$email 	=	$this->input->post('email');			
			$result	= 	$this->usuariosM->verificaUsuario(stripslashes($cpf),stripslashes($email));
			
			if( count($result) < 1)
			{
				$this->session->set_flashdata('erro', 'Dados inválidos!');
			}else{
				$url_encode = base_url('usuarios/resetar/'.base64_encode($result[0]->id).'/'.base64_encode($cpf));
				
				if($this->enviaLinkReset($email, $url_encode, $result[0]->nome )) {

					$this->session->set_flashdata('sucesso', 'Sucesso!');
				}else{
					$this->session->set_flashdata('erro', 'Não enviou email!');
				}	
			}			
			
		}

		$this->load->view('auth/esqueci-minha-senha');
	}

	public function resetar($id = NULL,$cpf = NULL)
	{			
		
		if( $this->input->post() )
		{
			
			if($this->usuariosM->redefinirSenha($this->input->post('id'),$this->input->post('senha')))
			{
				$this->session->set_flashdata('sucesso', 'Sucesso!');
			}else{
				$this->session->set_flashdata('erro', 'Não atualizou!');
			}	
		}	
		$id 	=	base64_decode($id);
		$cpf 	=	base64_decode($cpf);	
		$params	= 	array('id' => $id);				
		$this->load->view('auth/resetar-senha',$params);	
		
	}	

	private function enviaLinkReset($email, $url_encode,$nome)
	{
		$this->load->library('email');
		
		$body = '<html>
		<head>
		<title>Resetar senha</title>
		</head>
		<body style="margin:0 auto; border-radius: 10px; border: 2px solid #ffcc00; text-align: center; background: #000;"> 						
		<div style="margin: 0 auto; padding: 25px;">
		<img src="http://www.wertco.com.br/wertcofundoescuro.png" />

		<h2></h2>

		<div style="">						
		<p style="color: #fff; font-size:18px;LINE-HEIGHT: 40px;">Olá '.$nome.' <br/>Recebemos um pedido de redefinição de sua senha.<br/>
		Por favor, clique no botão abaixo e siga as instruções para criar uma nova senha.
		</p>
		<div style="    height: 30px;    width: 188px;    background: #ffcc00;    text-align: center;    margin: 0 auto;">
		<a href="http://www.wertco.com.br/'.$url_encode.'" style="text-transform: uppercase;font-size: 16px;color: #000;text-decoration: none;text-align: center;    display: block;width: 160px;font-family: Helvetica,Arial,sans-serif;font-weight: bold;margin: 0px auto;padding-top: 7px;" >Redefir senha</a>	
		</div>
		</div>
		</div>	

		</body>

		</html>';
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);

		$result = $this->email
		->from('webmaster@wertco.com.br')
		    //->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
		->to($email)
		->subject('WERTCO - Redefinição de Senha')
		->message($body)
		->send();

		return $result;		
	}

	public function cadastrar(){

		$usuario = $this->input->post();
		$usuario['senha'] = md5(md5($usuario['senha']));
		$usuario['ativo'] = 1;

 		$id = $this->usuariosM->add($usuario);

		if($id){
			echo json_encode(array( 'titulo' 	=> 	'Usuário Cadastrado',
									'texto' 	=> 	'Usuário cadastrado com sucesso',
									'tipo' 		=> 	'success',
									'id' 		=> 	$id));
		} else {
			echo json_encode(array( 'titulo' 	=> 	'Usuário não Cadastrado',
									'texto' 	=> 	'Não foi possível cadastrar o usuário',
									'tipo' 		=> 	'error',
									'id' 		=> 	$id));
		}
	}
	
}