<?php

class PortosModel extends CI_Model {

	public function add ($data) {
        
	    return $this->db->insert('portos', $data);		

	}

    public function select(){
        $sql =  "SELECT p.*, pa.nome_pt as pais 
                FROM    portos p
                INNER JOIN pais pa ON pa.id = p.pais_id";
        return $this->db->query($sql)->result_array();   
    }

    public function getPortosById($id){

        $sql =  "SELECT * FROM portos where id =".$id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }   

    public function excluirPorto($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('portos')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaPorto($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('portos', $dados)){
            return true;
        }else{
            return false;
        }

    }

}
?>