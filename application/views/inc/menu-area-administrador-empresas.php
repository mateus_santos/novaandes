<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministradorEmpresas/editarCliente');?>">
		<i class="m-menu__link-icon la la-user" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>					
<li class="m-menu__item " aria-haspopup="true" >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministradorEmpresas/gestaoEmpresas');?>">
		<i class="m-menu__link-icon la la-building-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gestão de Clientes</span>
			</span>
		</span>
	</a>
</li>			
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministradorEmpresas/gestaoUsuarios');?>">
		<i class="m-menu__link-icon fa fa-users" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gestão de Usuários</span>
			</span>
		</span>
	</a>
</li> 
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaClientes/agendamentos/');?>">
		<i class="m-menu__link-icon la la-truck" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Agendamentos</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoCotas');?>">
		<i class="m-menu__link-icon fa fa-pie-chart" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Cotas por Cliente</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioAgendamento/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório Caminhões Agendados</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioEstacionados/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório Caminhões no Estacionamento</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/relatorioSaida/');?>">
		<i class="m-menu__link-icon fa fa-file-text-o" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório de Saída</span>
			</span>
		</span>
	</a>
</li>