$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('tbody').each(function(){$(this).find('tr:even').css('background-color','#f0f0f0')});
	$('.valor').mask('#.##0,00', {reverse: true});
	$('#fornecedor_id').bind('change', function(){
		var razao_social = $(this).find('option:selected').text();
		$.ajax({
		    method: "POST",
		    url: base_url+'AreaAdministrador/verificaEmailFornec',
		    async: true,
		    data: { empresa_id     :   $(this).val() }
		}).done(function( data ) {
		    var dados = $.parseJSON(data);		    
		    $('#email').val(dados[0].email);
		    $('#fornecedor').val(razao_social);
		    $('.e-mail_esconde').fadeIn('slow');
		}); 
	});

	$('.valor').bind('focusout', function(){
		$.ajax({
		    method: "POST",
		    url: base_url+'AreaAdministrador/alterarValorCotacao',
		    async: true,
		    data: { valor     	:   $(this).val(),
		    		cotacao_id 	:  	$(this).attr('cotacao_id') }
		}).done(function( data ) {
		    var dados = $.parseJSON(data);		    
		    if(dados.retorno == 'sucesso'){
		    	swal({
                    title: "Ok!",
                    text:  "Valor da Cotação alterada com sucesso",
                    type:  'success'
                }).then(function() {
                    
                });  
		    }else{
		    	swal({
                    title: "Atenção!",
                    text:  "Aconteceu algum problema, entre em contato com o suporte técnico",
                    type:  'warning'
                }).then(function() {
                    
                });
		    }

		}); 
	});

	$('#solicitar_cotacao').bind('click', function(){

		var email 		= $('#email_solicitacao').val();
		var fornecedor 	= $(this).attr('fornecedor');
		var frete_id 	= $(this).attr('frete_id');
		var cotacao_id 	= $(this).attr('cotacao_id');

		$.ajax({
		    method: "POST",
		    url: base_url+'AreaAdministrador/enviaSolicitacaoFrete',
		    async: true,
		    data: { email 		: 	email,
		    		fornecedor 	: 	fornecedor,
		    		frete_id 	: 	frete_id,
		    		cotacao_id 	: 	cotacao_id 	}
		}).done(function( data ) {
		    var dados = $.parseJSON(data);		    
		    if(dados.retorno == 'sucesso'){
		    	swal({
                    title: "Ok!",
                    text:  "E-mail enviado para o fornecedor",
                    type:  'success'
                }).then(function() {
                	$("#modal_solicita_cotacao").modal('hide');
    
                }); 
		    }else{
		    	swal({
                    title: "Atenção!",
                    text:  "Aconteceu algum problema, entre em contato com o suporte técnico",
                    type:  'warning'
                }).then(function() {
                    
                });
		    }

		});
	});

}); 

function solicita_email(id, fornecedor, fornecedor_id, frete_id){

	$('#titulo_fornecedor').text('');
	$('#email_solicitacao').val('');
	$.ajax({
	    method: "POST",
	    url: base_url+'AreaAdministrador/buscaEmailFornec',
	    async: true,
	    data: { id : fornecedor_id }
	}).done(function( data ) {
	    var dados = $.parseJSON(data);		    
	    $('#email_solicitacao').val(dados.email);

	});

	$('#titulo_fornecedor').text(fornecedor);
	$('#solicitar_cotacao').attr('fornecedor',fornecedor);
	$('#solicitar_cotacao').attr('cotacao_id',id);
	$('#solicitar_cotacao').attr('frete_id',frete_id);
	$("#modal_solicita_cotacao").modal({
		show: true
	});
}

function listar_email(cotacao_id, fornecedor){
	
	$.ajax({
	    method: "POST",
	    url: base_url+'AreaAdministrador/listarEmailFrete',
	    async: true,
	    data: { cotacao_id : cotacao_id,
	    		fornecedor : fornecedor  }
	}).done(function( data ) {
	    var dados = $.parseJSON(data);		    
	    $('.lista_envios').empty();
	    $('.lista_envios').append(dados.retorno);
	    $("#modal_listar_envios").modal({
			show: true
		});
	});	

	$('#lista_fornecedor').text(fornecedor);

}