<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaClientes extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() ||  (($this->session->userdata('tipo_acesso') != 'clientes') && ($this->session->userdata('tipo_acesso') != 'administrador geral') && ($this->session->userdata('tipo_acesso') != 'administrador empresas')) ) {
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('AgendamentoModel', 'agendaM');		
		$this->load->model('MotoristaModel', 'motoristaM');
		$this->load->model('NotaFiscalModel', 'notaM');
		$this->load->helper('form');
		$this->load->helper('url');
	}
 
	public function index()
	{
		$parametros				=	$this->session->userdata();		
		$parametros['title']	=	"Área do Cliente";		
		$this->_load_view('area-administrador/index',$parametros);

	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){
			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-clientes/editar-cliente',	$parametros );	
			}

		}else{

			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-clientes/editar-cliente',	$parametros );	
		}
	
	}	

	public function agendamentos()
	{
		$parametros			 			=	$this->session->userdata();		
		$parametros['title']			=	"Agendamentos";		
		$empresa_id	 					= 	$this->session->userdata('empresa_id');

		if( $parametros['tipo_acesso'] == 'administrador geral'){
			$empresa_id = '';
			$parametros['empresas']		= 	$this->empresasM->getClientes();
		}

		if( $parametros['tipo_acesso'] == 'administrador empresas'){			
			$parametros['empresas']		= 	$this->empresasM->getClientesEmpresas($empresa_id);
		}

		$parametros['agendamentos'] 	= 	$this->agendaM->buscaAgendamentosDoDia($empresa_id, $parametros['tipo_acesso']);

		$this->_load_view('area-clientes/agendamentos/agendamentos',$parametros);

	}

	 public function retornaAgendamentosAjax(){

    	$parametros 	= 	$this->input->post();
    	$filtros = "";
    	if($parametros['dt_ini'] != '' & $parametros['dt_fim'] != ''){
    		$dt_ini = explode('/', $parametros['dt_ini']);
    		$dt_fim = explode('/', $parametros['dt_fim']);
    		$filtros.=" and agendamento.dt_agenda between '".$dt_ini[2]."-".$dt_ini[1]."-".$dt_ini[0]."' and '".$dt_fim[2]."-".$dt_fim[1]."-".$dt_fim[0]."'";
    	}	    
    	
    	if($parametros['placa'] != ''){
    		$filtros.=" and agendamento.placa = '".$parametros['placa']."'";
    	}
    	
    	if($parametros['nr_nf'] != ''){
			$filtros.=" and nota_fiscal.nr_nf = '".$parametros['nr_nf']."'";     				
    	}
    	
    	$empresa_id	 = 	$this->session->userdata('empresa_id');
		if( $this->session->userdata('tipo_acesso') == 'administrador geral'){
			$empresa_id = $parametros['empresa_id'];
		}

		$data['agendamentos']	= 	$this->agendaM->buscaAgendamentos($empresa_id, $filtros);
		$retorno 				= 	$this->load->view('/area-clientes/agendamentos/agendamentos-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));

    }

	public function cadastrarAgendamento()
	{
		$parametros			 		=	$this->session->userdata();		
		$parametros['title']		=	"Cadastrar Novo Agendamento";		
		$parametros['empresa'] 		= 	$this->session->userdata('razao_social');		
		$parametros['id_empresa'] 	= 	$this->session->userdata('empresa_id');
		if( $this->session->userdata('tipo_acesso') == 'administrador geral'){
			$parametros['id_empresa'] 	= 	'';
			$parametros['empresas']		= 	$this->empresasM->getClientes();
		}

		if( $this->session->userdata('tipo_acesso') == 'administrador empresas'){
			
			$parametros['empresas']		= 	$this->empresasM->getClientesEmpresas($parametros['id_empresa']);
		}
		$this->_load_view('area-clientes/agendamentos/cadastra-agendamento',$parametros);
	}

	/*
	*
	* Função upload da nota e validações de regras do agendamento
	* Method: AJAX/POST
	*
	*/

	public function uploadNfe()
	{
		
		$tipo 			= 	explode('.', $_FILES['file']['name']);
		$arquivo 		=	md5($_FILES['file']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
		
		$configuracao = array(
	        'upload_path'   	=> 	'./nfe_agendamento/',		        
	        'allowed_types' 	=> 	'xml',
	        'file_name'     	=> 	$arquivo
	    );      
		
	    $this->load->library('upload', $configuracao);
	    
	    if ($this->upload->do_upload('file')){
			//lê o xml e insere 	
			$xml=simplexml_load_file('./nfe_agendamento/'.$arquivo);
						
			if(isset($xml->protNFe->infProt->chNFe)){
				$verificaAgenda = $this->agendaM->verificaNotaAgendamento($xml->protNFe->infProt->chNFe);
			
				if( isset($verificaAgenda['id']) ){
					echo json_encode(array( 'retorno'		=> 'ja existe',
			    							'agendamento'	=> 	$verificaAgenda['id'],
			    							'placa'			=> 	$verificaAgenda['placa'] )	);	
				}else{

					$embalagens = $this->agendaM->buscaEmbalagem();
					$combo_embalagem = "";
					foreach( $embalagens as $embalagem ){
						$combo_embalagem.='<option value="'.$embalagem['id'].'">'.$embalagem['no_embalagem'].'</option>';
					}

					$arquivo_xml = "<input type='hidden' id='arquivo_xml_".$arquivo."' name='arquivo_xml[".$xml->NFe->infNFe->ide->nNF."][]' value='".$arquivo."' />";
					$nfe="";
					$nfe.= "<input type='hidden' name='nr_nf[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->ide->nNF."' />" ;
					$nfe.= "<input type='hidden' name='serie_nf[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->ide->serie."' />" ;
					$nfe.= "<input type='hidden' name='dt_emissao[".$xml->NFe->infNFe->ide->nNF."][]' value='".date('Y-m-d H:i:s', strtotime($xml->NFe->infNFe->ide->dhEmi))."' />" ;
					$nfe.= "<input type='hidden' name='no_emitente[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->emit->xNome."' />" ;
					$nfe.= "<input type='hidden' name='nr_doc_emitente[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->emit->CNPJ."' />" ;
					$nfe.= "<input type='hidden' name='no_destinatario[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->dest->xNome."' />" ;
					$nfe.= "<input type='hidden' name='cnpj_destinatario[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->dest->CNPJ."' />" ;
					$nfe.= "<input type='hidden' name='peso_bruto_total[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->vol->pesoB."' />" ;
					$nfe.= "<input type='hidden' name='peso_liq_total[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->vol->pesoL."' />" ;
					$nfe.= "<input type='hidden' name='quant_total[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->vol->qVol."' />" ;
					$nfe.= "<input type='hidden' name='unid_medida[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->vol->esp."' />" ;
					$nfe.= "<input type='hidden' name='valor[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->total->ICMSTot->vNF."' />" ;
					$nfe.= "<input type='hidden' name='vl_ipi[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->total->ICMSTot->vIPI."' />" ;
					$nfe.= "<input type='hidden' name='chave_acesso[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->protNFe->infProt->chNFe."' />" ;
					$nfe.= "<input type='hidden' name='nr_doc_transp[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->transporta->CNPJ."' />" ;
					$nfe.= "<input type='hidden' name='no_transp[".$xml->NFe->infNFe->ide->nNF."][]' value='".$xml->NFe->infNFe->transp->transporta->xNome."' />" ; 
					$html="";
					//teste de cfof
					$cfop = 0;
					foreach($xml->NFe->infNFe->det as $prod){
						// Se o CFOP da mercadoria iniciar diferente de 7, verifica se existe na tabela de cfop
						if(substr($prod->CFOP,0,1) != '7' ){
							
							$cfop_ = $this->agendaM->verificaCfop($prod->prod->CFOP);
							if( $cfop_['total_cfop'] == 0 ){
								$cfop++;
							}
						}	

						$html.='
								<tr>
									<td><input type="text" class="form-control" name="nr_pedido['.$xml->NFe->infNFe->ide->nNF.'][]" required /></td>
									<td><input type="text" class="form-control" name="mercadoria['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->xProd.'" />
										<input type="hidden" name="ncm['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->NCM.'" />
										<input type="hidden" name="ds_item['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->xProd.'" />
										<input type="hidden" name="qtde['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->qCom.'" />
										<input type="hidden" name="valor['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->vProd.'" />
										<input type="hidden" name="cfop['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.$prod->prod->CFOP.'" />
									</td> 
									<td>
										<select class="form-control" name="embalagem['.$xml->NFe->infNFe->ide->nNF.'][]" >
											<option value="">Selecione</option>
											'.$combo_embalagem.'
										</select>
									</td>
									<td><input type="text" class="form-control qtd" name="qtd['.$xml->NFe->infNFe->ide->nNF.'][]" required value="'.intval($prod->prod->qCom).'" /></td> 
								</tr>';
					}
					
					if($cfop > 0){
						echo json_encode(array('retorno'	=> 'cfop errado'));		
									
					}else{

				    	echo json_encode(array('retorno'	=> 'sucesso',
				    							'html' 		=> 	$html,
				    							'arquivo' 	=> 	$arquivo_xml,
				    							'nfe' 		=> 	$nfe	 )	);
				    }
			    }
			}else{
				echo json_encode(array('retorno' => 'Nfe errada' ));	
			}
	    }else{
	    	echo json_encode(array('retorno' => 'Erro de upload' ));
	    }

	}

	public function verificaCpfAgendamento()
	{
		$where = array( 'cpf' 	=> 	$this->input->post('cpf'),
						'cnh' 	=> 	'' );
		echo json_encode($this->agendaM->verificaCpfCnh( $where ) ); 
	}

	public function verificaCnhAgendamento()
	{
		$where = array(	'cpf' 	=> 	'',
						'cnh' 	=> 	$this->input->post('cnh') ); 
		echo json_encode($this->agendaM->verificaCpfCnh( $where )); 
	}

	/*
	*
	* Função para inserção de todo agendamento
	* Method: POST
	*
	*/
	public function inserirAgendamento()
	{
		//verifica se motorista já está cadastrado, senão cadastra o motorista
		if( ! $this->input->post('id_motorista') ){

			$insereMotorista = array(	'cpf' 		=> 	$this->input->post('cpf'),
										'nr_cnh' 	=> 	$this->input->post('cnh'),
										'nome'		=> 	$this->input->post('motorista'),
										'fone'		=> 	$this->input->post('telefone')	);

			$id_motorista = $this->motoristaM->insere($insereMotorista);
		}else{
			$id_motorista = $this->input->post('id_motorista');
		}

		$dt_agenda = explode('/',$this->input->post('data')); 
		//insere o agendamento
		$dadosAgenda = array(	'id_tipo_operacao' 	=> 	$this->input->post('id_tipo_operacao'),
								'dt_agenda' 		=> 	$dt_agenda[2]."-".$dt_agenda[1]."-".$dt_agenda[0],
								'id_empresa'		=> 	$this->input->post('id_empresa'),
								'placa'				=> 	strtoupper($this->input->post('placa')),
								'id_motorista'		=> 	$id_motorista,
								'id_terminal'		=> 	1,
								'id_usuario'		=> 	$this->session->userdata('usuario_id') );

		$id_agenda = $this->agendaM->insere($dadosAgenda);

		if($id_agenda != ''){
			//insere agendamento item			

			$i=0;
			$erro = 0;			
			//var_dump($this->input->post());
			if($this->input->post('id_tipo_operacao') == 2){
				$dadosItensAgenda = array(	'id_agenda' 	=> 	$id_agenda,
											'descricao'		=> 	$this->input->post('mercadoria'),
											'id_embalagem' 	=> 	0,
											'nr_pedido'		=> 	$this->input->post('nr_pedido'),
											'peso'			=> 	str_replace('.','',$this->input->post('qtd'))	);
										
				if(!$this->agendaM->insereItem($dadosItensAgenda)){
					$erro++;
				}
			}else{

				foreach( $this->input->post('nr_nf') as $index => $nr_nf ){
					//insere nota fiscal
					//echo $index.' - <br>';
					$i=0;
					foreach($nr_nf as $nr_nf){
						//echo $this->input->post('serie_nf')[$nr_nf][$i];die;					
						$dadosNf = array(	'id_agenda'			=> 	$id_agenda,
											'nr_nf' 			=> 	$nr_nf,
											'serie_nf'			=> 	$this->input->post('serie_nf')[$nr_nf][$i],
											'dt_emissao' 		=> 	$this->input->post('dt_emissao')[$nr_nf][$i],
											'no_emitente'		=> 	$this->input->post('no_emitente')[$nr_nf][$i],
											'nr_doc_emitente' 	=> 	$this->input->post('nr_doc_emitente')[$nr_nf][$i],	
											'no_destinatario'	=> 	$this->input->post('no_destinatario')[$nr_nf][$i],
											'cnpj_destinatario'	=> 	$this->input->post('cnpj_destinatario')[$nr_nf][$i],
											'peso_bruto_total'	=> 	$this->input->post('peso_bruto_total')[$nr_nf][$i],	
											'peso_liq_total'	=> 	$this->input->post('peso_liq_total')[$nr_nf][$i],
											'quant_total'		=> 	$this->input->post('quant_total')[$nr_nf][$i],
											'unid_medida'		=> 	$this->input->post('unid_medida')[$nr_nf][$i],
											'vl_ipi'			=> 	$this->input->post('vl_ipi')[$nr_nf][$i],
											'chave_acesso'		=> 	$this->input->post('chave_acesso')[$nr_nf][$i],
											'nr_doc_transp'		=> 	$this->input->post('nr_doc_transp')[$nr_nf][$i],
											'no_transp'			=> 	$this->input->post('no_transp')[$nr_nf][$i],
											'arquivo'			=> 	$this->input->post('arquivo_xml')[$nr_nf][$i]	 );
						
						
						$id_nf = $this->notaM->insere($dadosNf);
						if($id_nf != ''){
							$i = 0;	
							//insere itens do agendamento
							
							foreach($this->input->post('mercadoria')[$nr_nf] as $merc){

								$dadosItensAgenda = array(	'id_agenda' 	=> 	$id_agenda,
															'descricao'		=> 	$merc,
															'id_embalagem' 	=> 	$this->input->post('embalagem')[$nr_nf][$i],
															'nr_pedido'		=> 	$this->input->post('nr_pedido')[$nr_nf][$i],
															'peso'			=> 	str_replace('.','',$this->input->post('qtd')[$nr_nf][$i])	);
										
								$this->agendaM->insereItem($dadosItensAgenda);
								$i++;	

							}

							$i = 0;	
							foreach($this->input->post('ncm')[$nr_nf] as $ncm){
								$dadosItensNf = array(	'id_nota_fiscal'=> 	$id_nf,
														'ncm'			=> 	$ncm[$i],
														'ds_item' 		=> 	$this->input->post('ds_item')[$nr_nf][$i],
														'qtde'			=> 	$this->input->post('qtde')[$nr_nf][$i],
														'valor'			=> 	$this->input->post('valor')[$nr_nf][$i],
														'cfop'			=> 	$this->input->post('cfop')[$nr_nf][$i]	);

								$this->notaM->insereItem($dadosItensNf);
								$i++;	

							}
						}else{
							$erro++;
							
						}
						$i++;
					}
					$i++;
				}
				
				
			}
			if( $erro > 0){
				$this->session->set_flashdata('retorno', 'erro');	
				$this->session->set_flashdata('msg', 'Não inseriu a nota fiscal, entre em contato com o suporte técnico');
			    redirect('/AreaClientes/Agendamentos/');
			}
		}else{
			$this->session->set_flashdata('retorno', 'erro');	
			$this->session->set_flashdata('msg', 'Não inseriu o agendamento, entre em contato com o suporte técnico');	
 		    redirect('/AreaClientes/Agendamentos/');
		}		

		$this->session->set_flashdata('retorno', 'sucesso');	
 		redirect('/AreaClientes/Agendamentos/');

	}

	public function verificaHoraLimiteAgendamento()
	{
		//Configura o timezone a ser utilizado
   		date_default_timezone_set('America/Sao_Paulo');

		$dados = $this->agendaM->verificaCotasAgendamento($this->input->post('empresa_id'), $this->input->post('data'));
		
		if( $dados=='' ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			$hora_limite = substr_replace($dados['hr_limite_agenda'], ':', 2, 0);

			if( strtotime($hora_limite) <= strtotime(date('H:i')) ){
				echo json_encode(array(	'retorno' 		=> 'erro',
										'hora_limite' 	=> $hora_limite	));
			}else{
				echo json_encode(array('retorno' => 'sucesso'));
			}
		}

	}

	public function verificaCotasAgendamento()
	{
		
		$dados = $this->agendaM->verificaCotasAgendamento($this->input->post('empresa_id'), $this->input->post('dt_agendamento'));		
		//busca total agendandado		
		//$periodo_ini 	= 	$this->descobreIniPeriodo($this->input->post('dt_agendamento'));		
		//$periodo_fim 	= 	$this->input->post('dt_agendamento');
		$peso_agendado 	= 	$this->agendaM->buscaTotalAgendadoPeriodo($this->input->post('empresa_id'),$this->input->post('dt_agendamento') );		
		$total 			= 	str_replace('.','',$this->input->post('total'));
		// Soma o total agendado com o solicitado para agendar
		if(isset($peso_agendado['total'])){
			$total = $peso_agendado['total'] + $total;
		}
		//echo str_replace('.','',$dados['peso']) .'<'. intval($total);die;
		if(!isset($dados['peso'])){
			echo json_encode(array(	'retorno' 	=> 'erro',				
		            				'mensagem' 	=> 'Não foram inserido as cotas para você, entre em contato com o setor administrativo da Vanzin.'));
		}elseif( $dados['fl_sem_cota'] == 1 ){
			echo json_encode(array( 'retorno' 	=> 'sucesso'));

		}elseif(  str_replace('.','',$dados['peso']) < intval($total) ){
				
			echo json_encode(array( 'retorno' 	=> 'erro',				
		            				'mensagem' 	=> 'Peso agendado maior que o permitido para a empresa.') );

		}else{
			echo json_encode(array( 'retorno' 	=> 'sucesso'));
		}
	}
		
	private function descobreIniPeriodo($dt_agenda)
	{
		
		//Configura o timezone a ser utilizado
   		date_default_timezone_set('America/Sao_Paulo');
   		$data = date('Y-m-d');
		switch ( date('w', strtotime($dt_agenda)) ) {
			case 1:
				$data = $data;
				break;
			case 2:
				
				$data = date('Y-m-d', strtotime('-1 day', strtotime($data)));
				break;
			case 3:
				$data = date('Y-m-d', strtotime('-2 days', strtotime($data)));
				break;
			case 4:
				$data = date('Y-m-d', strtotime('-3 days', strtotime($data)));
				break;
			case 5:
				$data = date('Y-m-d', strtotime('-4 days', strtotime($data)));
				break;
			case 6:
				$data = date('Y-m-d', strtotime('-5 days', strtotime($data)));
				break;							
		}

		return $data;

	}

	public function editarAgendamento($id_agenda)
	{
		$parametros			 				=	$this->session->userdata();
		$parametros['title']				=	"Editar Agendamento";
		$parametros['empresa'] 				= 	$this->session->userdata('razao_social');
		$parametros['id_empresa'] 			= 	$this->session->userdata('empresa_id');
		if( $this->session->userdata('tipo_acesso') == 'administrador geral'){
			$parametros['id_empresa'] 	= 	'';
			$parametros['empresas']		= 	$this->empresasM->select();
		}
		if( $parametros['tipo_acesso'] == 'administrador empresas'){			
			$parametros['empresas']		= 	$this->empresasM->getClientesEmpresas($parametros['id_empresa']);
		}		
		$parametros['dados']				= 	$this->agendaM->buscaAgendamentoPorId($id_agenda);
		$parametros['agendamento_itens'] 	= 	$this->agendaM->buscaItensAgendamento($id_agenda);
		$parametros['embalagens'] 			= 	$this->agendaM->buscaEmbalagem();
		$this->_load_view('area-clientes/agendamentos/editar-agendamento',$parametros);
	}

	public function atualizaAgendamento()
	{
		
		//verifica se motorista já está cadastrado, senão cadastra o motorista
		if( ! $this->input->post('id_motorista') ){

			$insereMotorista = array(	'cpf' 		=> 	$this->input->post('cpf'),
										'nr_cnh' 	=> 	$this->input->post('cnh'),
										'nome'		=> 	$this->input->post('motorista'),
										'fone'		=> 	$this->input->post('telefone')	);

			$id_motorista = $this->motoristaM->insere($insereMotorista);
		}else{
			$id_motorista = $this->input->post('id_motorista');
		}		
		//atualiza o agendamento
		$dt_agenda = explode('/',$this->input->post('data'));
		$dadosAgenda = array(	'id'			=> 	$this->input->post('id_agenda'),
								'dt_agenda' 	=> 	$dt_agenda[2]."-".$dt_agenda[1]."-".$dt_agenda[0],
								'id_empresa'	=> 	$this->input->post('id_empresa'),
								'placa'			=> 	strtoupper($this->input->post('placa')),
								'id_motorista'	=> 	$id_motorista,
								'id_terminal'	=> 	1,
								'id_usuario'	=> 	$this->session->userdata('usuario_id') );

		$this->agendaM->atualiza($dadosAgenda);
		// veerifica se foi inserido uma nova nota
		if( $this->input->post('id_agenda_item')[0] != ''  ){
			$i = 0;
			foreach($this->input->post('mercadoria') as $merc){
				$dadosItensAgenda = array(	'id' 			=> 	$this->input->post('id_agenda_item')[$i],
											'id_agenda' 	=> 	$this->input->post('id_agenda'),
											'descricao'		=> 	$merc,
											'id_embalagem' 	=> 	$this->input->post('embalagem')[$i],
											'nr_pedido'		=> 	$this->input->post('nr_pedido')[$i],
											'peso'			=> 	str_replace('.','',$this->input->post('qtd')[$i])		);

				$this->agendaM->atualizaItem($dadosItensAgenda);
				$i++;	

			}
		}else{
			//excluir itens
			if($this->agendaM->excluirItens($this->input->post('id_agenda'))){
			//insere agendamento item
				$i = 0;
				foreach($this->input->post('mercadoria') as $merc){
					$dadosItensAgenda = array(	'id_agenda' 	=> 	$this->input->post('id_agenda'),
												'descricao'		=> 	$merc,
												'id_embalagem' 	=> 	$this->input->post('embalagem')[$i],
												'nr_pedido'		=> 	$this->input->post('nr_pedido')[$i],
												'peso'			=> 	$this->input->post('qtd')[$i]	);

					if(!$this->agendaM->insereItem($dadosItensAgenda)){
						echo '<pre>Erro!';
						die;
					}
					$i++;	

				}

			}

			if($this->notaM->excluir($this->input->post('id_agenda'))){
				//insere nota fiscal
				$dadosNf = array(	'id_agenda'			=> 	$this->input->post('id_agenda'),
									'nr_nf' 			=> 	$this->input->post('nr_nf'),
									'serie_nf'			=> 	$this->input->post('serie_nf'),
									'dt_emissao' 		=> 	$this->input->post('dt_emissao'),
									'no_emitente'		=> 	$this->input->post('no_emitente'),
									'nr_doc_emitente' 	=> 	$this->input->post('nr_doc_emitente'),	
									'no_destinatario'	=> 	$this->input->post('no_destinatario'),
									'cnpj_destinatario'	=> 	$this->input->post('cnpj_destinatario'),
									'peso_bruto_total'	=> 	$this->input->post('peso_bruto_total'),	
									'peso_liq_total'	=> 	$this->input->post('peso_liq_total'),
									'quant_total'		=> 	$this->input->post('quant_total'),
									'unid_medida'		=> 	$this->input->post('unid_medida'),
									'vl_ipi'			=> 	$this->input->post('vl_ipi'),
									'chave_acesso'		=> 	$this->input->post('chave_acesso'),
									'nr_doc_transp'		=> 	$this->input->post('nr_doc_transp'),
									'no_transp'			=> 	$this->input->post('no_transp'),
									'arquivo'			=> 	$this->input->post('arquivo_xml')	 );

				$id_nf = $this->notaM->insere($dadosNf);
				if($id_nf != ''){
					$i = 0;
					foreach($this->input->post('ncm') as $ncm){
						$dadosItensNf = array(	'id_nota_fiscal'=> 	$id_nf,
												'ncm'			=> 	$ncm,
												'ds_item' 		=> 	$this->input->post('ds_item')[$i],
												'qtde'			=> 	$this->input->post('qtde')[$i],
												'valor'			=> 	$this->input->post('valor')[$i],
												'cfop'			=> 	$this->input->post('cfop')[$i]	);

						$this->notaM->insereItem($dadosItensNf);
						$i++;	

					}
				}else{
					$this->session->set_flashdata('retorno', 'erro');	
					$this->session->set_flashdata('msg', 'Não inseriu a nota fiscal, entre em contato com o suporte técnico');
		 		    redirect('/AreaClientes/Agendamentos/');
				}
			}
					
		}

		$this->session->set_flashdata('retorno', 'sucesso');	
 		redirect('/AreaClientes/Agendamentos/');


	}

	public function excluirAgendamento()
	{
		
		$verificaEntrada = $this->agendaM->verificaEntradaPatio($this->input->post('id'));
		//Caso não tenha acesso ao patio, pode excluir
		if( $verificaEntrada['total'] > 0  ){
			echo json_encode(array('retorno' => false,
									'titulo' => "Ops!",
									'texto'	=> 	"Este agendamento já deu entrada no pátio, impossível realizar a exclusão.",
									'tipo' 	=> 	"warning"	 	 ));
		}else{
			if( $this->agendaM->excluir($this->input->post('id')) ){

				echo json_encode(array(	'retorno' 	=> 	true,
										'titulo' 	=> 	"Ok!",
										'texto'		=> 	"Agendamento excluído com sucesso.",
										'tipo' 		=> 	"success"	 	 ));
			}else{
				
				echo json_encode(array(	'retorno' 	=> 	false,
										'titulo' 	=> 	"Atenção!",
										'texto'		=> 	"Problema ao excluir, entre em contato com o suporte técnico.",
										'tipo' 		=> 	"danger"	 	 ));
			}	

		}

	}

	public function atualizaMotorista()
	{
		$update = array(	'id' 	=> 	$this->input->post('id_motorista'),
							'fone' 	=> 	$this->input->post('fone'),
							'nome' 	=> 	$this->input->post('nome')	 );

		if( $this->motoristaM->atualiza($update) ){
			echo json_encode(array('retorno' => 'sucesso'));	
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}
							
	}

}
