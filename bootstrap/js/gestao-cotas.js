$(document).ready(function(){
	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	
	$("#tab_resultado").empty();
	$("#tab_resultado").css('border','0px');
	$("#tab_botoes").css('display','none');
	$('#id_semana').change(function(){
		var dup = $('#duplicou').val();
			
		if (dup != 1 & $('#id_semana').val()!='') {
			busca_dia($('#id_semana').val());
			$('#duplicou').val('0');
			$('#msg').html('');
		}else if($('#id_semana').val()==''){
			$("#tab_resultado").empty();
		}
	});
	
	$('#limpa').click(function(){ 
		$('form :input').val('');
		$('.checa').prop("checked",false);
		
	});
	
	$('#duplica').click(function(){
		$('#id_semana').val('');
		$('#duplicou').val('1');
		$('#msg').html('Registro Duplicado');
		
	});


	
	var mask = function (val) {
		val = val.split(":");
		return (parseInt(val[0]) > 19)? "HZ:M0" : "H0:M0";
	}

	pattern = {
		onKeyPress: function(val, e, field, options) {
			field.mask(mask.apply({}, arguments), options);
		},
		translation: {
			'H': { pattern: /[0-2]/, optional: false },
			'Z': { pattern: /[0-3]/, optional: false },
			'M': { pattern: /[0-5]/, optional: false}
		}
	};


});

function busca_dia(id_semana){
	
	var v_id_empresa 		= '';
	var v_razao_social 		= '';
	var v_dia_semana 		= '';
	var v_hr_limite_acesso	= '';
	var v_hr_limite_agenda	= '';
	var v_peso 				= '';
	var v_fl_sem_cota 		= '';
	
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/gestaoCotasAjax/',
		async: true,
        data: { id_semana    :   id_semana }
	}).done(function(data){
		var dados = $.parseJSON(data);
		
		$("#tab_resultado").empty();
		
		var total_linhas = dados.retorno.cotas.length;
		
		if (total_linhas > 0){
			$("#tab_botoes").css('display','block');
			
			var div_linha  = "<div class='linha_titulo' id='linha_titulo'>";
				div_linha += "<div class='coluna col_cliente'>";
				div_linha += "<label class='titulo'>Cliente</label>";
				div_linha += "</div>";
				div_linha += "<div class='coluna'>";
				div_linha += "<label class='titulo'>Horário Limite Acesso</label>";
				div_linha += "</div>";
				div_linha += "<div class='coluna'>";
				div_linha += "<label class='titulo'>Horário Limite Agendamento</label>";
				div_linha += "</div>";
				div_linha += "<div class='coluna'>";
				div_linha += "<label class='titulo'>Peso</label>";
				div_linha += "</div>";
				div_linha += "<div class='coluna'>";
				div_linha += "<label class='titulo'>Sem Cota</label>";
				div_linha += "</div>";
				div_linha += "</div>";
				$("#tab_resultado ").append(div_linha);
		}
		
		
		
		for(var i=0; i < total_linhas; i++) {
		
			v_id_empresa 		= dados.retorno.cotas[i].id;
			v_razao_social 		= dados.retorno.cotas[i].razao_social;
			v_dia_semana 		= dados.retorno.cotas[i].dia_semana;
			v_hr_limite_acesso	= dados.retorno.cotas[i].hr_limite_acesso;
			v_hr_limite_agenda	= dados.retorno.cotas[i].hr_limite_agenda;
			
			//V_conta_limite_Acesso = dados.retorno.cotas[i].hr_limite_acesso.length;
			
			//alert(v_hr_limite_agenda+' - '+V_conta_limite_Acesso);
			if (dados.retorno.cotas[i].peso == null) {
				v_peso = '';
			}else{
				v_peso = dados.retorno.cotas[i].peso;
			}
			
			if (dados.retorno.cotas[i].fl_sem_cota == null) {
				v_fl_sem_cota = '0';
			}else{
				v_fl_sem_cota 		= dados.retorno.cotas[i].fl_sem_cota;
			}
			
				var div_linha = "<div class='linha'>";
					div_linha += "<div class='coluna col_cliente'>";
					div_linha += "<label class='listagem'><div id='dv_razao_social'>"+v_razao_social+" </div>  <input type='hidden' name='id_empresa"+[i]+"' id='id_empresa' value='"+v_id_empresa+"' /> <input type='hidden' name='razao_social"+[i]+"' id='razao_social' value='"+v_razao_social+"' /> </label>";
					div_linha += "</div>";
					div_linha += "<div class='coluna'>";
					div_linha += "<div class='m-input-icon m-input-icon--right'>";
					div_linha += "<input type='text' required	name='hr_limite_acesso"+[i]+"' id='hr_limite_acesso' class='form-control m-input hora' placeholder='' required value='"+v_hr_limite_acesso+"'>";
					div_linha += "<span class='m-input-icon__icon m-input-icon__icon--right'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span>";
					div_linha += "</span>";
					div_linha += "</div>";
					div_linha += "</div>";
					div_linha += "<div class='coluna'>"; 
					div_linha += "<div class='m-input-icon m-input-icon--right'>";
					div_linha += "<input type='text' required	name='hr_limite_agenda"+[i]+"' id='hr_limite_agenda' class='form-control m-input hora' placeholder='' required value='"+v_hr_limite_agenda+"'>";
					div_linha += "<span class='m-input-icon__icon m-input-icon__icon--right'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span>";
					div_linha += "</span>";
					div_linha += "</div>";
					div_linha += "</div>";
					div_linha += "<div class='coluna'>";
					div_linha += "<div class='m-input-icon m-input-icon--right'>";
					div_linha += "<input type='text' required	name='peso"+[i]+"' id='peso' class='form-control m-input peso' placeholder='' required value='"+v_peso+"'>";
					div_linha += "<span class='m-input-icon__icon m-input-icon__icon--right'><span><i class='fa fa-balance-scale' aria-hidden='true'></i></span>";
					div_linha += "</span>";
					div_linha += "</div>";
					div_linha += "</div>";
					div_linha += "<div class='coluna'>";
					div_linha += "<input type='checkbox' 	name='fl_sem_cota"+[i]+"' id='fl_sem_cota"+[i]+"' class='form-control m-checkbox checa' value='"+v_fl_sem_cota+"'>";
					div_linha += "</div>";
					div_linha += "</div>";
			$("#tab_resultado ").append(div_linha);
			
			
			if (v_fl_sem_cota  == 1) { 
				$('#fl_sem_cota'+i).prop("checked",true);
			}
			
			if (v_hr_limite_acesso == null){
				$(".hora").mask("9999",pattern);
			}else{
				$(".hora").mask("99:99",pattern);
			}
			
			if (v_hr_limite_agenda == null){
				$(".hora").mask("9999",pattern);
			}else{
				$(".hora").mask("99:99",pattern);
			}

				
			
		}
		
		$(".peso").mask("999.999.999", { reverse : true});
		//incluir o total de linhas para o for na inserção
		$("#total_linhas").val(total_linhas);
		
		$('.checa').click(function(){ 
			if ( $(this ).is( ":checked" ) ){
				$(this).val('1');
			}else{
				$(this).val('0');
			}		
		});

		$('.hora').blur(function(){
			if(($(this).val().length < 5) & ($(this).val().length > 0)){
				swal({
					title: "Ops!",
					text: "Hora inválida, por favor preencha a hora por completo",
					type: 'warning'
				}).then(function() {
					
				});
				$(this).val(''); 	
			}
		});
	
	});
		
	
}




