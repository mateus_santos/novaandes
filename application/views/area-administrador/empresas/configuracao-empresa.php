<div class="m-content">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<a href="<?php echo base_url('areaAdministrador/chamados'); ?>"><i class="la la-arrow-left"></i></a>
					</span>
					<h3 class="m-portlet__head-text">	
						<a href="<?php echo base_url('areaAdministrador/chamados'); ?>">Voltar</a>&nbsp;&nbsp;
					</h3>
				</div>
			</div> 
		</div> 
		<!--begin::Form-->
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarChamado');?>" method="post" enctype="multipart/form-data">
			<input type="hidden" id="empresa_id" value="<?= $empresa['id']?>">
			<div class="col-lg-12" >
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 62px;">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>										
								<h2 class="m-portlet__head-label m-portlet__head-label--success">
									<span>
										Configuração de Automação do Cliente
									</span>
								</h2>
							</div>
						</div>								
					</div>
					<div class="m-portlet__body row">
						<div class="col-lg-12">
							<label>Cliente</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" value="<?=$empresa['razao_social']?>" class="form-control m-input" placeholder="" disabled>
							</div>
						</div>
					</div>
					<div class="m-portlet__body row">
						<div class="col-lg-8">
							<label>Concentrador</label>
							<div class="m-input-icon m-input-icon--right">
								<select id="concentrador_id" class="form-control selectpicker" required>
									<option selected disabled>Selecione</option>
									<?php foreach($concentradores as $concentrador){ ?>
										<option <?php if($configuracao['concentrador_id'] == $concentrador['id']) echo "selected";?> value="<?= $concentrador['id']; ?>"><?= $concentrador['modelo']; ?></option>												
									<?php } ?>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i onclick="show_modal_add_concentrador()" class="la la-plus "></i></span></span>
							</div>
						</div>
						<div class="col-lg-4">
							<label>Identificação</label>
							<div class="m-input-icon m-input-icon--right">
								<select id="flag_identificacao" class="form-control selectpicker" required>
									<option selected disabled>Selecione</option>
									<option <?php if($configuracao['flag_identificacao'] === "0") echo "selected";?> value="0">Não</option>
									<option <?php if($configuracao['flag_identificacao'] === "1") echo "selected";?> value="1">Sim</option>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i onclick="salvar_configuracao()" class="la la-save "></i></span></span>
							</div>
						</div>
					</div>
					<div id="configuracoes" <?php if(isset($configuracao) && $configuracao['flag_identificacao'] == 0) echo "style='display: none'";?>>
						<?php foreach($bombas as $bomba): ?>
							<?php for($i=0;$i<$bomba['quantidade'];$i++): ?>
								<?php $flag = 0; ?>
								<?php foreach($configuracao_bombas as $key => $configuracao_bomba): ?>
									<?php if($bomba['id'] == $configuracao_bomba['produto_id']):?>
										<div class="form-group m-form__group row col-lg-12" style="border-top: 1px #f0f0f0 dotted;">
											<div class="col-lg-3">
												<label>Modelo</label>
												<div class="m-input-icon m-input-icon--right">
													<input type="text" id="modelo_<?=$configuracao_bomba['produto_id'].'_'.$i?>" value="<?=$configuracao_bomba['modelo']?>" class="form-control m-input" placeholder="" disabled>
												</div>
											</div>	
											<div class="col-lg-3">
												<label>Número de série</label>
												<div class="m-input-icon m-input-icon--right" style="padding: 0">
													<input type="text" id="serie_<?=$configuracao_bomba['produto_id'].'_'.$i?>" value="<?=$configuracao_bomba['numero_serie']?>" class="form-control m-input" placeholder="">
												</div>
											</div>
											<div class="col-lg-4">
												<label>Identificador</label>
												<div class="m-input-icon m-input-icon--right">
													<select id="identificador_<?=$configuracao_bomba['produto_id'].'_'.$i?>" class="form-control selectpicker">
														<?php foreach($identificadores as $identificador) { ?>
															<option <?php if($configuracao_bomba['identificador_id'] == $identificador['id']) echo "selected"; ?> value="<?=$identificador['id'] ?>"><?=$identificador['modelo'] ?></option>
														<?php } ?>	
													</select>
													<span class="m-input-icon__icon m-input-icon__icon--right"><span><i onclick="show_modal_add_identificador('<?=$configuracao_bomba['produto_id'].'_'.$i?>')" class="la la-plus "></i></span></span>
												</div>
											</div>							
											<div class="col-lg-2"class="m-datatable__cell">
												<span style="position: absolute; bottom: 0; display: flex" id="acoes_<?=$configuracao_bomba['produto_id'].'_'.$i?>">		
													<?php if($i == 0): ?>	
														<a href="#" onclick="replicar_identificador('<?=$configuracao_bomba['produto_id']?>')" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Replicar">
															<i class="la la-reply-all"></i>
														</a>
													<?php endif; ?>
													<a href="#" id="botao_salvar_<?=$configuracao_bomba['produto_id'].'_'.$i?>" onclick="salvar_configuracao_bomba('<?=$configuracao_bomba['id']?>', '<?=$configuracao_bomba['produto_id'].'_'.$i?>')" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Salvar">
														<i class="la la-save"></i>
													</a>
													<a href="#" id="botao_remover_<?=$configuracao_bomba['produto_id'].'_'.$i?>" onclick="remover_configuracao_bomba('<?=$configuracao_bomba['id']?>', '<?=$configuracao_bomba['produto_id'].'_'.$i?>')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Remover">
														<i class="la la-remove"></i>
													</a>
												</span>
											</div>
										</div>
									<?php unset($configuracao_bombas[$key]); $flag = 1; break; endif; ?>
								<?php endforeach; ?>
								<?php if($flag) continue; ?>
									<div class="form-group m-form__group row col-lg-12" style="border-top: 1px #f0f0f0 dotted;">
										<div class="col-lg-3">
											<label>Modelo</label>
											<div class="m-input-icon m-input-icon--right">
												<input type="text" id="modelo_<?=$bomba['id'].'_'.$i?>"" value="<?=$bomba['modelo']?>" class="form-control m-input" placeholder="" disabled>
											</div>
										</div>	
										<div class="col-lg-3">
											<label>Número de série</label>
											<div class="m-input-icon m-input-icon--right">
												<input type="text" id="serie_<?=$bomba['id'].'_'.$i?>" class="form-control m-input" placeholder="">
											</div>
										</div>		
										<div class="col-lg-4">
											<label>Identificador</label>
											<div class="m-input-icon m-input-icon--right">
												<select id="identificador_<?=$bomba['id'].'_'.$i?>" class="form-control selectpicker">
													<?php foreach($identificadores as $identificador) { ?>
														<option value="<?=$identificador['id'] ?>"><?=$identificador['modelo'] ?></option>
													<?php } ?>	
												</select>
												<span class="m-input-icon__icon m-input-icon__icon--right"><span><i onclick="show_modal_add_identificador('<?=$bomba['id'].'_'.$i?>')" class="la la-plus "></i></span></span>
											</div>
										</div>
										<div class="col-lg-2"class="m-datatable__cell">
											<span style="position: absolute; bottom: 0; display: flex" id="acoes_<?=$bomba['id'].'_'.$i?>">
												<?php if($i == 0): ?>	
													<a href="#" onclick="replicar_identificador('<?=$bomba['id']?>')" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Replicar">
														<i class="la la-reply-all"></i>
													</a>
												<?php endif; ?>
												<a href="#" id="botao_salvar_<?=$bomba['id'].'_'.$i?>" onclick="salvar_configuracao_bomba(0, '<?=$bomba['id'].'_'.$i?>')" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Salvar">
													<i class="la la-save"></i>
												</a>
											</span>
										</div>
									</div>
							<?php endfor; ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>	
		</form>
	</div>
</div>

<div class="modal fade" id="modal_add_concentrador" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Novo Modelo de Concentrador</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">
					<div class="form-group m-form__group col-lg-12" >
						<label for="exampleSelect1">Modelo</label>							
						<input type="text" class="form-control m-input m-input--air sub" placeholder="Modelo do concentrador" id="concentrador" style="margin-bottom: 10px;float: left;" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="adicionar_concentrador()">Cadastrar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_add_identificador" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Novo Modelo de Identificador</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">
					<div class="form-group m-form__group col-lg-12" >
						<label for="exampleSelect1">Modelo</label>							
						<input type="text" class="form-control m-input m-input--air sub" placeholder="Modelo do identificador" id="identificador" style="margin-bottom: 10px;float: left;" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="adicionar_identificador()">Cadastrar</button>
			</div>
		</div>
	</div>
</div>