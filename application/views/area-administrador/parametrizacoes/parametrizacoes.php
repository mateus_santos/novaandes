<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						<i class="m-menu__link-icon la la-cogs"></i>&nbsp;&nbsp; Configurações do Sistema						
					</h3>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body">
			<!--begin: Search Form -->
			<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="form-group m-form__group row align-items-center">
							<div class="col-md-4">
								<div class="m-input-icon m-input-icon--left">
									<input type="text" class="form-control m-input m-input--solid" placeholder="Pesquisar..." id="generalSearch">
									<span class="m-input-icon__icon m-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>							
						</div>
					</div>								
				</div>
			</div>
			<!--end: Search Form -->
			<!--begin: Datatable -->
			<table class="m-datatable" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="id">ID</th>
						<th title="unidade">Unidade</th>										
						<th title="moeda">Moeda</th>
						<th title="">Ações</th>
					</tr>
				</thead>
				<tbody>
					<tr>					
						<td><?php echo $dados['id'];?></td>
						<td><?php echo $dados['unidade'];?></td>						
						<td><?php echo $dados['moeda'];?></td>
						<td data-field="Actions" class="m-datatable__cell">
							<span style="overflow: visible; width: 110px;">								
								<a href="<?php echo base_url('AreaAdministrador/editarParametrizacoes/'.$dados['id']); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" fretes_id="<?php echo $dados['id']; ?>" >
									<i class="la la-edit"></i>
								</a>
							</span>
						</td>
					</tr>					
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>				
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
            title: "OK!",
            text: 'Cadastro realizado com sucesso!',
            type: "success"
        }).then(function() {
		 	
		}); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>