-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 06-Jul-2021 às 22:29
-- Versão do servidor: 10.4.18-MariaDB
-- versão do PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `novaandes`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas`
--

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL,
  `razao_social` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `fantasia` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnpj` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT 'CNJP OU CPF',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `observacoes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cidade` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `insc_estadual` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo_cadastro_id` int(11) DEFAULT NULL,
  `dthr_cadastro` timestamp NULL DEFAULT current_timestamp(),
  `cep` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cartao_cnpj` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo_ibge_cidade` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fl_internacional` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `empresas`
--

INSERT INTO `empresas` (`id`, `razao_social`, `fantasia`, `cnpj`, `email`, `telefone`, `endereco`, `observacoes`, `cidade`, `estado`, `pais`, `bairro`, `insc_estadual`, `tipo_cadastro_id`, `dthr_cadastro`, `cep`, `cartao_cnpj`, `codigo_ibge_cidade`, `latitude`, `longitude`, `fl_internacional`) VALUES
(1, 'MIL CRIACAO E DESENVOLVIMENTO WEB LTDA', 'mil criação e desenv', '12.481.170/0001-14', 'teste@teste.com.br', '(53) 3028-3044', 'AV SAO FRANCISCO DE PAULA, 2722 ', NULL, 'PELOTAS', 'RS', 'Brasil', 'AREAL', '1123456', 3, '2021-06-03 22:35:54', '96080-730', 'C:\\xampp2\\htdocs\\novaandes/Consultas/Receita_Federal/Receita_12481170000114_03-06-2021.html', NULL, NULL, NULL, 0),
(2, 'NOVA ANDES CONSULTORIA DE NEGOCIOS E COMUNICACAO ESTRATEGICA EIRELI', 'NOVA ANDES NEGOCIACAO E COMUNICACAO ESTRATEGICA', '34.765.464/0001-00', 'administrativo@novaandes.com.br', '(53) 3284-8100/ (53) 8130-0212', 'R QUARAI, 2059 ', NULL, 'PELOTAS', 'RS', 'Brasil', 'LARANJAL', ' ', 1, '2021-06-07 00:54:02', '96090530', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_34765464000100_06-06-2021.html', NULL, NULL, NULL, 0),
(3, 'ARCINDO COLODA COMERCIO DE MADEIRAS EIRELI', 'MADCOL', '08.573.173/0001-00', 'adml@madcol.com.br', '(42) 3553-1379/ (42) 3553-1836', 'R MARCILIO AGUIAR PEREIRA, 105 ', NULL, 'BITURUNA', 'PR', 'Brasil', 'NSA SRA APARECIDA', '9086044584', 3, '2021-06-09 14:54:08', '84640000', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_08573173000100_09-06-2021.html', NULL, NULL, NULL, 0),
(4, 'MADEIREIRA BELO HORIZONTE LTDA', 'MADEIREIRA BELO HORIZONTE LTDA', '82.654.849/0001-43', 'bianca@madeireirabelohorizonte.com.br', '4234361299', 'R DOS IMIGRANTES, 895 ', NULL, 'IMBITUVA', 'PR', 'Brasil', 'JARDIM TANGARA', '21500913-09', 3, '2021-06-09 17:12:38', '84430000', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_82654849000143_09-06-2021.html', NULL, NULL, NULL, 0),
(5, 'REAL LAMINAS E COMPENSADOS LTDA', 'REAL LAMINAS E COMPENSADOS LTDA', '18.130.296/0001-20', 'INDUSTRIAL01@REALLAMINAS.COM.BR', '(47) 3364-0044', 'R ALBERTO KOGLIN, 4510 ', NULL, 'DONA EMMA', 'SC', 'Brasil', 'CENTRO', '00000000', 3, '2021-06-09 17:28:39', '89155000', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_18130296000120_09-06-2021.html', NULL, NULL, NULL, 0),
(6, 'BONARDI INDUSTRIA QUIMICA LTDA', 'BONARDI ADESIVOS', '08.775.142/0001-32', 'francisco.hess@bonardiquimica.com.br', '4136051595', 'R OLIMPIO CARDOSO, 729 ', NULL, 'COLOMBO', 'PR', 'Brasil', 'PARQUE INDUSTRIAL GUARAITUBA II', '90481190-40', 4, '2021-06-09 17:56:51', '83411110', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_08775142000132_09-06-2021.html', NULL, NULL, NULL, 0),
(7, 'REVIGLIO THOME E CIA LTDA', 'REVIGLIO THOME E CIA LTDA', '01.652.564/0001-71', 'madeirathome@hotmail.com', '(42) 3677-1566', 'ROD PR 170, S N KM 49', NULL, 'PINHAO', 'PR', 'Brasil', 'SAO SEBASTIAO', '90123232-60', 3, '2021-06-09 18:00:39', '85170000', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_01652564000171_09-06-2021.html', NULL, NULL, NULL, 0),
(8, 'BRASSERIE 35 INDUSTRIA DE BEBIDAS LTDA', 'BRASSERIE 35', '13.692.639/0001-27', '', '(53) 3028-9001', 'ROD BR 116, S N KM 483 CASA B', NULL, 'TURUCU', 'RS', 'Brasil', 'CENTRO', '462/0002940', 2, '2021-06-09 19:38:52', '96148000', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_13692639000127_09-06-2021.html', NULL, NULL, NULL, 0),
(9, 'razao social', 'fantasia', '123456', 'email@email.com.br', '+55 (53) 9840-73868', 'adadadasd ad asd as dsa d', NULL, 'cidade', 'EX', 'méxico', 'bairro', '', 1, '2021-06-23 21:08:45', '96085-000', '', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa_anexos`
--

CREATE TABLE `empresa_anexos` (
  `id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `descricao` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `anexo` text COLLATE utf8_unicode_ci NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `dthr_insercao` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `empresa_anexos`
--

INSERT INTO `empresa_anexos` (`id`, `empresa_id`, `descricao`, `anexo`, `usuario_id`, `dthr_insercao`) VALUES
(1, 3, 'PROFORMA ', 'acba89ae1366067af715a7c70f7ef2a6.pdf', 3, '2021-06-09 16:45:55');

-- --------------------------------------------------------

--
-- Estrutura da tabela `entregas`
--

CREATE TABLE `entregas` (
  `id` int(11) NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `entregas`
--

INSERT INTO `entregas` (`id`, `descricao`) VALUES
(1, 'Aproximadamente 50 dias');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estados`
--

CREATE TABLE `estados` (
  `id` int(11) NOT NULL,
  `codigo_uf` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `uf` char(2) NOT NULL,
  `regiao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `forma_pagto`
--

CREATE TABLE `forma_pagto` (
  `id` int(11) NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `forma_pagto`
--

INSERT INTO `forma_pagto` (`id`, `descricao`) VALUES
(1, 'forma de pagamento mais comusm');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fretes`
--

CREATE TABLE `fretes` (
  `id` int(11) NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `fretes`
--

INSERT INTO `fretes` (`id`, `descricao`) VALUES
(1, 'Frete a combinar');

-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `acao` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dthr_alteracao` timestamp NOT NULL DEFAULT current_timestamp(),
  `ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `logs`
--

INSERT INTO `logs` (`id`, `controller`, `model`, `acao`, `user_id`, `dthr_alteracao`, `ip`) VALUES
(1, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 2, '2021-06-07 00:54:02', '187.23.157.48'),
(2, 'Área Administrador | atualiza usuário', 'usuarios', 'EDIÇÂO', 2, '2021-06-07 00:54:22', '187.23.157.48'),
(3, 'Área Administrador | editar empresa', 'empresas', 'EDIÇÃO', 2, '2021-06-07 00:54:41', '187.23.157.48'),
(4, 'Área Administrador | ativa usuário', 'usuarios', 'EDIÇÂO', 2, '2021-06-07 22:29:41', '2804:4b0:10b0:fa00:193e:ebbd:8ee2:d24a'),
(5, 'Área Administrador | ativa usuário', 'usuarios', 'EDIÇÂO', 2, '2021-06-07 22:29:47', '2804:4b0:10b0:fa00:193e:ebbd:8ee2:d24a'),
(6, 'Área Administrador | insere tipo de produto', 'tipo_produtos', 'INSERÇÃO', 2, '2021-06-07 22:42:27', '2804:4b0:10b0:fa00:193e:ebbd:8ee2:d24a'),
(7, 'Área Administrador | cadastra produtos', 'produtos', 'INSERÇÃO', 2, '2021-06-07 22:43:46', '2804:4b0:10b0:fa00:193e:ebbd:8ee2:d24a'),
(8, 'Área Administrador | atualizar produtos', 'produtos', 'EDIÇÃO', 2, '2021-06-07 22:44:19', '2804:4b0:10b0:fa00:193e:ebbd:8ee2:d24a'),
(9, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 3, '2021-06-09 14:54:08', '2804:4b0:10b0:fa00:d4c4:d581:f6cf:50ea'),
(10, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 3, '2021-06-09 17:12:38', '2804:4b0:10b0:fa00:d4c4:d581:f6cf:50ea'),
(11, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 3, '2021-06-09 17:28:39', '2804:4b0:10b0:fa00:d4c4:d581:f6cf:50ea'),
(12, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 3, '2021-06-09 17:56:51', '2804:4b0:10b0:fa00:d4c4:d581:f6cf:50ea'),
(13, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 3, '2021-06-09 18:00:39', '2804:4b0:10b0:fa00:d4c4:d581:f6cf:50ea'),
(14, 'Área Administrador | atualizar produtos', 'produtos', 'EDIÇÃO', 3, '2021-06-09 18:23:15', '2804:4b0:10b0:fa00:d4c4:d581:f6cf:50ea'),
(15, 'Área Administrador | cadastra produtos', 'produtos', 'INSERÇÃO', 3, '2021-06-09 18:33:16', '2804:4b0:10b0:fa00:d4c4:d581:f6cf:50ea'),
(16, 'Área Administrador | atualizar produtos', 'produtos', 'EDIÇÃO', 3, '2021-06-09 18:33:22', '2804:4b0:10b0:fa00:d4c4:d581:f6cf:50ea'),
(17, 'Área Administrador | excluir produtos', 'produtos', 'EXCLUSÃO', 3, '2021-06-09 18:33:25', '2804:4b0:10b0:fa00:d4c4:d581:f6cf:50ea'),
(18, 'Área Administrador | cadastro empresa', 'empresas', 'INSERÇÃO', 3, '2021-06-09 19:38:52', '2804:4b0:10b0:fa00:b036:b0d6:3161:3dd'),
(19, 'Área Administrador | cadastra produtos', 'produtos', 'INSERÇÃO', 2, '2021-06-14 14:54:13', '::1'),
(20, 'Área Administrador | insere tipo de produto', 'tipo_produtos', 'INSERÇÃO', 2, '2021-06-14 17:10:56', '::1'),
(21, 'Área Administrador | cadastra produtos', 'produtos', 'INSERÇÃO', 2, '2021-06-21 20:56:34', '::1'),
(22, 'Área Administrador | cadastra produtos', 'produtos', 'INSERÇÃO', 2, '2021-06-24 12:39:51', '::1'),
(23, 'Área Administrador | atualizar produtos', 'produtos', 'EDIÇÃO', 2, '2021-06-24 12:40:04', '::1'),
(24, 'Área Administrador | excluir produtos', 'produtos', 'EXCLUSÃO', 2, '2021-06-24 12:40:10', '::1'),
(25, 'Área Administrador | insere frete', 'fretes', 'INSERÇÃO', 2, '2021-06-24 12:41:01', '::1'),
(26, 'Área Administrador | cadastra entregas', 'entregas', 'INSERÇÃO', 2, '2021-06-24 12:41:32', '::1'),
(27, 'Área Administrador | cadastra formaPagtoM', 'forma_pagto', 'INSERÇÃO', 2, '2021-06-24 12:44:58', '::1'),
(28, 'Área Administrador | atualiza formaPagtoM', 'forma_pagto', 'EDIÇÃO', 2, '2021-06-24 12:51:10', '::1'),
(29, 'Área Administrador | atualizar parametro', 'parametrizacoes', 'EDIÇÃO', 2, '2021-07-06 20:12:53', '::1'),
(30, 'Área Administrador | atualizar parametro', 'parametrizacoes', 'EDIÇÃO', 2, '2021-07-06 20:13:22', '::1'),
(31, 'Área Administrador | atualizar parametro', 'parametrizacoes', 'EDIÇÃO', 2, '2021-07-06 20:13:34', '::1'),
(32, 'Área Administrador | atualizar parametro', 'parametrizacoes', 'EDIÇÃO', 2, '2021-07-06 20:13:41', '::1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `log_campos`
--

CREATE TABLE `log_campos` (
  `id` int(11) NOT NULL,
  `campo` varchar(255) DEFAULT NULL,
  `novo_valor` varchar(255) DEFAULT NULL,
  `log_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `log_campos`
--

INSERT INTO `log_campos` (`id`, `campo`, `novo_valor`, `log_id`) VALUES
(1, 'razao_social', 'NOVA ANDES CONSULTORIA DE NEGOCIOS E COMUNICACAO ESTRATEGICA EIRELI', 1),
(2, 'fantasia', 'NOVA ANDES NEGOCIACAO E COMUNICACAO ESTRATEGICA', 1),
(3, 'cnpj', '34.765.464/0001-00', 1),
(4, 'telefone', '(53) 3284-8100/ (53) 8130-0212', 1),
(5, 'endereco', 'R QUARAI, 2059 ', 1),
(6, 'email', 'administrativo@novaandes.com.br', 1),
(7, 'cidade', 'PELOTAS', 1),
(8, 'estado', 'RS', 1),
(9, 'tipo_cadastro_id', '1', 1),
(10, 'insc_estadual', ' ', 1),
(11, 'pais', 'Brasil', 1),
(12, 'bairro', 'LARANJAL', 1),
(13, 'cep', '96090530', 1),
(14, 'cartao_cnpj', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_34765464000100_06-06-2021.html', 1),
(15, 'nome', 'Mateus Santos', 2),
(16, 'id', '2', 2),
(17, 'email', 'webmaster@wertco.com.br', 2),
(18, 'cpf', '008.713.100-58', 2),
(19, 'telefone', '(53) 9840-73868', 2),
(20, 'celular', '(53) 9840-73868', 2),
(21, 'tipo_cadastro_id', '1', 2),
(22, 'empresa_id', '2', 2),
(23, 'ativo', '1', 2),
(24, 'razao_social', 'MIL CRIACAO E DESENVOLVIMENTO WEB LTDA', 3),
(25, 'id', '1', 3),
(26, 'fantasia', 'mil criação e desenv', 3),
(27, 'cnpj', '12.481.170/0001-14', 3),
(28, 'insc_estadual', '1123456', 3),
(29, 'email', 'teste@teste.com.br', 3),
(30, 'telefone', '(53) 3028-3044', 3),
(31, 'endereco', 'AV SAO FRANCISCO DE PAULA, 2722 ', 3),
(32, 'bairro', 'AREAL', 3),
(33, 'cep', '96080-730', 3),
(34, 'cidade', 'PELOTAS', 3),
(35, 'estado', 'RS', 3),
(36, 'pais', 'Brasil', 3),
(37, 'tipo_cadastro_id', '3', 3),
(38, 'id', '3', 4),
(39, 'ativo', '0', 4),
(40, 'id', '3', 5),
(41, 'ativo', '1', 5),
(42, 'descricao', 'PLYWOOD ', 6),
(43, 'id', NULL, 7),
(44, 'ncm', '4412 9910', 7),
(45, 'descricao', 'PINE PLYWOOD ESTRUCTURAL PERFORMANCE C+C GRADE PHENOLIC BONDED 18MM 1220 X 2440', 7),
(46, 'valor_unitario', '350.00', 7),
(47, 'tipo_produto_id', '1', 7),
(48, 'id', '1', 8),
(49, 'ncm', '4412 9910', 8),
(50, 'descricao', 'PINE PLYWOOD ESTRUCTURAL PERFORMANCE C+C GRADE PHENOLIC BONDED 18MM 1220 X 2440', 8),
(51, 'tipo_produto_id', '1', 8),
(52, 'valor_unitario', '3500.00', 8),
(53, 'razao_social', 'ARCINDO COLODA COMERCIO DE MADEIRAS EIRELI', 9),
(54, 'fantasia', 'MADCOL', 9),
(55, 'cnpj', '08.573.173/0001-00', 9),
(56, 'telefone', '(42) 3553-1379/ (42) 3553-1836', 9),
(57, 'endereco', 'R MARCILIO AGUIAR PEREIRA, 105 ', 9),
(58, 'email', 'adml@madcol.com.br', 9),
(59, 'cidade', 'BITURUNA', 9),
(60, 'estado', 'PR', 9),
(61, 'tipo_cadastro_id', '3', 9),
(62, 'insc_estadual', '9086044584', 9),
(63, 'pais', 'Brasil', 9),
(64, 'bairro', 'NSA SRA APARECIDA', 9),
(65, 'cep', '84640000', 9),
(66, 'cartao_cnpj', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_08573173000100_09-06-2021.html', 9),
(67, 'razao_social', 'MADEIREIRA BELO HORIZONTE LTDA', 10),
(68, 'fantasia', 'MADEIREIRA BELO HORIZONTE LTDA', 10),
(69, 'cnpj', '82.654.849/0001-43', 10),
(70, 'telefone', '4234361299', 10),
(71, 'endereco', 'R DOS IMIGRANTES, 895 ', 10),
(72, 'email', 'bianca@madeireirabelohorizonte.com.br', 10),
(73, 'cidade', 'IMBITUVA', 10),
(74, 'estado', 'PR', 10),
(75, 'tipo_cadastro_id', '3', 10),
(76, 'insc_estadual', '21500913-09', 10),
(77, 'pais', 'Brasil', 10),
(78, 'bairro', 'JARDIM TANGARA', 10),
(79, 'cep', '84430000', 10),
(80, 'cartao_cnpj', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_82654849000143_09-06-2021.html', 10),
(81, 'razao_social', 'REAL LAMINAS E COMPENSADOS LTDA', 11),
(82, 'fantasia', 'REAL LAMINAS E COMPENSADOS LTDA', 11),
(83, 'cnpj', '18.130.296/0001-20', 11),
(84, 'telefone', '(47) 3364-0044', 11),
(85, 'endereco', 'R ALBERTO KOGLIN, 4510 ', 11),
(86, 'email', 'INDUSTRIAL01@REALLAMINAS.COM.BR', 11),
(87, 'cidade', 'DONA EMMA', 11),
(88, 'estado', 'SC', 11),
(89, 'tipo_cadastro_id', '3', 11),
(90, 'insc_estadual', '00000000', 11),
(91, 'pais', 'Brasil', 11),
(92, 'bairro', 'CENTRO', 11),
(93, 'cep', '89155000', 11),
(94, 'cartao_cnpj', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_18130296000120_09-06-2021.html', 11),
(95, 'razao_social', 'BONARDI INDUSTRIA QUIMICA LTDA', 12),
(96, 'fantasia', 'BONARDI ADESIVOS', 12),
(97, 'cnpj', '08.775.142/0001-32', 12),
(98, 'telefone', '4136051595', 12),
(99, 'endereco', 'R OLIMPIO CARDOSO, 729 ', 12),
(100, 'email', 'francisco.hess@bonardiquimica.com.br', 12),
(101, 'cidade', 'COLOMBO', 12),
(102, 'estado', 'PR', 12),
(103, 'tipo_cadastro_id', '4', 12),
(104, 'insc_estadual', '90481190-40', 12),
(105, 'pais', 'Brasil', 12),
(106, 'bairro', 'PARQUE INDUSTRIAL GUARAITUBA II', 12),
(107, 'cep', '83411110', 12),
(108, 'cartao_cnpj', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_08775142000132_09-06-2021.html', 12),
(109, 'razao_social', 'REVIGLIO THOME E CIA LTDA', 13),
(110, 'fantasia', 'REVIGLIO THOME E CIA LTDA', 13),
(111, 'cnpj', '01.652.564/0001-71', 13),
(112, 'telefone', '(42) 3677-1566', 13),
(113, 'endereco', 'ROD PR 170, S N KM 49', 13),
(114, 'email', 'madeirathome@hotmail.com', 13),
(115, 'cidade', 'PINHAO', 13),
(116, 'estado', 'PR', 13),
(117, 'tipo_cadastro_id', '3', 13),
(118, 'insc_estadual', '90123232-60', 13),
(119, 'pais', 'Brasil', 13),
(120, 'bairro', 'SAO SEBASTIAO', 13),
(121, 'cep', '85170000', 13),
(122, 'cartao_cnpj', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_01652564000171_09-06-2021.html', 13),
(123, 'id', '1', 14),
(124, 'ncm', '4412 9910', 14),
(125, 'descricao', 'PINE PLYWOOD ESTRUCTURAL PERFORMANCE C+C GRADE PHENOLIC BONDED 18MM 1220 X 2440', 14),
(126, 'tipo_produto_id', '1', 14),
(127, 'valor_unitario', '350.00', 14),
(128, 'id', NULL, 15),
(129, 'ncm', '45465465', 15),
(130, 'descricao', 'madeira serrada', 15),
(131, 'valor_unitario', '213.12', 15),
(132, 'tipo_produto_id', '1', 15),
(133, 'id', '2', 16),
(134, 'ncm', '45465465', 16),
(135, 'descricao', 'madeira serrada', 16),
(136, 'tipo_produto_id', '1', 16),
(137, 'valor_unitario', '213.12', 16),
(138, 'id', '2', 17),
(139, 'razao_social', 'BRASSERIE 35 INDUSTRIA DE BEBIDAS LTDA', 18),
(140, 'fantasia', 'BRASSERIE 35', 18),
(141, 'cnpj', '13.692.639/0001-27', 18),
(142, 'telefone', '(53) 3028-9001', 18),
(143, 'endereco', 'ROD BR 116, S N KM 483 CASA B', 18),
(144, 'email', '', 18),
(145, 'cidade', 'TURUCU', 18),
(146, 'estado', 'RS', 18),
(147, 'tipo_cadastro_id', '2', 18),
(148, 'insc_estadual', '462/0002940', 18),
(149, 'pais', 'Brasil', 18),
(150, 'bairro', 'CENTRO', 18),
(151, 'cep', '96148000', 18),
(152, 'cartao_cnpj', '/home/novaandes/www/admin/Consultas/Receita_Federal/Receita_13692639000127_09-06-2021.html', 18),
(153, 'id', NULL, 19),
(154, 'ncm', 'ncm', 19),
(155, 'descricao', 'teste', 19),
(156, 'valor_unitario', '10000.00', 19),
(157, 'tipo_produto_id', '1', 19),
(158, 'descricao', 'teste', 20),
(159, 'id', NULL, 21),
(160, 'ncm', '12313132', 21),
(161, 'descricao', 'asdada', 21),
(162, 'valor_unitario', '23131231.23', 21),
(163, 'tipo_produto_id', '1', 21),
(164, 'id', NULL, 22),
(165, 'ncm', '123456646', 22),
(166, 'descricao', 'descrição de teste', 22),
(167, 'valor_unitario', '15445.45', 22),
(168, 'tipo_produto_id', '2', 22),
(169, 'id', '5', 23),
(170, 'ncm', '123456646', 23),
(171, 'descricao', 'descrição de teste', 23),
(172, 'tipo_produto_id', '2', 23),
(173, 'valor_unitario', '15445.40', 23),
(174, 'id', '5', 24),
(175, 'descricao', 'Frete a combinar', 25),
(176, 'descricao', 'Aproximadamente 50 dias', 26),
(177, 'descricao', 'forma de pagamento mais comum', 27),
(178, 'id', '1', 28),
(179, 'descricao', 'forma de pagamento mais comusm', 28),
(180, 'id', '1', 29),
(181, 'moeda', 'US$', 29),
(182, 'unidade', 'Kg', 29),
(183, 'id', '1', 30),
(184, 'moeda', 'US$', 30),
(185, 'unidade', 'M³', 30),
(186, 'id', '1', 31),
(187, 'moeda', 'R$', 31),
(188, 'unidade', 'M³', 31),
(189, 'id', '1', 32),
(190, 'moeda', 'US$', 32),
(191, 'unidade', 'M³', 32);

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamentos`
--

CREATE TABLE `orcamentos` (
  `id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `emissao` timestamp NOT NULL DEFAULT current_timestamp(),
  `validade` varchar(20) NOT NULL,
  `status_orcamento_id` int(11) NOT NULL DEFAULT 1,
  `origem` varchar(50) NOT NULL,
  `solicitante_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `valor_orcamento` decimal(10,2) NOT NULL,
  `frete_id` int(11) NOT NULL,
  `entrega_id` int(11) NOT NULL,
  `forma_pagto_id` int(11) NOT NULL,
  `contato_posto` varchar(100) NOT NULL,
  `cel_contato_posto` varchar(100) NOT NULL,
  `tel_contato_posto` varchar(100) NOT NULL,
  `contato_id` int(11) NOT NULL,
  `observacao` text DEFAULT NULL,
  `fl_especial` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Acionadores `orcamentos`
--
DELIMITER $$
CREATE TRIGGER `inserir_orcamentos_emissao` AFTER INSERT ON `orcamentos` FOR EACH ROW insert into orcamentos_emissao (orcamento_id, dthr_emissao, usuario_emissao, motivo_emissao) VALUES (NEW.id, NEW.emissao, NEW.usuario_id, 'Criação do orçamento - Geração inicial' )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamentos_emissao`
--

CREATE TABLE `orcamentos_emissao` (
  `id` int(11) NOT NULL,
  `orcamento_id` int(11) NOT NULL,
  `dthr_emissao` datetime NOT NULL,
  `usuario_emissao` int(11) NOT NULL,
  `motivo_emissao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamento_alertas`
--

CREATE TABLE `orcamento_alertas` (
  `id` int(11) NOT NULL,
  `orcamento_andamento_id` int(11) NOT NULL,
  `fl_visualizado` int(11) NOT NULL DEFAULT 0,
  `dthr_alerta` date NOT NULL,
  `dthr_visualizado` datetime NOT NULL,
  `dthr_cadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamento_andamentos`
--

CREATE TABLE `orcamento_andamentos` (
  `id` int(11) NOT NULL,
  `orcamento_id` int(11) NOT NULL,
  `status_orcamento_id` int(11) NOT NULL,
  `andamento` text NOT NULL,
  `dthr_andamento` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `usuario_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamento_descontos`
--

CREATE TABLE `orcamento_descontos` (
  `id` int(11) NOT NULL,
  `orcamento_id` int(11) NOT NULL,
  `valor_desconto` decimal(10,2) NOT NULL,
  `motivo_desconto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamento_produtos`
--

CREATE TABLE `orcamento_produtos` (
  `id` int(11) NOT NULL,
  `orcamento_id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `qtd` int(11) NOT NULL,
  `valor` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamento_responsavel`
--

CREATE TABLE `orcamento_responsavel` (
  `id` int(11) NOT NULL,
  `orcamento_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `parametrizacoes`
--

CREATE TABLE `parametrizacoes` (
  `id` int(11) NOT NULL,
  `unidade` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'm³',
  `moeda` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'U$'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `parametrizacoes`
--

INSERT INTO `parametrizacoes` (`id`, `unidade`, `moeda`) VALUES
(1, 'M³', 'US$');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `ncm` varchar(25) DEFAULT NULL,
  `descricao` text NOT NULL,
  `valor_unitario` decimal(10,2) DEFAULT NULL,
  `tipo_produto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `ncm`, `descricao`, `valor_unitario`, `tipo_produto_id`) VALUES
(1, '4412 9910', 'PINE PLYWOOD ESTRUCTURAL PERFORMANCE C+C GRADE PHENOLIC BONDED 18MM 1220 X 2440', '350.00', 1),
(3, 'ncm', 'Teste', '10000.00', 1),
(4, '12313132', 'asdada', '23131231.23', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `status_orcamentos`
--

CREATE TABLE `status_orcamentos` (
  `id` int(11) NOT NULL,
  `descricao` varchar(50) NOT NULL,
  `ordem` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `status_orcamentos`
--

INSERT INTO `status_orcamentos` (`id`, `descricao`, `ordem`) VALUES
(1, 'aberto', 1),
(2, 'fechado', 3),
(3, 'perdido', 4),
(4, 'cancelado', 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `subtipo_cadastros`
--

CREATE TABLE `subtipo_cadastros` (
  `id` int(11) NOT NULL,
  `tipo_cadastro_pai` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `nvl` int(11) NOT NULL,
  `obs` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_cadastros`
--

CREATE TABLE `tipo_cadastros` (
  `id` int(11) NOT NULL,
  `descricao` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipo_cadastros`
--

INSERT INTO `tipo_cadastros` (`id`, `descricao`) VALUES
(1, 'administrador geral'),
(2, 'clientes'),
(3, 'fornecedores'),
(4, 'parceiros');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_produtos`
--

CREATE TABLE `tipo_produtos` (
  `id` int(11) NOT NULL,
  `descricao` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tipo_produtos`
--

INSERT INTO `tipo_produtos` (`id`, `descricao`) VALUES
(1, 'PLYWOOD '),
(2, 'teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `cpf` varchar(20) DEFAULT NULL,
  `telefone` varchar(50) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `endereco` text DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `pais` varchar(30) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `tipo_cadastro_id` int(11) NOT NULL,
  `dthr_cadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `ativo` int(11) NOT NULL DEFAULT 0,
  `foto` varchar(255) NOT NULL,
  `dt_treinamento` date DEFAULT NULL,
  `local_treinamento` varchar(250) NOT NULL,
  `fl_termos` int(11) NOT NULL DEFAULT 0,
  `dthr_aceite_termos` datetime NOT NULL DEFAULT current_timestamp(),
  `subtipo_cadastro_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `cpf`, `telefone`, `celular`, `email`, `endereco`, `cidade`, `estado`, `pais`, `empresa_id`, `senha`, `tipo_cadastro_id`, `dthr_cadastro`, `ativo`, `foto`, `dt_treinamento`, `local_treinamento`, `fl_termos`, `dthr_aceite_termos`, `subtipo_cadastro_id`) VALUES
(2, 'Mateus Santos', '008.713.100-58', '(53) 9840-73868', '(53) 9840-73868', 'webmaster@wertco.com.br', 'Avenida ferreira viana, 2962,m casa 260', NULL, NULL, '', 2, 'f36363b4ec88a8be2baf199aa07110db', 1, '2018-02-09 18:38:15', 1, 'img_perfil/08f4011097c1d8aadc7740f05d6ee8ba.png', '0000-00-00', '', 1, '2018-12-12 08:14:20', 0),
(3, 'Sebastian salcedo', '418.608.245-60', '(53) 9949 - 4945', '(53) 9994 - 94945', 'ssalcedo@novaandes.com.br', NULL, NULL, NULL, '', 2, '7dafccdc682f4d0d9972cb6ca8ffcde7', 1, '2021-06-07 01:05:02', 1, '', NULL, '', 0, '2021-06-06 22:05:02', 0),
(4, 'Anderson coloda ', '012.628.579-96', '(42) 3553 - 1012', '(42) 9938 - 4625', 'adm1@madcol.com.br', NULL, NULL, NULL, '', 3, '887192d0071a0fe87de8ec4e25144f73', 3, '2021-06-09 16:43:08', 1, '', NULL, '', 0, '2021-06-09 13:43:08', 0),
(5, 'Max Eduardo da Silva Kruger', '047.191.200-08', '(53) 9914 - 32870', '(53) 9914 - 32870', 'maxkruger11@gmail.com', NULL, NULL, NULL, '', 2, '8bda2356580b60b14de6acdf45569c5a', 1, '2021-06-09 17:32:59', 1, '', NULL, '', 0, '2021-06-09 14:32:59', 0),
(6, 'teste', '', '(53) 9840 - 73868', '', 'mateus@mateus.com.br', NULL, NULL, NULL, '', 1, 'f36363b4ec88a8be2baf199aa07110db', 3, '2021-06-14 14:45:14', 1, '', NULL, '', 0, '2021-06-14 11:45:14', 0),
(7, 'teste', '', '(53) 9840 - 73868', '(53) 9840 - 73868', 'adasd@asdasd.com.br', NULL, NULL, NULL, '', 8, 'f36363b4ec88a8be2baf199aa07110db', 2, '2021-06-14 14:45:57', 1, '', NULL, '', 0, '2021-06-14 11:45:57', 0);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_empresas_x_tipo_cadastros` (`tipo_cadastro_id`);

--
-- Índices para tabela `empresa_anexos`
--
ALTER TABLE `empresa_anexos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_empresa_anexos_empresax` (`empresa_id`);

--
-- Índices para tabela `entregas`
--
ALTER TABLE `entregas`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `forma_pagto`
--
ALTER TABLE `forma_pagto`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `fretes`
--
ALTER TABLE `fretes`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `log_campos`
--
ALTER TABLE `log_campos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `orcamentos`
--
ALTER TABLE `orcamentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_orcamento_status` (`status_orcamento_id`);

--
-- Índices para tabela `orcamentos_emissao`
--
ALTER TABLE `orcamentos_emissao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_orcamentos_X_orcamentos_emissao` (`orcamento_id`),
  ADD KEY `fk_usuarios_X_orcamentos_emissao` (`usuario_emissao`);

--
-- Índices para tabela `orcamento_alertas`
--
ALTER TABLE `orcamento_alertas`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `orcamento_andamentos`
--
ALTER TABLE `orcamento_andamentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_orcamento_status_andamento` (`orcamento_id`),
  ADD KEY `fk_resp_andamentos` (`usuario_id`);

--
-- Índices para tabela `orcamento_descontos`
--
ALTER TABLE `orcamento_descontos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `orcamento_produtos`
--
ALTER TABLE `orcamento_produtos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `orcamento_responsavel`
--
ALTER TABLE `orcamento_responsavel`
  ADD PRIMARY KEY (`id`,`orcamento_id`);

--
-- Índices para tabela `parametrizacoes`
--
ALTER TABLE `parametrizacoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tipo_produtos` (`tipo_produto_id`);

--
-- Índices para tabela `status_orcamentos`
--
ALTER TABLE `status_orcamentos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `subtipo_cadastros`
--
ALTER TABLE `subtipo_cadastros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subtipo_cadastro_x_tipo_cadastro` (`tipo_cadastro_pai`);

--
-- Índices para tabela `tipo_cadastros`
--
ALTER TABLE `tipo_cadastros`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tipo_produtos`
--
ALTER TABLE `tipo_produtos`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_usuario_X_empresa` (`empresa_id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `empresa_anexos`
--
ALTER TABLE `empresa_anexos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `entregas`
--
ALTER TABLE `entregas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `forma_pagto`
--
ALTER TABLE `forma_pagto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `fretes`
--
ALTER TABLE `fretes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de tabela `log_campos`
--
ALTER TABLE `log_campos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- AUTO_INCREMENT de tabela `orcamentos`
--
ALTER TABLE `orcamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `orcamentos_emissao`
--
ALTER TABLE `orcamentos_emissao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `orcamento_alertas`
--
ALTER TABLE `orcamento_alertas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `orcamento_andamentos`
--
ALTER TABLE `orcamento_andamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `orcamento_descontos`
--
ALTER TABLE `orcamento_descontos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `orcamento_produtos`
--
ALTER TABLE `orcamento_produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `orcamento_responsavel`
--
ALTER TABLE `orcamento_responsavel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `parametrizacoes`
--
ALTER TABLE `parametrizacoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `status_orcamentos`
--
ALTER TABLE `status_orcamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de tabela `subtipo_cadastros`
--
ALTER TABLE `subtipo_cadastros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tipo_cadastros`
--
ALTER TABLE `tipo_cadastros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `tipo_produtos`
--
ALTER TABLE `tipo_produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `subtipo_cadastros`
--
ALTER TABLE `subtipo_cadastros`
  ADD CONSTRAINT `fk_subtipo_cadastro_x_tipo_cadastro` FOREIGN KEY (`tipo_cadastro_pai`) REFERENCES `tipo_cadastros` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
