<?php

class ItensModel extends CI_Model {

	public function insereItens ($data) {
        
		return $this->db->insert('itens', $data);
	}

    public function insereSubItens ($data) {
        
        return $this->db->insert('subitens', $data);
    }

    public function verificaItem($sql)
    {
    	
    	$query = $this->db->query($sql);
    	return $query->row_array();

    }

    public function getIdItem($sql)
    {
        
        $query = $this->db->query($sql);
        $item = $query->row_array();
        return $item['id'];

    }   

    public function verificaOp($sql)
    {
        $query = $this->db->query($sql);
        return $query->row_array();

    }

    public function excluirItens($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('Itens')){
            return true;
        }else{
            return false;
        }
    }

    public function getItensPorEstado($estado_id)
    {

    	$sql =  "SELECT count(*) as total from Itens WHERE estado_id =".$estado_id;
    	$query = $this->db->query($sql);
    	return $query->result_array();

    }

    public function getItensById($id)
    {

    	$sql =  "SELECT i.id, i.valor_tributo,i.estado_id FROM Itens i WHERE i.id=".$id;
    	$query = $this->db->query($sql);
    	return $query->result_array();

    } 

    public function atualizaItens($dados)
    {
        $update = array('valor_tributo' 	=>	str_replace(',','.',$dados['valor_tributo']));
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('Itens', $update)){
            return true;
        }else{
            return false;
        }

    }

    public function getItensPorUF($uf)
    {
        
        $sql =  "SELECT i.fator FROM Itens i, estados e where i.estado_id = e.id and trim(e.uf) = trim('".$uf."')";        
        $query = $this->db->query($sql);
        return $query->result();
        
    }
	
	public function retornaItens($term){

        $sql = "SELECT i.id, concat('#',i.id, ' | ', i.descricao) as label FROM itens i WHERE concat(i.id, i.descricao) like '%".$term."%'";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;

    }
	
	public function retornaProdutosAvulsos($term){

        $sql = "SELECT 	p.id, concat('#',p.id, ' | ', p.descricao) as label, i.id as item_id
				FROM	produtos p, itens i
			   WHERE 	p.id = i.produto_id and concat(p.id, p.descricao) like '%".$term."%' and
						p.tipo_produto_id=5";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;

    }


}
?>