<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Gestão de Usuários
					</h3>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body">
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="Field #1" width="5%">ID</th>
						<th title="Field #2" width="15%">Nome</th>
						<th title="Field #3" width="10%">CPF</th>
						<th title="Field #4" width="5%">E-mail</th>
						<th title="Field #5" width="30%">Empresa</th>
						<th title="Field #6" width="5%">Tipo de Acesso</th>						
						<th title="Field #8" width="15%">Ativo</th>
						<th title="Field #9" width="5%">Ações</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					<?php foreach($dados as $dado){ ?>
						<td style="text-align: center;"><?php echo $dado->id; ?></td>
						<td><?php echo $dado->nome; ?></td>
						<td><?php echo $dado->cpf; ?></td>
						<td><?php echo $dado->email; ?></td>
						<td><?php echo $dado->razao_social; ?></td>
						<td><?php echo $dado->descricao; ?></td>
						<td>
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" name="ativo<?php echo $dado->id;?>" onclick="atualizarStatus('1',<?php echo $dado->id?>,'<?php echo $dado->email;?>');" class="ativo" <?php if($dado->ativo=='1') echo 'checked="checked"' ?> value="1" user_id="<?php echo $dado->id; ?>"> Sim
									<span></span>
								</label>									
								<label class="m-radio">
									<input type="radio" name="ativo<?php echo $dado->id;?>" class="ativo" <?php if($dado->ativo=='0') echo 'checked="checked"' ?> value="0" user_id="<?php echo $dado->id; ?>" onclick="atualizarStatus('0',<?php echo $dado->id?>,'<?php echo $dado->email;?>');" > Não
									<span></span>
								</label>								
							</div>
						</td>
						<td data-field="Actions" class="m-datatable__cell">
							<span style="overflow: visible; width: 110px;">								
								<a href="<?php echo base_url('AreaAdministradorEmpresas/editarUsuario/'.$dado->id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar ">
									<i class="la la-edit"></i>
								</a>
								<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir">
									<i class="la la-trash"></i> 
								</a>
							</span>
						</td>
					</tr>
						<?php } ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>				
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
			
		});  
	</script>	
<?php unset($_SESSION['sucesso']); } ?>