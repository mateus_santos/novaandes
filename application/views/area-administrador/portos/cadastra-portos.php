<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/portos'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/portos'); ?>">Voltar</a>   Cadastrar Porto						
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/cadastraPortos');?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">						
						<div class="col-lg-6">
							<label class="m-input-icon m-input-icon--right">Abreviação:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="abreviacao" id="abreviacao" class="form-control m-input" placeholder="" required >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-6">
							<label>Descrição:</label>
							<div class="m-input-icon m-input-icon--right">
								<textarea  required name="descricao" class="form-control m-input" placeholder="" required ></textarea>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-comment-o"></i></span></span>
							</div>							
						</div>
					</div>						
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label for="Acesso">País</label>							
							<select class="form-control m-input" name="pais_id" required>
								<option value="">Selecione o pais</option>
							<?php foreach ($pais as $pa) { ?>
								<option value="<?php echo $pa['id']; ?>" >
									<?php echo $pa['nome_pt']; ?>									
								</option>
							<?php } ?>							
							</select>
						</div>
					</div>	
				</div>	 				
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	