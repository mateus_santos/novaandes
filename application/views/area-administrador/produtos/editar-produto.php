<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">						
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/produtos'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/produtos'); ?>">Voltar</a>						
						</h3>						
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarProduto');?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>NCM:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required	name="ncm"	class="form-control m-input" placeholder="" required  value="<?php echo $dados['ncm']; ?>">		
								<input type="hidden"	name="id"	class="form-control m-input" placeholder="" value="<?php echo $dados['id']; ?>">	
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-code"></i></span>
								</span>
							</div>
						</div>						
						<div class="col-lg-6">
							<label>Descrição:</label>
							<div class="m-input-icon m-input-icon--right">
								<textarea  required name="descricao" class="form-control m-input" placeholder="" required ><?php echo $dados['descricao']; ?></textarea>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-comment-o"></i></span></span>
							</div>							
						</div>
					</div>
					<div class="form-group m-form__group row">						
						<div class="col-lg-4">
							<label>Valor(USD)::</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="valor_unitario" id="valor_unitario" class="form-control m-input" placeholder="" required value="<?php echo $dados['valor_unitario']; ?>" >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-dollar"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label for="Acesso">Tipo</label>							
							<select class="form-control m-input" name="tipo_produto_id" required>
								<option value="">Selecione o tipo do produto</option>
							<?php foreach ($dados_tipo_produto as $tipo_produto) { ?>
								<option value="<?php echo $tipo_produto->id; ?>" <?php if( $tipo_produto->id == $dados['tipo_produto_id'] ){ echo "selected='selected'";	}?>>
									<?php echo $tipo_produto->descricao; ?>									
								</option>
							<?php } ?>							
							</select>
						</div>
					</div>	
				</div>	 				
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	