<?php

class PedidosModel extends CI_Model {

	public function add ($data) {

	    return $this->db->insert('pedidos', $data);
	}    

    public function selectPedidos(){

        $sql =  "SELECT     p.*, concat(e.id,' | ',e.cnpj, ' | ', e.razao_social) as cliente, s.descricao as status, e.email, o.contato_posto, o.empresa_id, o.id as orcamento_id   
                    FROM    pedidos p, empresas e, orcamentos o, status_pedidos s 
                    WHERE   o.empresa_id = e.id     and 
                            p.orcamento_id = o.id   and 
                            p.status_pedido_id = s.id";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function selectPedidosPorEmpresa($empresa_id){

        $sql =  "SELECT     p.*, concat(e.id,' | ',e.cnpj, ' | ', e.razao_social) as cliente, s.descricao as status, e.email, o.contato_posto, o.empresa_id, o.id as orcamento_id   
                    FROM    pedidos p, empresas e, orcamentos o, status_pedidos s 
                    WHERE   o.empresa_id = e.id     and 
                            p.orcamento_id = o.id   and 
                            p.status_pedido_id = s.id and
                            o.empresa_id = e.id and 
                            e.id = ".$empresa_id;
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getPedidosById($id){

        $sql =  "SELECT     p.*, concat(e.cnpj,' | ',e.razao_social) as cliente, concat(u.nome,' | ',t.cnpj,' | ', t.razao_social) as tecnico, e.email 
                    FROM    pedidos p 
                INNER JOIN  orcamentos o ON p.orcamento_id = o.id 
                INNER JOIN  empresas e ON o.empresa_id = e.id 
                LEFT JOIN   usuarios u ON u.id = p.tecnico_id 
                LEFT JOIN   empresas t ON u.empresa_id = t.id 
                WHERE       p.id = ".$id;
                           
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function excluirPedidos($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('pedidos')){
            return true;
        }else{
            return false;
        }
    }

    public function excluirPedidoItens($id){
        
        $this->db->where('pedido_id', $id);
        if($this->db->delete('pedido_itens')){
            return true;
        }else{
            return false;
        }
    }

    public function excluirPedidoFormPagto($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('pedido_form_pag')){
            return true;
        }else{
            return false;
        }
    }

    public function excluirFormPagtoPorPedido($pedido_id){
        
        $this->db->where('pedido_id', $pedido_id);
        if($this->db->delete('pedido_form_pag')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaPedidos($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('pedidos', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizaStatusPedido($dados)
    {
                        
        $this->db->where('id', $dados['id']);

        if($this->db->update('pedidos', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function pedidosEmpresasPdf($id){

        $sql =  "SELECT     p.*, e.cnpj,e.razao_social, e.insc_estadual, e.telefone, e.endereco,e.bairro,e.cep, e.email,e.cidade, e.estado,
							concat(u.nome,' | ',t.cnpj,' | ', t.razao_social) as tecnico,
							f.descricao as frete, concat(i.nome,' | ',indi_emp.cnpj,' | ', indi_emp.razao_social,' | ',indi_emp.email,' | ',indi_emp.telefone) as indicador, o.cel_contato_posto, contato_p.celular as celular_contato, e.tp_cadastro
                    FROM    pedidos p
                INNER JOIN  orcamentos o        ON  p.orcamento_id  = o.id
                LEFT JOIN   fretes f            ON  o.frete_id      = f.id
                INNER JOIN  empresas e          ON  o.empresa_id    = e.id
                LEFT JOIN   usuarios u          ON  u.id = p.tecnico_id
                LEFT JOIN   usuarios i          ON  (i.id = o.indicador_id or i.id = o.indicador)
                LEFT JOIN   empresas indi_emp   ON  i.empresa_id = indi_emp.id
                LEFT JOIN   usuarios contato_p  ON  contato_p.id = o.contato_id
                LEFT JOIN   empresas t          ON  u.empresa_id = t.id 
                WHERE       p.id = ".$id;
                             
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function buscaIcmsOrcamentoPedido($id) 
    {
         
        $sql =  "SELECT i.valor_tributo
                    FROM        pedidos p
                    INNER JOIN  orcamentos o    ON o.id = p.orcamento_id                
                    INNER JOIN  empresas e      on o.empresa_id = e.id 
                    INNER JOIN  estados es      ON es.uf = e.estado  
                    INNER JOIN  icms i          on i.estado_id = es.id
                    WHERE p.id = ".$id;

        $query = $this->db->query($sql);
        return $query->row();
    }

    public function pedidosProdutos($id){
        $sql =  "SELECT     pro.descricao, pro.modelo, pro.codigo, pi.qtd, pi.produtos, pi.valor, pro.tipo_produto_id, pi.produto_id, pro.modelo_tecnico, pro.nr_produtos, pi.id,pi.valor_stx
                    FROM    pedidos p 
                INNER JOIN  pedido_itens pi ON pi.pedido_id = p.id                                 
                INNER JOIN  produtos pro    ON pro.id = pi.produto_id               
                WHERE p.id = ".$id;
                           
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function pedidosFormaPagto($id){
        $sql =  "SELECT     pfp.*
                    FROM    pedidos p 
                INNER JOIN  pedido_form_pag pfp ON pfp.pedido_id = p.id                                                 
                WHERE p.id = ".$id;
                           
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function insereFormaPagamento ($data) {
        
        return $this->db->insert('pedido_form_pag', $data);     

    }

    public function inserePedidoItens ($data) {
        
        return $this->db->insert('pedido_itens', $data);     

    }
	
	public function retornaClientePorPedido($id){
		$sql = "SELECT concat(e.cnpj,' - ', e.razao_social) as cliente, e.id as cliente_id, e.cidade, e.estado as uf, e.pais FROM empresas e
				INNER JOIN orcamentos o ON o.empresa_id = e.id
				INNER JOIN pedidos p ON o.id = p.orcamento_id
				where p.id=".$id;
				
		$query = $this->db->query($sql);
        return $query->row();
	}
	
	public function buscaOpsPorPedido($pedido_id){
		$sql = "SELECT * FROM ordem_producao WHERE pedido_id = ".$pedido_id." and status_op_id != 4";
		$query = $this->db->query($sql);
         
        return $query->result_array();
	}

    public function selectPedidosFeira(){

        $sql =  "SELECT p.*, concat(e.id,' | ',e.cnpj, ' | ', e.razao_social) as cliente, s.descricao as status, e.email, o.contato_posto, o.empresa_id, o.id as orcamento_id       
                FROM    pedidos p, empresas e, orcamentos o, status_pedidos s, usuarios u
                WHERE   o.empresa_id = e.id and
                         p.orcamento_id = o.id and 
                         p.status_pedido_id = s.id and 
                         u.id = p.usuario_geracao and
                         u.tipo_cadastro_id = 11 and 
                         date(p.dthr_geracao) between '2019-08-13' and '2019-08-15'
                         
                ORDER BY p.id DESC";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function buscaStatus(){
        $sql =  "SELECT * FROM status_pedidos";        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function buscaStatusByid($status_id){
        $sql =  "SELECT * FROM status_pedidos WHERE id=".$status_id;        
        return $this->db->query($sql)->row_array();
    }

    public function getRelatorioPedidos($filtros){
        $where="1=1";
        
        if( $filtros['status_id'] != '' ){
            $where.=" and p.status_pedido_id = ".$filtros['status_id'];
            
        }

        if( $filtros['dt_ini'] != '' && $filtros['dt_fim'] != ''){
            $where.=" and date(p.dthr_geracao) between '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'";
            
        }

        if( $filtros['indicador_id'] != '' ){
            $where.=" and i.id =".$filtros['indicador_id']." or indic.id=".$filtros['indicador_id'];
            
        }

        $sql = "SELECT  p.id, s.descricao as status, concat(e.cnpj, '|',e.razao_social) as cliente, concat(i.cnpj, '|',i.razao_social) as indicador, COALESCE(sum( op.qtd * op.valor ), p.valor_total) as valor_total, i.id as indicador_id, sum(if(pro.tipo_produto_id in (1,2,3,6),op.qtd,0) ) as total_bombas, sum(pro.nr_bicos*op.qtd) as total_bicos
                  FROM  orcamentos o
                  INNER JOIN    pedidos p               ON  p.orcamento_id      =   o.id   
                  INNER JOIN    pedido_itens     op     ON  p.id                =   op.pedido_id                  
                  LEFT  JOIN    empresas e              ON  o.empresa_id        =   e.id 
                  LEFT  JOIN    usuarios u              ON  o.solicitante_id    =   u.id
                  LEFT  JOIN    empresas i              ON  u.empresa_id        =   i.id 
                  LEFT  JOIN    usuarios ind            ON  o.indicador_id      =   ind.id
                  INNER JOIN    status_pedidos s        ON  s.id                =   p.status_pedido_id 
                  LEFT  JOIN    empresas indic          ON  ind.empresa_id      =   indic.id
                  INNER JOIN    produtos pro            ON op.produto_id        =   pro.id
                  WHERE ".$where."
                  group by p.id  
                ORDER BY p.id DESC";
        
        $query = $this->db->query($sql);
        $retorno['orcamento'] = $query->result_array();    

        return $retorno;
    }

    public function getRelatorioPrevisaoVendas($filtros)
     {
         
        $where="";
        
        if( $filtros['produto_id'] != '' ){
            $where.=" and pro.id = ".$filtros['produto_id'];
            
        }

        $sql = "SELECT a.modelo, sum(total_bombas) as total_bombas, sum(total_bicos) as total_bicos from 
(SELECT  pro.id, pro.modelo, sum(if(pro.tipo_produto_id in (1,2,3,6),op.qtd,0) ) as total_bombas, sum(pro.nr_bicos*op.qtd) as total_bicos
                FROM    orcamentos o 
                INNER JOIN pedidos p ON p.orcamento_id = o.id 
                INNER JOIN pedido_itens op ON p.id = op.pedido_id 
                LEFT JOIN empresas e ON o.empresa_id = e.id 
                LEFT JOIN usuarios u ON o.solicitante_id = u.id 
                INNER JOIN status_pedidos s ON s.id = p.status_pedido_id 
                INNER JOIN produtos pro ON op.produto_id = pro.id 
                WHERE p.status_pedido_id = 1 and pro.tipo_produto_id in (1,2,3,6)  ".$where."
                GROUP BY pro.id     
                union 
                SELECT pro.id, pro.modelo, sum(if(pro.tipo_produto_id in (1,2,3,6),op.qtd,0) ) as total_bombas, sum(pro.nr_bicos*op.qtd) as total_bicos
                  FROM  orcamentos o
                  INNER JOIN    orcamento_produtos   op ON  o.id                =   op.orcamento_id                  
                  LEFT  JOIN    empresas e              ON  o.empresa_id        =   e.id 
                  LEFT  JOIN    usuarios u              ON  o.solicitante_id    =   u.id
                  LEFT  JOIN    empresas i              ON  u.empresa_id        =   i.id                  
                  INNER JOIN    status_orcamentos s     ON  s.id                =   o.status_orcamento_id                   
                  INNER JOIN    produtos pro            ON op.produto_id        =   pro.id
                  WHERE o.status_orcamento_id = 2 and o.id not in (select orcamento_id from pedidos) and pro.tipo_produto_id in (1,2,3,6)  ".$where."
                  group by pro.id) as a group by a.id
                ORDER BY 2 DESC";
        
        $query = $this->db->query($sql);
        $retorno['orcamento'] = $query->result_array();    

        return $retorno;

     } 

    public function sincronismoStx(){
        $sql = 'SELECT COALESCE(max(sincronismo_id),0)+1 as sincronismo_id FROM stx_pedido';
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function insereStxPedido ($data) {
        
        return $this->db->insert('stx_pedido', $data);     

    } 

    public function insereAndamento($dados)
    {
        return $this->db->insert('pedido_andamentos', $dados);
    }

    public function numeroItem($pedido_id){
        $sql    =   "SELECT COALESCE(count(*),0)+1 as total from stx_pedido_item where pedido_internet = ".$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getSincronismoStx($pedido_id){
        $sql = 'SELECT sincronismo_id FROM stx_pedido WHERE pedido_internet = '.$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function sincronismoStxItem(){
        $sql = 'SELECT COALESCE(max(sincronismo_id),0)+1 as sincronismo_id FROM stx_pedido_item';
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function insereStxPedidoItem ($data) {
        
        return $this->db->insert('stx_pedido_item', $data);     

    }

    public function buscaFormaEntrega($pedido_id){
        
        $sql = "SELECT e.* 
                FROM    entregas e, pedidos p, orcamentos o 
                WHERE   e.id = o.entrega_id and 
                        o.id = p.orcamento_id and 
                        p.id = ".$pedido_id;

        $query = $this->db->query($sql);

        return $query->row_array();
    }

    public function totalPedidosConfirmadosAno(){
    
        $sql = "SELECT  CASE
                            WHEN z.usuario_id = 645 THEN 'A'
                            WHEN z.usuario_id = 10  THEN 'B'
                            ELSE 'C'
                        END zona, count(pedidos.id) as total, format(sum(pedidos.valor_total ),2,'de_DE') as total_valor, sum(pedidos.valor_total) as  valor_total
                FROM    pedidos
                INNER JOIN  orcamentos o on o.id = pedidos.orcamento_id
                INNER JOIN  empresas e on e.id = o.empresa_id
                INNER JOIN  zona_atuacao z ON z.estado = e.estado
                INNER JOIN  (select sum(pee.valor) as total, pee.pedido_id from pedido_itens pee group by pee.pedido_id) pi  on pi.pedido_id = pedidos.id
                WHERE   (pedidos.id in (select op.pedido_id from ordem_producao op where extract(YEAR FROM op.dthr_geracao) = '".date('Y')."') or pedidos.status_pedido_id = 2) and 
                            extract(year from pedidos.dthr_geracao ) = '".date('Y')."'
                GROUP BY    z.usuario_id 
                ORDER BY zona";
        
        $query = $this->db->query($sql);

        return $query->result_array();
    
    }

    public function totalBicosBombas($pedido_id){

        $sql = "    SELECT  sum(p.nr_bicos*pi.qtd) as total_bicos, sum(pi.qtd) as total_bombas 
                    FROM    produtos p, pedidos pe, pedido_itens pi 
                    WHERE   p.id = pi.produto_id and 
                            pe.id = pi.pedido_id and 
                            pe.id = ".$pedido_id." and 
                            p.tipo_produto_id in (1,2,3,6) 
                    GROUP by pe.id";

        $query = $this->db->query($sql);            

        return $query->row_array();

    }

    public function buscaPedidosAprovacao()
    {
        $sql = "SELECT p.*, concat(e.cnpj,'-',e.razao_social) as cliente FROM pedidos p, orcamentos o, empresas e 
                WHERE  p.orcamento_id = o.id and o.empresa_id = e.id and status_pedido_id = 9";
        return $this->db->query($sql)->result_array();
    }

    public function totalFechadoMesPed(){

        $sql="  SELECT  sum(pi.valor*pi.qtd) as total, extract(MONTH FROM p.dthr_geracao) as mes 
                FROM    pedidos p, pedido_itens pi, produtos pr
                WHERE p.id = pi.pedido_id and pi.produto_id = pr.id and extract(YEAR FROM p.dthr_geracao) = '".date('Y')."' and p.status_pedido_id != 7 group by extract(MONTH FROM p.dthr_geracao) ORDER BY mes ASC";    

        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();     
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = $this->random_color();
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= 'Janeiro';
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= '#ffcc00';
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function totalConfirmadosMesPed(){

        $sql="  SELECT  sum( pi.valor * pi.qtd ) as total, extract(MONTH FROM op.dthr_geracao) as mes 
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id ) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        extract( year from op.dthr_geracao ) = '".date('Y')."' 
                GROUP BY extract(MONTH FROM op.dthr_geracao) 
                ORDER BY mes ASC";    

        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();     
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = "'".$this->random_color()."'";
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= 'Janeiro';
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= '#ffcc00';
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function totalConfirmadosMesPedAnterior(){

        $sql="  SELECT  sum( pi.valor * pi.qtd ) as total, extract(MONTH FROM op.dthr_geracao) as mes 
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op ON op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        op.dthr_geracao  between '".date('Y',strtotime('-1 year'))."-01-01' and '".date('Y-m-d', strtotime('-1 year'))."' 
                GROUP BY extract(MONTH FROM op.dthr_geracao) 
                ORDER BY mes ASC";    
                
        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();     
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'"; 
                $total = $total+$dados['total'];
                $cor[] = "'".$this->random_color()."'";
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= 'Janeiro';
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= '#ffcc00';
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function buscaPedidosConfirmadosMes($mes)
    {
        $sql = "SELECT  pe.id, concat(e.cnpj,' - ',e.razao_social) as cliente, 
                        s.descricao as status, concat('R$ ',format(sum( pi.valor * pi.qtd ),2,'de_DE')) as valor
                        FROM    pedidos pe
                        inner join orcamentos o on pe.orcamento_id = o.id
                        inner join empresas e on o.empresa_id = e.id 
                        INNER JOIN status_pedidos s on pe.status_pedido_id = s.id
                        INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                        INNER JOIN produtos p on pi.produto_id = p.id
                        INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                        WHERE   pe.status_pedido_id != 7 and
                                extract( year from op.dthr_geracao ) = '".date('Y')."' AND
                                extract( MONTH from op.dthr_geracao ) = '".$this->mes($mes)."'
                        GROUP BY pe.id";
                        
        return $this->db->query($sql)->result_array();
    }

    public function selectPedidoTotalStatus(){

         $sql = "SELECT     count(p.status_pedido_id) as valor, s.descricao 
                FROM        status_pedidos s, pedidos p 
                WHERE       s.id = p.status_pedido_id and extract(YEAR FROM p.dthr_geracao ) = '".date('Y')."'
                GROUP BY    p.status_pedido_id 
                ORDER BY    s.ordem ASC";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".str_replace("'", "",$dados['descricao'])."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }
        if(count($retorno) > 0) {
            $return['valor'] = '['; 
            $return['valor'].= implode(',',$valor['valor']);
            $return['valor'].=']';
            $return['descricao'] = '['; 
            $return['descricao'].= implode(',',$descricao['descricao']);
            $return['descricao'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            $return['cores']    = $cores;
            return $return;
        }else{
            $return['valor'] = '0,00';
            $return['descricao'] = '';
            $return['cor'] = '#ffcc00';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            $return['cores']    = '#000';
        }
    }

    public function totalBombasPed(){
    
        $sql="  SELECT  sum(pi.qtd) as total,  sum(pi.valor*pi.qtd) as valor_total
                FROM    pedidos p, pedido_itens pi,produtos pr 
                WHERE   pr.id = pi.produto_id    and 
                        pi.pedido_id = p.id  and 
                        pr.tipo_produto_id in (1,2,3,6)  and
                        p.status_pedido_id != 7   and 
                        pr.modelo != 'ML40L' and 
                        extract(YEAR FROM p.dthr_geracao) = '".date('Y')."'";
        
        $query = $this->db->query($sql); 
        
        return $query->row_array();

    }

    public function totalBombasOpcionaisPed(){
    
        $sql=" SELECT   sum(pi.valor*pi.qtd) as valor
                FROM    pedidos p, pedido_itens pi,produtos pr 
                WHERE   pr.id = pi.produto_id    and 
                        pi.pedido_id = p.id  and 
                        pr.tipo_produto_id in (1,2,3,6)  and
                        p.status_pedido_id != 7   and                         
                        extract(YEAR FROM p.dthr_geracao) = '".date('Y')."'";
        
        $query = $this->db->query($sql);
        
        return $query->row_array();

    }

    public function totalBombasConfirmadasPed(){
    
        $sql="  SELECT  sum(pi.qtd) as qtd_total, format(sum( pi.valor * pi.qtd ),2,'de_DE') as valor_total, format(sum( pi.valor * pi.qtd ) / sum(pi.qtd),2,'de_DE') as media  
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                inner JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op ON op.pedido_id = pe.id
                where   pe.status_pedido_id != 7 and 
                        p.tipo_produto_id   in (1,2,3,6) and 
                        p.modelo != 'ML40L' and 
                        extract( year from op.dthr_geracao ) = '".date('Y')."'
                ";
        
        $query = $this->db->query($sql); 
        
        return $query->row_array();

    }

    public function totalBombasOpcionaisConfirmadasPed(){
    
        $sql=" SELECT   sum(pi.valor*pi.qtd) as valor
                FROM    pedidos p, 
                        pedido_itens pi,
                        produtos pr, 
                        (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op 
                WHERE   pr.id = pi.produto_id    and 
                        pi.pedido_id = p.id      and 
                        op.pedido_id = p.id      and
                        pr.tipo_produto_id in (1,2,3,6)  and
                        p.status_pedido_id != 7  and                         
                        extract(YEAR FROM p.dthr_geracao) = '".date('Y')."'";
        
        $query = $this->db->query($sql);
        
        return $query->row_array();

    }

    public function relatorioMensalBombas()
    {
        $sql = "SELECT p.modelo, sum(pi.qtd) as qtd_total, format(sum( pi.valor * pi.qtd ),2,'de_DE') as valor_total, 
                       format(sum( pi.valor * pi.qtd ) / sum(pi.qtd),2,'de_DE') as media, extract(MONTH FROM op.dthr_geracao) as mes  
                FROM pedidos pe
                INNER JOIN pedido_itens pi ON pe.id = pi.pedido_id
                INNER JOIN produtos p ON pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7    and 
                        p.modelo != 'ML40L'         and 
                        p.tipo_produto_id in (1,2,3,6) and
                        extract( year from op.dthr_geracao ) = '".date('Y')."'
                GROUP BY extract(MONTH FROM op.dthr_geracao), p.modelo
                order by mes asc";

        return $this->db->query($sql)->result_array();
    
    }

    public function relatorioVendasModelo()
    {
        $sql = "SELECT p.modelo, sum(pi.qtd) as qtd_total, format(sum( pi.valor * pi.qtd ),2,'de_DE') as valor_total, 
                       format(sum( pi.valor * pi.qtd ) / sum(pi.qtd),2,'de_DE') as media 
                FROM pedidos pe
                INNER JOIN pedido_itens pi ON pe.id = pi.pedido_id
                INNER JOIN produtos p ON pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7    and 
                        p.modelo != 'ML40L'         and 
                        p.tipo_produto_id in (1,2,3,6) and
                        extract( year from op.dthr_geracao ) = '".date('Y')."'
                GROUP BY  p.modelo
                ORDER BY qtd_total DESC";

        return $this->db->query($sql)->result_array();
    }

    public function top5ModelosMaisVendidos()
    {
        $sql = "SELECT  p.modelo, count(p.id) as total 
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        p.tipo_produto_id in (1,2,3) and                       
                        extract( year from op.dthr_geracao ) = '".date('Y')."' 
                GROUP BY p.modelo                  
                ORDER BY total  DESC 
                LIMIT 5";
                
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['total'][] = $dados['total'];
            $descricao['descricao'][] = "'".$dados['modelo']."'";
            $total = $total+$dados['total'];
        }              

        if(count($retorno) > 0) {

            $return['valor'] = '['; 
            $return['valor'].= implode(',',$valor['total']);
            $return['valor'].=']';
            $return['descricao'] = '['; 
            $return['descricao'].= implode(',',$descricao['descricao']);
            $return['descricao'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }else{
            $return['valor'] = '0,00';
            $return['descricao'] = '';
            $return['cor'] = '#ffcc00';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            $return['cores']    = '#000';
        }
    }

    public function pedidosConfirmados3meses($mes_ini, $mes_fim)
    {
        $sql = "SELECT  sum(pi.qtd) as qtd_total, sum(pi.qtd) / 3 as media 
                FROM    pedidos pe 
                INNER JOIN  pedido_itens pi ON pe.id = pi.pedido_id 
                INNER JOIN  produtos p ON pi.produto_id = p.id 
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op ON op.pedido_id = pe.id 
                WHERE   pe.status_pedido_id != 7 and 
                        p.tipo_produto_id in (1,2,3,6) and  
                        p.modelo != 'ML40L' and 
                         op.dthr_geracao between '".$mes_ini."' AND  '".$mes_fim."'";

        return $this->db->query($sql)->row_array();
    }

    private function mes($mes){
        switch ($mes) {
            case 1:
                return 'Janeiro';
                break;
            case 2:
                return 'Fevereiro';
                break;    
            case 3:
                return 'Março';
                break;
            case 4:
                return 'Abril';
                break;    
            case 5:
                return 'Maio';
                break;    
            case 6:
                return 'Junho';
                break;       
            case 7:
                return 'Julho';
                break;         
            case 8:
                return 'Agosto';
                break;    
            case 9:
                return 'Setembro';
                break;        
            case 10:
                return 'Outubro';
                break;        
            case 11:
                return 'Novembro';
                break;        
            case 12:
                return 'Dezembro';
            case 'Janeiro':
                return '01';
            case 'Fevereiro':
                return '02';
            case 'Março':
                return '03';
            case 'Abril':
                return '04';
            case 'Maio':
                return '05';   
            case 'Junho':
                return '06';     
            case 'Julho':
                return '07';
            case 'Agosto':
                return '08';
            case 'Setembro':
                return '09';     
            case 'Outubro':
                return '10';
            case 'Novembro':
                return '11';
            case 'Dezembro':
                return '12';     
                break;                    
        }
    }

    private function random_color() {
        $letters = '0123456789ABCDEF';
        $color = '#';
        for($i = 0; $i < 6; $i++) {
            $index = rand(0,15);
            $color .= $letters[$index];
        }
        return $color;
    }

    public function buscaAndamentos($pedido_id)
    {
        $sql =  "SELECT pa.id, 
                        date_format(pa.dthr_andamento, '%d/%m/%Y %H:%i:%s') as dt_andamento, 
                        pa.andamento, 
                        u.nome as usuario, 
                        sp.descricao as status
                    FROM pedido_andamentos pa
                    INNER JOIN pedidos p        ON  p.id = pa.pedido_id
                    LEFT JOIN status_pedidos sp ON  pa.status_pedido_id = sp.id
                    LEFT JOIN usuarios u        ON  u.id = pa.usuario_id
                    WHERE  p.id = ".$pedido_id;


        $query = $this->db->query($sql);
        $row = $query->result_array();
        return $row;
    }

    public function buscaEmailSolAprovacao($pedido_id)
    {
        $sql = "SELECT  u.email     FROM pedido_andamentos pa, usuarios u 
                WHERE   pa.usuario_id       =   u.id            and 
                        pa.pedido_id        =   ".$pedido_id."  and 
                        pa.status_pedido_id =   9";

        return $this->db->query($sql)->row_array();
    }

    public function relatorioNrSerieOpPedidos($filtros)
    {
        $sql = "SELECT      b.id, b.modelo, pe.id as pedido_id, op.id as op_id, concat(e.cnpj, '-', e.razao_social) as cliente 
                FROM        bombas_nr_serie b 
                INNER JOIN  pedidos pe          on  b.pedido_id = pe.id 
                LEFT  JOIN  ordem_producao op   on  b.ordem_producao_id = op.id and pe.id = op.pedido_id
                INNER JOIN  orcamentos o        on  o.id = pe.orcamento_id
                INNER JOIN  empresas e          on  e.id = o.empresa_id  
                WHERE  ".$filtros;

        return $this->db->query($sql)->result_array();
    }

    public function buscaAndamentoPedido($pedido_id)
    {
        $sql="  SELECT  pa.pedido_id, pa.andamento, DATE_FORMAT(pa.dthr_andamento, '%d/%m/%Y %H:%i:%s') as dthr_andamento, 
                        upper(u.nome) as usuario, s.descricao as status 
                FROM    pedido_andamentos pa, usuarios u , status_pedidos s
                WHERE   pa.usuario_id = u.id and s.id = pa.status_pedido_id
                        and pa.pedido_id = ".$pedido_id."
                ORDER BY pa.dthr_andamento DESC ";

        return $this->db->query($sql)->result_array();
    }

    public function relatorioPedidosConfirmados($where)
    {
        $sql = "SELECT  format(sum( pi.valor * pi.qtd ),2,'de_DE') as total, extract(MONTH FROM op.dthr_geracao) as mes, pe.id, sum(if(p.tipo_produto_id in (1,2,3,6),pi.qtd,0) ) as total_bombas, sum(p.nr_bicos*pi.qtd) as total_bicos, concat(e.cnpj,'-',e.razao_social) as cliente 
                FROM    pedidos pe
                INNER JOIN orcamentos o on o.id = pe.id
                INNER JOIN empresas e on e.id = o.empresa_id
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id                
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id ) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        extract( year from op.dthr_geracao ) = '".date('Y')."' ".$where."  
                GROUP BY pe.id 
                ORDER BY mes ASC";
                
        return $this->db->query($sql)->result_array();        
    }

    public function relatorioProgramacaoSemanal($where)
    {
        $condicao = '';
        if($where['dt_ini'] != '' && $where['dt_fim'] != ''){
            $condicao = " and ps.dt_semana_prog BETWEEN '".$where['dt_ini']."' and '".$where['dt_fim']."'";
        }

        $sql = "SELECT      ps.pedido_id, date_format(ps.dt_semana_prog,'%d/%m/%Y') as dt_semana_prog , SPLIT_STRING(op.modelo, '-',1) as tipo, if(ps.modelos_quebra != '', concat( ps.modelos_quebra,' - ', ps.nr_bicos,' - ', ps.nr_bombas ), op.modelo) as modelo, op.qtd, op.combustivel, pe.pintura, op.descricao, op.informacoes, if(ps.modelos_quebra != '', ps.nr_bombas,op.qtd)  as nr_quebrados, s.descricao as status
                FROM        programacao_semanal ps
                INNER JOIN  pedidos pe on pe.id = ps.pedido_id
                INNER JOIN  ordem_producao op on op.pedido_id = pe.id                
                INNER JOIN  status_pedidos s on pe.status_pedido_id = s.id
                where       op.status_op_id <> 4 ".$condicao."
                ORDER BY    ps.dt_semana_prog ASC, ps.pedido_id Asc  ";
        
        return $this->db->query($sql)->result_array();
    }

    public function relatorioProgramacaoSemanalPorModelo($where)
    {
        $condicao = '';
        if($where['dt_ini'] != '' && $where['dt_fim'] != ''){
            $condicao = " and ps.dt_semana_prog BETWEEN '".$where['dt_ini']."' and '".$where['dt_fim']."'";
        }

        $sql = "SELECT     op.modelo as modelo, sum(op.qtd) as qtd, sum(p.nr_bicos*op.qtd) as bicos 
                FROM        programacao_semanal ps
                INNER JOIN  pedidos pe on pe.id = ps.pedido_id
                INNER JOIN  ordem_producao op on op.pedido_id = pe.id                
                INNER JOIN  status_pedidos s on pe.status_pedido_id = s.id
                LEFT JOIN   produtos p on p.modelo = op.modelo
                where       op.status_op_id <> 4 ".$condicao." 
                group by op.modelo  
                ORDER BY op.modelo ASC ";
        
        return $this->db->query($sql)->result_array();
    }

    public function insereMotivoReprovacao ($data) {

        return $this->db->insert('motivo_reprovacao_inspecao', $data);
    }

     public function inserirFotos ($data) {

        return $this->db->insert('pedido_fotos', $data);
    }

    public function listarFotosPedido($pedido_id)
    {
        $sql = "SELECT * FROM pedido_fotos WHERE pedido_id = ".$pedido_id;
        return $this->db->query($sql)->result_array();
    }

    public function verificaPedidosPorCliente($empresa_id)
    {
        $sql = "SELECT count(*) as total FROM pedidos p
                inner join orcamentos o ON o.id = p.orcamento_id
                WHERE o.empresa_id = ".$empresa_id;
                
        return $this->db->query($sql)->result_array();
    }

    public function editarCombustiveis($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('pedido_itens', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function verificaCombustiveis($pedido_id)
    {
        $sql = "SELECT count(*) as total from pedido_itens pi 
                INNER JOIN produtos p on pi.produto_id = p.id 
                WHERE pi.pedido_id = ".$pedido_id." and ( upper(pi.produtos) like '%DEFINIR%' OR pi.produtos = '') and p.tipo_produto_id <> 4";
                
        return $this->db->query($sql)->row_array();
    }

}
?>