  $(document).ready(function(){  
   $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI); 

    $('div#cadastro').hide();
    
    $('.tp_cadastro').bind('click', function(){
        $('div#cadastro').fadeIn('slow');
        if( $(this).val() == 0 ){
            verificaCnpj();
            $('#cnpj').mask('00.000.000/0000-00');
                        
            $('#inscricao_estadual').attr('required', true);
            $('#fantasia').attr('required', true);
            $('#cartao_cnpj').attr('required', true);
            $('#estadoE').removeAttr('name');
            $('#estado').attr('name','estado');
            $('#estadoE').fadeOut('slow');
            $('#estado').fadeIn('slow');
             $('#campo_ie').fadeIn('slow');
            $('#campo_fantasia').fadeIn('slow');     
        }else{

            $('#cnpj').unbind();
            $('#cnpj').unmask();
            $('#label_documento').text('Documento');
            $('#cnpj').attr('placeholder','Insira um documento');
            $('#inscricao_estadual').attr('required', false);
            $('#fantasia').attr('required', false);
            $('#cartao_cnpj').attr('required', false);
            $('#estadoE').attr('type','text');
            $('#estadoE').fadeIn('slow');
            $('#estadoE').attr('name','estado');
            $('#estado').attr('required',false);
            $('#estado').removeAttr('name');
            $('#estado').fadeOut('slow');
            $('#campo_ie').fadeOut('slow');
            $('#campo_fantasia').fadeOut('slow');            
        }
    });
    
 });

function isCNPJValid(cnpj) {  
    var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
    if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
        return false;
    for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
    if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
    if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    return true; 
};

function verificaCnpj(){
    $('#cnpj').bind('focusout', function(){
            var cnpj = $(this).val(); 
            var tp_cadastro = $('.tp_cadastro:checked').val();
            var erro = 0;

            if(cnpj != ""){ 

                if(  tp_cadastro == '0' ) {
                    if(!isCNPJValid(cnpj)){
                        swal({
                            title: "Atenção!",
                            text: "CNPJ inválido!",
                            type: 'warning'
                        }).then(function() {
                            $('#cnpj').val('');
                        });
                        erro = 1;
                    }else{
                        erro = 0;
                    }
                }
                if(  erro == 0 ){
                    $.ajax({
                    method: "POST",
                    url: base_url+'clientes/verifica_cnpj',
                    async: true,
                    data: { cnpj    :   cnpj }
                    }).done(function( data ) {
                        var dados = $.parseJSON(data);                          
                        if(dados.length > 0){
                            
                            swal({
                                title: "Atenção!",
                                text: "CNPJ já cadastrado!",
                                type: 'warning'
                            }).then(function() {
                                $('#cnpj').val('');
                            });    
                            
                        }else{                                  
                            var cnpj_sem_mascara = cnpj.replace(/[^\d]+/g,'');

                            $.ajax({
                                method: "POST",
                                url: base_url+'clientes/ValidaCnpj',
                                async: true,
                                data: { cnpj    :   cnpj_sem_mascara }
                            }).done(function( data ) {
                                var dados = $.parseJSON(data);   
                                
                                if (dados.receita.erro == 0 && dados.receita.situacao == "ATIVA" ){
                                    $('#razao_social').attr('disabled', false);     
                                    $('#razao_social').val(dados.receita.retorno.razao_social);
                                    $('#razao_social').attr('style','border-color: #5fda17;');
                                    $('#fantasia').attr('disabled', false);
                                    var fantasia = dados.receita.retorno.nome_fantasia;
                                    if(dados.receita.retorno.nome_fantasia == '') fantasia = dados.receita.retorno.razao_social;
                                    $('#fantasia').val(fantasia);
                                    $('#fantasia').attr('style','border-color: #5fda17;');
                                    $('#telefone').attr('disabled', false);
                                    $('#telefone').val(dados.receita.retorno.telefone);
                                    $('#telefone').attr('style','border-color: #5fda17;');
                                    $('#endereco').attr('disabled', false); 
                                    $('#endereco').val(dados.receita.retorno.logradouro+', '+dados.receita.retorno.numero+' '+dados.receita.retorno.complemento);                                
                                    $('#endereco').attr('style','border-color: #5fda17;');
                                    $('#email').attr('disabled', false); 
                                    $('#email').val('');                                                                                                
                                    $('#cidade').attr('disabled', false); 
                                    $('#cidade').val(dados.receita.retorno.municipio_ibge);  
                                    $('#cidade').attr('style','border-color: #5fda17;');
                                    $('#estado').attr('disabled', false); 
                                    $('#estado').val(dados.receita.retorno.uf);
                                    $('#estado').attr('style','border-color: #5fda17;');
                                    $('#bairro').attr('disabled', false); 
                                    $('#bairro').val(dados.receita.retorno.bairro);
                                    $('#bairro').attr('style','border-color: #5fda17;');
                                    $('#cep').attr('disabled', false); 
                                    $('#cep').val(dados.receita.retorno.cep);                          
                                    $('#cep').attr('style','border-color: #5fda17;');
                                    $('#pais').attr('disabled', false);
                                    $('#pais').attr('style','border-color: #5fda17;');                                                
                                    $('#inscricao_estadual').attr('disabled', false);
                                    $('#inscricao_estadual').val('');                        
                                    $('#cartao_cnpj').val(dados.receita.save);                    
                                    
                                    $('#pais').val('Brasil');         
                                    $('#salvar').val('1');
                                }else{
                                   swal({
                                        title: "Atenção!",
                                        text: dados.msg,
                                        type: 'warning'
                                    }).then(function() {
                                        $('#cnpj').val('');
                                    });

                                }
                            });
                        }                           
                    });    
                
                }            
            }
        }); 
}